from datetime import datetime

from django import forms
from django.forms.widgets import Input
from django.utils.timezone import make_aware

from common.fields import PickDateWidget
from .models import Event, EventCategory, Rule


class ColorInput(Input):
    input_type = 'color'


class EventCategoryForm(forms.ModelForm):
    class Meta:
        model = EventCategory
        fields = '__all__'
        widgets = {
            'default_color': ColorInput,
        }


class AgendaEditForm(forms.ModelForm):
    REPET_CHOICES = (
        ('', 'Aucune'),
        ('WEEKLY', 'Chaque semaine'),
        ('BI-WEEKLY', 'Une semaine sur deux'),
        ('DAILY', 'Chaque jour'),
        ('DAILY-open', 'Chaque jour, du lundi au vendredi'),
    )

    date = forms.DateField(label="Date", widget=PickDateWidget)
    heure_de = forms.TimeField(
        label="De",
        widget=forms.TimeInput(attrs={'class': 'TimeField', 'placeholder': '00:00'}, format='%H:%M')
    )
    heure_a = forms.TimeField(
        label="À",
        widget=forms.TimeInput(attrs={'class': 'TimeField', 'placeholder': '00:00'}, format='%H:%M')
    )
    repetition = forms.ChoiceField(label='Répétition', choices=REPET_CHOICES, required=False)
    repetition_fin = forms.DateField(
        label='Jusqu’au (y compris)', widget=PickDateWidget, required=False
    )

    class Meta:
        model = Event
        fields = [
            'date', 'heure_de', 'heure_a', 'category', 'color', 'title', 'description',
            'repetition', 'repetition_fin'
        ]
        widgets = {
            'color': ColorInput,
        }

    def clean(self):
        if self.cleaned_data.get('repetition_fin') and not self.cleaned_data.get('repetition'):
            raise forms.ValidationError(
                "Vous ne pouvez pas indiquer une fin de répétition sans valeur de répétition"
            )

    def save(self, **kwargs):
        if set(self.changed_data).intersection({'date', 'heure_de', 'heure_a'}):
            dt = self.cleaned_data['date']
            start = self.cleaned_data['heure_de']
            end = self.cleaned_data['heure_a']
            self.instance.start = make_aware(datetime(dt.year, dt.month, dt.day, start.hour, start.minute))
            self.instance.end = make_aware(datetime(dt.year, dt.month, dt.day, end.hour, end.minute))
            for occ in self.instance.exceptions.all():
                occ.orig_start = make_aware(datetime(
                    occ.orig_start.year, occ.orig_start.month, occ.orig_start.day, start.hour, start.minute
                ))
                occ.save()

        if 'repetition' in self.changed_data:
            repet = self.cleaned_data.get('repetition')
            if repet == 'WEEKLY':
                self.instance.rule, _ = Rule.objects.get_or_create(name='Chaque semaine', frequency='WEEKLY')
            elif repet == 'BI-WEEKLY':
                self.instance.rule, _ = Rule.objects.get_or_create(
                    name='Une semaine sur deux', frequency='WEEKLY', params='interval:2'
                )
            elif repet == 'DAILY':
                self.instance.rule, _ = Rule.objects.get_or_create(name='Chaque jour', frequency='DAILY')
            elif repet == 'DAILY-open':
                self.instance.rule, _ = Rule.objects.get_or_create(
                    name='Chaque jour, du lundi au vendredi', frequency='DAILY',
                    params='byweekday:MO,TU,WE,TH,FR'
                )
        if 'repetition_fin' in self.changed_data:
            repet_fin = self.cleaned_data.get('repetition_fin')
            if repet_fin:
                self.instance.end_recurring_period = make_aware(
                    datetime(repet_fin.year, repet_fin.month, repet_fin.day, 23, 59)
                )
        return super().save(**kwargs)
