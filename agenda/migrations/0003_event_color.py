from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0002_rule_name_longer'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='color',
            field=models.CharField(blank=True, max_length=7, verbose_name='Couleur'),
        ),
    ]
