from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0003_event_color'),
    ]

    operations = [
        migrations.CreateModel(
            name='MemOccurrence',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('agenda.event',),
        ),
        migrations.CreateModel(
            name='Occurrence',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('orig_start', models.DateTimeField(verbose_name='Début d’origine')),
                ('cancelled', models.BooleanField(default=False)),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='exceptions', to='agenda.Event')),
            ],
        ),
    ]
