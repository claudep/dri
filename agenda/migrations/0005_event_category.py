from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0004_memoccurrence_occurrence'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='Nom')),
                ('default_color', models.CharField(blank=True, max_length=7, verbose_name='Couleur par défaut')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Catégorie d’évènement',
                'verbose_name_plural': 'Catégories d’évènement',
            },
        ),
        migrations.AddField(
            model_name='event',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='agenda.EventCategory', verbose_name='Catégorie'),
        ),
    ]
