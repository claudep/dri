from datetime import timedelta

from dateutil import rrule
from django.db import models
from django.utils import timezone
from django.utils.dateformat import format as dateformat

from person.models import Person, User


# Thanks https://github.com/llazzaro/django-scheduler/ for inspiration
# TODO: implement dateutil rruleset for exceptions

RRULE_FREQ_MAP = {
    "YEARLY": rrule.YEARLY,
    "MONTHLY": rrule.MONTHLY,
    "WEEKLY": rrule.WEEKLY,
    "DAILY": rrule.DAILY,
    "HOURLY": rrule.HOURLY,
    "MINUTELY": rrule.MINUTELY,
    "SECONDLY": rrule.SECONDLY,
}
RRULE_WDAY_MAP = {'MO': 0, 'TU': 1, 'WE': 2, 'TH': 3, 'FR': 4, 'SA': 5, 'SU': 6}


class Rule(models.Model):
    # https://tools.ietf.org/html/rfc5545
    # See also https://dateutil.readthedocs.io/en/stable/rrule.html
    FREQ_CHOICES = (
        ("YEARLY", "Chaque année"),
        ("MONTHLY", "Chaque mois"),
        ("WEEKLY", "Chaque semaine"),
        ("DAILY", "Chaque jour"),
        ("HOURLY", "Chaque heure"),
        ("MINUTELY", "Chaque minute"),
        ("SECONDLY", "Chaque seconde"),
    )
    name = models.CharField("Nom", max_length=40)
    description = models.TextField("Description")
    frequency = models.CharField("Fréquence", choices=FREQ_CHOICES, max_length=10)
    params = models.TextField("Params", blank=True)

    class Meta:
        verbose_name = "Règle"
        verbose_name_plural = "Règles"

    def __str__(self):
        return "Règle %s - params %s" % (self.name, self.params)


class EventManager(models.Manager):
    def get_for_person_back(self, pers, start, limit, category=None):
        events = []
        days_back = 7
        while len(events) < limit:
            # provide one week of events
            start_less_one_week = start - timedelta(days=days_back)
            source = self.get_for_person(pers, start_less_one_week, start, category=category)
            if not source:
                days_back = days_back * 10
                if days_back > 100:
                    break
            source.extend(events)
            events = source
            start = start_less_one_week
        if len(events) > limit:
            events = events[-limit:]
        return events

    def get_for_persons(self, persons, start, end, category=None, limit=None):
        events_qs = Event.objects.filter(person__in=persons)
        return self._filter_events(events_qs, start, end, category=category, limit=limit)

    def get_for_person(self, pers, start, end, category=None, limit=None):
        events_qs = pers.events
        return self._filter_events(events_qs, start, end, category=category, limit=limit)

    def _filter_events(self, events_qs, start, end, category=None, limit=None):
        unique_events = events_qs.filter(
            rule__isnull=True, start__gte=start, start__lt=end
        ).select_related('creator')
        if category is not None:
            unique_events = unique_events.filter(category__name=category)

        recur_events = events_qs.filter(
            rule__isnull=False
        ).filter(
            models.Q(end_recurring_period__gte=start) | models.Q(end_recurring_period__isnull=True)
        ).select_related('rule', 'creator').prefetch_related('exceptions')
        if category is not None:
            recur_events = recur_events.filter(category__name=category)

        if limit:
            events = []
            # Create a list of generators, then pick the most recent Event at each
            # iteration. Remove the generator when it is exhausted.
            queues = ([iter(unique_events)] if len(unique_events) else []) + [
                ev.get_occurrences(start, end=None) for ev in recur_events
            ]
            nexts = [next(q) for q in queues]
            while nexts:
                next_idx = nexts.index(min(nexts))
                events.append(nexts[next_idx])
                try:
                    nexts[next_idx] = next(queues[next_idx])
                except StopIteration:
                    del nexts[next_idx]
                    del queues[next_idx]
                if len(events) >= limit:
                    break
        else:
            events = list(unique_events)
            for ev in recur_events:
                events.extend([occ for occ in ev.get_occurrences(start, end)])
        return sorted(events)

    def get_by_category(self, categ, start, end):
        unique_events = self.get_queryset().filter(
            rule__isnull=True, start__gte=start, start__lt=end, category=categ
        ).select_related('creator')
        recur_events = self.get_queryset().filter(
            rule__isnull=False, category=categ
        ).filter(
            models.Q(end_recurring_period__gte=start) | models.Q(end_recurring_period__isnull=True)
        ).select_related('rule', 'creator').prefetch_related('exceptions')
        events = list(unique_events)
        for ev in recur_events:
            events.extend([occ for occ in ev.get_occurrences(start, end)])
        return sorted(events)


class EventCategory(models.Model):
    name = models.CharField('Nom', max_length=50)
    default_color = models.CharField("Couleur par défaut", max_length=7, blank=True)

    class Meta:
        ordering = ['name']
        verbose_name = "Catégorie d’évènement"
        verbose_name_plural = "Catégories d’évènement"

    def __str__(self):
        return self.name


class Event(models.Model):
    start = models.DateTimeField("Début", db_index=True)
    end = models.DateTimeField("Fin", db_index=True, null=True, blank=True)
    title = models.CharField("Titre", max_length=255)
    description = models.TextField("Description", blank=True)
    category = models.ForeignKey(
        EventCategory, on_delete=models.SET_NULL, blank=True, null=True, verbose_name="Catégorie"
    )
    color = models.CharField("Couleur", max_length=7, blank=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='events')
    creator = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Créateur')
    rule = models.ForeignKey(
        Rule, on_delete=models.CASCADE, null=True, blank=True, verbose_name="Règle",
    )
    end_recurring_period = models.DateTimeField(
        "Dernière occurrence", null=True, blank=True, db_index=True,
    )

    objects = EventManager()

    class Meta:
        verbose_name = "Évènement"
        verbose_name_plural = "Évènements"

    def __str__(self):
        return "%s: %s (%s)" % (
            self.title, dateformat(self.start, 'd.m.Y'), self.hours,
        )

    def __lt__(self, other):
        return self.start < other.start

    def can_edit(self, user):
        return user == self.creator or user.has_perm('agenda.change_event')

    @property
    def hours(self):
        return f"{dateformat(self.start, 'H:i')} - {dateformat(self.end, 'H:i')}"

    @property
    def text_color(self):
        return 'white' if self.color and color_is_dark(self.color) else 'black'

    def spans(self):
        """Return number of 15min slices it occupies."""
        return int((self.end - self.start).seconds / 60 / 15)

    def get_rrule(self):
        if self.rule is None:
            return
        frequency = RRULE_FREQ_MAP[self.rule.frequency]

        def convert_param(key, value):
            if key == 'byweekday':
                return key, [RRULE_WDAY_MAP[v] for v in value.split(',')]
            elif key == 'interval':
                return key, int(value)
            else:
                return key, value

        if self.rule.params:
            params = {key: val for key, val in [
                convert_param(*(param.split(':'))) for param in self.rule.params.split(';')
            ]}
        else:
            params = {}
        return rrule.rrule(frequency, dtstart=self.start, until=self.end_recurring_period, **params)

    def get_occurrences(self, start, end=None):
        """Return list of date occurrences of this event between start and end."""
        start_rule = self.get_rrule()
        duration = self.end - self.start
        exceptions = {occ.orig_start: occ for occ in self.exceptions.all()}
        start_local_dt = timezone.localtime(self.start)
        for dt in start_rule.xafter(start, inc=True):
            if end and dt > end:
                break
            dt = fix_occurrence_date(dt, start_local_dt)
            yield MemOccurrence(
                id=self.id, start=dt, end=dt + duration, title=self.title,
                description=self.description, color=self.color, creator=self.creator,
                person=self.person, exc=exceptions,
            )


class MemOccurrence(Event):
    class Meta:
        proxy = True

    is_recurring = True

    def __init__(self, *args, exc={}, **kwargs):
        super().__init__(*args, **kwargs)
        self.cancelled = False
        if self.start in exc and exc[self.start].cancelled:
            self.cancelled = True


class Occurrence(models.Model):
    """Recurring event exceptions."""
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='exceptions')
    orig_start = models.DateTimeField("Début d’origine")
    cancelled = models.BooleanField(default=False)

    def __str__(self):
        return "Exception pour l’événement «%s»" % self.event


def fix_occurrence_date(dt, start_dt):
    tz_diff = timezone.localtime(dt).utcoffset() - start_dt.utcoffset()
    if tz_diff:
        return dt - tz_diff
    return dt


def color_is_dark(c):
    rgb = int(c.strip('#'), 16)
    red = rgb >> 16 & 0xff
    green = rgb >> 8 & 0xff
    blue = rgb >> 0 & 0xff
    luma = 0.2126 * red + 0.7152 * green + 0.0722 * blue  # per ITU-R BT.709
    return luma < 120
