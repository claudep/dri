from datetime import time
from itertools import chain

from reportlab.lib.pagesizes import A4
from reportlab.platypus import Paragraph, SimpleDocTemplate, Table, TableStyle
from reportlab.lib.colors import HexColor, black, toColor, white
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import cm

from django.utils.dateformat import format as date_format

from person.pdf_output import PersonPDFModel


class AgendaHebdoPDF(PersonPDFModel):
    title = "Plan hebdomadaire"

    def __init__(self, **params):
        self.events = params.pop('events')
        self.day_divs = params.pop('day_divs')
        super().__init__(**params)

    def produce_pdf(self, response):
        self.doc = SimpleDocTemplate(response, title=self.title, pagesize=A4,
            leftMargin=2*cm, rightMargin=1.5*cm, topMargin=1*cm, bottomMargin=1.7*cm)
        page_width = self.doc.width
        style_n = ParagraphStyle(
            'normal', fontName='Helvetica', fontSize=9, leading=10, spaceAfter=0.5*cm
        )
        style_n_white = ParagraphStyle('white', parent=style_n, textColor=white)
        story = []
        self.person_header(story)
        tstyle = (
            ('INNERGRID', (0, 0), (-1, -1), 0.25, black),
            ('BOX', (0, 0), (-1, -1), 0.25, black),
            ('BACKGROUND', (0, 0), (-1, 0), toColor('rgb(220,220,220)')),
            ('LEFTPADDING', (0, 1), (-1, -1), 0),
            ('RIGHTPADDING', (0, 1), (-1, -1), 0),
            ('BOTTOMPADDING', (0, 1), (-1, -1), 0),
            ('TOPPADDING', (0, 1), (-1, -1), 0),
        )
        midi_start = self.day_divs.index(time(12, 0))
        midi_end = self.day_divs.index(time(13, 30)) - 1
        souper_start = self.day_divs.index(time(18, 0))
        souper_end = self.day_divs.index(time(19, 30)) - 1
        tday_style = [
            ('LINEABOVE', (0, 0), (-1, -1), 0.25, toColor('rgb(120,120,120)')),
            ('BACKGROUND', (0, midi_start), (-1, midi_end), toColor('rgb(220,220,220)')),
            ('BACKGROUND', (0, souper_start), (-1, souper_end), toColor('rgb(220,220,220)')),
            ('VALIGN', (0, 0), (-1, -1), 'TOP'),
            ('LEFTPADDING', (0, 1), (-1, -1), 2),
            ('TOPPADDING', (0, 1), (-1, -1), 1),
        ]

        col_width = page_width / len(self.events)
        thead = [date_format(day, 'D j F') for day in self.events]
        tbody = []
        for day, events in self.events.items():
            cells = []
            this_day_style = tday_style.copy()
            has_overlap = any(getattr(ev, 'overlap', None) for ev in chain(*events.values()))
            col_count = 2 if has_overlap else 1
            for idx, dv in enumerate(self.day_divs):
                evts = events.get(dv)
                if evts:
                    dv_cells = ['', '']
                    for ev in evts:
                        overlap = getattr(ev, 'overlap', None)
                        row_span = ev.spans()
                        col_span = 2 if has_overlap and not overlap else 1
                        col_start = 1 if overlap == 'even' else 0
                        this_day_style.extend([
                            ('SPAN', (col_start, idx), (col_start + col_span - 1, idx + row_span - 1)),
                            ('BACKGROUND', (col_start, idx), (col_start + col_span - 1, idx + row_span - 1), HexColor(ev.color or '#dddddd')),
                        ])
                        st = style_n_white if ev.text_color == 'white' else style_n
                        dv_cells[col_start] = Paragraph(
                            ' '.join([date_format(ev.start, 'H:i'), ev.title]),
                            style=st
                        )
                    cells.append(dv_cells)
                elif dv.minute == 0:
                    cells.append([f'{dv.hour}h'] + ([''] if has_overlap else []))
                    this_day_style.append(
                        ('TEXTCOLOR', (0, idx), (-1, idx + 1), toColor('rgb(190,190,190)'))
                    )
                else:
                    cells.append([''] * col_count)
            
            tbody.append(
                Table(
                    cells,
                    colWidths=[col_width / col_count] * col_count,
                    rowHeights=[0.45 * cm] * len(self.day_divs),
                    style=this_day_style
                )
            )
        story.append(Table(
            [thead, tbody],
            colWidths=[col_width] * len(self.events),
            style=TableStyle(tstyle)
        ))
        self.doc.build(story, onFirstPage=self.draw_first_header_footer)
