from datetime import time
from django import template
from django.utils.timezone import localtime

register = template.Library()


@register.filter
def get_events(events, day_div):
    return events.get(day_div)


@register.filter
def is_hour(time_):
    return time_.minute == 0


@register.filter
def is_meal(time_):
    return (time(12, 0) <= time_ < time(13, 30)) or (time(18, 30) <= time_ < time(19, 30))


@register.filter
def as_hour_min(dtime):
    return str(localtime(dtime).time())[:5]
