from datetime import date, datetime, timedelta

from freezegun import freeze_time

from django.contrib.auth.models import Permission
from django.test import RequestFactory, TestCase
from django.urls import reverse
from django.utils.timezone import localtime, make_aware, now, utc

from person.models import Person, User
from .models import Event, Rule, fix_occurrence_date
from .views import PersonAgendaHebdoPrintView, WeekEventsMixin


class AgendaTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(username='jill', first_name='Jill', last_name='Martin')
        cls.user.user_permissions.add(Permission.objects.get(codename='access_fhmn'))
        cls.person = Person.objects.create(
            nom='Smith', prenom='John', situation='fhmn_int',
        )

    def test_create_event(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('person-agenda-add', args=[self.person.pk]), data={
            'date': '12.3.2020',
            'heure_de': '13:15',
            'heure_a': '16:00',
            'title': 'Rendez-vous important',
            'description': '',
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertRedirects(
            response, reverse('person-agenda', args=[self.person.pk]), fetch_redirect_response=False
        )
        event = self.person.events.first()
        self.assertEqual(event.creator, self.user)
        self.assertEqual(event.start, make_aware(datetime(2020, 3, 12, 13, 15)))
        self.assertEqual(event.end, make_aware(datetime(2020, 3, 12, 16, 0)))
        events = Event.objects.get_for_person(
            self.person, make_aware(datetime(2020, 3, 1, 0, 0)), make_aware(datetime(2020, 3, 30, 23, 59))
        )
        self.assertEqual(events, [event])
        events = Event.objects.get_for_person(
            self.person, make_aware(datetime(2020, 3, 1, 0, 0)), make_aware(datetime(2020, 3, 30, 23, 59)),
            limit=4
        )
        self.assertEqual(events, [event])

    def test_edit_event(self):
        demain = datetime.today() + timedelta(days=1)
        start_time = make_aware(datetime(demain.year, demain.month, demain.day, 9, 30))
        end_time = make_aware(datetime(demain.year, demain.month, demain.day, 10, 45))
        event = Event.objects.create(
            person=self.person, creator=self.user,
            start=start_time, end=end_time, title='Single', rule=None
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('person-agenda-edit', args=[self.person.pk, event.pk]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertContains(
            response,
            '<input type="text" name="heure_de" value="09:30" class="TimeField" placeholder="00:00" required id="id_heure_de">',
            html=True
        )
        self.assertContains(
            response,
            '<input type="text" name="heure_a" value="10:45" class="TimeField" placeholder="00:00" required id="id_heure_a">',
            html=True
        )
        # TODO: test POSTing

    def test_edit_recur_occurrence(self):
        weekly_rule = Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        event = Event.objects.create(
            person=self.person, creator=self.user,
            start=datetime(2020, 2, 1, 15, 0, tzinfo=utc), title='Recur over DST', rule=weekly_rule
        )
        with freeze_time("2020-02-26"):
            self.client.force_login(self.user)
            response = self.client.get(
                reverse('person-agenda-edit', args=[self.person.pk, event.pk]),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest'
            )
            self.assertContains(
                response,
                '<input type="text" name="heure_de" value="16:00" class="TimeField" placeholder="00:00" required id="id_heure_de">',
                html=True
            )
        with freeze_time("2020-06-11"):
            self.client.force_login(self.user)
            response = self.client.get(
                reverse('person-agenda-edit', args=[self.person.pk, event.pk]),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest'
            )
            self.assertContains(
                response,
                '<input type="text" name="heure_de" value="16:00" class="TimeField" placeholder="00:00" required id="id_heure_de">',
                html=True
            )

    def test_create_recurring_event(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('person-agenda-add', args=[self.person.pk]), data={
            'date': '12.3.2020',
            'heure_de': '13:15',
            'heure_a': '16:00',
            'title': 'Rendez-vous important',
            'description': '',
            'repetition': 'WEEKLY',
            'repetition_fin': '8.10.2020',
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertRedirects(
            response, reverse('person-agenda', args=[self.person.pk]), fetch_redirect_response=False
        )
        event = self.person.events.first()
        self.assertEqual(event.creator, self.user)
        self.assertEqual(event.rule.frequency, 'WEEKLY')
        self.assertEqual(event.end_recurring_period, make_aware(datetime(2020, 10, 8, 23, 59)))
        # Time stored in UTC
        self.assertEqual(event.start.tzname(), 'UTC')
        self.assertEqual(event.start.hour, 12)
        occs = list(event.get_occurrences(start=event.start, end=event.start + timedelta(days=30)))
        # UTC hours adapt to consider the DST correction.
        self.assertEqual([occ.start.hour for occ in occs], [12, 12, 12, 11, 11])

    def test_person_events_with_recurrence(self):
        single = Event.objects.create(
            person=self.person, creator=self.user,
            start=now() + timedelta(days=1), end=now() + timedelta(days=1, hours=2),
            title='Single', rule=None
        )
        weekly_rule = Rule.objects.create(name='Hebdo', frequency='WEEKLY')
        Event.objects.create(
            person=self.person, creator=self.user,
            start=now() - timedelta(days=30), end=now() - timedelta(days=29, hours=23),
            title='Recurring', rule=weekly_rule
        )
        Event.objects.create(
            person=self.person, creator=self.user,
            start=now() - timedelta(days=30), end=now() - timedelta(days=29, hours=23),
            title='Recurred in the past', rule=weekly_rule,
            end_recurring_period=now() - timedelta(days=10)
        )
        # Get events for one week
        events = Event.objects.get_for_person(
            self.person, now() - timedelta(days=3), now() + timedelta(days=4)
        )
        self.assertEqual(len(events), 2)
        self.assertEqual(events[1], single)
        self.assertEqual(events[0].title, 'Recurring')

        # Get events with a limit (used by lists)
        events = Event.objects.get_for_person(
            self.person, now(), now() + timedelta(days=1000), limit=10
        )
        self.assertEqual(len(events), 10)

    def test_cancel_event_occurrence(self):
        daily_rule = Rule.objects.create(name='Chaque jour', frequency='DAILY')
        today = date.today()
        maint = datetime(today.year, today.month, today.day, 13, 15, tzinfo=utc)
        ev = Event.objects.create(
            person=self.person, creator=self.user,
            start=maint - timedelta(days=30), end=maint - timedelta(days=29, hours=23),
            title='Recurring', rule=daily_rule
        )

        self.client.force_login(self.user)
        response = self.client.post(
            reverse('person-agenda-toggleocc', args=[self.person.pk, ev.pk]),
            data={'occ_date': fix_occurrence_date(maint, localtime(ev.start)).isoformat()},
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertRedirects(
            response, reverse('person-agenda', args=[self.person.pk]), fetch_redirect_response=False
        )
        self.assertEqual(ev.exceptions.count(), 1)
        response = self.client.get(
            reverse('person-agenda', args=[self.person.pk]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertContains(response, 'class="event-content cancelled"', count=1)
        # Restore it
        response = self.client.post(
            reverse('person-agenda-toggleocc', args=[self.person.pk, ev.pk]),
            data={'occ_date': fix_occurrence_date(maint, localtime(ev.start)).isoformat()},
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertRedirects(
            response, reverse('person-agenda', args=[self.person.pk]),
            fetch_redirect_response=False
        )
        self.assertEqual(ev.exceptions.count(), 0)

    def test_get_target_week(self):
        klass = WeekEventsMixin()
        klass.kwargs = {}
        with freeze_time("2020-01-03"):
            week_data = klass.get_target_week()
            self.assertEqual(week_data[:2], (2020, 1))
            self.assertEqual(week_data[2].date(), date(2019, 12, 30))
        with freeze_time("2021-01-11"):
            week_data = klass.get_target_week()
            self.assertEqual(week_data[:2], (2021, 2))
            self.assertEqual(week_data[2].date(), date(2021, 1, 11))
        klass.kwargs = {'year': '2020', 'week': '3'}
        week_data = klass.get_target_week()
        self.assertEqual(week_data[:2], (2020, 3))

    def test_week_print(self):
        self.client.force_login(self.user)
        url_kwargs = {'pk': self.person.pk, 'year': 2020, 'week': 5}
        request = RequestFactory().get(reverse('person-agenda-print', kwargs=url_kwargs))
        view = PersonAgendaHebdoPrintView()

        # No events
        response = self.client.get(reverse('person-agenda-print', kwargs=url_kwargs))
        self.assertEqual(response.status_code, 200)

        daily_rule = Rule.objects.create(name='Chaque jour', frequency='DAILY')
        Event.objects.create(
            person=self.person, creator=self.user,
            start=make_aware(datetime(2020, 1, 1, 13, 15)),
            end=make_aware(datetime(2020, 1, 1, 14, 15)),
            title='Recurring', rule=daily_rule
        )
        Event.objects.create(
            person=self.person, creator=self.user,
            start=make_aware(datetime(2020, 1, 30, 8, 0)),
            end=make_aware(datetime(2020, 1, 30, 10, 0)),
            title='Single',
        )
        view.setup(request, **url_kwargs)
        ctxt = view.pdf_context()
        self.assertEqual(len(ctxt['events'][date(2020, 1, 30)]), 2)

    def test_daily_by_location(self):
        person2 = Person.objects.create(
            nom='Einstein', prenom='Alfred', situation='fhmn_int',
        )
        person_ne = Person.objects.create(
            nom='Dupond', prenom='Jean', situation='fhne_int',
        )
        daily_rule = Rule.objects.create(name='Chaque jour', frequency='DAILY')
        Event.objects.create(
            person=self.person, creator=self.user,
            start=make_aware(datetime(2020, 1, 1, 13, 15)),
            end=make_aware(datetime(2020, 1, 1, 14, 15)),
            title='Recurring', rule=daily_rule
        )
        Event.objects.create(
            person=self.person, creator=self.user,
            start=make_aware(datetime(2020, 1, 30, 8, 0)),
            end=make_aware(datetime(2020, 1, 30, 10, 0)),
            title='Single',
        )
        Event.objects.create(
            person=person2, creator=self.user,
            start=make_aware(datetime(2020, 1, 30, 12, 0)),
            end=make_aware(datetime(2020, 1, 30, 12, 30)),
            title='Single',
        )
        Event.objects.create(
            person=person_ne, creator=self.user,
            start=make_aware(datetime(2020, 1, 30, 12, 0)),
            end=make_aware(datetime(2020, 1, 30, 12, 30)),
            title='Single',
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('site-agenda-day', args=['fhmn', 2020, 1, 30]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(list(response.context['events'].keys()), [person2, self.person])
        self.assertEqual(len(response.context['events'][self.person]), 2)
