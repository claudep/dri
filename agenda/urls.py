from django.urls import path, re_path

from . import views

urlpatterns = [
    path('person/<int:pk>/add/', views.PersonAgendaEditView.as_view(),
        name='person-agenda-add'),
    path('person/<int:pk>/edit/<int:obj_pk>/', views.PersonAgendaEditView.as_view(),
        name='person-agenda-edit'),
    path('person/<int:pk>/delete/<int:obj_pk>', views.PersonAgendaDeleteView.as_view(),
        name='person-agenda-delete'),
    path('person/<int:pk>/edit/<int:obj_pk>/cancel/', views.PersonAgendaToggleOccurrence.as_view(),
        name='person-agenda-toggleocc'),
    path('person/<int:pk>/list/', views.PersonAgendaListView.as_view(),
        name='person-agenda-list'),
    path('person/<int:pk>/<int:year>/<int:week>/pdf/',
        views.PersonAgendaHebdoPrintView.as_view(),
        name='person-agenda-print'),
    re_path(r'^person/(?P<pk>[0-9]+)/(?:(?P<year>[0-9]+)/(?P<week>[0-9]+)/)?',
        views.PersonAgendaView.as_view(),
        name='person-agenda'),
    path('infos/<slug:location>/agendas/journee/<int:year>/<int:month>/<int:day>/',
        views.AgendaDailyView.as_view(),
        name='site-agenda-day'),
    path('infos/<slug:location>/agendas/<slug:categ>/<int:year>/<int:week>/',
        views.AgendaCategoryView.as_view(),
        name='categ-agenda'),
]
