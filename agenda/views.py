import math
from collections import OrderedDict, defaultdict
from datetime import date, datetime, time, timedelta
from urllib.parse import urlencode

from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.generic import DeleteView, TemplateView, View

from common.views import CreateUpdateView, BasePDFView
from person.models import Person
from person.views import GeneralTabMixin, OnlyAjaxMixin
from .forms import AgendaEditForm
from .models import Event, EventCategory, Occurrence
from .pdf_output import AgendaHebdoPDF


class WeekEventsMixin:
    def get_target_week(self):
        if self.kwargs.get('week'):
            year, week_num = int(self.kwargs['year']), int(self.kwargs['week'])
        else:
            year, week_num = date.today().year, date.today().isocalendar()[1]
        week_start = timezone.make_aware(
            datetime.strptime(f'{year}-{week_num}-1', "%G-%V-%w")
        )
        return year, week_num, week_start

    def events_in_week(self, events, week_start):
        monday = week_start.date()
        week_days = [
            monday + timedelta(days=i)
            for i in range(7)
        ]
        week_events = {dt: [] for dt in week_days}
        # Start by putting each event in its week day
        for event in events:
            week_events[timezone.localtime(event.start).date()].append(event)
        # Checking events overlap
        for day in week_events:
            check_overlap(week_events[day])
        # Putting events as a dict with the hour quarter as key
        for day in week_events:
            events = week_events[day]
            week_events[day] = {}
            for event in events:
                hour = round_quarter(timezone.localtime(event.start))
                week_events[day].setdefault(hour, []).append(event)
        return week_events

    def get_day_divs(self, start_hour, end_hour):
        return [time(h, m) for h in range(start_hour, end_hour + 1) for m in range(0, 60, 15)]


class PersonAgendaView(OnlyAjaxMixin, WeekEventsMixin, TemplateView):
    template_name = 'agenda/person-agenda.html'
    mode = 'week'

    def get_events(self, person, week_start):
        return Event.objects.get_for_person(person, week_start, week_start + timedelta(days=7))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        person = get_object_or_404(Person, pk=self.kwargs['pk'])
        year, week_num, week_start = self.get_target_week()

        events = self.get_events(person, week_start)
        week_events = self.events_in_week(events, week_start)

        # Year transitions
        if week_num == 0:
            # ISO spec says 28.12 is always in the last week (52 or 53)
            prev_events = reverse('person-agenda', args=[person.pk, year - 1, date(year, 12, 28).isocalendar()[1]])
        else:
            prev_events = reverse('person-agenda', args=[person.pk, year, week_num - 1])
        if week_num > 52:
            next_events = reverse('person-agenda', args=[person.pk, year + 1, 0])
        else:
            next_events = reverse('person-agenda', args=[person.pk, year, week_num + 1])

        context.update({
            'person': person,
            'start_hour': 8,
            'end_hour': 19,
            'day_divs': self.get_day_divs(8, 19),
            'week_events': week_events,
            'add_url': reverse('person-agenda-add', args=[person.pk]),
            'print_url': reverse('person-agenda-print', args=[person.pk, year, week_num]),
            'prev_events': prev_events,
            'next_events': next_events,
        })
        return context


class PersonAgendaHebdoPrintView(WeekEventsMixin, BasePDFView):
    pdf_class = AgendaHebdoPDF

    def get_events(self, person, week_start):
        return Event.objects.get_for_person(person, week_start, week_start + timedelta(days=7))

    def pdf_context(self):
        person = get_object_or_404(Person, pk=self.kwargs['pk'])
        year, week_num, week_start = self.get_target_week()
        events = self.get_events(person, week_start)
        return {
            'person': person,
            'day_divs': self.get_day_divs(8, 19),
            'events': self.events_in_week(events, week_start),
        }


class PersonAgendaListView(OnlyAjaxMixin, TemplateView):
    paginate_by = 25
    template_name = 'agenda/person-agenda-list.html'
    mode = 'list'

    def get_queryset(self):
        self.person = get_object_or_404(Person, pk=self.kwargs['pk'])
        if 'after' in self.request.GET:
            start = datetime.fromisoformat(self.request.GET['after']) + timedelta(minutes=5)
        elif 'before' in self.request.GET:
            to_ = datetime.fromisoformat(self.request.GET['before']) - timedelta(minutes=5)
            return Event.objects.get_for_person_back(self.person, to_, self.paginate_by)
        else:
            from_ = date.today()
            start = timezone.make_aware(datetime(from_.year, from_.month, from_.day, 0, 0))
        end = start + timedelta(days=1000)
        return Event.objects.get_for_person(self.person, start, end, limit=self.paginate_by)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        events = self.get_queryset()
        context.update({
            'events': events,
            'person': self.person,
        })
        if events:
            context.update({
                'prev_events': reverse('person-agenda-list', args=[self.person.pk]
                    ) + '?' + urlencode({'before': events[0].start.isoformat()}),
                'next_events': reverse('person-agenda-list', args=[self.person.pk]
                    ) + '?' + urlencode({'after': events[-1].start.isoformat()}),
            })
        return context


class PersonAgendaEditView(OnlyAjaxMixin, CreateUpdateView):
    model = Event
    pk_url_kwarg = 'obj_pk'
    form_class = AgendaEditForm
    template_name = 'agenda/person-agenda-add.html'

    @property
    def is_create(self):
        return self.pk_url_kwarg not in self.kwargs

    def get_initial(self):
        initial = super().get_initial()
        if not self.is_create:
            initial['date'] = timezone.localtime(self.object.start).date()
            initial['heure_de'] = timezone.localtime(self.object.start).time()
            initial['heure_a'] = timezone.localtime(self.object.end).time()
            if self.object.rule:
                if self.object.rule.name == 'Chaque jour, du lundi au vendredi':
                    initial['repetition'] = 'DAILY-open'
                else:
                    initial['repetition'] = self.object.rule.frequency
                if self.object.end_recurring_period:
                    initial['repetition_fin'] = self.object.end_recurring_period.date()
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'occ' in self.request.GET:
            context['occ_date'] = self.request.GET['occ']
            context['occ_obj'] = self.object.exceptions.filter(
                orig_start=datetime.fromisoformat(context['occ_date'])
            ).first()
        context['categ_color_map'] = {
            cat.pk: cat.default_color for cat in EventCategory.objects.all()
        }
        return context

    def form_valid(self, form):
        form.instance.creator = self.request.user
        form.instance.person_id = self.kwargs['pk']
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('person-agenda', args=[self.kwargs['pk']])


class PersonAgendaToggleOccurrence(View):
    def post(self, request, *args, **kwargs):
        event = get_object_or_404(Event, pk=self.kwargs['obj_pk'])
        occ_date = datetime.fromisoformat(request.POST['occ_date'])
        try:
            occ = event.exceptions.get(orig_start=occ_date)
            occ.delete()
        except Occurrence.DoesNotExist:
            Occurrence.objects.create(
                event=event,
                orig_start=occ_date,
                cancelled=True,
            )
        return HttpResponseRedirect(reverse('person-agenda', args=[self.kwargs['pk']]))


class PersonAgendaDeleteView(DeleteView):
    model = Event
    pk_url_kwarg = 'obj_pk'

    def get_success_url(self):
        return reverse('person-agenda', args=[self.kwargs['pk']])


class AgendaDailyView(GeneralTabMixin, TemplateView):
    template_name = 'agenda/day-by-location.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('person.access_%s' % kwargs['location']):
            raise PermissionDenied("Désolé, vous n'êtes pas autorisé à afficher cette page.")
        if 0 in [kwargs['year'], kwargs['month'], kwargs['day']]:
            self.day = date.today()
        else:
            self.day = date(self.kwargs['year'], self.kwargs['month'], self.kwargs['day'])
        return super().dispatch(request, *args, **kwargs)

    def get_events(self):
        day_midnight = datetime(self.day.year, self.day.month, self.day.day, 0, 0, tzinfo=timezone.get_default_timezone())
        return Event.objects.get_for_persons(
            Person.objects.filter(actif=True, situation__startswith=self.kwargs['location']),
            day_midnight, day_midnight + timedelta(days=1)
        )

    def get_context_data(self, **kwargs):
        events = defaultdict(list)
        for ev in self.get_events():
            events[ev.person].append(ev)
        prev_day, next_day = self.day - timedelta(days=1), self.day + timedelta(days=1)
        context = {
            **super().get_context_data(**kwargs),
            'events': OrderedDict(
                (pers, events[pers]) for pers in sorted(events.keys(), key=lambda pers: (pers.nom, pers.prenom))
            ),
            'day': self.day,
            'pdf_url': '',
            'prev_events': reverse(
                'site-agenda-day', args=[self.kwargs['location'], prev_day.year, prev_day.month, prev_day.day]
            ),
            'next_events': reverse(
                'site-agenda-day', args=[self.kwargs['location'], next_day.year, next_day.month, next_day.day]
            ),
        }
        return context


class AgendaCategoryView(GeneralTabMixin, WeekEventsMixin, TemplateView):
    template_name = 'agenda/categ-agenda.html'
    category_name = None
    mode = 'week'

    def get_events(self, week_start):
        self.category = get_object_or_404(EventCategory, name__istartswith=self.kwargs['categ'])
        return Event.objects.get_by_category(
            self.category, week_start, week_start + timedelta(days=7)
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year, week_num, week_start = self.get_target_week()
        events = self.get_events(week_start)
        week_events = self.events_in_week(events, week_start)
        context.update({
            'category': self.category,
            'start_hour': 8,
            'end_hour': 19,
            'day_divs': self.get_day_divs(8, 19),
            'week_events': week_events,
            'prev_events': reverse('categ-agenda', kwargs={**self.kwargs, 'year': year, 'week': week_num - 1}),
            'next_events': reverse('categ-agenda', kwargs={**self.kwargs, 'year': year, 'week': week_num + 1}),
        })
        return context


def round_quarter(time_):
    return time(time_.hour, math.floor(time_.minute / 15) * 15)


def check_overlap(event_list):
    overlap = 'odd'
    for ev in event_list:
        for other in event_list:
            if ev != other and ev.start < other.end and ev.end > other.start:
                ev.overlap = overlap
                overlap = 'even' if overlap == 'odd' else 'odd'
                break
