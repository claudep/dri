from django.contrib import admin

from .models import BilanAnimation, AnimationFHMN, ObservationFHMN, ObservationFHNE

admin.site.register(BilanAnimation)
admin.site.register(AnimationFHMN)
admin.site.register(ObservationFHMN)
admin.site.register(ObservationFHNE)
