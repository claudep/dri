from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('animation', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='animationfhmn',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='bilananimation',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='observationfhmn',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='observationfhne',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
    ]
