from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('animation', '0002_add_db_index'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='observationfhmn',
            name='date_fermee',
        ),
        migrations.RemoveField(
            model_name='observationfhmn',
            name='etat',
        ),
        migrations.AlterField(
            model_name='observationfhmn',
            name='observations',
            field=models.TextField(verbose_name='Observations'),
        ),
        migrations.AlterField(
            model_name='observationfhne',
            name='observations',
            field=models.TextField(verbose_name='Observations'),
        ),
    ]
