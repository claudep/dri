from datetime import date

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from person.models import Attachment, Person, PersonRelatedModel


class BilanAnimation(PersonRelatedModel):
    date_bilan = models.DateField("Date du bilan")
    participants = models.CharField("Participants", max_length=70, blank=True)
    rappel_objectifs = models.TextField("Rappel des objectifs", blank=True)
    situation = models.TextField("Point de la situation", blank=True)
    commentaires = models.TextField("Commentaires et propositions", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)
    deroulement = models.TextField("Déroulement de l'entretien", blank=True)

    _view_name_base = 'bilananim'
    list_display = ['date_bilan', 'auteur']
 
    def __str__(self):
        return "Bilan animation pour %s (%s)" % (self.person, self.date_bilan)


class AnimationFHMN(PersonRelatedModel):
    activites = models.TextField("Activités", blank=True)
    ressources = models.TextField("Ressources", blank=True)
    difficultes = models.TextField("Difficultés", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)

    _view_name_base = 'animfhmn'
    list_display = ['date', 'auteur']

    def __str__(self):
        return "Animation FHMN pour %s (%s)" % (self.person, self.date)


class ObservationFHMN(PersonRelatedModel):
    observations = models.TextField("Observations")
    fichiers = GenericRelation(Attachment, verbose_name='Fichiers')

    _view_name_base = 'obsanim'

    def __str__(self):
        return "Observation anim. FHMN pour %s (%s)" % (self.person, self.date)

    def can_edit(self, user):
        return user.is_superuser or (user == self.createur and self.date.date() == date.today())


class ObservationFHNE(PersonRelatedModel):
    intervenant = models.CharField("Intervenant", max_length=50, blank=True)
    rubrique = models.CharField("Rubrique", max_length=120, blank=True)
    observations = models.TextField("Observations")
    fichiers = GenericRelation(Attachment, verbose_name='Fichiers')

    _view_name_base = 'obsanim'

    def __str__(self):
        return "Observation anim. FHNE pour %s (%s)" % (self.person, self.date)

    def can_edit(self, user):
        return user.is_superuser or (user == self.createur and self.date.date() == date.today())
