from django.test import TestCase

from person.tests import ModelBaseTextMixin
from .models import BilanAnimation, AnimationFHMN, ObservationFHMN, ObservationFHNE


class BilanAnimationTests(ModelBaseTextMixin, TestCase):
    ModelClass = BilanAnimation
    model_id = 'bilananim'
    custom_data = {'date_bilan': '2019-03-04', 'participants': "Elle et lui"}


class AnimationFHMNTests(ModelBaseTextMixin, TestCase):
    ModelClass = AnimationFHMN
    model_id = 'animfhmn'
    custom_data = {'activites': "Des jeux…"}


class ObservationFHMNTests(ModelBaseTextMixin, TestCase):
    ModelClass = ObservationFHMN
    model_id = 'obsanim'
    pers_situation = 'fhmn_int'
    custom_data = {'observations': "Des choses…"}


class ObservationFHNETests(ModelBaseTextMixin, TestCase):
    ModelClass = ObservationFHNE
    model_id = 'obsanim'
    pers_situation = 'fhne_int'
    custom_data = {'observations': "Des choses…"}
