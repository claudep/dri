from django.urls import path

from . import views

urlpatterns = [
    path('bilan/', views.BilanAnimationListView.as_view(), name='bilananim-list'),
    path('bilan/<int:obj_pk>/', views.BilanAnimationDetailView.as_view(), name='bilananim-detail'),
    path('bilan/<int:obj_pk>/edit/', views.BilanAnimationEditView.as_view(), name='bilananim-edit'),
    path('bilan/new/', views.BilanAnimationEditView.as_view(), name='bilananim-new'),

    path('animfhmn/', views.AnimationFHMNListView.as_view(), name='animfhmn-list'),
    path('animfhmn/<int:obj_pk>/', views.AnimationFHMNDetailView.as_view(), name='animfhmn-detail'),
    path('animfhmn/<int:obj_pk>/edit/', views.AnimationFHMNEditView.as_view(), name='animfhmn-edit'),
    path('animfhmn/new/', views.AnimationFHMNEditView.as_view(), name='animfhmn-new'),

    path('obsanim/', views.ObservationAnimListView.as_view(), name='obsanim-list'),
    path('obsanim/<int:obj_pk>/', views.ObservationAnimDetailView.as_view(), name='obsanim-detail'),
    path('obsanim/<int:obj_pk>/edit/', views.ObservationAnimEditView.as_view(), name='obsanim-edit'),
    path('obsanim/new/', views.ObservationAnimEditView.as_view(), name='obsanim-new'),
]
