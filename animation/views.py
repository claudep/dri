from person.models import Person
from person.views import BaseDetailView, BaseListView, BaseEditView

from .models import BilanAnimation, AnimationFHMN, ObservationFHMN, ObservationFHNE


class BilanAnimationBaseView:
    model = BilanAnimation


class BilanAnimationListView(BilanAnimationBaseView, BaseListView):
    new_form_label = 'Créer nouveau bilan animation'


class BilanAnimationDetailView(BilanAnimationBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class BilanAnimationEditView(BilanAnimationBaseView, BaseEditView):
    pass


class AnimationFHMNBaseView:
    model = AnimationFHMN


class AnimationFHMNListView(AnimationFHMNBaseView, BaseListView):
    new_form_label = 'Créer nouvelle animation FHMN'


class AnimationFHMNDetailView(AnimationFHMNBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class AnimationFHMNEditView(AnimationFHMNBaseView, BaseEditView):
    pass


class ObservationAnimBaseView:
    """Switch model depending on resident location."""
    @property
    def is_fhmn(self):
        return True if Person.objects.get(pk=self.kwargs['pk']).derniere_situation.startswith('fhmn') else False

    @property
    def model(self):
        return ObservationFHMN if self.is_fhmn else ObservationFHNE

    @property
    def list_display(self):
        if self.is_fhmn:
            return ['date', 'observations:w100:files', 'auteur']
        else:
            return ['date', 'rubrique', 'observations:w100:files', 'auteur']

    @property
    def exclude(self):
        if self.is_fhmn:
            return ['date', 'visa', 'createur', 'etat', 'date_fermee']
        else:
            return ['date', 'visa', 'createur']

    @property
    def search_fields(self):
        if self.is_fhmn:
            return ['observations']
        else:
            return ['observations', 'rubrique']


class ObservationAnimListView(ObservationAnimBaseView, BaseListView):
    new_form_label = 'Créer nouvelle observation'


class ObservationAnimDetailView(ObservationAnimBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class ObservationAnimEditView(ObservationAnimBaseView, BaseEditView):
    return_to_list = True
