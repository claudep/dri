from django.contrib import admin

from .models import (
    ActionAtelier, AtelierARIHANE, AtelierARIHANEDoc, AtelierCommunication,
    AtelierCrea1, AtelierCrea2, AtelierCuisine, AtelierInfo, AtelierPoste,
    AtelierPosteDoc, BilanAtelier, ObservationAtelier
)


@admin.register(ActionAtelier)
class ActionAtelierAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'auteur', 'etat', 'date_fermee']
    list_filter = ['etat']
    search_fields = ['problemes', 'objectifs']


@admin.register(BilanAtelier)
class BilanAtelierAdmin(admin.ModelAdmin):
    list_display = ['date', 'person', 'date_bilan']


admin.site.register(AtelierARIHANE)
admin.site.register(AtelierARIHANEDoc)
admin.site.register(AtelierCommunication)
admin.site.register(AtelierCrea1)
admin.site.register(AtelierCrea2)
admin.site.register(AtelierCuisine)
admin.site.register(AtelierInfo)
admin.site.register(AtelierPoste)
admin.site.register(AtelierPosteDoc)
admin.site.register(ObservationAtelier)
