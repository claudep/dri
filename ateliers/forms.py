from django import forms

from common.fields import PickDateWidget
from person.forms import AttachmentFormMixin

from .models import AtelierARIHANE, BilanAtelier


class AtelierARIHANEForm(forms.ModelForm):
    class Meta:
        model = AtelierARIHANE
        exclude = ['date', 'visa']
        widgets = {
            'person': forms.HiddenInput,
            'createur': forms.HiddenInput,
            'date_debut': PickDateWidget,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name in self.fields:
            if field_name.startswith('remarques_'):
                self.fields[field_name].widget = forms.Textarea()


class BilanAtelierForm(AttachmentFormMixin, forms.ModelForm):
    class Meta:
        model = BilanAtelier
        exclude = ['createur',  'date', 'visa']
        widgets = {
            'person': forms.HiddenInput,
        }
