# Generated by Django 2.1.1 on 2018-11-02 10:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ateliers', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='actionatelier',
            old_name='action',
            new_name='actions',
        ),
    ]
