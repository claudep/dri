from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ateliers', '0002_rename_actions'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ateliercrea1',
            name='resistance',
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_comport_coll',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Comportement avec les collègues'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_comport_coll_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_comport_msp',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Comportement avec le MSP'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_comport_msp_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_effort',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Effort fourni'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_effort_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_horaire',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Horaire ponctualite'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_horaire_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_initiatives',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Initiatives'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_initiatives_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_interch',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Interchangeabilité du poste de travail'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_interch_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_productivite',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Productivité'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_productivite_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_qualite',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Qualité du travail'),
        ),
        migrations.AddField(
            model_name='ateliercrea1',
            name='item_qualite_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
    ]
