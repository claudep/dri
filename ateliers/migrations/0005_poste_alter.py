from django.db import migrations, models

POSTE_CHOICES = [('NP', 'NP'), ('0', '0'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4')]

class Migration(migrations.Migration):

    dependencies = [
        ('ateliers', '0004_crea2_item_fields'),
    ]

    operations = [
        migrations.AlterField(
            model_name='atelierposte',
            name='acte_jud',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Acte judiciaire'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='acte_poursuite',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Acte de poursuite'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='activer_pos',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Activer le EFT POS'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='activer_pos2',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Activer le EFT POS2'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='activer_scanner',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Activer le scanner'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='allumer_caisse',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Allumer la caisse'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='amabilite',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Amabilité face à la clientèle'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='argent_pochette',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name="Mettre l'argent dans la pochette"),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='caisses_grises',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Caisses grises pour lettres'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='colis_ch',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Colis CH'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='colis_etranger',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Colis étranger'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='colis_propre',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Colis à remettre en main propre'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='colis_remb',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Colis contre remboursement'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='colis_sign',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Colis signature'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='coller_etiq',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Coller correctement les étiquettes'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='coller_etiq_jaunes',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Coller étiquette jaune retour'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='coller_etiqu_colis',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Coller correctement les étiquettes sur le colis'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='comprendre',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Comprendre la demande du client'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='compter_caisse',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Compter caisse et timbres courant'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='compter_reserve',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Compter la réserve'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='compter_vignettes',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Compter vignettes cartons'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='constat_erreur',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Constater une erreur'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='controle_date',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name="Contrôle date tampon de l'agence"),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='depot_rem1',
            field=models.TextField(blank=True, verbose_name='Dépot Remarques 1'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='depot_rem2',
            field=models.TextField(blank=True, verbose_name='Dépot Remarques 2'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='depot_rem3',
            field=models.TextField(blank=True, verbose_name='Dépot Remarques 3'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='depot_rem4',
            field=models.TextField(blank=True, verbose_name='Dépot Remarques 4'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='desactiver_pos',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Désactiver le EFT POS'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='dispobox',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Dispobox'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='effect_ops',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Effectuer les opérations'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='effect_retrait',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name="Effectuer un retrait d'argent"),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='effect_total',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Effectuer le total'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='erreurs',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Chercher les erreurs'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='express',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Spéciaux express'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='extraire_z1',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Extraire le Z1'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='gas',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='GAS'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='indep_rem',
            field=models.TextField(blank=True, verbose_name='Indépendance Remarques'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='inscr_colis',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Inscrire nom et date sur le colis'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='inscr_rbt',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Inscrire les rbt sur le carnet'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='inscr_retour',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Inscription dans le carnet des retours'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='inscr_z1',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Inscrire le ticket Z1'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='lettre_ch',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Lettres CH'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='lettre_etranger',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Lettres étranger'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='lettre_form',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Lettres avec formulaire à coller sur couvercle'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='lettre_pp',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Lettres PP'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='lettre_reception',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Lettre recommandée accusé de réception'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='lettre_recomm',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Lettre recommandée'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='lettre_remb',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Lettre contre remboursemen'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='maxilettre',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Maxi lettres étranger'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='mil_aveugle',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Militaire aveugle'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='mil_cec',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='MIL CEC'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='noter_avances',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Noter les avances clients'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='parler_distinct',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Parler distinctement au client'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='petit_mat',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Petits matériels éponge stylos'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='prep_rem1',
            field=models.TextField(blank=True, verbose_name='Prép. avant ouverture et fermeture Remarques 1'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='prep_rem2',
            field=models.TextField(blank=True, verbose_name='Prép. avant ouverture et fermeture Remarques 2'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='prio_eco',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Prio ou Eco'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='r_accrecep',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='R avec accusé de réception'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='r_accrecep1',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='R avec accusé de réception'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='r_contre_remb',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='R et contre remboursement'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='r_propre',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='R avec remise en main propre'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='r_rembours',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='R avec contre remboursement'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='r_remise_propre',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='R et à remettre en mains propre'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='rang_colis',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Rangement des colis'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='rec_310',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Recommandé avec formulaire 310'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='rech_postech',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Recherches sur poste.ch'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='rempl_voiture',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Remplir la lettre de voiture'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='remplir_conf',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Remplir la confirmation de dépôt'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='remplir_conf_r',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Remplir la confirmation de dépôt si R'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='scanner_recep',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Scanner en réception'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='scanner_retours',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Scanner les retours'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='stress',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Gérer le stress'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='suppl_propre',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Suppl. remise en main propre'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='suppl_recomm',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Suppl. recommandé'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='suppl_remb',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Suppl. contre remboursement'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='ticket_balance1',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Obtenir le bon ticket sur la balance'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='ticket_balance2',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Obtenir le bon ticket sur la balance'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='ticket_balance3',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Obtenir le bon ticket sur la balance'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='tracer_numpostal',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Tracer numéro postal'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='transcrire_caisse',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Transcrire le rapport de caisse'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='travaux_guich_rem1',
            field=models.TextField(blank=True, verbose_name='Travaux au guichet Remarques 1'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='travaux_guich_rem2',
            field=models.TextField(blank=True, verbose_name='Travaux au guichet Remarques 2'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='travaux_guich_rem3',
            field=models.TextField(blank=True, verbose_name='Travaux au guichet Remarques 3'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='travaux_guich_rem4',
            field=models.TextField(blank=True, verbose_name='Travaux au guichet Remarques 4'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='travaux_guich_rem5',
            field=models.TextField(blank=True, verbose_name='Travaux au guichet Remarques 5'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='travaux_ordi_rem1',
            field=models.TextField(blank=True, verbose_name='Travaux sur l’ordi Remarques 1'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='travaux_ordi_rem2',
            field=models.TextField(blank=True, verbose_name='Travaux sur l’ordi Remarques 2'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='travaux_ordi_rem3',
            field=models.TextField(blank=True, verbose_name='Travaux sur l’ordi Remarques 3'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='travaux_ordi_rem4',
            field=models.TextField(blank=True, verbose_name='Travaux sur l’ordi Remarques 4'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='travaux_ordi_rem5',
            field=models.TextField(blank=True, verbose_name='Travaux sur l’ordi Remarques 5'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='trouver_colis',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Trouver les colis à retourner'),
        ),
        migrations.AlterField(
            model_name='atelierposte',
            name='trouver_lettres',
            field=models.CharField(blank=True, choices=POSTE_CHOICES, max_length=2, verbose_name='Trouver les lettres à retourner'),
        ),
    ]
