from django.db import migrations, models

EVAL_CHOICES = [('NP', 'NP'), ('0', '0'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4')]

class Migration(migrations.Migration):

    dependencies = [
        ('ateliers', '0006_cuisine_items'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='atelierarihane',
            name='nombres_aide_ctrl',
        ),
        migrations.RemoveField(
            model_name='atelierarihane',
            name='total_items',
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='absenteisme',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Absentéisme'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='adaptation',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name="Capacité de s'adapter au changement"),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='agrafeuse',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Agrafeuse sur pied'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='assemblage',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Assemblage'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='auto_correction',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Auto correction'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='autonomie',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Autonomie'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='boucler_mois',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Boucler la fin de mois'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='calculs_excel',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Calculs dans excel'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='collaboration',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Capacité de collaboration'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='comportement',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Autre'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='composer_lettre',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Composer une lettre'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='compta_excel',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Compta simple avec excel'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='connaissance_prof',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Connaissance prof. complémentaire'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='corriger_erreur',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Corriger une erreur'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='creation_site',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Création de sites internet'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='creer_facture',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Créer une facture'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='creer_form',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Créer un formulaire'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='elements_demandes',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Faire figurer les éléments demandés'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='enregistrer_fichier',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Enregistrer son fichier au bon endroit'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='enregistrer_paiement',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Enregistrer un paiement'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='enveloppes_etiq',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Étiqueter des enveloppes'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='enveloppes_fermer',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Fermer les enveloppes'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='enveloppes_timbrer',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Timbrer les enveloppes avec le pp'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='equilibrer_affiche',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name="Équilibrer l'affiche"),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='esprit_initiative',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name="Esprit d'initiative"),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='esprit_ouverture',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name="Ouverture d'esprit"),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='heures_employes',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Tenues des heures des employés'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='inserer_texte',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Insérer du texte'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='machine_boudin_m',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Machine à relier boudin métallique'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='machine_boudin_p',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Machine à relier boudin plastique'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='machine_chaud',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Machine à relier à chaud'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='maj_sites',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Mise à jour de sites internet'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='massicot_e',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Massicot électrique'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='massicot_m',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Massicot manuel'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='memorisation',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Mémorisation des consignes'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='mep_indesign',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Mise en page élaborée indesign'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='mep_word_chap',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Mise en page élaborée word chapitre'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='mep_word_lettre',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Mise en page simple word lettre'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='modifier_pdf',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Modifier un pdf'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='msp_complexe',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Mise sous pli complexe avec objets'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='msp_plus_cinq',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Mise sous pli plus de 5 docs'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='msp_simple',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Mise sous pli simple 1 à 5 docs'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='nombres_aide',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name="Nombres d'aide ou contrôle par heure"),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='numeriser',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Numériser des documents'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='ordi_allumer',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name="Allumer l'ordi"),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='ordi_arreter',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name="Arrêter l'ordinateur"),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='organisation',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Organisation'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='photoc_c200',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Photocopies simples sur C200'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='photoc_c353',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Photocopies simples sur C353'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='photoc_speciales',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Photocopies spéciales réduc agr'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='place_ordre',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Tient sa place en ordre'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='plastifieuse',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Plastifieuse'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='plieuse',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Plieuse'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='polyvalence',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Polyvalence'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='ponctuel',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Est ponctuel'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='publipostage',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Réaliser un publipostage'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='qualite_travail',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Qualité du travail'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='regularite',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Régularité'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='reperer_erreur',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Repérer une erreur'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='respect_consignes',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Respect des consignes'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='respect_hierarchie',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Respect de la hiérarchie'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='respect_reglement',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Respect du règlement'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='responsabilites',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Sens des responsabilités'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='retouche_photo',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Retoucher une photo'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='rythme',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Rythme de travail'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='saisie_texte',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Saisie de texte au km'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='salle_manger',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='En salle à manger'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='suivi_animation',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name="Suivi de l'animation"),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='support_pression',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Capacité de supporter la pression'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='transformer_pdf',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Transformer en pdf'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='travail_seul',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Se met seul au travail'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='trouver_fichier',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Trouver son fichier'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='trouver_illustration',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Trouver une illustration adaptée'),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='utiliser_aide',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name="Utiliser l'aide du logiciel ou internet"),
        ),
        migrations.AlterField(
            model_name='atelierarihane',
            name='utiliser_logiciel',
            field=models.CharField(blank=True, choices=EVAL_CHOICES, max_length=2, verbose_name='Utiliser le bon logiciel'),
        ),
    ]
