# Generated by Django 2.1.1 on 2018-11-30 14:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ateliers', '0007_arihane_modifs'),
    ]

    operations = [
        migrations.AddField(
            model_name='atelierinfo',
            name='item_comport_coll',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Comportement avec les collègues'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_comport_coll_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_comport_msp',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Comportement avec le MSP'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_comport_msp_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_effort',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Effort fourni'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_effort_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_horaire',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Horaire ponctualite'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_horaire_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_initiatives',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Initiatives'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_initiatives_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_interch',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Interchangeabilité du poste de travail'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_interch_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_productivite',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Productivité'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_productivite_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_qualite',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Qualité du travail'),
        ),
        migrations.AddField(
            model_name='atelierinfo',
            name='item_qualite_rem',
            field=models.TextField(blank=True, verbose_name='Remarques'),
        ),
    ]
