from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ateliers', '0009_remove_obsolete_items'),
    ]

    operations = [
        migrations.AddField(
            model_name='bilanatelier',
            name='date_bilan',
            field=models.DateField(blank=True, null=True, verbose_name='Date du bilan'),
        ),
        migrations.AlterModelOptions(
            name='bilanatelier',
            options={'get_latest_by': 'date_bilan', 'ordering': ('-date_bilan',), 'verbose_name': 'Bilan d’atelier', 'verbose_name_plural': 'Bilans d’ateliers'},
        ),
    ]
