from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ateliers', '0010_bilanatelier_date_bilan'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActionAction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('action', models.ForeignKey(on_delete=models.deletion.CASCADE, to='ateliers.ActionAtelier')),
                ('createur', models.ForeignKey(on_delete=models.deletion.PROTECT, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
                ('date', models.DateTimeField(verbose_name='Date')),
                ('texte', models.TextField(verbose_name='Action')),
            ],
            options={
                'verbose_name': "Action d'action",
                'verbose_name_plural': "Action d'action",
            },
        ),
        migrations.CreateModel(
            name='ActionEval',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('action', models.ForeignKey(on_delete=models.deletion.CASCADE, to='ateliers.ActionAtelier')),
                ('createur', models.ForeignKey(on_delete=models.deletion.PROTECT, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
                ('date', models.DateTimeField(verbose_name='Date')),
                ('texte', models.TextField(verbose_name='Évaluation')),
            ],
            options={
                'verbose_name': "Évaluation d'action",
                'verbose_name_plural': "Évaluations d'action",
            },
        ),
        migrations.RenameField(
            model_name='actionatelier',
            old_name='actions',
            new_name='actions_old',
        ),
        migrations.RenameField(
            model_name='actionatelier',
            old_name='evaluations',
            new_name='evaluations_old',
        ),
    ]
