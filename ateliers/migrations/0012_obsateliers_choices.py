from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ateliers', '0011_action_eval_action'),
    ]

    operations = [
        migrations.AlterField(
            model_name='observationatelier',
            name='atelier',
            field=models.CharField(blank=True, choices=[('arihane', 'ARIHANE'), ('crea1', 'Créa 1'), ('crea2', 'Créa 2'), ('cuisine', 'Cuisine'), ('info', 'Info FHMN'), ('poste', 'Poste'), ('comm', 'Communication')], max_length=10, verbose_name='Atelier'),
        ),
    ]
