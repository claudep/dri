from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ateliers', '0015_atelierpostedoc'),
    ]

    operations = [
        migrations.AddField(
            model_name='bilanatelier',
            name='libelle',
            field=models.CharField(choices=[('bilan', 'Bilan'), ('interm', 'Bilan intermédiaire')], default='bilan', max_length=8, verbose_name='Libellé'),
            preserve_default=False,
        ),
    ]
