import os
from datetime import date

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.urls import reverse

from person.models import ActionBase, Attachment, Person, PersonRelatedModel, User


ACTIVITE_CHOICES = (
    ('i', 'Impossible'),
    ('ag', 'Avec guidance'),
    ('ai', 'Avec aide'),
    ('s', 'Seul'),
)

class ActionAtelier(ActionBase):
    class Meta:
        verbose_name = "Action atelier"
        verbose_name_plural = "Actions atelier"

    _view_name_base = 'actionatelier'
    edit_perm = 'ateliers.change_actionatelier'

    def __str__(self):
        return "Action atelier pour %s (%s)" % (self.person, self.date)


class ActionEval(models.Model):
    action = models.ForeignKey(ActionAtelier, on_delete=models.CASCADE)
    date = models.DateTimeField("Date")
    createur = models.ForeignKey(
        User, on_delete=models.PROTECT, verbose_name='Créateur', related_name='+'
    )
    texte = models.TextField("Évaluation")

    class Meta:
        verbose_name = "Évaluation d'action"
        verbose_name_plural = "Évaluations d'action"

    def __str__(self):
        return "Évaluation d'action pour %s (%s)" % (self.action.person, self.date)


class ActionAction(models.Model):
    action = models.ForeignKey(ActionAtelier, on_delete=models.CASCADE)
    date = models.DateTimeField("Date")
    createur = models.ForeignKey(
        User, on_delete=models.PROTECT, verbose_name='Créateur', related_name='+'
    )
    texte = models.TextField("Action")

    class Meta:
        verbose_name = "Action d'action"
        verbose_name_plural = "Action d'action"

    def __str__(self):
        return "Action d'action pour %s (%s)" % (self.action.person, self.date)


class BilanAtelier(PersonRelatedModel):
    LIBELLE_CHOICES = (
        ('bilan', 'Bilan'),
        ('interm', 'Bilan intermédiaire'),
    )
    date_bilan = models.DateField("Date du bilan", null=True, blank=True)
    libelle = models.CharField("Libellé", max_length=8, choices=LIBELLE_CHOICES)
    msp_presents = models.CharField("Participants", max_length=50, blank=True)
    commentaire = models.TextField("Commentaires et propositions", blank=True)
    rappel_objectifs = models.TextField("Rappel des objectifs", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)
    resultats = models.TextField("Résultats", blank=True)
    deroulement = models.TextField("Déroulement de l'entretien", blank=True)
    horaire = models.TextField("Horaire travailleur", blank=True)
    fichiers = GenericRelation(Attachment)

    _view_name_base = 'bilanatelier'
    edit_perm = 'ateliers.change_bilanatelier'
    list_display = ['date_bilan', 'libelle', 'auteur']

    class Meta:
        verbose_name = "Bilan d’atelier"
        verbose_name_plural = "Bilans d’ateliers"
        get_latest_by = 'date_bilan'
        ordering = ('-date_bilan',)

    def __str__(self):
        return "Bilan atelier du %s pour %s" % (self.date_bilan, self.person)


class ObservationAtelier(PersonRelatedModel):
    ATELIER_CHOICES = (
        ('arihane', 'ARIHANE'),
        ('crea1', 'Créa 1'),
        ('crea2', 'Créa 2'),
        ('cuisine', 'Cuisine'),
        ('info', 'Info FHMN'),
        ('poste', 'Poste'),
        ('comm', 'Communication'),
    )
    atelier = models.CharField("Atelier", max_length=10, choices=ATELIER_CHOICES, blank=True)
    observation = models.TextField("Observation", blank=True)
    fichiers = GenericRelation(Attachment)

    class Meta:
        verbose_name = "Observation atelier"
        verbose_name_plural = "Observations atelier"

    _view_name_base = 'obsatelier'
    list_display = ['date', 'observation:w100:files', 'atelier', 'auteur']
    searchable = {'txt': ['observation']}

    def __str__(self):
        return "Observation atelier du %s pour %s" % (self.date, self.person)

    def can_edit(self, user):
        return user.is_superuser or (user == self.createur and self.date.date() == date.today())


class AtelierARIHANEDoc(PersonRelatedModel):
    date = models.DateTimeField("Date")
    doc = models.FileField(upload_to='documents', verbose_name="Fichier")
    title = models.CharField("Titre", max_length=150, blank=True)
    date_doc = models.DateField("Date du document", null=True)

    _view_name_base = 'at_arihane'

    def __str__(self):
        return "Document ARIHANE pour %s (%s)" % (self.person, self.date)

    def get_absolute_url(self):
        return self.doc.url

    @property
    def titre(self):
        return self.title if self.title else os.path.basename(self.doc.path)


# Ancien modèle, conservé pour archivage uniquement
class AtelierARIHANE(PersonRelatedModel):
    EVAL_CHOICES = (
        ('NP', 'NP'),  # non pertinent
        ('0', '0'),  # aucun soutien
        ('1', '1'),  # incitation de départ et/ou contrôle final
        ('2', '2'),  # indication et supervision
        ('3', '3'),  # ?
        ('4', '4'),  # ?
    )
    date_debut = models.DateField("Début de l'activité", null=True, blank=True)
    # Utilisation des machines
    photoc_c200 = models.CharField("Photocopies simples sur C200", max_length=2, choices=EVAL_CHOICES, blank=True)
    photoc_c353 = models.CharField("Photocopies simples sur C353", max_length=2, choices=EVAL_CHOICES, blank=True)
    photoc_speciales = models.CharField("Photocopies spéciales réduc agr", max_length=2, choices=EVAL_CHOICES, blank=True)
    numeriser = models.CharField("Numériser des documents", max_length=2, choices=EVAL_CHOICES, blank=True)
    massicot_e = models.CharField("Massicot électrique", max_length=2, choices=EVAL_CHOICES, blank=True)
    massicot_m = models.CharField("Massicot manuel", max_length=2, choices=EVAL_CHOICES, blank=True)
    plieuse = models.CharField("Plieuse", max_length=2, choices=EVAL_CHOICES, blank=True)
    machine_boudin_p = models.CharField("Machine à relier boudin plastique", max_length=2, choices=EVAL_CHOICES, blank=True)
    machine_boudin_m = models.CharField("Machine à relier boudin métallique", max_length=2, choices=EVAL_CHOICES, blank=True)
    machine_chaud = models.CharField("Machine à relier à chaud", max_length=2, choices=EVAL_CHOICES, blank=True)
    plastifieuse = models.CharField("Plastifieuse", max_length=2, choices=EVAL_CHOICES, blank=True)
    agrafeuse = models.CharField("Agrafeuse sur pied", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_util_mach = models.CharField("Remarques utilisation machines", max_length=100, blank=True)

    ordi_allumer = models.CharField("Allumer l'ordi", max_length=2, choices=EVAL_CHOICES, blank=True)
    trouver_fichier = models.CharField("Trouver son fichier", max_length=2, choices=EVAL_CHOICES, blank=True)
    enregistrer_fichier = models.CharField("Enregistrer son fichier au bon endroit", max_length=2, choices=EVAL_CHOICES, blank=True)
    utiliser_logiciel = models.CharField("Utiliser le bon logiciel", max_length=2, choices=EVAL_CHOICES, blank=True)
    utiliser_aide = models.CharField("Utiliser l'aide du logiciel ou internet", max_length=2, choices=EVAL_CHOICES, blank=True)
    ordi_arreter = models.CharField("Arrêter l'ordinateur", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_util_ordi = models.CharField("Remarques utilisation ordi", max_length=100, blank=True)
    # Travaux d'apprêts
    assemblage = models.CharField("Assemblage", max_length=2, choices=EVAL_CHOICES, blank=True)
    msp_simple = models.CharField("Mise sous pli simple 1 à 5 docs", max_length=2, choices=EVAL_CHOICES, blank=True)
    msp_plus_cinq = models.CharField("Mise sous pli plus de 5 docs", max_length=2, choices=EVAL_CHOICES, blank=True)
    msp_complexe = models.CharField("Mise sous pli complexe avec objets", max_length=2, choices=EVAL_CHOICES, blank=True)
    enveloppes_etiq = models.CharField("Étiqueter des enveloppes", max_length=2, choices=EVAL_CHOICES, blank=True)
    enveloppes_fermer = models.CharField("Fermer les enveloppes", max_length=2, choices=EVAL_CHOICES, blank=True)
    enveloppes_timbrer = models.CharField("Timbrer les enveloppes avec le pp", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_apprets = models.CharField("Remarques Tableau d'apprets", max_length=100, blank=True)

    saisie_texte = models.CharField("Saisie de texte au km", max_length=2, choices=EVAL_CHOICES, blank=True)
    mep_word_lettre = models.CharField("Mise en page simple word lettre", max_length=2, choices=EVAL_CHOICES, blank=True)
    mep_word_chap = models.CharField("Mise en page élaborée word chapitre", max_length=2, choices=EVAL_CHOICES, blank=True)
    composer_lettre = models.CharField("Composer une lettre", max_length=2, choices=EVAL_CHOICES, blank=True)
    publipostage = models.CharField("Réaliser un publipostage", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_ordi1 = models.CharField("Remarques Travaux sur ordi", max_length=100, blank=True)

    compta_excel = models.CharField("Compta simple avec excel", max_length=2, choices=EVAL_CHOICES, blank=True)
    calculs_excel = models.CharField("Calculs dans excel", max_length=2, choices=EVAL_CHOICES, blank=True)
    heures_employes = models.CharField("Tenues des heures des employés", max_length=2, choices=EVAL_CHOICES, blank=True)
    suivi_animation = models.CharField("Suivi de l'animation", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_ordi2 = models.CharField("Remarques Travaux sur ordi", max_length=100, blank=True)

    mep_indesign = models.CharField("Mise en page élaborée indesign", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_ordi3 = models.CharField("Remarques Travaux sur ordi", max_length=100, blank=True)

    retouche_photo = models.CharField("Retoucher une photo", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_ordi4 = models.CharField("Remarques Travaux sur ordi", max_length=100, blank=True)

    creer_form = models.CharField("Créer un formulaire", max_length=2, choices=EVAL_CHOICES, blank=True)
    transformer_pdf = models.CharField("Transformer en pdf", max_length=2, choices=EVAL_CHOICES, blank=True)
    modifier_pdf = models.CharField("Modifier un pdf", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_ordi5 = models.CharField("Remarques Travaux sur ordi", max_length=100, blank=True)

    creer_facture = models.CharField("Créer une facture", max_length=2, choices=EVAL_CHOICES, blank=True)
    enregistrer_paiement = models.CharField("Enregistrer un paiement", max_length=2, choices=EVAL_CHOICES, blank=True)
    boucler_mois = models.CharField("Boucler la fin de mois", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_ordi6 = models.CharField("Remarques Travaux sur ordi", max_length=100, blank=True)

    maj_sites = models.CharField("Mise à jour de sites internet", max_length=2, choices=EVAL_CHOICES, blank=True)
    creation_site = models.CharField("Création de sites internet", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_ordi7 = models.CharField("Remarques Travaux sur ordi", max_length=100, blank=True)

    trouver_illustration = models.CharField("Trouver une illustration adaptée", max_length=2, choices=EVAL_CHOICES, blank=True)
    inserer_texte = models.CharField("Insérer du texte", max_length=2, choices=EVAL_CHOICES, blank=True)
    elements_demandes = models.CharField("Faire figurer les éléments demandés", max_length=2, choices=EVAL_CHOICES, blank=True)
    equilibrer_affiche = models.CharField("Équilibrer l'affiche", max_length=2, choices=EVAL_CHOICES, blank=True)
    reperer_erreur = models.CharField("Repérer une erreur", max_length=2, choices=EVAL_CHOICES, blank=True)
    corriger_erreur = models.CharField("Corriger une erreur", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_creation = models.CharField("Remarques Tableau de création", max_length=100, blank=True)

    esprit_ouverture = models.CharField("Ouverture d'esprit", max_length=2, choices=EVAL_CHOICES, blank=True)
    collaboration = models.CharField("Capacité de collaboration", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_collegues = models.CharField("Remarques avec ses collègues", max_length=100, blank=True)

    respect_reglement = models.CharField("Respect du règlement", max_length=2, choices=EVAL_CHOICES, blank=True)
    respect_hierarchie = models.CharField("Respect de la hiérarchie", max_length=2, choices=EVAL_CHOICES, blank=True)
    support_pression = models.CharField("Capacité de supporter la pression", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_msp = models.CharField("Remarques avec les MSP", max_length=100, blank=True)

    salle_manger = models.CharField("En salle à manger", max_length=2, choices=EVAL_CHOICES, blank=True)
    comportement = models.CharField("Autre", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_collab = models.CharField("Remarques avec les collaborateurs du Foyer", max_length=100, blank=True)

    ponctuel = models.CharField("Est ponctuel", max_length=2, choices=EVAL_CHOICES, blank=True)
    travail_seul = models.CharField("Se met seul au travail", max_length=2, choices=EVAL_CHOICES, blank=True)
    place_ordre = models.CharField("Tient sa place en ordre", max_length=2, choices=EVAL_CHOICES, blank=True)
    memorisation = models.CharField("Mémorisation des consignes", max_length=2, choices=EVAL_CHOICES, blank=True)
    respect_consignes = models.CharField("Respect des consignes", max_length=2, choices=EVAL_CHOICES, blank=True)
    rythme = models.CharField("Rythme de travail", max_length=2, choices=EVAL_CHOICES, blank=True)
    regularite = models.CharField("Régularité", max_length=2, choices=EVAL_CHOICES, blank=True)
    autonomie = models.CharField("Autonomie", max_length=2, choices=EVAL_CHOICES, blank=True)
    organisation = models.CharField("Organisation", max_length=2, choices=EVAL_CHOICES, blank=True)
    qualite_travail = models.CharField("Qualité du travail", max_length=2, choices=EVAL_CHOICES, blank=True)
    auto_correction = models.CharField("Auto correction", max_length=2, choices=EVAL_CHOICES, blank=True)
    polyvalence = models.CharField("Polyvalence", max_length=2, choices=EVAL_CHOICES, blank=True)
    esprit_initiative = models.CharField("Esprit d'initiative", max_length=2, choices=EVAL_CHOICES, blank=True)
    responsabilites = models.CharField("Sens des responsabilités", max_length=2, choices=EVAL_CHOICES, blank=True)
    adaptation = models.CharField("Capacité de s'adapter au changement", max_length=2, choices=EVAL_CHOICES, blank=True)
    absenteisme = models.CharField("Absentéisme", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_travail = models.CharField("Remarques face au travail", max_length=100, blank=True)

    nombres_aide = models.CharField("Nombres d'aide ou contrôle par heure", max_length=2, blank=True)
    connaissance_prof = models.CharField("Connaissance prof. complémentaire", max_length=2, choices=EVAL_CHOICES, blank=True)
    remarques_indep = models.CharField("Remarques Indépendance", max_length=100, blank=True)

    _view_name_base = 'at_arihane'

    class Meta:
        verbose_name = 'Atelier ARIHANE'
        verbose_name_plural = 'Ateliers ARIHANE'

    def __str__(self):
        return "Atelier ARIHANE pour %s (%s)" % (self.person, self.date)

    @property
    def total_items(self):
        return sum([
            int(getattr(self, f.name))
            for f in self._meta.get_fields()
            if f.name not in ('nombres_aide', 'connaissance_prof') and f.max_length == 2 and getattr(self, f.name, '').isdigit()
        ])


class AtelierCommunication(PersonRelatedModel):
    ordinateur = models.CharField("Ordinateur", max_length=100, blank=True)
    email = models.CharField("Email", max_length=100, blank=True)
    horaire = models.CharField("Horaire", max_length=100, blank=True)
    autre1 = models.TextField("Autre", blank=True)
    autre2 = models.TextField("Autre", blank=True)
    comprehension = models.TextField("Compréhension", blank=True)
    ecriture = models.TextField("Écriture", blank=True)
    expression = models.TextField("Expression", blank=True)
    fatigabilite = models.TextField("Fatigabilité", blank=True)
    interets = models.TextField("Intérêts souhaits", blank=True)
    lecture = models.TextField("Lecture", blank=True)
    memoire = models.TextField("Mémoire", blank=True)
    moyens = models.TextField("Moyens", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)
    souris = models.TextField("Utilisation de la souris", blank=True)
    clavier = models.TextField("Utilisation du clavier", blank=True)
    vision = models.TextField("Vision", blank=True)

    _view_name_base = 'at_comm'
    edit_perm = 'ateliers.change_ateliercommunication'

    def __str__(self):
        return "Atelier Communication pour %s (%s)" % (self.person, self.date)


class ItemTableMixin(models.Model):
    item_comport_msp = models.SmallIntegerField("Comportement avec le MSP", null=True, blank=True)
    item_comport_msp_rem = models.TextField("Remarques", blank=True)
    item_comport_coll = models.SmallIntegerField("Comportement avec les collègues", null=True, blank=True)
    item_comport_coll_rem = models.TextField("Remarques", blank=True)
    item_productivite = models.SmallIntegerField("Productivité", null=True, blank=True)
    item_productivite_rem = models.TextField("Remarques", blank=True)
    item_interch = models.SmallIntegerField("Interchangeabilité du poste de travail", null=True, blank=True)
    item_interch_rem = models.TextField("Remarques", blank=True)
    item_qualite = models.SmallIntegerField("Qualité du travail", null=True, blank=True)
    item_qualite_rem = models.TextField("Remarques", blank=True)
    item_effort = models.SmallIntegerField("Effort fourni", null=True, blank=True)
    item_effort_rem = models.TextField("Remarques", blank=True)
    item_horaire = models.SmallIntegerField("Horaire ponctualite", null=True, blank=True)
    item_horaire_rem = models.TextField("Remarques", blank=True)
    item_initiatives = models.SmallIntegerField("Initiatives", null=True, blank=True)
    item_initiatives_rem = models.TextField("Remarques", blank=True)

    class Meta:
        abstract = True

    @property
    def total_items(self):
        return sum([
            self.item_comport_msp or 0, self.item_comport_coll or 0, self.item_productivite or 0,
            self.item_interch or 0, self.item_qualite or 0, self.item_effort or 0,
            self.item_horaire or 0, self.item_initiatives or 0
        ])


class AtelierCrea1(ItemTableMixin, PersonRelatedModel):
    horaire = models.TextField("Horaire", blank=True)

    tricotin = models.CharField("Tricotin", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    tricotin_rem = models.TextField("Remarques", blank=True)
    tricot = models.CharField("Tricot machine", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    tricot_rem = models.TextField("Remarques", blank=True)
    couture = models.CharField("Couture broderie etc", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    couture_rem = models.TextField("Remarques", blank=True)

    animaux_perles = models.CharField("Animaux en perles de rocaille", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    animaux_perles_rem = models.TextField("Remarques", blank=True)
    collier = models.CharField("Création de collier", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    collier_rem = models.TextField("Remarques", blank=True)

    papier_repousse = models.CharField("Papier repoussé", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    papier_repousse_rem = models.TextField("Remarques", blank=True)
    picage = models.CharField("Picage avec pochoir", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    picage_rem = models.TextField("Remarques", blank=True)
    tampon = models.CharField("Tampon peinture poudre", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    tampon_rem = models.TextField("Remarques", blank=True)
    collage_sable = models.CharField("Collage de sable", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    collage_sable_rem = models.TextField("Remarques", blank=True)
    peinture_div = models.CharField("Peintures diverses", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    peinture_div_rem = models.TextField("Remarques", blank=True)

    feutrage = models.CharField("Feutrage à plat", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    feutrage_rem = models.TextField("Remarques", blank=True)
    fleurs = models.CharField("Fabrication de fleurs", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    fleurs_rem = models.TextField("Remarques", blank=True)
    boules = models.CharField("Fabrication de boules", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    boules_rem = models.TextField("Remarques", blank=True)

    papier = models.CharField("Papier", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    papier_rem = models.TextField("Remarques", blank=True)
    fils = models.CharField("Fils", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    fils_rem = models.TextField("Remarques", blank=True)
    batonnets = models.CharField("Bâtonnets", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    batonnets_rem = models.TextField("Remarques", blank=True)

    mobidai = models.CharField("Mobidai", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    mobidai_rem = models.TextField("Remarques", blank=True)
    capteur_reves = models.CharField("Capteur de rêves", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    capteur_reves_rem = models.TextField("Remarques", blank=True)
    macrame = models.CharField("Macramé", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    macrame_rem = models.TextField("Remarques", blank=True)
    peinture_pochoir = models.CharField("Peinture au pochoir sur tissu", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    peinture_pochoir_rem = models.TextField("Remarques", blank=True)
    peinture_soie = models.CharField("Peinture de fils de soie", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    peinture_soie_rem = models.TextField("Remarques", blank=True)
    dessin = models.CharField("Dessin peinture etc", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    dessin_rem = models.TextField("Remarques", blank=True)

    mosaique = models.CharField("Mosaïque, papier mâché, bougie, pâte fimo, autres…", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    mosaique_rem = models.TextField("Remarques", blank=True)

    tenir_stand = models.CharField("Tenir le stand répondre à la clientèle", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    tenir_stand_rem = models.TextField("Remarques", blank=True)
    tenir_caisse = models.CharField("Tenir la caisse", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    tenir_caisse_rem = models.TextField("Remarques", blank=True)
    vente_remplir = models.CharField("Remplir la feuille de vente", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    vente_remplir_rem = models.TextField("Remarques", blank=True)

    activites_rem = models.TextField("Activités remarque", blank=True)

    # Importation initiale, à supprimer
    crea1_item = models.SmallIntegerField("Créa1 item 1 à 8", null=True, blank=True)
    crea1_item_rem = models.TextField("Remarques", blank=True)
    crea1_item_total = models.SmallIntegerField("Créa1 item total", null=True, blank=True)

    # Observations complémentaires
    resistance_fatigue = models.TextField("Résistance à la fatigue", blank=True)
    sante = models.TextField("Santé", blank=True)
    auto_evaluation = models.TextField("Auto-évaluation", blank=True)
    memoire_consignes = models.TextField("Mémoire des consignes", blank=True)
    reaction_diff = models.TextField("Réaction face à une difficulté", blank=True)
    orientation = models.TextField("Orientation spatio-temporelle", blank=True)

    ressources = models.TextField("Ressources personnelles", blank=True)
    difficultes = models.TextField("Principales difficultés", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)

    _view_name_base = 'at_crea1'

    class Meta:
        verbose_name = 'Atelier Créativité 1'
        verbose_name_plural = 'Ateliers Créativité 1'

    def __str__(self):
        return "Atelier Créa1 pour %s (%s)" % (self.person, self.date)


class AtelierCrea2(ItemTableMixin, PersonRelatedModel):
    horaire = models.TextField("Horaire", blank=True)
    activites = models.TextField("Activités réalisées", blank=True)

    resistance = models.TextField("Résistance à la fatigue", blank=True)
    sante = models.TextField("Santé", blank=True)
    auto_evaluation = models.TextField("Auto-évaluation", blank=True)
    memoire = models.TextField("Mémoire des consignes", blank=True)
    reaction_diff = models.TextField("Réaction face à une difficulté", blank=True)
    orientation = models.TextField("Orientation spatio temporelle", blank=True)

    ressources = models.TextField("Ressources personnelles", blank=True)
    difficultes = models.TextField("Principales difficultés", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)

    _view_name_base = 'at_crea2'

    class Meta:
        verbose_name = 'Atelier Créativité 2'
        verbose_name_plural = 'Ateliers Créativité 2'

    def __str__(self):
        return "Atelier Créa2 pour %s (%s)" % (self.person, self.date)


class AtelierCuisine(ItemTableMixin, PersonRelatedModel):
    horaire = models.TextField("Horaire", blank=True)
    # généralités
    chercher_mat = models.CharField("Aller chercher du matériel", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    installer_place = models.CharField("Installer sa place de travail", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    ranger = models.CharField("Ranger et nettoyer sa place", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    utiliser_balance = models.CharField("Utiliser balance", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    utiliser_mixer = models.CharField("Utiliser mixer", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    suivre_recette = models.CharField("Suivre une recette", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    sachets_vente = models.CharField("Emplir des sachets pour la vente", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    etiquetage = models.CharField("Etiquetage produits pour la vente", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    sachets_fermer = models.CharField("Fermer les sachets avec ficelle", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    generalites_rem = models.TextField("Généralités remarques", blank=True)

    auto_controle = models.CharField("Auto contrôle en cours", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    auto_controle_fini = models.CharField("Auto contrôle fini", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    telephoner = models.CharField("Téléphoner ou recevoir un appel", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    recevoir = models.CharField("Recevoir clients", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    gestion_stock = models.CharField("Gestion du stock", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    generalites_rem_10_15 = models.TextField("Généralités remarques 10à15", blank=True)
    tempo_moteur = models.CharField("Tempo moteur dans une chaîne de travail", max_length=2, choices=ACTIVITE_CHOICES, blank=True)

    prep_pate = models.CharField("Préparer la pâte", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    cuisson = models.CharField("Cuisson", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    bricelet_finition = models.CharField("Finition du bricele", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    bricelets_rem = models.TextField("Bricelets salés remarques", blank=True)

    prep_herbes = models.CharField("Préparation des herbes à sécher", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    prep_legumes = models.CharField("Préparation des légumes à sécher", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    melange = models.CharField("Mélange", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    emplissage_bocaux = models.CharField("Emplissage des bocaux", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    sels_rem = models.TextField("Sels remarques", blank=True)

    prep_ingredients = models.CharField("Préparation des ingrédients", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    melange_cake_sale = models.CharField("Mélange cake salés", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    mise_moule = models.CharField("Mise en moule ou plaque", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    cakes_sales_rem = models.TextField("Cakes salés remarques", blank=True)

    prep_ingredients_cake = models.CharField("Préparation des ingrédients", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    melange_cake_sucre = models.CharField("Mélange", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    mise_moule_cake = models.CharField("Mise en moule ou plaque", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    glacage = models.CharField("Glaçage", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    cakes_sucres_rem = models.TextField("Cakes sucrés remarques", blank=True)

    badigeonner = models.CharField("Badigeonner", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    emplissage = models.CharField("Emplissage", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    rouler = models.CharField("Rouler", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    couper = models.CharField("Couper", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    mise_cuisson = models.CharField("Mise place pour la cuisson au four", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    feuilletes_rem = models.TextField("Feuilletés remarques", blank=True)

    peler_legumes = models.CharField("Peler légumes", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    couper_legumes = models.CharField("Couper légumes", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    mise_assiette = models.CharField("Mise en place sur assiette", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    melange_sauce = models.CharField("Mélange sauce", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    mise_pot = models.CharField("Mise en pot", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    legumes_dips_rem = models.TextField("Légumes dips remarques", blank=True)
    # Confitures
    prep_fruits = models.CharField("Préparation des fruits", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    brasser = models.CharField("Brasser et surveiller la cuisson", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    mise_bocaux = models.CharField("Mise en bocaux", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    confitures_rem = models.TextField("Confitures remarques", blank=True)
    # Amandes
    prep_ingredients_amandes = models.CharField("Préparation des ingrédients", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    cuisson_amandes = models.CharField("Cuisson amandes", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    amandes_rem = models.TextField("Amandes au sucre remarques", blank=True)
    # Petits biscuits sucrés
    prep_ingredients_biscuits = models.CharField("Préparation ingrédients biscuits", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    conditionnement = models.CharField("Conditionnement pâte", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    # not used?:
    prep_pate_biscuits = models.CharField("Préparation pâte biscuits", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    biscuits_sucres_rem1 = models.TextField("Petits biscuits sucrés remarques 1à3", blank=True)
    faconnage_piece = models.CharField("Façonnage emporte pièce", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    faconnage_main = models.CharField("Façonnage à la main", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    mise_plaque = models.CharField("Mise en plaque", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    decoration = models.CharField("Décoration", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    biscuits_sucres_rem2 = models.TextField("Petits biscuits sucrés remarques 4à7", blank=True)

    activites_rem = models.TextField("Activités remarques", blank=True)

    resistance = models.TextField("Résistance à la fatigue", blank=True)
    sante = models.TextField("Cuisine santé", blank=True)
    auto_evaluation = models.TextField("Auto-évaluation", blank=True)
    memoire_consignes = models.TextField("Mémoire des consignes", blank=True)
    memoire_diff = models.TextField("Réaction face à une difficulté", blank=True)
    orientation = models.TextField("Orientation spatio temporelle", blank=True)

    difficultes = models.TextField("Principales difficultés", blank=True)
    ressources = models.TextField("Ressources personnelles", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)

    _view_name_base = 'at_cuisine'

    class Meta:
        verbose_name = 'Atelier cuisine'
        verbose_name_plural = 'Ateliers cuisine'

    def __str__(self):
        return "Atelier Cuisine pour %s (%s)" % (self.person, self.date)


class AtelierInfo(ItemTableMixin, PersonRelatedModel):
    horaire = models.TextField("Horaire", blank=True)

    travail_seul = models.CharField("Se mettre seul au travail", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    travail_seul_rem = models.TextField("Remarques", blank=True)
    saisie_texte = models.CharField("Saisie de texte au km", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    saisie_texte_rem = models.TextField("Remarques", blank=True)

    mep_simple = models.CharField("Mise en page simple", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    mep_simple_rem = models.TextField("Remarques", blank=True)
    mep_elaboree = models.CharField("Mise en page élaborée", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    mep_elaboree_rem = models.TextField("Remarques", blank=True)
    rediger = models.CharField("Rédiger une lettre", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    rediger_rem = models.TextField("Remarques", blank=True)
    publipostage = models.CharField("Réaliser un publipostage", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    publipostage_rem = models.TextField("Remarques", blank=True)

    ajout_adr = models.CharField("Ajouter et modifier des adresses", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    ajout_adr_rem = models.TextField("Remarques", blank=True)
    creer_bd = models.CharField("Créer une base de données", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    creer_bd_rem = models.TextField("Remarques", blank=True)

    compta_simple = models.CharField("Compta simple avec Excel", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    compta_simple_rem = models.TextField("Remarques", blank=True)
    compta_double = models.CharField("Compta double avec Excel", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    compta_double_rem = models.TextField("Remarques", blank=True)

    affiche = models.CharField("Réaliser une affiche", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    affiche_rem = models.TextField("Remarques", blank=True)
    mep_journal = models.CharField("Mettre en page un journal", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    mep_journal_rem = models.TextField("Remarques", blank=True)
    recherche_crea = models.CharField("Recherche création", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    recherche_crea_rem = models.TextField("Remarques", blank=True)

    dessin_illustr = models.CharField("Réaliser un dessin dans illustrator", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    dessin_illustr_rem = models.TextField("Remarques", blank=True)
    retoucher = models.CharField("Retoucher dans Photoshop", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    retoucher_rem = models.TextField("Remarques", blank=True)

    scanner = models.CharField("Scanner des images", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    scanner_rem = models.TextField("Remarques", blank=True)
    ocr = models.CharField("Reconnaissance de texte", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    ocr_rem = models.TextField("Remarques", blank=True)

    courriel = models.CharField("Courriel", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    courriel_rem = models.TextField("Remarques", blank=True)
    recherche = models.CharField("Recherche", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    recherche_rem = models.TextField("Remarques", blank=True)

    pliage = models.CharField("Pliage", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    pliage_rem = models.TextField("Remarques", blank=True)
    assemblage = models.CharField("Assemblage", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    assemblage_rem = models.TextField("Remarques", blank=True)
    coupe = models.CharField("Coupe", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    coupe_rem = models.TextField("Remarques", blank=True)
    photocopies = models.CharField("Photocopies", max_length=2, choices=ACTIVITE_CHOICES, blank=True)
    photocopies_rem = models.TextField("Remarques", blank=True)

    activites_rem = models.TextField("Remarques", blank=True)

    resistance = models.TextField("Résistance à la fatigue", blank=True)
    sante = models.TextField("Santé", blank=True)
    auto_evaluation = models.TextField("Auto-évaluation", blank=True)
    memoire = models.TextField("Mémoire des consignes", blank=True)
    reaction_diff = models.TextField("Réaction face à une difficulté", blank=True)
    orientation = models.TextField("Orientation spatio temporelle", blank=True)

    ressources = models.TextField("Ressources personnelles", blank=True)
    difficultes = models.TextField("Principales difficultés", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)

    _view_name_base = 'at_info'

    class Meta:
        verbose_name = 'Atelier Info'
        verbose_name_plural = 'Ateliers Info'

    def __str__(self):
        return "Atelier Info pour %s (%s)" % (self.person, self.date)


class AtelierPosteDoc(PersonRelatedModel):
    date = models.DateTimeField("Date")
    doc = models.FileField(upload_to='documents', verbose_name="Fichier")
    title = models.CharField("Titre", max_length=150, blank=True)
    date_doc = models.DateField("Date du document", null=True)

    _view_name_base = 'at_poste'

    def __str__(self):
        return "Document Atelier Poste pour %s (%s)" % (self.person, self.date)

    def get_absolute_url(self):
        return self.doc.url

    @property
    def titre(self):
        return self.title if self.title else os.path.basename(self.doc.path)


# Ancien modèle, conservé pour archivage uniquement
class AtelierPoste(PersonRelatedModel):
    EVAL_CHOICES = (
        ('NP', 'NP'),  # non pertinent
        ('0', '0'),  # aucun soutien
        ('1', '1'),  # incitation de départ et/ou contrôle final
        ('2', '2'),  # indication et supervision
        ('3', '3'),  # ?
        ('4', '4'),  # ?
    )
    debut_act = models.DateField("Début de l'activité", null=True, blank=True)

    caisses_grises = models.CharField("Caisses grises pour lettres", max_length=2, choices=EVAL_CHOICES, blank=True)
    petit_mat = models.CharField("Petits matériels éponge stylos", max_length=2, choices=EVAL_CHOICES, blank=True)
    controle_date = models.CharField("Contrôle date tampon de l'agence", max_length=2, choices=EVAL_CHOICES, blank=True)
    activer_scanner = models.CharField("Activer le scanner", max_length=2, choices=EVAL_CHOICES, blank=True)
    activer_pos = models.CharField("Activer le EFT POS", max_length=2, choices=EVAL_CHOICES, blank=True)
    prep_rem1 = models.TextField("Prép. avant ouverture et fermeture Remarques 1", blank=True)

    compter_caisse = models.CharField("Compter caisse et timbres courant", max_length=2, choices=EVAL_CHOICES, blank=True)
    compter_reserve = models.CharField("Compter la réserve", max_length=2, choices=EVAL_CHOICES, blank=True)
    compter_vignettes = models.CharField("Compter vignettes cartons", max_length=2, choices=EVAL_CHOICES, blank=True)
    prep_rem2 = models.TextField("Prép. avant ouverture et fermeture Remarques 2", blank=True)

    transcrire_caisse = models.CharField("Transcrire le rapport de caisse", max_length=2, choices=EVAL_CHOICES, blank=True)
    noter_avances = models.CharField("Noter les avances clients", max_length=2, choices=EVAL_CHOICES, blank=True)
    inscr_z1 = models.CharField("Inscrire le ticket Z1", max_length=2, choices=EVAL_CHOICES, blank=True)
    constat_erreur = models.CharField("Constater une erreur", max_length=2, choices=EVAL_CHOICES, blank=True)
    erreurs = models.CharField("Chercher les erreurs", max_length=2, choices=EVAL_CHOICES, blank=True)
    rech_postech = models.CharField("Recherches sur poste.ch", max_length=2, choices=EVAL_CHOICES, blank=True)
    travaux_ordi_rem1 = models.TextField("Travaux sur l’ordi Remarques 1", blank=True)

    scanner_recep = models.CharField("Scanner en réception", max_length=2, choices=EVAL_CHOICES, blank=True)
    inscr_colis = models.CharField("Inscrire nom et date sur le colis", max_length=2, choices=EVAL_CHOICES, blank=True)
    rang_colis = models.CharField("Rangement des colis", max_length=2, choices=EVAL_CHOICES, blank=True)
    travaux_ordi_rem2 = models.TextField("Travaux sur l’ordi Remarques 2", blank=True)

    trouver_lettres = models.CharField("Trouver les lettres à retourner", max_length=2, choices=EVAL_CHOICES, blank=True)
    trouver_colis = models.CharField("Trouver les colis à retourner", max_length=2, choices=EVAL_CHOICES, blank=True)
    tracer_numpostal = models.CharField("Tracer numéro postal", max_length=2, choices=EVAL_CHOICES, blank=True)
    coller_etiq_jaunes = models.CharField("Coller étiquette jaune retour", max_length=2, choices=EVAL_CHOICES, blank=True)
    inscr_retour = models.CharField("Inscription dans le carnet des retours", max_length=2, choices=EVAL_CHOICES, blank=True)
    scanner_retours = models.CharField("Scanner les retours", max_length=2, choices=EVAL_CHOICES, blank=True)
    travaux_ordi_rem3 = models.TextField("Travaux sur l’ordi Remarques 3", blank=True)

    inscr_rbt = models.CharField("Inscrire les rbt sur le carnet", max_length=2, choices=EVAL_CHOICES, blank=True)
    argent_pochette = models.CharField("Mettre l'argent dans la pochette", max_length=2, choices=EVAL_CHOICES, blank=True)
    effect_total = models.CharField("Effectuer le total", max_length=2, choices=EVAL_CHOICES, blank=True)
    travaux_ordi_rem4 = models.TextField("Travaux sur l’ordi Remarques 4", blank=True)

    lettre_ch = models.CharField("Lettres CH", max_length=2, choices=EVAL_CHOICES, blank=True)
    lettre_etranger = models.CharField("Lettres étranger", max_length=2, choices=EVAL_CHOICES, blank=True)
    maxilettre = models.CharField("Maxi lettres étranger", max_length=2, choices=EVAL_CHOICES, blank=True)
    colis_ch = models.CharField("Colis CH", max_length=2, choices=EVAL_CHOICES, blank=True)
    colis_etranger = models.CharField("Colis étranger", max_length=2, choices=EVAL_CHOICES, blank=True)
    express = models.CharField("Spéciaux express", max_length=2, choices=EVAL_CHOICES, blank=True)
    travaux_ordi_rem5 = models.TextField("Travaux sur l’ordi Remarques 5", blank=True)

    amabilite = models.CharField("Amabilité face à la clientèle", max_length=2, choices=EVAL_CHOICES, blank=True)
    parler_distinct = models.CharField("Parler distinctement au client", max_length=2, choices=EVAL_CHOICES, blank=True)
    comprendre = models.CharField("Comprendre la demande du client", max_length=2, choices=EVAL_CHOICES, blank=True)
    stress = models.CharField("Gérer le stress", max_length=2, choices=EVAL_CHOICES, blank=True)
    travaux_guich_rem1 = models.TextField("Travaux au guichet Remarques 1", blank=True)

    activer_pos2 = models.CharField("Activer le EFT POS2", max_length=2, choices=EVAL_CHOICES, blank=True)
    effect_retrait = models.CharField("Effectuer un retrait d'argent", max_length=2, choices=EVAL_CHOICES, blank=True)
    desactiver_pos = models.CharField("Désactiver le EFT POS", max_length=2, choices=EVAL_CHOICES, blank=True)
    travaux_guich_rem2 = models.TextField("Travaux au guichet Remarques 2", blank=True)

    allumer_caisse = models.CharField("Allumer la caisse", max_length=2, choices=EVAL_CHOICES, blank=True)
    effect_ops = models.CharField("Effectuer les opérations", max_length=2, choices=EVAL_CHOICES, blank=True)
    extraire_z1 = models.CharField("Extraire le Z1", max_length=2, choices=EVAL_CHOICES, blank=True)
    travaux_guich_rem3 = models.TextField("Travaux au guichet Remarques 3", blank=True)

    colis_sign = models.CharField("Colis signature", max_length=2, choices=EVAL_CHOICES, blank=True)
    colis_remb = models.CharField("Colis contre remboursement", max_length=2, choices=EVAL_CHOICES, blank=True)
    colis_propre = models.CharField("Colis à remettre en main propre", max_length=2, choices=EVAL_CHOICES, blank=True)
    dispobox = models.CharField("Dispobox", max_length=2, choices=EVAL_CHOICES, blank=True)
    travaux_guich_rem4 = models.TextField("Travaux au guichet Remarques 4", blank=True)

    lettre_recomm = models.CharField("Lettre recommandée", max_length=2, choices=EVAL_CHOICES, blank=True)
    lettre_reception = models.CharField("Lettre recommandée accusé de réception", max_length=2, choices=EVAL_CHOICES, blank=True)
    lettre_remb = models.CharField("Lettre contre remboursemen", max_length=2, choices=EVAL_CHOICES, blank=True)
    acte_jud = models.CharField("Acte judiciaire", max_length=2, choices=EVAL_CHOICES, blank=True)
    acte_poursuite = models.CharField("Acte de poursuite", max_length=2, choices=EVAL_CHOICES, blank=True)
    travaux_guich_rem5 = models.TextField("Travaux au guichet Remarques 5", blank=True)

    ticket_balance1 = models.CharField("Obtenir le bon ticket sur la balance", max_length=2, choices=EVAL_CHOICES, blank=True)
    coller_etiqu_colis = models.CharField("Coller correctement les étiquettes sur le colis", max_length=2, choices=EVAL_CHOICES, blank=True)
    remplir_conf = models.CharField("Remplir la confirmation de dépôt", max_length=2, choices=EVAL_CHOICES, blank=True)
    suppl_propre = models.CharField("Suppl. remise en main propre", max_length=2, choices=EVAL_CHOICES, blank=True)
    suppl_remb = models.CharField("Suppl. contre remboursement", max_length=2, choices=EVAL_CHOICES, blank=True)
    mil_cec = models.CharField("MIL CEC", max_length=2, choices=EVAL_CHOICES, blank=True)
    gas = models.CharField("GAS", max_length=2, choices=EVAL_CHOICES, blank=True)
    depot_rem1 = models.TextField("Dépot Remarques 1", blank=True)

    ticket_balance2 = models.CharField("Obtenir le bon ticket sur la balance", max_length=2, choices=EVAL_CHOICES, blank=True)
    coller_etiq = models.CharField("Coller correctement les étiquettes", max_length=2, choices=EVAL_CHOICES, blank=True)
    remplir_conf_r = models.CharField("Remplir la confirmation de dépôt si R", max_length=2, choices=EVAL_CHOICES, blank=True)
    r_accrecep = models.CharField("R avec accusé de réception", max_length=2, choices=EVAL_CHOICES, blank=True)
    r_rembours = models.CharField("R avec contre remboursement", max_length=2, choices=EVAL_CHOICES, blank=True)
    r_propre = models.CharField("R avec remise en main propre", max_length=2, choices=EVAL_CHOICES, blank=True)
    mil_aveugle = models.CharField("Militaire aveugle", max_length=2, choices=EVAL_CHOICES, blank=True)
    lettre_pp = models.CharField("Lettres PP", max_length=2, choices=EVAL_CHOICES, blank=True)
    rec_310 = models.CharField("Recommandé avec formulaire 310", max_length=2, choices=EVAL_CHOICES, blank=True)
    lettre_form = models.CharField("Lettres avec formulaire à coller sur couvercle", max_length=2, choices=EVAL_CHOICES, blank=True)
    depot_rem2 = models.TextField("Dépot Remarques 2", blank=True)

    ticket_balance3 = models.CharField("Obtenir le bon ticket sur la balance", max_length=2, choices=EVAL_CHOICES, blank=True)
    rempl_voiture = models.CharField("Remplir la lettre de voiture", max_length=2, choices=EVAL_CHOICES, blank=True)
    depot_rem3 = models.TextField("Dépot Remarques 3", blank=True)

    prio_eco = models.CharField("Prio ou Eco", max_length=2, choices=EVAL_CHOICES, blank=True)
    suppl_recomm = models.CharField("Suppl. recommandé", max_length=2, choices=EVAL_CHOICES, blank=True)
    r_accrecep1 = models.CharField("R avec accusé de réception", max_length=2, choices=EVAL_CHOICES, blank=True)
    r_contre_remb = models.CharField("R et contre remboursement", max_length=2, choices=EVAL_CHOICES, blank=True)
    r_remise_propre = models.CharField("R et à remettre en mains propre", max_length=2, choices=EVAL_CHOICES, blank=True)
    depot_rem4 = models.TextField("Dépot Remarques 4", blank=True)

    nb_ctrl_heure1 = models.SmallIntegerField("Nombre d'aide ou contrôle par heure 1", null=True, blank=True)
    nb_ctrl_heure2 = models.SmallIntegerField("Nombre d'aide ou contrôle par heure 2", null=True, blank=True)
    indep_rem = models.TextField("Indépendance Remarques", blank=True)

    _view_name_base = 'at_poste'

    class Meta:
        verbose_name = 'Atelier Poste'
        verbose_name_plural = 'Ateliers Poste'

    def __str__(self):
        return "Atelier Poste pour %s (%s)" % (self.person, self.date)

    @property
    def total_items(self):
        return sum([
            int(getattr(self, f.name))
            for f in self._meta.get_fields()
            if f.max_length == 2 and getattr(self, f.name, '').isdigit()
        ])
