from datetime import datetime

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from person.tests import ModelBaseTextMixin
from .models import (
    ActionAtelier, AtelierARIHANEDoc, AtelierCommunication, AtelierCrea1,
    AtelierCrea2, AtelierInfo, AtelierPosteDoc, BilanAtelier, ObservationAtelier,
)


class ActionAtelierTests(ModelBaseTextMixin, TestCase):
    ModelClass = ActionAtelier
    model_id = 'actionatelier'
    custom_data = {'problemes': "Des problèmes…"}

    def test_add_action_evaluation(self):
        act = ActionAtelier.objects.create(
            person=self.person, problemes="Des problèmes…", objectifs="Des objectifs…"
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('actionatelier-detail', args=[self.person.pk, act.pk]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        addeval_url = reverse('actionatelier-addeval', args=[self.person.pk, act.pk])
        addaction_url = reverse('actionatelier-addaction', args=[self.person.pk, act.pk])
        self.assertContains(
            response,
            '<img class="icon-standalone" src="/static/img/add.svg" '
            'data-url="%s" onclick="addDynamicRow(this, \'evaluations\')" '
            'title="Ajouter une évaluation">' % addeval_url,
            html=True
        )
        self.assertContains(
            response,
            '<img class="icon-standalone" src="/static/img/add.svg" '
            'data-url="%s" onclick="addDynamicRow(this, \'actions\')" '
            'title="Ajouter une action">' % addaction_url,
            html=True
        )
        response = self.client.post(addeval_url, data={'texte': 'Une évaluation'})
        self.assertEqual(act.evaluations.first().texte, 'Une évaluation')
        response = self.client.post(addaction_url, data={'texte': 'Une action'})
        self.assertEqual(act.actions.first().texte, 'Une action')


class AtelierARIHANETests(ModelBaseTextMixin, TestCase):
    ModelClass = AtelierARIHANEDoc
    model_id = 'at_arihane'

    @property
    def custom_data(self):
        return {
            'title': "Un document",
            'doc': SimpleUploadedFile('face.txt', b'ahskdhj'),
            'date_doc': "2018-11-11",
        }

    def test_model_list(self):
        self.client.force_login(self.user)
        create_kwargs = {**self.custom_data, 'person': self.person, 'date': timezone.now()}
        new_obj = self.ModelClass.objects.create(**create_kwargs)
        response = self.client.get(
            reverse('%s-list' % self.model_id, args=[self.person.pk]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        # User has no special permissions, should not be able to add element.
        self.assertNotIn('new_form_url', response.context)
        self.assertContains(
            response,
            '<a href="%s">Un document</a>' % new_obj.doc.url
        )


class AtelierCommunicationTests(ModelBaseTextMixin, TestCase):
    ModelClass = AtelierCommunication
    model_id = 'at_comm'
    custom_data = {'horaire': "De 12 à 14h"}


class AtelierCrea1Tests(ModelBaseTextMixin, TestCase):
    ModelClass = AtelierCrea1
    model_id = 'at_crea1'
    custom_data = {'horaire': "De 12 à 14h"}


class AtelierCrea2Tests(ModelBaseTextMixin, TestCase):
    ModelClass = AtelierCrea2
    model_id = 'at_crea2'
    custom_data = {'horaire': "De 12 à 14h"}


class AtelierInfoTests(ModelBaseTextMixin, TestCase):
    ModelClass = AtelierInfo
    model_id = 'at_info'
    custom_data = {'horaire': "De 12 à 14h"}


class AtelierPosteTests(AtelierARIHANETests):
    ModelClass = AtelierPosteDoc
    model_id = 'at_poste'


class BilanAtelierTests(ModelBaseTextMixin, TestCase):
    ModelClass = BilanAtelier
    model_id = 'bilanatelier'
    custom_data = {'objectifs': "Viser plus haut", 'libelle': 'bilan'}

    def test_bilan_list_ordering(self):
        """ date_bilan has priority over date when ordered in a list."""
        BilanAtelier.objects.bulk_create([
            BilanAtelier(person=self.person, date=timezone.make_aware(datetime(2019, 3, 1)), date_bilan='2019-03-30'),
            BilanAtelier(person=self.person, date=timezone.make_aware(datetime(2019, 3, 2)), date_bilan='2019-02-25'),
            BilanAtelier(person=self.person, date=timezone.make_aware(datetime(2019, 3, 3)), date_bilan='2019-01-01'),
        ])
        self.client.force_login(self.user)
        list_url = reverse('bilanatelier-list', args=[self.person.pk])
        response = self.client.get(list_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertQuerysetEqual(
            response.context['object_list'],
            ['<BilanAtelier: Bilan atelier du 2019-03-30 pour Smith John>',
             '<BilanAtelier: Bilan atelier du 2019-02-25 pour Smith John>',
             '<BilanAtelier: Bilan atelier du 2019-01-01 pour Smith John>'],
             transform=repr
        )

    def test_bilan_new_upload(self):
        self.client.force_login(self.user)
        url = reverse('bilanatelier-new', args=[self.person.pk])
        response = self.client.post(url, data={
            'person': self.person.pk,
            'libelle': 'bilan',
            'commentaire': 'Avec pièce jointe',
            'attachments-TOTAL_FORMS': 1,
            'attachments-INITIAL_FORMS': 0,
            'attachments-0-fichier': SimpleUploadedFile('face.txt', b'ahskdhj'),
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        bilan = BilanAtelier.objects.get(libelle='bilan')
        self.assertRedirects(response, reverse('bilanatelier-detail', args=[self.person.pk, bilan.pk]))
        self.assertEqual(bilan.createur, self.user)
        self.assertEqual(bilan.fichiers.count(), 1)

    def test_bilan_edit_upload(self):
        bilan = BilanAtelier.objects.create(createur=self.user, person=self.person, libelle='bilan')
        self.client.force_login(self.user)
        url = reverse('bilanatelier-edit', args=[self.person.pk, bilan.pk])
        response = self.client.post(url, data={
            'person': self.person.pk,
            'libelle': 'bilan',
            'commentaire': 'Avec pièce jointe',
            'attachments-TOTAL_FORMS': 1,
            'attachments-INITIAL_FORMS': 0,
            'attachments-0-fichier': SimpleUploadedFile('face.txt', b'ahskdhj'),
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertRedirects(response, reverse('bilanatelier-detail', args=[self.person.pk, bilan.pk]))
        bilan.refresh_from_db()
        self.assertEqual(bilan.createur, self.user)


class ObservationAtelierTests(ModelBaseTextMixin, TestCase):
    ModelClass = ObservationAtelier
    model_id = 'obsatelier'
    custom_data = {'atelier': "crea1", 'observation': "Une obs"}
