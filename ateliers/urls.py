from django.urls import path

from person.views import ActionAddItemView, ActionCloseView
from . import views

urlpatterns = [
    path('action/', views.ActionAtelierListView.as_view(), name='actionatelier-list'),
    path('action/<int:obj_pk>/', views.ActionAtelierDetailView.as_view(), name='actionatelier-detail'),
    path('action/<int:obj_pk>/edit/', views.ActionAtelierEditView.as_view(), name='actionatelier-edit'),
    path('action/new/', views.ActionAtelierEditView.as_view(), name='actionatelier-new'),
    path('action/<int:obj_pk>/addeval/', ActionAddItemView.as_view(_model='ateliers.ActionEval'),
        name='actionatelier-addeval'),
    path('action/<int:obj_pk>/addaction/', ActionAddItemView.as_view(_model='ateliers.ActionAction'),
        name='actionatelier-addaction'),
    path('action/<int:obj_pk>/close/', ActionCloseView.as_view(_model='ateliers.ActionAtelier'),
        name='actionatelier-closeaction'),

    path('bilans/', views.BilanAtelierListView.as_view(), name='bilanatelier-list'),
    path('bilans/<int:obj_pk>/', views.BilanAtelierDetailView.as_view(), name='bilanatelier-detail'),
    path('bilans/<int:obj_pk>/edit/', views.BilanAtelierEditView.as_view(), name='bilanatelier-edit'),
    path('bilans/new/', views.BilanAtelierEditView.as_view(), name='bilanatelier-new'),

    path('obsatelier/', views.ObservationAtelierListView.as_view(), name='obsatelier-list'),
    path('obsatelier/<int:obj_pk>/', views.ObservationAtelierDetailView.as_view(), name='obsatelier-detail'),
    path('obsatelier/<int:obj_pk>/edit/', views.ObservationAtelierEditView.as_view(), name='obsatelier-edit'),
    path('obsatelier/new/', views.ObservationAtelierEditView.as_view(), name='obsatelier-new'),

    path('arihane/', views.AtelierARIHANEListView.as_view(), name='at_arihane-list'),
    path('arihane/<int:obj_pk>/', views.AtelierARIHANEDetailView.as_view(), name='at_arihane-detail'),
    path('arihane/<int:obj_pk>/edit/', views.AtelierARIHANEEditView.as_view(), name='at_arihane-edit'),
    path('arihane/new/', views.AtelierARIHANEEditView.as_view(), name='at_arihane-new'),
    path('arihane/documents/<int:pk_doc>/delete/', views.AtelierARIHANEDeleteView.as_view(),
         name='at_arihane-delete'),

    path('comm/', views.AtelierCommListView.as_view(), name='at_comm-list'),
    path('comm/<int:obj_pk>/', views.AtelierCommDetailView.as_view(), name='at_comm-detail'),
    path('comm/<int:obj_pk>/edit/', views.AtelierCommEditView.as_view(), name='at_comm-edit'),
    path('comm/new/', views.AtelierCommEditView.as_view(), name='at_comm-new'),

    path('crea1/', views.AtelierCrea1ListView.as_view(), name='at_crea1-list'),
    path('crea1/<int:obj_pk>/', views.AtelierCrea1DetailView.as_view(), name='at_crea1-detail'),
    path('crea1/<int:obj_pk>/edit/', views.AtelierCrea1EditView.as_view(), name='at_crea1-edit'),
    path('crea1/<int:obj_pk>/duplicate/', views.AtelierCrea1EditView.as_view(mode='duplicate'), name='at_crea1-dup'),
    path('crea1/new/', views.AtelierCrea1EditView.as_view(), name='at_crea1-new'),

    path('crea2/', views.AtelierCrea2ListView.as_view(), name='at_crea2-list'),
    path('crea2/<int:obj_pk>/', views.AtelierCrea2DetailView.as_view(), name='at_crea2-detail'),
    path('crea2/<int:obj_pk>/edit/', views.AtelierCrea2EditView.as_view(), name='at_crea2-edit'),
    path('crea2/<int:obj_pk>/duplicate/', views.AtelierCrea2EditView.as_view(mode='duplicate'), name='at_crea2-dup'),
    path('crea2/new/', views.AtelierCrea2EditView.as_view(), name='at_crea2-new'),

    path('cuisine/', views.AtelierCuisineListView.as_view(), name='at_cuisine-list'),
    path('cuisine/<int:obj_pk>/', views.AtelierCuisineDetailView.as_view(), name='at_cuisine-detail'),
    path('cuisine/<int:obj_pk>/edit/', views.AtelierCuisineEditView.as_view(), name='at_cuisine-edit'),
    path('cuisine/<int:obj_pk>/duplicate/', views.AtelierCuisineEditView.as_view(mode='duplicate'), name='at_cuisine-dup'),
    path('cuisine/new/', views.AtelierCuisineEditView.as_view(), name='at_cuisine-new'),

    path('info/', views.AtelierInfoListView.as_view(), name='at_info-list'),
    path('info/<int:obj_pk>/', views.AtelierInfoDetailView.as_view(), name='at_info-detail'),
    path('info/<int:obj_pk>/edit/', views.AtelierInfoEditView.as_view(), name='at_info-edit'),
    path('info/<int:obj_pk>/duplicate/', views.AtelierInfoEditView.as_view(mode='duplicate'), name='at_info-dup'),
    path('info/new/', views.AtelierInfoEditView.as_view(), name='at_info-new'),

    path('poste/', views.AtelierPosteListView.as_view(), name='at_poste-list'),
    path('poste/<int:obj_pk>/', views.AtelierPosteDetailView.as_view(), name='at_poste-detail'),
    path('poste/<int:obj_pk>/edit/', views.AtelierPosteEditView.as_view(), name='at_poste-edit'),
    path('poste/new/', views.AtelierPosteEditView.as_view(), name='at_poste-new'),
    path('poste/documents/<int:pk_doc>/delete/', views.AtelierPosteDeleteView.as_view(),
         name='at_poste-delete'),
]
