from django import forms
from django.db import transaction
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import View

from person.models import Modification
from person.views import (
    BaseActionEditView, BaseActionDetailView, BaseDetailView, BaseListView, BaseEditView
)

from .forms import AtelierARIHANEForm, BilanAtelierForm
from .models import (
    ActionAtelier, BilanAtelier, ObservationAtelier, AtelierARIHANEDoc,
    AtelierCommunication, AtelierCrea1, AtelierCrea2, AtelierCuisine,
    AtelierInfo, AtelierPosteDoc
)


class ActionAtelierBaseView:
    model = ActionAtelier
    action_title = 'Actions ateliers'


class ActionAtelierListView(ActionAtelierBaseView, BaseListView):
    new_form_label = 'Créer une nouvelle action'


class ActionAtelierDetailView(ActionAtelierBaseView, BaseActionDetailView):
    pass


class ActionAtelierEditView(ActionAtelierBaseView, BaseActionEditView):
    template_name = 'base-action.html'


class BilanAtelierBaseView:
    model = BilanAtelier


class BilanAtelierListView(BilanAtelierBaseView, BaseListView):
    new_form_label = 'Créer un nouveau bilan'


class BilanAtelierDetailView(BilanAtelierBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'ateliers/bilanatelier-detail.html'


class BilanAtelierEditView(BilanAtelierBaseView, BaseEditView):
    template_name = 'ateliers/bilanatelier-detail.html'
    form_class = BilanAtelierForm

    def get_initial(self):
        initial = super().get_initial()
        if self.is_create:
            try:
                initial['rappel_objectifs'] = BilanAtelier.objects.filter(
                    person__pk=self.kwargs['pk']).latest().objectifs
            except BilanAtelier.DoesNotExist:
                pass
        return initial


class ObservationAtelierBaseView:
    model = ObservationAtelier


class ObservationAtelierListView(ObservationAtelierBaseView, BaseListView):
    new_form_label = 'Créer une nouvelle observation'
    search_fields = ['atelier', 'observation']


class ObservationAtelierDetailView(ObservationAtelierBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class ObservationAtelierEditView(ObservationAtelierBaseView, BaseEditView):
    return_to_list = True


class RadioForChoicesMixin:
    """Mixin to force fields with choices to have a RadioSelect widget."""
    def get_form_class(self):
        widgets = {'person': forms.HiddenInput, 'createur': forms.HiddenInput}
        for field in self.model._meta.get_fields():
            if field.choices:
                widgets[field.name] = forms.RadioSelect
        return forms.modelform_factory(self.model, exclude=BaseEditView.exclude, widgets=widgets)


class AtelierDuplicateMixin:
    mode = 'edition'

    @property
    def is_create(self):
        return True if self.mode == 'duplicate' else super().is_create

    def get_object(self):
        if self.mode == 'duplicate':
            obj = self.get_queryset().get(pk=self.kwargs.get(self.pk_url_kwarg))
            obj.pk = None
            obj.person = None
            obj.date = None
            obj.createur = None
            obj.visa = ''
            return obj
        return super().get_object()


class AtelierARIHANEBaseView:
    model = AtelierARIHANEDoc


class AtelierARIHANEListView(AtelierARIHANEBaseView, BaseListView):
    new_form_label = 'Ajouter document d’atelier ARIHANE'
    template_name = 'ateliers/document-list.html'


class AtelierARIHANEDetailView(AtelierARIHANEBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class AtelierARIHANEEditView(AtelierARIHANEBaseView, BaseEditView):
    pass


class AtelierARIHANEDeleteView(View):
    def post(self, request, *args, **kwargs):
        doc = get_object_or_404(AtelierARIHANEDoc, pk=self.kwargs['pk_doc'])
        with transaction.atomic():
            pers = doc.person
            msg = "Suppression d’un document d’atelier ARIHANE"
            doc.doc.delete(save=False)
            doc.delete()
            Modification.save_modification(pers, self.request.user, created=False, msg=msg)
        return JsonResponse({'results': 'OK'})


class AtelierCommBaseView:
    model = AtelierCommunication


class AtelierCommListView(AtelierCommBaseView, BaseListView):
    new_form_label = 'Créer nouvelle fiche atelier communication'


class AtelierCommDetailView(AtelierCommBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'ateliers/comm.html'


class AtelierCommEditView(AtelierCommBaseView, BaseEditView):
    template_name = 'ateliers/comm.html'


class AtelierCrea1BaseView:
    model = AtelierCrea1


class AtelierCrea1ListView(AtelierCrea1BaseView, BaseListView):
    new_form_label = 'Créer nouvelle fiche atelier'


class AtelierCrea1DetailView(AtelierCrea1BaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'ateliers/crea1.html'


class AtelierCrea1EditView(RadioForChoicesMixin, AtelierDuplicateMixin, AtelierCrea1BaseView, BaseEditView):
    template_name = 'ateliers/crea1.html'


class AtelierCrea2BaseView:
    model = AtelierCrea2


class AtelierCrea2ListView(AtelierCrea2BaseView, BaseListView):
    new_form_label = 'Créer nouvelle fiche atelier'


class AtelierCrea2DetailView(AtelierCrea2BaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'ateliers/crea2.html'


class AtelierCrea2EditView(AtelierDuplicateMixin, AtelierCrea2BaseView, BaseEditView):
    template_name = 'ateliers/crea2.html'


class AtelierCuisineBaseView:
    model = AtelierCuisine


class AtelierCuisineListView(AtelierCuisineBaseView, BaseListView):
    new_form_label = 'Créer nouvelle fiche atelier'


class AtelierCuisineDetailView(AtelierCuisineBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'ateliers/cuisine.html'


class AtelierCuisineEditView(RadioForChoicesMixin, AtelierDuplicateMixin, AtelierCuisineBaseView, BaseEditView):
    template_name = 'ateliers/cuisine.html'


class AtelierInfoBaseView:
    model = AtelierInfo


class AtelierInfoListView(AtelierInfoBaseView, BaseListView):
    new_form_label = 'Créer nouvelle fiche atelier'


class AtelierInfoDetailView(AtelierInfoBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'ateliers/info.html'


class AtelierInfoEditView(RadioForChoicesMixin, AtelierDuplicateMixin, AtelierInfoBaseView, BaseEditView):
    template_name = 'ateliers/info.html'


class AtelierPosteBaseView:
    model = AtelierPosteDoc


class AtelierPosteListView(AtelierPosteBaseView, BaseListView):
    new_form_label = 'Ajouter document d’atelier Poste'
    template_name = 'ateliers/document-list.html'


class AtelierPosteDetailView(AtelierPosteBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class AtelierPosteEditView(AtelierPosteBaseView, BaseEditView):
    pass


class AtelierPosteDeleteView(View):
    def post(self, request, *args, **kwargs):
        doc = get_object_or_404(AtelierPosteDoc, pk=self.kwargs['pk_doc'])
        with transaction.atomic():
            pers = doc.person
            msg = "Suppression d’un document d’atelier Poste"
            doc.doc.delete(save=False)
            doc.delete()
            Modification.save_modification(pers, self.request.user, created=False, msg=msg)
        return JsonResponse({'results': 'OK'})
