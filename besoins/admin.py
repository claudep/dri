from django.contrib import admin

from .models import RecueilBesoins, BesoinsAlimentaires, Impotence

admin.site.register(RecueilBesoins)
admin.site.register(BesoinsAlimentaires)
admin.site.register(Impotence)
