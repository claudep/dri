import itertools
from django import forms

from person.forms import UtilsMixin
from .models import BesoinsAlimentaires, Impotence, RecueilBesoins


class NullBooleanRadio(forms.NullBooleanSelect):
    input_type = 'radio'
    template_name = 'django/forms/widgets/radio.html'
    option_template_name = 'django/forms/widgets/radio_option.html'
    checked_attribute = {'checked': True}

    def __init__(self, attrs=None):
        choices = (
            ('unknown', '?'),
            ('true', 'Oui'),
            ('false', 'Non'),
        )
        super(forms.NullBooleanSelect, self).__init__(attrs, choices)


class BesoinsFormBase(UtilsMixin, forms.ModelForm):
    class Meta:
        model = RecueilBesoins
        fields = '__all__'

    impotence_fields = ()

    def __init__(self, *args, impotence=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.impotence_form = None
        if self.impotence_fields:
            fields = itertools.chain(
                *[[base, '%s_quand' % base, '%s_aide' % base, '%s_precisions' % base] for base in self.impotence_fields]
            )
            ImpotenceForm = forms.modelform_factory(Impotence, fields=list(fields))
            kwargs.pop('instance')
            self.impotence_form = ImpotenceForm(instance=impotence, **kwargs)

    def is_valid(self):
        if self.impotence_form:
            return all([super().is_valid(), self.impotence_form.is_valid()])
        else:
            return super().is_valid()

    def has_changed(self):
        return any([
            super().has_changed(),
            self.impotence_form.has_changed() if self.impotence_form else False
        ])

    def save(self, **kwargs):
        if self.impotence_form:
            self.impotence_form.save(**kwargs)
        return super().save(**kwargs)


class SecuriteForm(BesoinsFormBase):
    class Meta(BesoinsFormBase.Meta):
        fields = [
            'securite_risque_chute', 'securite_risque_chute_cause',
            'securite_risque_errance', 'securite_risque_errance_cause',
            'securite_risque_agr', 'securite_risque_agr_descr', 'securite_mrsa_resist',
            'securite_commentaires', 'securite_contentions',
        ]


class CommuniquerForm(BesoinsFormBase):
    class Meta(BesoinsFormBase.Meta):
        fields = [
            'communiquer_general', 'communiquer_ouie', 'communiquer_vue',
            'communiquer_langage', 'collaboration', 'collaboration_commentaires',
            'comportement', 'comportement_commentaires', 'communication',
            'communication_commentaires', 'communiquer_commentaires',
            # communiquer_besoin_aide ?
        ]
    impotence_fields = ['depl_contact']


class RespirerForm(BesoinsFormBase):
    class Meta(BesoinsFormBase.Meta):
        fields = [
            'respirer_tabagisme', 'respirer_tabagisme_comm',
            'respirer_dyspnee', 'respirer_dyspnee_comm',
            'respirer_bronches', 'respirer_bronches_comm',
            'respirer_toux', 'respirer_toux_comm', 'respirer_commentaires',
            'respirer_aerosol', 'respirer_aerosol_comm',
            'respirer_oxygene', 'respirer_oxygene_comm',
        ]


class HygieneForm(BesoinsFormBase):
    class Meta(BesoinsFormBase.Meta):
        fields = [
            'hygiene_toilette', 'hygiene_bain_douche', 'hygiene_cheveux', 'hygiene_dents',
            'hygiene_coiffer', 'hygiene_divers', 'hygiene_peau',
        ]
    impotence_fields = ['baigner', 'laver', 'peigner', 'raser']


class VetirForm(BesoinsFormBase):
    class Meta(BesoinsFormBase.Meta):
        fields = [
            'vetir_habillage', 'vetir_deshabillage', 'vetir_vetements', 'vetir_fermetures',
            'vetir_temperature', 'vetir_aerer', 'vetir_divers',
        ]


class NourrirForm(BesoinsFormBase):
    class Meta(BesoinsFormBase.Meta):
        model = BesoinsAlimentaires
        exclude = ['person', 'date', 'createur', 'visa']

    impotence_fields = ['manger_lit', 'couper_alim', 'porter_bouche', 'alim_spec']


class EliminerForm(BesoinsFormBase):
    class Meta(BesoinsFormBase.Meta):
        fields = [
            'eliminer_incontinence', 'eliminer_transferts', 'eliminer_moyens',
            'eliminer_medication', 'eliminer_protections', 'eliminer_divers'
        ]

    impotence_fields = ['wc_ordrehab', 'wc_propr', 'wc_habituel']


class MouvoirForm(BesoinsFormBase):
    class Meta(BesoinsFormBase.Meta):
        fields = [
            'motricite_deplacements', 'motricite_probleme', 'motricite_probleme_prec',
            'motricite_mobiliser', 'motricite_transferts',
        ]

    impotence_fields = ['depl_maison', 'depl_ext']


class ReposerForm(BesoinsFormBase):
    class Meta(BesoinsFormBase.Meta):
        fields = [
            'reposer_sieste', 'reposer_lit', 'reposer_matelas', 'reposer_divers',
        ]
    impotence_fields = ['lever']


class OccuperForm(BesoinsFormBase):
    class Meta(BesoinsFormBase.Meta):
        fields = [
            'soccuper_travail_ext', 'soccuper_ateliers_foyer', 'soccuper_loisirs',
            'soccuper_vacances', 'soccuper_camp_foyer', 'soccuper_animations_foyer',
            'soccuper_divers',
        ]


class ApprendreForm(BesoinsFormBase):
    class Meta(BesoinsFormBase.Meta):
        fields = [
            'appr_traitement_medic', 'appr_traitement_maintenir', 'appr_traitement_controle',
            'appr_poids', 'appr_soin', 'appr_un_traitement', 'appr_maladie', 'appr_traitement',
            'appr_soigner', 'appr_divers', 'appr_mesures_prev',
        ]
        widgets = {
            'appr_traitement_medic': NullBooleanRadio,
            'appr_traitement_maintenir': NullBooleanRadio,
            'appr_traitement_controle': NullBooleanRadio,
            'appr_poids': NullBooleanRadio,
            'appr_soin': NullBooleanRadio,
            'appr_un_traitement': NullBooleanRadio,
            'appr_maladie': NullBooleanRadio,
            'appr_traitement': NullBooleanRadio,
            'appr_soigner': NullBooleanRadio,
        }


class CroyancesForm(BesoinsFormBase):
    class Meta(BesoinsFormBase.Meta):
        fields = [
            'croyances_habitudes', 'croyances_accompagnement',
            'croyances_directives', 'croyances_representant', 'croyances_divers',
        ]


class SexualiteForm(BesoinsFormBase):
    class Meta(BesoinsFormBase.Meta):
        fields = ['sexualite_attentes']
