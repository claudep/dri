# Generated by Django 2.1.1 on 2018-10-27 14:50

import common.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('person', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BesoinsAlimentaires',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(blank=True, null=True, verbose_name='Date')),
                ('visa', models.CharField(blank=True, max_length=25, verbose_name='Visa')),
                ('accompagnements', models.CharField(blank=True, max_length=50, verbose_name='Accompagnements')),
                ('aliments', models.CharField(blank=True, max_length=50, verbose_name='Aliments')),
                ('allergies', models.CharField(blank=True, max_length=150, verbose_name='Allergies alimentaires')),
                ('boissons', models.CharField(blank=True, max_length=50, verbose_name='Boissons')),
                ('desirs', models.CharField(blank=True, max_length=100, verbose_name='Désirs particuliers')),
                ('diagnostic', models.CharField(blank=True, max_length=50, verbose_name='Diagnostic')),
                ('divers', models.TextField(blank=True, verbose_name='Divers')),
                ('mat_grasses', models.CharField(blank=True, max_length=20, verbose_name='Matières grasses')),
                ('naimepas', models.CharField(blank=True, max_length=150, verbose_name="N'aime pas")),
                ('portions', models.CharField(blank=True, max_length=20, verbose_name='Portions')),
                ('problemes', models.CharField(blank=True, max_length=120, verbose_name='Problèmes')),
                ('complements', models.CharField(blank=True, max_length=100, verbose_name='Compléments alimentaires')),
                ('regimes', models.CharField(blank=True, max_length=50, verbose_name='Régimes')),
                ('createur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
                ('person', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='person.Person')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Impotence',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('wc_habituel', models.BooleanField(default=False, verbose_name='Aller aux toilettes de manière habituelle')),
                ('wc_habituel_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('wc_habituel_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('wc_habituel_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('wc_ordrehab', models.BooleanField(default=False, verbose_name='Mettre en ordre les habits')),
                ('wc_ordrehab_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('wc_ordrehab_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('wc_ordrehab_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('wc_propr', models.BooleanField(default=False, verbose_name='Laver le corps - contrôler la propreté')),
                ('wc_propr_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('wc_propr_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('wc_propr_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('alim_spec', models.BooleanField(default=False, verbose_name='Ne peut manger que des aliments spéciaux')),
                ('alim_spec_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('alim_spec_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('alim_spec_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('manger_lit', models.BooleanField(default=False, verbose_name='Apporter les aliments au lit')),
                ('manger_lit_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('manger_lit_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('manger_lit_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('couper_alim', models.BooleanField(default=False, verbose_name='Couper les aliments')),
                ('couper_alim_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('couper_alim_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('couper_alim_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('porter_bouche', models.BooleanField(default=False, verbose_name='Porter les aliments à la bouche')),
                ('porter_bouche_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('porter_bouche_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('porter_bouche_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('depl_maison', models.BooleanField(default=False, verbose_name='Se déplacer dans la maison')),
                ('depl_maison_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('depl_maison_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('depl_maison_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('depl_ext', models.BooleanField(default=False, verbose_name="Se déplacer à l'extérieur")),
                ('depl_ext_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('depl_ext_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('depl_ext_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('lever', models.BooleanField(default=False, verbose_name="Se lever/S'asseoir/Se coucher")),
                ('lever_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('lever_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('lever_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('baigner', models.BooleanField(default=False, verbose_name='Se baigner/se doucher')),
                ('baigner_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('baigner_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('baigner_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('laver', models.BooleanField(default=False, verbose_name='Se laver')),
                ('laver_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('laver_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('laver_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('peigner', models.BooleanField(default=False, verbose_name='Se peigner')),
                ('peigner_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('peigner_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('peigner_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('raser', models.BooleanField(default=False, verbose_name='Se raser')),
                ('raser_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('raser_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('raser_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('vetir', models.BooleanField(default=False, verbose_name='')),
                ('vetir_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('vetir_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('vetir_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('depl_contact', models.BooleanField(default=False, verbose_name="Établir des contact avec l'entourage")),
                ('depl_contact_quand', models.CharField(blank=True, max_length=20, verbose_name='Depuis quand')),
                ('depl_contact_aide', models.CharField(blank=True, max_length=40, verbose_name="Genre d'aide apportée")),
                ('depl_contact_precisions', models.CharField(blank=True, max_length=200, verbose_name='Précisions')),
                ('person', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='person.Person')),
            ],
        ),
        migrations.CreateModel(
            name='RecueilBesoins',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('alim_allergies', models.BooleanField(default=False, verbose_name='Allergies')),
                ('alim_allergies_causes', models.TextField(blank=True, verbose_name='Causes allergies')),
                ('alim_aide', models.TextField(blank=True, verbose_name='Aide à l’alimentation')),
                ('alim_aide_prothese', models.CharField(blank=True, max_length=30, verbose_name='Prothèse')),
                ('alim_aide_hydr', models.TextField(blank=True, verbose_name='Aide à l’hydratation')),
                ('alim_habit_partic', models.TextField(blank=True, verbose_name='Habitudes, particularités')),
                ('alim_moyens_aux', models.TextField(blank=True, verbose_name='Moyens auxiliaires')),
                ('appr_soigner', models.BooleanField(blank=True, null=True, verbose_name='Apprendre à se soigner')),
                ('appr_traitement_medic', models.BooleanField(blank=True, null=True, verbose_name='Médicamenteux')),
                ('appr_traitement_maintenir', models.BooleanField(blank=True, null=True, verbose_name='Maintenir le traitement')),
                ('appr_traitement_controle', models.BooleanField(blank=True, null=True, verbose_name='Contrôler le traitement')),
                ('appr_divers', models.TextField(blank=True, verbose_name='Divers')),
                ('appr_mesures_prev', models.TextField(blank=True, verbose_name='Mesures de prévention')),
                ('appr_maladie', models.BooleanField(blank=True, null=True, verbose_name='Apprendre sa maladie')),
                ('appr_poids', models.BooleanField(blank=True, null=True, verbose_name='Apprendre son poids')),
                ('appr_traitement', models.BooleanField(blank=True, null=True, verbose_name='Apprendre son traitement')),
                ('appr_soin', models.BooleanField(blank=True, null=True, verbose_name='Apprendre un soin')),
                ('appr_un_traitement', models.BooleanField(blank=True, null=True, verbose_name='Apprendre un traitement')),
                ('apprendre_maj', models.DateField(blank=True, null=True, verbose_name='Date mise à jour')),
                ('apprendre_visa', models.CharField(blank=True, max_length=20, verbose_name='Visa')),
                ('communiquer_general', models.TextField(blank=True, verbose_name='Communiquer, général')),
                ('communiquer_ouie', models.TextField(blank=True, verbose_name='Ouïe')),
                ('communiquer_vue', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('alteration', 'Altération de la vue'), ('lunettes', 'Lunettes')], max_length=12), blank=True, null=True, size=None, verbose_name='Vue')),
                ('communiquer_langage', models.TextField(blank=True, verbose_name='Langage')),
                ('communiquer_besoin_aide', models.TextField(blank=True, verbose_name='?Besoin aide?')),
                ('collaboration', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('spontanee', 'Spontanée'), ('stimulation', 'Besoin de stimulation'), ('passivite', 'Passivité'), ('reconnaitre', 'Reconnaître les personnes'), ('opposition', "Attitude d'opposition"), ('refus', 'Refus modifier comportements préjudiciables')], max_length=12), blank=True, null=True, size=None, verbose_name='Collaboration')),
                ('collaboration_commentaires', models.TextField(blank=True, verbose_name='Commentaires')),
                ('comportement', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('anxiete', 'Anxiété'), ('peur', 'Peur'), ('colere', 'Colère'), ('tristesse', 'Tristesse'), ('repli', 'Repli sur soi'), ('depressif', 'Etat dépressif'), ('aggressif', 'Agressivité'), ('affabul', 'Affabulation'), ('hallucin', 'Hallucinations'), ('delire', 'Délire'), ('perte', "Perte d'intérêt"), ('plusenvie', 'Plus envie de vivre')], max_length=12), blank=True, null=True, size=None, verbose_name='Comportement')),
                ('comportement_commentaires', models.TextField(blank=True, verbose_name='Commentaires')),
                ('communication', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('aisement', 'Spontanément, aisément'), ('peu', 'Peu, mais demande le nécessaire'), ('sollicit', 'Que sur sollicitation'), ('repond', 'Répond aux questions multiples'), ('incoherent', 'Incohérence par troubles psy'), ('mutisme', 'Mutisme')], max_length=12), blank=True, null=True, size=None, verbose_name='Communication')),
                ('communication_commentaires', models.TextField(blank=True, verbose_name='Communication commentaires')),
                ('communiquer_commentaires', models.TextField(blank=True, verbose_name='Commentaires')),
                ('communiquer_maj', models.DateField(blank=True, null=True, verbose_name='Date mise à jour')),
                ('communiquer_visa', models.CharField(blank=True, max_length=20, verbose_name='Visa')),
                ('croyances_accompagnement', models.TextField(blank=True, verbose_name='Désirs d’accompagnement')),
                ('croyances_directives', models.TextField(blank=True, verbose_name='Directives anticipées')),
                ('croyances_divers', models.TextField(blank=True, verbose_name='Divers')),
                ('croyances_habitudes', models.TextField(blank=True, verbose_name='Habitudes religieuses')),
                ('croyances_representant', models.TextField(blank=True, verbose_name='Représentant thérapeutique')),
                ('croyances_maj', models.DateField(blank=True, null=True, verbose_name='Date mise à jour')),
                ('croyances_visa', models.CharField(blank=True, max_length=20, verbose_name='Visa')),
                ('eliminer_divers', models.TextField(blank=True, verbose_name='Divers')),
                ('eliminer_incontinence', models.TextField(blank=True, verbose_name='Incontinence urinaire et/ou fécale')),
                ('eliminer_medication', models.TextField(blank=True, verbose_name='Médication')),
                ('eliminer_transferts', models.TextField(blank=True, verbose_name='Modes de transfert')),
                ('eliminer_moyens', models.TextField(blank=True, verbose_name='Moyens auxiliaires utilisés')),
                ('eliminer_protections', models.TextField(blank=True, verbose_name='Protections utilisées')),
                ('eliminer_maj', models.DateField(blank=True, null=True, verbose_name='Date mise à jour')),
                ('eliminer_visa', models.CharField(blank=True, max_length=20, verbose_name='Visa')),
                ('motricite_deplacements', models.TextField(blank=True, verbose_name='Déplacements')),
                ('motricite_probleme', models.BooleanField(default=False, verbose_name='Problème de motricité')),
                ('motricite_probleme_prec', models.TextField(blank=True, verbose_name='Préciser')),
                ('motricite_mobiliser', models.TextField(blank=True, verbose_name='Se mobiliser')),
                ('motricite_transferts', models.TextField(blank=True, verbose_name='Transferts')),
                ('motricite_maj', models.DateField(blank=True, null=True, verbose_name='Date mise à jour')),
                ('motricite_visa', models.CharField(blank=True, max_length=20, verbose_name='Visa')),
                ('hygiene_toilette', models.TextField(blank=True, verbose_name='Toilette')),
                ('hygiene_bain_douche', models.TextField(blank=True, verbose_name='Bain / Douche')),
                ('hygiene_cheveux', models.TextField(blank=True, verbose_name='Cheveux, shampoings')),
                ('hygiene_dents', models.TextField(blank=True, verbose_name='Dents')),
                ('hygiene_coiffer', models.TextField(blank=True, verbose_name='Se coiffer, maquillage, rasage')),
                ('hygiene_divers', models.TextField(blank=True, verbose_name='Divers')),
                ('hygiene_peau', models.TextField(blank=True, verbose_name='Intégrité de la peau')),
                ('hygiene_maj', models.DateField(blank=True, null=True, verbose_name='Date mise à jour')),
                ('hygiene_visa', models.CharField(blank=True, max_length=20, verbose_name='Visa')),
                ('respirer_aerosol', models.BooleanField(default=False, verbose_name='Aérosol')),
                ('respirer_aerosol_comm', models.TextField(blank=True, verbose_name='Commentaires')),
                ('respirer_oxygene', models.BooleanField(default=False, verbose_name='Oxygène')),
                ('respirer_oxygene_comm', models.TextField(blank=True, verbose_name='Commentaires')),
                ('respirer_tabagisme', models.BooleanField(default=False, verbose_name='Tabagisme')),
                ('respirer_tabagisme_comm', models.TextField(blank=True, verbose_name='Commentaires')),
                ('respirer_bronches', models.BooleanField(default=False, verbose_name='Encombrements bronchiques')),
                ('respirer_bronches_comm', models.TextField(blank=True, verbose_name='Commentaires')),
                ('respirer_dyspnee', models.BooleanField(default=False, verbose_name='Dyspnée')),
                ('respirer_dyspnee_comm', models.TextField(blank=True, verbose_name='Commentaires')),
                ('respirer_toux', models.BooleanField(default=False, verbose_name='Toux, expectorations')),
                ('respirer_toux_comm', models.TextField(blank=True, verbose_name='Commentaires')),
                ('respirer_commentaires', models.TextField(blank=True, verbose_name='Commentaires')),
                ('respirer_maj', models.DateField(blank=True, null=True, verbose_name='Date mise à jour')),
                ('respirer_visa', models.CharField(blank=True, max_length=20, verbose_name='Visa')),
                ('reposer_sieste', models.TextField(blank=True, verbose_name='Sieste')),
                ('reposer_lit', models.TextField(blank=True, verbose_name='Position du lit')),
                ('reposer_matelas', models.TextField(blank=True, verbose_name='Matelas anti-escares')),
                ('reposer_divers', models.TextField(blank=True, verbose_name='Divers')),
                ('reposer_maj', models.DateField(blank=True, null=True, verbose_name='Date mise à jour')),
                ('reposer_visa', models.CharField(blank=True, max_length=20, verbose_name='Visa')),
                ('vetir_habillage', models.TextField(blank=True, verbose_name='Habillage')),
                ('vetir_deshabillage', models.TextField(blank=True, verbose_name='Déshabillage')),
                ('vetir_vetements', models.TextField(blank=True, verbose_name="Mettre des vêtements pour l'extérieur")),
                ('vetir_fermetures', models.TextField(blank=True, verbose_name='Fermetures: lacets, boutons, éclair…')),
                ('vetir_temperature', models.TextField(blank=True, verbose_name="Maintenir sa température, sentir s'il fait chaud ou froid")),
                ('vetir_aerer', models.TextField(blank=True, verbose_name='Aérer sa chambre')),
                ('vetir_divers', models.TextField(blank=True, verbose_name='Divers')),
                ('vetir_maj', models.DateField(blank=True, null=True, verbose_name='Date mise à jour')),
                ('vetir_visa', models.CharField(blank=True, max_length=20, verbose_name='Visa')),
                ('securite_risque_agr', models.BooleanField(default=False, verbose_name='Agressivité')),
                ('securite_risque_agr_descr', models.CharField(blank=True, max_length=250, verbose_name='Description')),
                ('securite_commentaires', models.TextField(blank=True, verbose_name='Commentaires')),
                ('securite_contentions', models.CharField(blank=True, max_length=250, verbose_name='Contentions')),
                ('securite_maj', models.DateField(blank=True, null=True, verbose_name='Date mise à jour')),
                ('securite_visa', models.CharField(blank=True, max_length=20, verbose_name='Visa')),
                ('securite_semainier', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('seul', 'Seul'), ('partiel', 'Aide partielle'), ('complet', 'Aide complète')], max_length=12), blank=True, null=True, size=None, verbose_name='Gestion du semainier')),
                ('securite_type_semainier', models.CharField(blank=True, choices=[('petit', 'PETIT'), ('grand', 'GRAND')], max_length=6, verbose_name='Type de semainier')),
                ('securite_mrsa_resist', models.BooleanField(default=False, verbose_name='MRSA résistant')),
                ('securite_risque_chute', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('nul', 'Nul'), ('faible', 'Faible'), ('moyen', 'Moyen'), ('eleve', 'Élevé')], max_length=6), blank=True, null=True, size=None, verbose_name='Risque de chute')),
                ('securite_risque_chute_cause', models.TextField(blank=True, verbose_name='Cause')),
                ('securite_risque_errance', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('nul', 'Nul'), ('faible', 'Faible'), ('moyen', 'Moyen'), ('eleve', 'Élevé')], max_length=6), blank=True, null=True, size=None, verbose_name="Risque d'errance")),
                ('securite_risque_errance_cause', models.TextField(blank=True, verbose_name='Cause')),
                ('soccuper_animations_foyer', models.TextField(blank=True, verbose_name='Animations avec le foyer')),
                ('soccuper_ateliers_foyer', models.TextField(blank=True, verbose_name='Ateliers au foyer')),
                ('soccuper_camp_foyer', models.TextField(blank=True, verbose_name='Camp avec le foyer')),
                ('soccuper_divers', models.TextField(blank=True, verbose_name='Divers et projets')),
                ('soccuper_loisirs', models.TextField(blank=True, verbose_name='Loisirs')),
                ('soccuper_travail_ext', models.TextField(blank=True, verbose_name='Travail extérieur')),
                ('soccuper_vacances', models.TextField(blank=True, verbose_name='Vacances')),
                ('soccuper_maj', models.DateField(blank=True, null=True, verbose_name='Date mise à jour')),
                ('soccuper_visa', models.CharField(blank=True, max_length=20, verbose_name='Visa')),
                ('sexualite_attentes', models.TextField(blank=True, verbose_name='Attentes et attitudes')),
                ('sexualite_maj', models.DateField(blank=True, null=True, verbose_name='Date mise à jour')),
                ('sexualite_visa', models.CharField(blank=True, max_length=20, verbose_name='Visa')),
                ('person', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='person.Person')),
            ],
        ),
    ]
