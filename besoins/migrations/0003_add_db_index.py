from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0002_migrate_alim_fields'),
    ]

    operations = [
        migrations.AlterField(
            model_name='besoinsalimentaires',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
    ]
