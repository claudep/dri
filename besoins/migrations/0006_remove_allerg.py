from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0005_extend_fields'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='recueilbesoins',
            name='alim_allergies',
        ),
        migrations.RemoveField(
            model_name='recueilbesoins',
            name='alim_allergies_causes',
        ),
    ]
