from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0007_add_maju_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='recueilbesoins',
            name='carte_vie_maj',
            field=models.DateField(blank=True, null=True, verbose_name='Date mise à jour'),
        ),
    ]
