from django.core.exceptions import ObjectDoesNotExist
from django.db import migrations


def populate_carte_vie_maj(apps, schema_editor):
    RecueilBesoins = apps.get_model("besoins", "RecueilBesoins")
    for bes in RecueilBesoins.objects.all():
        dates = [dt for dt in [
            bes.securite_maj, bes.communiquer_maj, bes.respirer_maj, bes.hygiene_maj,
            bes.vetir_maj, bes.eliminer_maj, bes.motricite_maj, bes.reposer_maj,
        ] if dt is not None]
        try:
            if bes.person.besoinsalimentaires.date:
                dates.append(bes.person.besoinsalimentaires.date.date())
        except ObjectDoesNotExist:
            pass
        if dates:
            bes.carte_vie_maj = max(dates)
            bes.save()


class Migration(migrations.Migration):

    dependencies = [
        ('besoins', '0008_carte_vie_maj_date'),
    ]

    operations = [
        migrations.RunPython(populate_carte_vie_maj, migrations.RunPython.noop),
    ]
