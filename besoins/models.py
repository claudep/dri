from django.db import models
from django.urls import reverse

from common.fields import ChoiceArrayField

from person.models import ObjectHistoryMixin, Modification, Person, PersonRelatedModel, User

# Pas de champs BesoinsAlimentaires car tous figurent sur la carte
CARTE_VIE_FIELDS = (
    'communiquer_general', 'communiquer_ouie', 'communiquer_langage', 'communiquer_commentaires',
    'eliminer_incontinence', 'eliminer_transferts', 'eliminer_moyens', 'eliminer_medication',
    'eliminer_protections', 'eliminer_divers',
    'hygiene_toilette', 'hygiene_bain_douche', 'hygiene_cheveux', 'hygiene_dents',
    'hygiene_coiffer', 'hygiene_divers', 'hygiene_peau',
    'motricite_probleme', 'motricite_probleme_prec', 'motricite_deplacements',
    'motricite_transferts', 'motricite_mobiliser',
    'reposer_sieste', 'reposer_lit', 'reposer_matelas', 'reposer_divers',
    'respirer_tabagisme', 'respirer_dyspnee', 'respirer_bronches', 'respirer_toux',
    'respirer_aerosol', 'respirer_oxygene', 'respirer_commentaires',
    'securite_risque_chute', 'securite_risque_errance', 'securite_risque_agr',
    'securite_mrsa_resist', 'securite_contentions', 'securite_commentaires',
    'vetir_habillage', 'vetir_deshabillage', 'vetir_divers',
)

TAB_MAPPING = {
    'securite': {'title': 'Sécurité', 'template': 'besoins/securite.html'},
    'communiquer': {'title': 'Communiquer', 'template': 'besoins/communiquer.html'},
    'respirer':  {'title': 'Respirer', 'template': 'besoins/respirer.html'},
    'hygiene':  {'title': 'Hygiène', 'template': 'besoins/hygiene.html'},
    'vetir':  {'title': 'Se vêtir', 'template': 'besoins/vetir.html'},
    'nourrir':  {'title': 'Se nourrir', 'template': 'besoins/nourrir.html'},
    'eliminer':  {'title': 'Éliminer', 'template': 'besoins/eliminer.html'},
    'mouvoir':  {'title': 'Se mouvoir', 'template': 'besoins/mouvoir.html'},
    'reposer':  {'title': 'Se reposer', 'template': 'besoins/reposer.html'},
    'soccuper':  {'title': 'S’occuper', 'template': 'besoins/occuper.html'},
    'apprendre':  {'title': 'Apprendre', 'template': 'besoins/apprendre.html'},
    'croyances':  {'title': 'Croyances', 'template': 'besoins/croyances.html'},
    'sexualite':  {'title': 'Vivre sa sexualité', 'template': 'besoins/sexualite.html'},
}


class RecueilBesoins(ObjectHistoryMixin, models.Model):
    RISQUE_CHOICES = (
        ('nul', 'Nul'),
        ('faible', 'Faible'),
        ('moyen', 'Moyen'),
        ('eleve', 'Élevé'),
    )
    VUE_CHOICES = (
        ('alteration', "Altération de la vue"),
        ('lunettes', "Lunettes"),
    )
    COMMUNIC_CHOICES = (
        ('aisement', "Spontanément, aisément"),
        ('peu', "Peu, mais demande le nécessaire"),
        ('sollicit', "Que sur sollicitation"),
        ('repond', "Répond aux questions multiples"),
        ('incoherent', "Incohérence par troubles psy"),
        ('mutisme', "Mutisme"),
    )
    COMPORT_CHOICES = (
        ('anxiete', "Anxiété"),
        ('peur', "Peur"),
        ('colere', "Colère"),
        ('tristesse', "Tristesse"),
        ('repli', "Repli sur soi"),
        ('depressif', "Etat dépressif"),
        ('aggressif', "Agressivité"),
        ('affabul', "Affabulation"),
        ('hallucin', "Hallucinations"),
        ('delire', "Délire"),
        ('perte', "Perte d'intérêt"),
        ('plusenvie', "Plus envie de vivre"),
    )
    COLLAB_CHOICES = (
        ('spontanee', "Spontanée"),
        ('stimulation', "Besoin de stimulation"),
        ('passivite', "Passivité"),
        ('reconnaitre', "Reconnaître les personnes"),
        ('opposition', "Attitude d'opposition"),
        ('refus', "Refus modifier comportements préjudiciables"),
    )
    TYPE_SEMAINIER_CHOICES = (
        ('petit', 'PETIT'),
        ('grand', 'GRAND'),
        ('stand', 'STANDARD'),
    )
    SEMAINIER_CHOICES = (
        ('seul', "Seul"),
        ('partiel', "Aide partielle"),
        ('complet', "Aide complète"),
    )

    person = models.OneToOneField(Person, on_delete=models.CASCADE)
    # Date de dernière modification d'un champ de la carte de vie
    carte_vie_maj = models.DateField("Date mise à jour", null=True, blank=True)

    # Apprendre
    appr_soigner = models.BooleanField("Apprendre à se soigner", null=True, blank=True)
    appr_traitement_medic = models.BooleanField("Médicamenteux", null=True, blank=True)
    appr_traitement_maintenir = models.BooleanField("Maintenir le traitement", null=True, blank=True)
    appr_traitement_controle = models.BooleanField("Contrôler le traitement", null=True, blank=True)
    appr_divers = models.TextField("Divers", blank=True)
    appr_mesures_prev = models.TextField("Mesures de prévention", blank=True)
    appr_maladie = models.BooleanField("Apprendre sa maladie", null=True, blank=True)
    appr_poids = models.BooleanField("Apprendre son poids", null=True, blank=True)
    appr_traitement = models.BooleanField("Apprendre son traitement", null=True, blank=True)
    appr_soin = models.BooleanField("Apprendre un soin", null=True, blank=True)
    appr_un_traitement = models.BooleanField("Apprendre un traitement", null=True, blank=True)
    apprendre_maj = models.DateField("Date mise à jour", null=True, blank=True)
    apprendre_visa = models.CharField("Visa", max_length=20, blank=True)
    apprendre_maju = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Mis à jour par', related_name='+'
    )

    # 10, besoin de communiquer
    communiquer_general = models.TextField("Communiquer, général", blank=True)
    communiquer_ouie = models.TextField("Ouïe", blank=True)
    communiquer_vue = ChoiceArrayField(
        models.CharField(max_length=12, choices=VUE_CHOICES, blank=True),
        verbose_name="Vue", blank=True, null=True,
    )
    communiquer_langage = models.TextField("Langage", blank=True)
    communiquer_besoin_aide = models.TextField("?Besoin aide?", blank=True)
    collaboration = ChoiceArrayField(
        models.CharField(max_length=12, choices=COLLAB_CHOICES, blank=True),
        verbose_name="Collaboration", blank=True, null=True,
    )
    collaboration_commentaires = models.TextField("Commentaires", blank=True)
    comportement = ChoiceArrayField(
        models.CharField(max_length=12, choices=COMPORT_CHOICES, blank=True),
        verbose_name="Comportement", blank=True, null=True,
    )
    comportement_commentaires = models.TextField("Commentaires", blank=True)
    communication = ChoiceArrayField(
        models.CharField(max_length=12, choices=COMMUNIC_CHOICES, blank=True),
        verbose_name="Communication", blank=True, null=True,
    )
    communication_commentaires = models.TextField("Communication commentaires", blank=True)
    communiquer_commentaires = models.TextField("Commentaires", blank=True)
    communiquer_maj = models.DateField("Date mise à jour", null=True, blank=True)
    communiquer_visa = models.CharField("Visa", max_length=20, blank=True)
    communiquer_maju = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Mis à jour par', related_name='+'
    )
    # Croyances
    croyances_accompagnement = models.TextField("Désirs d’accompagnement", blank=True)
    croyances_directives = models.TextField("Directives anticipées", blank=True)
    croyances_divers = models.TextField("Divers", blank=True)
    croyances_habitudes = models.TextField("Habitudes religieuses", blank=True)
    croyances_representant = models.TextField("Représentant thérapeutique", blank=True)
    croyances_maj = models.DateField("Date mise à jour", null=True, blank=True)
    croyances_visa = models.CharField("Visa", max_length=20, blank=True)
    croyances_maju = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Mis à jour par', related_name='+'
    )
    # Éliminer
    eliminer_incontinence = models.TextField("Incontinence urinaire et/ou fécale", blank=True)
    eliminer_transferts = models.TextField("Modes de transfert", blank=True)
    eliminer_moyens = models.TextField("Moyens auxiliaires utilisés", blank=True)
    eliminer_medication = models.TextField("Médication", blank=True)
    eliminer_protections = models.TextField("Protections utilisées", blank=True)
    eliminer_divers = models.TextField("Divers", blank=True)
    eliminer_maj = models.DateField("Date mise à jour", null=True, blank=True)
    eliminer_visa = models.CharField("Visa", max_length=20, blank=True)
    eliminer_maju = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Mis à jour par', related_name='+'
    )
    # Se mouvoir
    motricite_deplacements = models.TextField("Déplacements", blank=True)
    motricite_probleme = models.BooleanField("Problème de motricité", default=False)
    motricite_probleme_prec = models.TextField("Préciser", blank=True)
    motricite_mobiliser = models.TextField("Se mobiliser", blank=True)
    motricite_transferts = models.TextField("Transferts", blank=True)
    motricite_maj = models.DateField("Date mise à jour", null=True, blank=True)
    motricite_visa = models.CharField("Visa", max_length=20, blank=True)
    motricite_maju = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Mis à jour par', related_name='+'
    )
    # Hygiène
    hygiene_toilette = models.TextField("Toilette", blank=True)
    hygiene_bain_douche = models.TextField("Bain / Douche", blank=True)
    hygiene_cheveux = models.TextField("Cheveux, shampoings", blank=True)
    hygiene_dents = models.TextField("Dents", blank=True)
    hygiene_coiffer = models.TextField("Se coiffer, maquillage, rasage", blank=True)
    hygiene_divers = models.TextField("Divers", blank=True)
    hygiene_peau = models.TextField("Intégrité de la peau", blank=True)
    hygiene_maj = models.DateField("Date mise à jour", null=True, blank=True)
    hygiene_visa = models.CharField("Visa", max_length=20, blank=True)
    hygiene_maju = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Mis à jour par', related_name='+'
    )
    # Respirer
    respirer_aerosol = models.BooleanField("Aérosol", default=False)
    respirer_aerosol_comm = models.TextField("Commentaires", blank=True)
    respirer_oxygene = models.BooleanField("Oxygène", default=False)
    respirer_oxygene_comm = models.TextField("Commentaires", blank=True)
    respirer_tabagisme = models.BooleanField("Tabagisme", default=False)
    respirer_tabagisme_comm = models.TextField("Commentaires", blank=True)
    respirer_bronches = models.BooleanField("Encombrements bronchiques", default=False)
    respirer_bronches_comm = models.TextField("Commentaires", blank=True)
    respirer_dyspnee = models.BooleanField("Dyspnée", default=False)
    respirer_dyspnee_comm = models.TextField("Commentaires", blank=True)
    respirer_toux = models.BooleanField("Toux, expectorations", default=False)
    respirer_toux_comm = models.TextField("Commentaires", blank=True)
    respirer_commentaires = models.TextField("Commentaires", blank=True)
    respirer_maj = models.DateField("Date mise à jour", null=True, blank=True)
    respirer_visa = models.CharField("Visa", max_length=20, blank=True)
    respirer_maju = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Mis à jour par', related_name='+'
    )
    # Se reposer
    reposer_sieste = models.TextField("Sieste", blank=True)
    reposer_lit = models.TextField("Position du lit", blank=True)
    reposer_matelas = models.TextField("Matelas anti-escares", blank=True)
    reposer_divers = models.TextField("Divers", blank=True)
    reposer_maj = models.DateField("Date mise à jour", null=True, blank=True)
    reposer_visa = models.CharField("Visa", max_length=20, blank=True)
    reposer_maju = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Mis à jour par', related_name='+'
    )
    # Se vêtir
    vetir_habillage = models.TextField("Habillage", blank=True)
    vetir_deshabillage = models.TextField("Déshabillage", blank=True)
    vetir_vetements = models.TextField("Mettre des vêtements pour l'extérieur", blank=True)
    vetir_fermetures = models.TextField("Fermetures: lacets, boutons, éclair…", blank=True)
    vetir_temperature = models.TextField("Maintenir sa température, sentir s'il fait chaud ou froid", blank=True)
    vetir_aerer = models.TextField("Aérer sa chambre", blank=True)
    vetir_divers = models.TextField("Divers", blank=True)
    vetir_maj = models.DateField("Date mise à jour", null=True, blank=True)
    vetir_visa = models.CharField("Visa", max_length=20, blank=True)
    vetir_maju = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Mis à jour par', related_name='+'
    )
    # Sécurité
    securite_risque_agr = models.BooleanField("Agressivité", default=False)
    securite_risque_agr_descr = models.CharField("Description", max_length=250, blank=True)
    securite_commentaires = models.TextField("Commentaires", blank=True)
    securite_contentions = models.CharField("Contentions", max_length=250, blank=True)
    securite_maj = models.DateField("Date mise à jour", null=True, blank=True)
    securite_visa = models.CharField("Visa", max_length=20, blank=True)
    securite_maju = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Mis à jour par', related_name='+'
    )
    securite_semainier = ChoiceArrayField(
        models.CharField(max_length=12, choices=SEMAINIER_CHOICES, blank=True),
        verbose_name="Gestion du semainier", blank=True, null=True,
    ) # Emplacement ??
    securite_type_semainier = models.CharField(
        "Type de semainier", max_length=6, choices=TYPE_SEMAINIER_CHOICES, blank=True
    )
    securite_mrsa_resist = models.BooleanField("MRSA résistant", default=False)
    securite_risque_chute = ChoiceArrayField(
        models.CharField(max_length=6, choices=RISQUE_CHOICES, blank=True),
        verbose_name="Risque de chute", blank=True, null=True,
    )
    securite_risque_chute_cause = models.TextField("Cause", blank=True)
    securite_risque_errance = ChoiceArrayField(
        models.CharField(max_length=6, choices=RISQUE_CHOICES, blank=True),
        verbose_name="Risque d'errance", blank=True, null=True,
    )
    securite_risque_errance_cause = models.TextField("Cause", blank=True)
    # S'occuper
    soccuper_animations_foyer = models.TextField("Animations avec le foyer", blank=True)
    soccuper_ateliers_foyer = models.TextField("Ateliers au foyer", blank=True)
    soccuper_camp_foyer = models.TextField("Camp avec le foyer", blank=True)
    soccuper_divers = models.TextField("Divers et projets", blank=True)
    soccuper_loisirs = models.TextField("Loisirs", blank=True)
    soccuper_travail_ext = models.TextField("Travail extérieur", blank=True)
    soccuper_vacances = models.TextField("Vacances", blank=True)
    soccuper_maj = models.DateField("Date mise à jour", null=True, blank=True)
    soccuper_visa = models.CharField("Visa", max_length=20, blank=True)
    soccuper_maju = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Mis à jour par', related_name='+'
    )
    # Vivre sa sexualité
    sexualite_attentes = models.TextField("Attentes et attitudes", blank=True)
    sexualite_maj = models.DateField("Date mise à jour", null=True, blank=True)
    sexualite_visa = models.CharField("Visa", max_length=20, blank=True)
    sexualite_maju = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Mis à jour par', related_name='+'
    )

    _view_name_base = 'besoins'

    def __str__(self):
        return "Recueil de besoins pour %s" % self.person

    def get_absolute_url(self):
        return reverse('tab-load', args=[self.person.pk, 'person', 'recueil'])

    def can_edit(self, user):
        return user.has_perm('besoins.change_recueilbesoins')

    def object_history(self, part=None, **kwargs):
        if part:
            modifs = super().object_history(as_qs=True).filter(precision__contains=f"«{TAB_MAPPING[part]['title']}»")
            if not modifs:
                user = getattr(self, f'{part}_maju', None)
                date_maj = getattr(self, f'{part}_maj', None)
                visa = getattr(self, f'{part}_visa', None)
                if (user or date_maj or visa):
                    modifs = [Modification(
                        person=self.person, date=date_maj, user=user, visa=visa,
                        precision=f"Modification de la fiche «{TAB_MAPPING[part]['title']}»"
                    )]
        else:
            try:
                besoinsalim = self.person.besoinsalimentaires
            except BesoinsAlimentaires.DoesNotExist:
                modifs = super().object_history()
            else:
                modifs = super().object_history(as_qs=True).union(
                    self.person.besoinsalimentaires.object_history(as_qs=True)
                ).order_by('-date')
        return modifs


class BesoinsAlimentaires(PersonRelatedModel):
    person = models.OneToOneField(Person, on_delete=models.CASCADE)
    diagnostic = models.CharField("Diagnostic", max_length=50, blank=True)
    problemes = models.CharField("Problèmes", max_length=150, blank=True)
    allergies = models.CharField("Allergies alimentaires", max_length=150, blank=True)
    # Besoins
    regimes = models.CharField("Régimes", max_length=50, blank=True)
    portions = models.CharField("Portions", max_length=25, blank=True)
    protheses = models.CharField("Prothèses", max_length=30, blank=True)
    aide_alim = models.TextField("Aide à l’alimentation", blank=True)
    aide_hydr = models.TextField("Aide à l’hydratation", blank=True)
    moyens_aux = models.TextField("Moyens auxiliaires", blank=True)
    complements = models.CharField("Compléments alimentaires", max_length=150, blank=True)
    desirs = models.CharField("Désirs particuliers", max_length=150, blank=True)
    naimepas = models.CharField("N'aime pas", max_length=150, blank=True)
    # Petit déjeûner
    boissons = models.CharField("Boissons", max_length=50, blank=True)
    aliments = models.CharField("Aliments", max_length=50, blank=True)
    accompagnements = models.CharField("Accompagnements", max_length=50, blank=True)
    mat_grasses = models.CharField("Matières grasses", max_length=20, blank=True)
    divers = models.TextField("Divers", blank=True)

    def __str__(self):
        return "Besoins alimentaires pour %s (%s)" % (self.person, self.date)

    def get_absolute_url(self):
        return reverse('person-detail', args=[self.person.pk]) + '?totab=nourrir'

    def can_edit(self, user):
        return user.has_perm('besoins.change_besoinsalimentaires')


class Impotence(models.Model):
    # pour les champs aide (mais pas encore utilisé à cause des exceptions)
    GENRE_AIDE_CHOICES = (
        ('compl', "Directe complète"),
        ('part', "Directe partielle"),
        ('ind', "Indirecte"),
    )

    person = models.OneToOneField(Person, on_delete=models.CASCADE)
    # toilettes
    wc_habituel = models.BooleanField("Aller aux toilettes de manière habituelle", default=False)
    wc_habituel_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    wc_habituel_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    wc_habituel_precisions = models.CharField("Précisions", max_length=200, blank=True)
    wc_ordrehab = models.BooleanField("Mettre en ordre les habits", default=False)
    wc_ordrehab_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    wc_ordrehab_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    wc_ordrehab_precisions = models.CharField("Précisions", max_length=200, blank=True)
    wc_propr = models.BooleanField("Laver le corps - contrôler la propreté", default=False)
    wc_propr_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    wc_propr_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    wc_propr_precisions = models.CharField("Précisions", max_length=200, blank=True)
    # manger
    alim_spec = models.BooleanField("Ne peut manger que des aliments spéciaux", default=False)
    alim_spec_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    alim_spec_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    alim_spec_precisions = models.CharField("Précisions", max_length=200, blank=True)
    manger_lit = models.BooleanField("Apporter les aliments au lit", default=False)
    manger_lit_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    manger_lit_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    manger_lit_precisions = models.CharField("Précisions", max_length=200, blank=True)
    couper_alim = models.BooleanField("Couper les aliments", default=False)
    couper_alim_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    couper_alim_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    couper_alim_precisions = models.CharField("Précisions", max_length=200, blank=True)
    porter_bouche = models.BooleanField("Porter les aliments à la bouche", default=False)
    porter_bouche_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    porter_bouche_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    porter_bouche_precisions = models.CharField("Précisions", max_length=200, blank=True)
    # mouvoir
    depl_maison = models.BooleanField("Se déplacer dans la maison", default=False)
    depl_maison_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    depl_maison_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    depl_maison_precisions = models.CharField("Précisions", max_length=200, blank=True)
    depl_ext = models.BooleanField("Se déplacer à l'extérieur", default=False)
    depl_ext_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    depl_ext_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    depl_ext_precisions = models.CharField("Précisions", max_length=200, blank=True)

    lever = models.BooleanField("Se lever/S'asseoir/Se coucher", default=False)
    lever_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    lever_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    lever_precisions = models.CharField("Précisions", max_length=200, blank=True)
    # hygiene
    baigner = models.BooleanField("Se baigner/se doucher", default=False)
    baigner_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    baigner_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    baigner_precisions = models.CharField("Précisions", max_length=200, blank=True)
    laver = models.BooleanField("Se laver", default=False)
    laver_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    laver_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    laver_precisions = models.CharField("Précisions", max_length=200, blank=True)
    peigner = models.BooleanField("Se peigner", default=False)
    peigner_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    peigner_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    peigner_precisions = models.CharField("Précisions", max_length=200, blank=True)
    raser = models.BooleanField("Se raser", default=False)
    raser_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    raser_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    raser_precisions = models.CharField("Précisions", max_length=200, blank=True)

    vetir = models.BooleanField("", default=False)
    vetir_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    vetir_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    vetir_precisions = models.CharField("Précisions", max_length=200, blank=True)

    depl_contact = models.BooleanField("Établir des contact avec l'entourage", default=False)
    depl_contact_quand = models.CharField("Depuis quand", max_length=20, blank=True)
    depl_contact_aide = models.CharField("Genre d'aide apportée", max_length=40, blank=True)
    depl_contact_precisions = models.CharField("Précisions", max_length=200, blank=True)

    def __str__(self):
        return "Impotence pour %s" % self.person

    def can_edit(self, user):
        return user.has_perm('besoins.change_impotence')
