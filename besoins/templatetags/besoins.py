from django import template
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from common.templatetags.fhutils import display_label
from besoins.models import BesoinsAlimentaires, CARTE_VIE_FIELDS
register = template.Library()


@register.filter
def auteur(besoin, panel_name):
    user = getattr(besoin, f'{panel_name}_maju', None)
    if user:
        return format_html('<span title="{}">{}</span>', user.get_full_name(), user.initiales)
    else:
        return getattr(besoin, f'{panel_name}_visa', '')


@register.simple_tag(takes_context=True)
def display_label_besoins(context, field_name, *args):
    label = display_label(context, field_name)
    if isinstance(context['object'], BesoinsAlimentaires) or field_name in CARTE_VIE_FIELDS:
        return mark_safe('<span class="cartevie" title="Visible sur la carte de vie">*</span>') + label
    else:
        return label
