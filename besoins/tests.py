from datetime import date

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse

from person.forms import PersonDetailForm
from person.models import Person, User
from .models import BesoinsAlimentaires, RecueilBesoins


class BesoinsTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(username='jill', first_name='Jill', last_name='Martin')
        cls.user.user_permissions.add(Permission.objects.get(codename='view_recueilbesoins'))

    def setUp(self):
        self.pers = Person.objects.create(nom='Smith', prenom='John')

    def test_carte_de_vie(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('person-cartedevie', args=[self.pers.pk]))
        self.assertEqual(response.status_code, 200)
        # Individual png output
        for carte_num in (1, 2, 3, 4):
            response = self.client.get(reverse('person-cartedevie-png', args=[self.pers.pk, carte_num]))
            self.assertEqual(response.status_code, 200)

    def test_carte_de_vie_avec_besoins(self):
        ba = BesoinsAlimentaires.objects.create(person=self.pers, createur=self.user)
        self.test_carte_de_vie()

    def test_carte_de_vie_recap(self):
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('cartedevie-recap', args=[self.pers.pk]), HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertContains(response, 'Télécharger la carte de vie')

    def _test_besoins_edition(self, data_set):
        rb = RecueilBesoins.objects.create(person=self.pers)
        self.client.force_login(self.user)
        edit_url = reverse('besoins-edit', args=[self.pers.pk, data_set['panel']])
        response = self.client.get(edit_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, data_set['form_check'], html=True)
        response = self.client.post(
            edit_url, data=data_set['post_data'], HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertRedirects(
            response, reverse('besoins-detail', args=[self.pers.pk, data_set['panel']]),
        )
        rb.refresh_from_db()
        self.assertEqual(getattr(rb, data_set['maj_field']), date.today())
        return rb

    def test_edition_securite(self):
        rb = self._test_besoins_edition({
            'panel': 'securite',
            'form_check': '<label for="id_securite_risque_chute_0">'
                          '<input name="securite_risque_chute" value="nul" class="choicearray" id="id_securite_risque_chute_0" type="checkbox">'
                          'Nul</label>',
            'post_data': {
                'securite_risque_chute': ['faible', 'moyen'],
                'securite_commentaires': 'Attention aux chutes',
            },
            'maj_field': 'securite_maj'
        })
        self.assertEqual(rb.securite_risque_chute, ['faible', 'moyen'])
        self.assertEqual(
            rb.latest_history().precision,
            "Modification de la fiche de données «Sécurité» (Risque de chute, Commentaires)"
        )

    def test_edition_communiquer(self):
        rb = self._test_besoins_edition({
            'panel': 'communiquer',
            'form_check': '<input name="communication" value="aisement" class="choicearray" id="id_communication_0" type="checkbox">',
            'post_data': {'communication': ['aisement']},
            'maj_field': 'communiquer_maj'
        })
        self.assertEqual(rb.communication, ['aisement'])

    def test_edition_respirer(self):
        rb = self._test_besoins_edition({
            'panel': 'respirer',
            'form_check': '<input name="respirer_tabagisme" id="id_respirer_tabagisme" type="checkbox">',
            'post_data': {'respirer_tabagisme': 'on'},
            'maj_field': 'respirer_maj'
        })
        self.assertTrue(rb.respirer_tabagisme)

    def test_edition_hygiene(self):
        post_data = {
            'hygiene_toilette': 'ok',
            # Champs Impotence:
            'laver': 'on', 'laver_quand': 'hier', 'laver_aide': 'partielle', 'laver_precisions': 'bla',
        }
        rb = self._test_besoins_edition({
            'panel': 'hygiene',
            'form_check': '<textarea name="hygiene_toilette" id="id_hygiene_toilette" cols="40" rows="10"></textarea>',
            'post_data': post_data,
            'maj_field': 'hygiene_maj'
        })
        self.assertEqual(rb.hygiene_toilette, 'ok')
        impot = Person.objects.get(nom='Smith').impotence
        self.assertTrue(impot.laver)
        self.assertEqual(impot.laver_quand, 'hier')
        self.assertEqual(impot.laver_aide, 'partielle')
        self.assertEqual(impot.laver_precisions, 'bla')
        # Now change only the impotence data
        post_data['laver_quand'] = 'le matin'
        response = self.client.post(
            reverse('besoins-edit', args=[self.pers.pk, 'hygiene']),
            data=post_data, HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.pers.impotence.refresh_from_db()
        self.assertEqual(self.pers.impotence.laver_quand, 'le matin')

    def test_edition_vetir(self):
        rb = self._test_besoins_edition({
            'panel': 'vetir',
            'form_check': '<textarea name="vetir_habillage" id="id_vetir_habillage" cols="40" rows="10"></textarea>',
            'post_data': {'vetir_habillage': 'dépendant'},
            'maj_field': 'vetir_maj'
        })
        self.assertEqual(rb.vetir_habillage, 'dépendant')

    def test_edition_nourrir(self):
        pers = Person.objects.create(nom='Smith', prenom='John')
        ba = BesoinsAlimentaires.objects.create(person=pers, createur=self.user)
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('nourrir-detail', args=[pers.pk]), HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        edit_url = reverse('nourrir-edit', args=[pers.pk])
        response = self.client.get(edit_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, '<input name="diagnostic" id="id_diagnostic" maxlength="50" type="text">', html=True)
        response = self.client.post(
            edit_url, data={'diagnostic': 'Aïe'}, HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertRedirects(
            response, reverse('nourrir-detail', args=[pers.pk]), fetch_redirect_response=False
        )
        ba.refresh_from_db()
        self.assertEqual(ba.date.date(), date.today())
        self.assertEqual(ba.createur, self.user)
        self.assertEqual(ba.diagnostic, 'Aïe')
        self.assertEqual(
            ba.latest_history().precision,
            "Modification de la fiche de données «Se nourrir» (Diagnostic)"
        )

    def test_edition_eliminer(self):
        rb = self._test_besoins_edition({
            'panel': 'eliminer',
            'form_check': '<textarea name="eliminer_incontinence" id="id_eliminer_incontinence" cols="40" rows="10"></textarea>',
            'post_data': {'eliminer_incontinence': 'parfois'},
            'maj_field': 'eliminer_maj'
        })
        self.assertEqual(rb.eliminer_incontinence, 'parfois')

    def test_edition_mouvoir(self):
        rb = self._test_besoins_edition({
            'panel': 'mouvoir',
            'form_check': '<textarea name="motricite_deplacements" id="id_motricite_deplacements" cols="40" rows="10"></textarea>',
            'post_data': {'motricite_deplacements': 'moyenne'},
            'maj_field': 'motricite_maj'
        })
        self.assertEqual(rb.motricite_deplacements, 'moyenne')

    def test_edition_reposer(self):
        rb = self._test_besoins_edition({
            'panel': 'reposer',
            'form_check': '<textarea name="reposer_sieste" id="id_reposer_sieste" cols="40" rows="10"></textarea>',
            'post_data': {'reposer_sieste': 'parfois'},
            'maj_field': 'reposer_maj'
        })
        self.assertIsNotNone(rb.carte_vie_maj)
        self.assertEqual(rb.reposer_sieste, 'parfois')

    def test_edition_occuper(self):
        rb = self._test_besoins_edition({
            'panel': 'soccuper',
            'form_check': '<textarea name="soccuper_travail_ext" id="id_soccuper_travail_ext" cols="40" rows="10"></textarea>',
            'post_data': {'soccuper_travail_ext': 'oui'},
            'maj_field': 'soccuper_maj'
        })
        # Aucun champ ne figure sur la carte de vie
        self.assertIsNone(rb.carte_vie_maj)
        self.assertEqual(rb.soccuper_travail_ext, 'oui')

    def test_edition_apprendre(self):
        rb = self._test_besoins_edition({
            'panel': 'apprendre',
            'form_check': '<li><label><input name="appr_traitement_medic" value="unknown" type="radio" checked>?</label></li>',
            'post_data': {'appr_traitement_medic': '2', 'appr_traitement_maintenir': '3'},
            'maj_field': 'apprendre_maj'
        })
        self.assertTrue(rb.appr_traitement_medic)
        self.assertIs(rb.appr_traitement_maintenir, False)

    def test_edition_croyance(self):
        rb = self._test_besoins_edition({
            'panel': 'croyances',
            'form_check': '<textarea name="croyances_accompagnement" id="id_croyances_accompagnement" cols="40" rows="10">\n</textarea>',
            'post_data': {'croyances_accompagnement': 'Oui'},
            'maj_field': 'croyances_maj'
        })
        self.assertEqual(rb.croyances_accompagnement, 'Oui')

    def test_edition_sexualite(self):
        self._test_besoins_edition({
            'panel': 'sexualite',
            'form_check': '<textarea name="sexualite_attentes" id="id_sexualite_attentes" cols="40" rows="10">\n</textarea>',
            'post_data': {'sexualite_attentes': 'Aucune'},
            'maj_field': 'sexualite_maj'
        })
