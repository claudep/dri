from django.urls import path

from . import views

urlpatterns = [
    path('cartedevie/pdf/', views.CarteDeVieView.as_view(), name='person-cartedevie'),
    path('cartedevie/<int:num>.png', views.CarteDeViePreview.as_view(), name='person-cartedevie-png'),
    path('cartedevie/', views.CarteDeVieRecapView.as_view(), name='cartedevie-recap'),
    path('nourrir/', views.NourrirDetailView.as_view(), kwargs={'categ': 'nourrir'}, name='nourrir-detail'),
    path('nourrir/edit/', views.NourrirEditView.as_view(), kwargs={'categ': 'nourrir'}, name='nourrir-edit'),
    path('<slug:categ>/', views.BesoinDetailView.as_view(), name='besoins-detail'),
    path('<slug:categ>/edit/', views.BesoinEditView.as_view(), name='besoins-edit'),
]
