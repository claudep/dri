import os
import subprocess
from tempfile import NamedTemporaryFile

from django.conf import settings
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.mail import send_mail
from django.http import FileResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.generic import TemplateView

from common.views import BasePDFView
from person.models import Person
from person.pdf_output import CarteDeViePDF
from person.views import BaseDetailView, BaseEditView, BasePersonPDFView, OnlyAjaxMixin
from notice.models import NoticeSubscription
from .forms import (
    SecuriteForm, CommuniquerForm, RespirerForm, HygieneForm, VetirForm,
    NourrirForm, EliminerForm, MouvoirForm, ReposerForm, OccuperForm,
    ApprendreForm, CroyancesForm, SexualiteForm,
)
from .models import (
    CARTE_VIE_FIELDS, TAB_MAPPING, BesoinsAlimentaires, Impotence, RecueilBesoins
)

TAB_MAPPING['securite']['form'] = SecuriteForm
TAB_MAPPING['communiquer']['form'] = CommuniquerForm
TAB_MAPPING['respirer']['form'] = RespirerForm
TAB_MAPPING['hygiene']['form'] = HygieneForm
TAB_MAPPING['vetir']['form'] = VetirForm
TAB_MAPPING['nourrir']['form'] = NourrirForm
TAB_MAPPING['eliminer']['form'] = EliminerForm
TAB_MAPPING['mouvoir']['form'] = MouvoirForm
TAB_MAPPING['reposer']['form'] = ReposerForm
TAB_MAPPING['soccuper']['form'] = OccuperForm
TAB_MAPPING['apprendre']['form'] = ApprendreForm
TAB_MAPPING['croyances']['form'] = CroyancesForm
TAB_MAPPING['sexualite']['form'] = SexualiteForm


class BesoinBaseView:
    model = RecueilBesoins
    mapping = TAB_MAPPING

    def get_object(self):
        pers = Person.objects.get(pk=self.kwargs['pk'])
        try:
            return pers.recueilbesoins
        except RecueilBesoins.DoesNotExist:
            return RecueilBesoins(person=pers)

    def get_impotence(self):
        pers = Person.objects.get(pk=self.kwargs['pk'])
        try:
            return pers.impotence
        except Impotence.DoesNotExist:
            return Impotence(person=pers)


class BesoinDetailView(BesoinBaseView, BaseDetailView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'impotence': self.get_impotence(),
            'object_history_url': self.object.object_history_url(third_arg=self.kwargs['categ']),
            'latest_history': self.object.latest_history(part=self.kwargs['categ']),
        })
        return context

    def get_template_names(self):
        return [self.mapping[self.kwargs['categ']]['template']]


class BesoinEditView(BesoinBaseView, BaseEditView):
    maj_field = None
    is_create = False

    def get_template_names(self):
        return [self.mapping[self.kwargs['categ']]['template'] or 'base-edit.html']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'impotence': self.get_impotence(),
        })
        return context

    @property
    def modif_msg(self):
        return "Modification de la fiche de données «%s» ({fields})" % self.mapping[self.kwargs['categ']]['title']

    def get_form_class(self):
        return self.mapping[self.kwargs['categ']]['form']

    def get_form_kwargs(self):
        return dict(super().get_form_kwargs(), impotence=self.get_impotence())

    def form_valid(self, form):
        maj_field = self.maj_field or '%s_maj' % self.kwargs['categ'].replace('mouvoir', 'motricite')
        setattr(form.instance, maj_field, timezone.now())
        if set(form.changed_data).intersection(CARTE_VIE_FIELDS):
            form.instance.carte_vie_maj = timezone.now()
        maju_field = maj_field + 'u'
        setattr(form.instance, maju_field, self.request.user)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('besoins-detail', args=[self.object.person.pk, self.kwargs['categ']])


class NourrirBaseView(BesoinBaseView):
    model = BesoinsAlimentaires
    template_name = 'besoins/nourrir.html'
    maj_field = 'date'

    def get_object(self):
        pers = Person.objects.get(pk=self.kwargs['pk'])
        try:
            return pers.besoinsalimentaires
        except BesoinsAlimentaires.DoesNotExist:
            return BesoinsAlimentaires(person=pers)


class NourrirDetailView(NourrirBaseView, BesoinDetailView):
    pass


class NourrirEditView(NourrirBaseView, BesoinEditView):
    def form_valid(self, form):
        regime_fields = ['regimes', 'portions', 'moyens_aux', 'complements', 'naimepas']
        regime_changes = [
            (form.fields[fname].label, form.cleaned_data[fname])
            for fname in form.changed_data if fname in regime_fields
        ]
        if regime_changes:
            recipients = NoticeSubscription.email_for_event('regimes', form.instance.person)
            if recipients:
                send_mail(
                    '[DRI] Changement liste des régimes',
                    'Madame, Monsieur,\n'
                    'Une modification du contenu de la liste des régimes a été effectuée pour le résident {nom}:\n'
                    '{modifs}\n\n'
                    'Liste à jour: {pdf}\n\n'
                    'Ceci est un message automatique, ne pas répondre.'.format(
                        nom=form.instance.person,
                        modifs='\n'.join(
                            '  - %s: %s' % (label, val) for label, val in regime_changes
                        ),
                        pdf=self.request.build_absolute_uri(
                            reverse('regimes-pdf', args=[form.instance.person.situation])
                        ),
                    ),
                    settings.DEFAULT_FROM_EMAIL,
                    recipients,
                )
        return super().form_valid(form)


class CarteDeVieView(PermissionRequiredMixin, BasePersonPDFView):
    pdf_class = CarteDeViePDF
    permission_required = 'besoins.view_recueilbesoins'


class CarteDeViePreview(PermissionRequiredMixin, BasePDFView):
    permission_required = 'besoins.view_recueilbesoins'

    def render_to_response(self, **kwargs):
        pdf_doc = CarteDeViePDF(**{'person': get_object_or_404(Person, pk=self.kwargs['pk'])})
        with NamedTemporaryFile(suffix='.pdf') as fh:
            pdf_doc.produce_carte(fh, num=self.kwargs['num'])
            fh.flush()
            command = ['pdftoppm', '-png', fh.name, fh.name[:-4]]
            result = subprocess.run(command, check=True)
            dest = fh.name.replace('.pdf', '-1.png')
            response = FileResponse(open(dest, 'rb'))
            os.remove(dest)
        return response


class CarteDeVieRecapView(PermissionRequiredMixin, OnlyAjaxMixin, TemplateView):
    template_name = 'besoins/cartedevie-recap.html'
    permission_required = 'besoins.view_recueilbesoins'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        person = get_object_or_404(Person, pk=self.kwargs['pk'])
        try:
            recueil = person.recueilbesoins
        except RecueilBesoins.DoesNotExist:
            recueil = RecueilBesoins(person=person)
        try:
            alim = person.besoinsalimentaires
        except BesoinsAlimentaires.DoesNotExist:
            alim = BesoinsAlimentaires(person=person)

        context.update({
            'person_id': self.kwargs['pk'],
            'latest_history': max(
                [dt for dt in [recueil.latest_history(), alim.latest_history()] if dt is not None]
            , default=None),
            'object_history_url': recueil.object_history_url(),
        })
        return context
