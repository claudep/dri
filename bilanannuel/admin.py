from django.contrib import admin

from .models import (
    BilanAnnuel, BilanAnnuelEntree, SuiviAccompagnement, SuiviCategorie
)


@admin.register(BilanAnnuel)
class BilanAnnuelAdmin(admin.ModelAdmin):
    list_display = ['date_bilan', 'auteur', 'person', 'libelle']


@admin.register(BilanAnnuelEntree)
class BilanAnnuelEntreeAdmin(admin.ModelAdmin):
    list_display = ['bilan', 'date', 'createur', 'secteur']
    search_fields = ['createur__last_name', 'contenu']


@admin.register(SuiviCategorie)
class SuiviCategorieAdmin(admin.ModelAdmin):
    list_display = ['nom', 'famille']
    ordering = ['nom']


admin.site.register(SuiviAccompagnement)
