from django import forms
from django.utils.text import slugify

from .models import BilanAnnuelEntree, SuiviAccompagnement, SuiviCategorie, SuiviItem


class BilanAnnuelItemForm(forms.ModelForm):
    obsolete_choices = ['musico']

    class Meta:
        model = BilanAnnuelEntree
        fields = ['secteur', 'contenu']

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.instance.pk is None:
            self.fields['secteur'].choices = [ch for ch in self.fields['secteur'].choices if ch[0] not in self.obsolete_choices]


class CategoryField(forms.ModelChoiceField):
    """Auto-create SuiviCategorie instances when user insert free text."""
    def clean(self, value):
        try:
            return super().clean(value)
        except forms.ValidationError:
            if value:
                cat = SuiviCategorie.objects.create(famille=self.queryset.first().famille, nom=value)
                return cat
            raise


class SuiviItemForm(forms.ModelForm):
    class Meta:
        model = SuiviItem
        fields = '__all__'
        field_classes = {
            'categorie': CategoryField,
        }

    def __init__(self, famille=None, data=None, **kwargs):
        super().__init__(data=data, **kwargs)
        self.fields['categorie'].queryset = SuiviCategorie.objects.filter(famille=famille)
        self.fields['categorie'].widget.attrs['class'] = 'dynamic-autocomplete'


class SuiviAccForm(forms.ModelForm):
    class Meta:
        model = SuiviAccompagnement
        fields = ['objectifs']

    def __init__(self, *args, data=None, **kwargs):
        super().__init__(*args, data=data, **kwargs)
        SuiviItemFormset = forms.inlineformset_factory(SuiviAccompagnement, SuiviItem, form=SuiviItemForm, extra=2)
        self.item_groups = []
        for val, label, extra_default in SuiviCategorie.FAMILLE_CHOICES:
            self.item_groups.append({
                'label': label,
                'famille': val,
                'formset': SuiviItemFormset(
                    instance=self.instance, queryset=SuiviItem.objects.filter(categorie__famille=val),
                    data=data, prefix=val, form_kwargs={'famille': val}
                ),
            })
            if self.instance.pk is None:
                self.item_groups[-1]['formset'].extra = extra_default

    def is_valid(self):
        return super().is_valid() and all([gr['formset'].is_valid() for gr in self.item_groups])

    def has_changed(self):
        return any([super().has_changed()] + [gr['formset'].has_changed() for gr in self.item_groups])

    def save(self, **kwargs):
        instance = super().save(**kwargs)
        for grp in self.item_groups:
            grp['formset'].save()
        return instance
