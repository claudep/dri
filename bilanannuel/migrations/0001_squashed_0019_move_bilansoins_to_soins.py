from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


def add_categories(apps, schema_editor):
    SuiviCategorie = apps.get_model('bilanannuel', 'SuiviCategorie')
    SuiviCategorie.objects.bulk_create([
        SuiviCategorie(famille='phys', nom='Boire'),
        SuiviCategorie(famille='phys', nom='Manger'),
        SuiviCategorie(famille='phys', nom='Dormir'),
        SuiviCategorie(famille='phys', nom='Respirer'),
        SuiviCategorie(famille='phys', nom='Eliminer'),
        SuiviCategorie(famille='phys', nom='Se laver'),
        SuiviCategorie(famille='phys', nom='Douleurs'),
        SuiviCategorie(famille='phys', nom='Mobilité'),
        SuiviCategorie(famille='phys', nom='Habillements'),
        SuiviCategorie(famille='phys', nom='Sommeil'),  # Temp duplic. 'Dormir' for import
        SuiviCategorie(famille='secu', nom='Mobilisation'),
        SuiviCategorie(famille='secu', nom='Positionnement'),
        SuiviCategorie(famille='secu', nom='Activ. thérapeutiques'),
        SuiviCategorie(famille='secu', nom='Activ. professionnelles'),
        SuiviCategorie(famille='secu', nom='Tâches ménagères'),
        SuiviCategorie(famille='secu', nom='Administration'),
        SuiviCategorie(famille='secu', nom='Déplacement'),
        SuiviCategorie(famille='secu', nom='Fausses-routes'),
        SuiviCategorie(famille='secu', nom='Sécurité'),
        SuiviCategorie(famille='secu', nom='AVQ'),
        SuiviCategorie(famille='secu', nom='App. auditifs'),
        SuiviCategorie(famille='secu', nom='Demandes'),
        SuiviCategorie(famille='soc', nom='Communication'),
        SuiviCategorie(famille='soc', nom='Relations sociales'),
        SuiviCategorie(famille='soc', nom='Loisirs'),
        SuiviCategorie(famille='soc', nom='Compétences sociales'),
        SuiviCategorie(famille='soc', nom='Famille'),
        SuiviCategorie(famille='soc', nom='Activités'),
        SuiviCategorie(famille='soc', nom='Humeur'),
        SuiviCategorie(famille='soc', nom='Expression'),
        SuiviCategorie(famille='soc', nom='Travail en ateliers'),
        SuiviCategorie(famille='estime', nom='Psychisme'),
        SuiviCategorie(famille='estime', nom='Cognition'),
        SuiviCategorie(famille='estime', nom='Respect'),
        SuiviCategorie(famille='estime', nom='Positionnement'),
        SuiviCategorie(famille='estime', nom='Ecriture/lecture'),
        SuiviCategorie(famille='acc', nom='Formation'),
        SuiviCategorie(famille='acc', nom='Rêve'),
        SuiviCategorie(famille='acc', nom='Personnel'),
        SuiviCategorie(famille='acc', nom='Spiritualite'),
        SuiviCategorie(famille='acc', nom='Sexualite'),
    ])


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('person', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='BilanAnnuel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date')),
                ('date_bilan', models.DateField(verbose_name='Date du bilan')),
                ('visa', models.CharField(blank=True, max_length=25, verbose_name='Visa')),
                ('libelle', models.CharField(choices=[('annuel', 'Bilan annuel'), ('interm', 'Bilan intermédiaire'), ('uat', 'Bilan UAT'), ('1mois', 'Bilan à 1 mois'), ('3mois', 'Bilan à 3 mois'), ('9mois', 'Bilan à 9 mois')], max_length=8, verbose_name='Libellé')),
                ('participants', models.TextField(blank=True, verbose_name='Participants')),
                ('createur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='person.person')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SuiviAccompagnement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date')),
                ('visa', models.CharField(blank=True, max_length=25, verbose_name='Visa')),
                ('referent1', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Référent 1')),
                ('referent2', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Référent 2')),
                ('objectifs', models.TextField(blank=True, verbose_name='Objectifs principaux')),
                ('createur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='person.person')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SuiviCategorie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('famille', models.CharField(choices=[('phys', 'Besoins physiologiques'), ('secu', 'Besoins sécuritaires'), ('soc', 'Besoins sociaux'), ('estime', 'Estime de soi'), ('acc', 'Accomplissement')], max_length=6)),
                ('nom', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='SuiviItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('independant', models.CharField(blank=True, choices=[('', '---'), ('M', 'M'), ('D', 'D')], max_length=1, verbose_name='Indépendant')),
                ('partiel', models.CharField(blank=True, choices=[('', '---'), ('M', 'M'), ('D', 'D')], max_length=1, verbose_name='Partiel.')),
                ('dependant', models.CharField(blank=True, choices=[('', '---'), ('M', 'M'), ('D', 'D')], max_length=1, verbose_name='Dépendant')),
                ('qui', models.CharField(blank=True, max_length=20, verbose_name='Qui')),
                ('actions', models.TextField(blank=True, verbose_name='Actions et commentaires')),
                ('evaluation', models.TextField(blank=True, verbose_name='Évaluation')),
                ('categorie', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='bilanannuel.suivicategorie')),
                ('suivi', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bilanannuel.suiviaccompagnement')),
            ],
        ),
        migrations.CreateModel(
            name='BilanAnnuelEntree',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(blank=True, null=True, verbose_name='Date')),
                ('secteur', models.CharField(choices=[('resident', 'Résident'), ('medecin', 'Médecin'), ('physio', 'Physiothérapie'), ('soins', 'Soins'), ('ateliers', 'Ateliers'), ('uaj', 'UAJ'), ('animation', 'Animation'), ('ergo', 'Ergothérapie'), ('musico', 'Musicothérapie'), ('neuropsy', 'Neuropsychologie'), ('objectifs', 'Objectifs globaux d’accompagnement')], max_length=10, verbose_name='Secteur')),
                ('contenu', models.TextField(verbose_name='Contenu')),
                ('bilan', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bilanannuel.bilanannuel')),
                ('createur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
            ],
        ),
        migrations.CreateModel(
            name='SuiviItemEvaluation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(verbose_name='Date')),
                ('texte', models.TextField(verbose_name='Évaluation')),
                ('createur', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='_evaluations', to='bilanannuel.suiviitem')),
            ],
            options={
                'verbose_name': 'Évaluation de suivi',
                'verbose_name_plural': 'Évaluations de suivi',
            },
        ),
        migrations.RunPython(add_categories),
    ]
