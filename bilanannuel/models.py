from datetime import date

from django.db import models
from django.urls import reverse

from person.models import Person, PersonRelatedModel, User

AUTONOME_CHOICES = (
    ('', '---'),
    ('M', 'M'),
    ('D', 'D'),
)


class BilanAnnuel(PersonRelatedModel):
    LIBELLE_CHOICES = (
        ('annuel', 'Bilan annuel'),
        ('interm', 'Bilan intermédiaire'),
        ('uat', 'Bilan UAT'),
        ('1mois', 'Bilan à 1 mois'),
        ('3mois', 'Bilan à 3 mois'),
        ('9mois', 'Bilan à 9 mois'),
    )
    libelle = models.CharField("Libellé", max_length=8, choices=LIBELLE_CHOICES)
    date_bilan = models.DateField("Date du bilan")
    participants = models.TextField("Participants", blank=True)

    _view_name_base = 'bilanannuel'
    edit_perm = 'bilanannuel.change_bilanannuel'
    list_display = ['date', 'date_bilan', 'libelle', 'auteur']

    def __str__(self):
        return "Bilan annuel pour %s (%s)" % (self.person, self.date_bilan or self.date)


class BilanAnnuelEntree(models.Model):
    class SecteurChoices(models.TextChoices):
        RESIDENT = 'resident', 'Résident'
        MEDECIN = 'medecin', 'Médecin'
        PHYSIO = 'physio', 'Physiothérapie'
        SOINS = 'soins', 'Soins'
        ATELIERS = 'ateliers', 'Ateliers'
        UAJ = 'uaj', 'UAJ'
        ANIMATION = 'animation', 'Animation'
        ERGO = 'ergo', 'Ergothérapie'
        MUSICO = 'musico', 'Musicothérapie'
        NEUROPSY = 'neuropsy', 'Neuropsychologie'
        OBJECTIFS = 'objectifs', 'Objectifs globaux d’accompagnement'

    bilan = models.ForeignKey(BilanAnnuel, on_delete=models.CASCADE)
    date = models.DateTimeField("Date", null=True, blank=True)
    createur = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Créateur')
    secteur = models.CharField("Secteur", max_length=10, choices=SecteurChoices.choices)
    contenu = models.TextField("Contenu")

    def __str__(self):
        return "Élément de bilan annuel par %s (%s)" % (self.createur, self.date)

    def can_edit(self, user):
        return user == self.createur and date.today() <= self.bilan.date_bilan


class SuiviAccompagnement(PersonRelatedModel):
    objectifs = models.TextField("Objectifs principaux", blank=True)
    # Référents au moment du suivi, figés seulement quand le suivi suivant est créé.
    referent1 = models.ForeignKey(
        User, null=True, blank=True, on_delete=models.PROTECT, related_name='+',
        verbose_name="Référent 1"
    )
    referent2 = models.ForeignKey(
        User, null=True, blank=True, on_delete=models.PROTECT, related_name='+',
        verbose_name="Référent 2"
    )

    _view_name_base = 'suiviacc'

    def __str__(self):
        return "Suivi et accompagnement pour %s (%s)" % (self.person, self.date.date() if self.date else '-')

    def get_referent1(self):
        return self.referent1 or self.person.referent1

    def get_referent2(self):
        return self.referent2 or self.person.referent2


class SuiviCategorie(models.Model):
    # Le chiffre final est une indication du nombre de ligne dans un nouveau formulaire
    FAMILLE_CHOICES = (
        ('phys', 'Besoins physiologiques', 6),
        ('secu', 'Besoins sécuritaires', 6),
        ('soc', 'Besoins sociaux', 3),
        ('estime', 'Estime de soi', 3),
        ('acc', 'Accomplissement', 2),
    )
    famille = models.CharField(max_length=6, choices=(tpl[:2] for tpl in FAMILLE_CHOICES))
    nom = models.CharField(max_length=40)

    def __str__(self):
        return self.nom


class SuiviItem(models.Model):
    suivi = models.ForeignKey(SuiviAccompagnement, on_delete=models.CASCADE)
    categorie = models.ForeignKey(SuiviCategorie, on_delete=models.PROTECT)
    independant = models.CharField("Indépendant", max_length=1, choices=AUTONOME_CHOICES, blank=True)
    partiel = models.CharField("Partiel.", max_length=1, choices=AUTONOME_CHOICES, blank=True)
    dependant = models.CharField("Dépendant", max_length=1, choices=AUTONOME_CHOICES, blank=True)
    qui = models.CharField("Qui", max_length=20, blank=True)
    actions = models.TextField("Actions et commentaires", blank=True)
    evaluation = models.TextField("Évaluation", blank=True)

    def __str__(self):
        return 'Ligne de suivi (%s - %s)' % (self.suivi, self.categorie)

    @property
    def eval_url(self):
        return reverse('suiviacc-addeval', args=[self.suivi.person_id, self.pk])

    @property
    def evaluations(self):
        return self._evaluations.order_by('-date')


class SuiviItemEvaluation(models.Model):
    item = models.ForeignKey(SuiviItem, on_delete=models.CASCADE, related_name='_evaluations')
    date = models.DateTimeField("Date")
    createur = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Créateur')
    texte = models.TextField("Évaluation")

    class Meta:
        verbose_name = "Évaluation de suivi"
        verbose_name_plural = "Évaluations de suivi"

    def __str__(self):
        return "Évaluation de suivi pour %s (%s)" % (self.item.person, self.date)
