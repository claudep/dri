from datetime import date

from django.test import TestCase
from django.urls import reverse
from django.utils.text import slugify

from person.tests import ModelBaseTextMixin
from .models import BilanAnnuel, SuiviAccompagnement, SuiviCategorie


class BilanAnnuelTests(ModelBaseTextMixin, TestCase):
    ModelClass = BilanAnnuel
    model_id = 'bilanannuel'
    custom_data = {'date_bilan': '2019-03-04', 'libelle': 'annuel'}

    def test_add_edit_bilan_subitem(self):
        bilan = BilanAnnuel.objects.create(
            person=self.person, participants='moi, lui', date_bilan=date.today()
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('bilanannuel-additem', args=[self.person.pk, bilan.pk]),
            data={'secteur': 'soins', 'contenu': 'Rapport des soins'},
            follow=True, HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertContains(response, 'Soins')
        item = bilan.bilanannuelentree_set.first()
        self.assertEqual(item.createur, self.user)
        # Edition
        response = self.client.post(
            reverse('bilanannuel-edititem', args=[self.person.pk, bilan.pk, item.pk]),
            data={'secteur': 'soins', 'contenu': 'Changement de paragraphe'},
            follow=True, HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        item.refresh_from_db()
        self.assertEqual(item.contenu, 'Changement de paragraphe')


class SuiviAccompagnementTests(ModelBaseTextMixin, TestCase):
    ModelClass = SuiviAccompagnement
    model_id = 'suiviacc'
    custom_data = {'objectifs': 'Tous'}

    def test_model_creation(self):
        self.client.force_login(self.user)
        edit_url = reverse('suiviacc-new', args=[self.person.pk])
        post_data = {
            'objectifs': 'Tous',
            'phys-TOTAL_FORMS': '2',
            'phys-INITIAL_FORMS': '0',
            'phys-0-categorie': SuiviCategorie.objects.get(nom='Manger').pk,
            'phys-0-partiel': 'M',
            'phys-1-categorie': 'Partir en voyage',  # Free text
            'phys-1-partiel': 'M',
            'secu-TOTAL_FORMS': '2',
            'secu-INITIAL_FORMS': '0',
            'soc-TOTAL_FORMS': '2',
            'soc-INITIAL_FORMS': '0',
            'soc-0-categorie': SuiviCategorie.objects.get(nom='Loisirs').pk,
            'soc-0-independant': 'D',
            'soc-0-qui': 'Moi',
            'estime-TOTAL_FORMS': '2',
            'estime-INITIAL_FORMS': '0',
            'acc-TOTAL_FORMS': '2',
            'acc-INITIAL_FORMS': '0',
        }
        response = self.client.post(edit_url, data=post_data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        if response.status_code == 200:
            self.fail('\n'.join([str(response.context['form'].errors)] + [str(gr['formset'].errors) for gr in response.context['form'].item_groups]))
        suivi = SuiviAccompagnement.objects.filter(person=self.person).first()
        self.assertQuerysetEqual(
            suivi.suiviitem_set.all().order_by('categorie__famille', 'categorie__nom'),
            [
                '<SuiviItem: Smith John, Manger>',
                '<SuiviItem: Smith John, Partir en voyage>',
                '<SuiviItem: Smith John, Loisirs>',
            ],
            transform=lambda obj: '<%s: %s, %s>' % (obj.__class__.__name__, obj.suivi.person, obj.categorie)
        )
        self.assertEqual(suivi.date.date(), date.today())
        self.assertEqual(suivi.person, self.person)
        self.assertEqual(suivi.auteur, 'Jill Martin')
        self.assertIsNotNone(self.person.referent1)
        # Référents non figés pour le dernier suivi (cf test_referents_fixed_after_new)
        self.assertIsNone(suivi.referent1)
        self.assertIsNone(suivi.referent2)

        # Re-edit
        edit_url = reverse('suiviacc-edit', args=[self.person.pk, suivi.pk])
        response = self.client.get(edit_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(
            response,
            '<input name="soc-0-qui" id="id_soc-0-qui" maxlength="20" type="text" value="Moi">',
            html=True
        )
        items = suivi.suiviitem_set.all().order_by('categorie__famille', 'categorie__nom')
        post_data.update({
            'phys-TOTAL_FORMS': '4',
            'phys-INITIAL_FORMS': '2',
            'phys-0-id': str(items[0].pk),
            'phys-0-suivi': str(suivi.pk),
            'phys-1-id': str(items[1].pk),
            'phys-1-suivi': str(suivi.pk),
            'phys-1-actions': 'Action',
            'soc-TOTAL_FORMS': '3',
            'soc-INITIAL_FORMS': '1',
            'soc-0-id': str(items[2].pk),
            'soc-0-suivi': str(suivi.pk),
        })
        response = self.client.post(edit_url, data=post_data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(suivi.suiviitem_set.count(), 3)
        line = suivi.suiviitem_set.get(categorie__nom='Partir en voyage')
        self.assertEqual(line.actions, 'Action')

    def test_referents_fixed_after_new(self):
        """Les référents ne sont figés que lorsqu'un nouveau suivi est créé."""
        suivi = SuiviAccompagnement.objects.create(person=self.person, objectifs='Quelques-uns')
        self.assertIsNone(suivi.referent1)
        self.assertIsNone(suivi.referent2)
        # Ajout nouveau suivi qui va figer les référents de l'ancien
        self.client.force_login(self.user)
        edit_url = reverse('suiviacc-new', args=[self.person.pk])
        post_data = {
            'objectifs': 'Tous',
            'phys-TOTAL_FORMS': '2',
            'phys-INITIAL_FORMS': '0',
            'secu-TOTAL_FORMS': '2',
            'secu-INITIAL_FORMS': '0',
            'soc-TOTAL_FORMS': '2',
            'soc-INITIAL_FORMS': '0',
            'estime-TOTAL_FORMS': '2',
            'estime-INITIAL_FORMS': '0',
            'acc-TOTAL_FORMS': '2',
            'acc-INITIAL_FORMS': '0',
        }
        response = self.client.post(edit_url, data=post_data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 302)
        suivi.refresh_from_db()
        self.assertEqual(suivi.referent1, self.person.referent1)
        self.assertEqual(suivi.referent2, self.person.referent2)
