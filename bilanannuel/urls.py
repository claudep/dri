from django.urls import path

from . import views

urlpatterns = [
    path('bilan/', views.BilanAnnuelListView.as_view(), name='bilanannuel-list'),
    path('bilan/<int:obj_pk>/', views.BilanAnnuelDetailView.as_view(), name='bilanannuel-detail'),
    path('bilan/<int:obj_pk>/edit/', views.BilanAnnuelEditView.as_view(), name='bilanannuel-edit'),
    path('bilan/<int:obj_pk>/additem/', views.BilanAnnuelAddItemView.as_view(),
        name='bilanannuel-additem'),
    path('bilan/<int:obj_pk>/edititem/<int:item_pk>/', views.BilanAnnuelEditItemView.as_view(),
        name='bilanannuel-edititem'),
    path('bilan/new/', views.BilanAnnuelEditView.as_view(), name='bilanannuel-new'),

    path('suivi/', views.SuiviAccListView.as_view(), name='suiviacc-list'),
    path('suivi/<int:obj_pk>/', views.SuiviAccDetailView.as_view(), name='suiviacc-detail'),
    path('suivi/<int:obj_pk>/edit/', views.SuiviAccEditView.as_view(), name='suiviacc-edit'),
    path('suivi/<int:obj_pk>/addeval/', views.SuiviAccAddEvalView.as_view(), name='suiviacc-addeval'),
    path('suivi/new/', views.SuiviAccEditView.as_view(), name='suiviacc-new'),
]
