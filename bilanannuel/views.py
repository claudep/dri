from django.db import transaction
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from django.views.generic import CreateView, UpdateView

from common.views import FormErrorsHeaderMixin
from person.models import Modification, Person
from person.views import BaseDetailView, BaseListView, BaseEditView

from .forms import BilanAnnuelItemForm, SuiviAccForm
from .models import (
    BilanAnnuel, BilanAnnuelEntree, SuiviAccompagnement, SuiviCategorie,
    SuiviItemEvaluation
)


class BilanAnnuelBaseView:
    model = BilanAnnuel


class BilanAnnuelListView(BilanAnnuelBaseView, BaseListView):
    new_form_label = 'Créer nouveau bilan Annuel'


class BilanAnnuelDetailView(BilanAnnuelBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'bilanannuel/bilanannuel-detail.html'
    modif_label = 'Modifier le bilan'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Sort items in the same order as SECTEUR_CHOICES
        sort_keys = [i[0] for i in BilanAnnuelEntree.SecteurChoices.choices]
        context['subitems'] = sorted(
            context['bilanannuel'].bilanannuelentree_set.all(),
            key=lambda item: (sort_keys.index(item.secteur), item.date or item.bilan.date)
        )
        return context


class BilanAnnuelEditView(BilanAnnuelBaseView, BaseEditView):
    template_name = 'bilanannuel/bilanannuel-detail.html'
    fields = ['participants', 'libelle', 'date_bilan']

    def form_valid(self, form):
        form.instance.date = timezone.now()
        if not form.instance.person_id:
            form.instance.person_id = self.kwargs['pk']
        if not form.instance.createur:
            form.instance.createur = self.request.user
        return super().form_valid(form)


class BilanAnnuelAddItemView(CreateView):
    form_class = BilanAnnuelItemForm
    template_name = 'base-edit.html'  #'bilanannuel/bilanannuel-additem.html'

    def form_valid(self, form):
        form.instance.bilan_id = self.kwargs['obj_pk']
        form.instance.date = timezone.now()
        form.instance.createur = self.request.user
        with transaction.atomic():
            self.object = form.save()
            msg = 'Ajout d’un paragraphe «%s» dans le bilan du %s' % (
                self.object.get_secteur_display(),
                self.object.bilan.date_bilan,
            )
            Modification.save_modification(self.object.bilan, self.request.user, msg=msg)
        return HttpResponseRedirect(self.object.bilan.get_absolute_url())


class BilanAnnuelEditItemView(UpdateView):
    model = BilanAnnuelEntree
    fields = ['secteur', 'contenu']
    template_name = 'base-edit.html'

    def get_object(self):
        return get_object_or_404(self.model, pk=self.kwargs['item_pk'])

    def get_success_url(self):
        return self.object.bilan.get_absolute_url()


class SuiviAccBaseView:
    model = SuiviAccompagnement


class SuiviAccListView(SuiviAccBaseView, BaseListView):
    new_form_label = 'Créer nouvelle fiche suivi et accompagnement'


class SuiviAccDetailView(SuiviAccBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'bilanannuel/suivi-detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['item_groups'] = []
        for val, label, _ in SuiviCategorie.FAMILLE_CHOICES:
            context['item_groups'].append({
                'label': label,
                'famille': val,
                'qs': self.object.suiviitem_set.filter(categorie__famille=val)
            })
        return context


class SuiviAccEditView(SuiviAccBaseView, BaseEditView):
    template_name = 'bilanannuel/suivi-detail.html'
    form_class = SuiviAccForm

    def form_valid(self, form):
        person = get_object_or_404(Person, pk=self.kwargs['pk'])
        # Figer les référents des anciens suivis, le cas échéant
        for suivi in person.suiviaccompagnement_set.filter(referent1__isnull=True).exclude(pk=form.instance.pk):
            suivi.referent1 = person.referent1
            suivi.referent2 = person.referent2
            suivi.save()
        return super().form_valid(form)


class SuiviAccAddEvalView(FormErrorsHeaderMixin, CreateView):
    model = SuiviItemEvaluation
    fields = ['texte']
    template_name = 'ais_additem.html'

    def form_valid(self, form):
        form.instance.item_id = self.kwargs['obj_pk']
        form.instance.date = timezone.now()
        form.instance.createur = self.request.user
        with transaction.atomic():
            self.object = form.save()
            msg = 'Ajout d’une évaluation de suivi pour %s' % (
                self.object.item.suivi.person
            )
            Modification.save_modification(self.object.item, self.request.user, created=True, msg=msg)
        return HttpResponse('OK')
