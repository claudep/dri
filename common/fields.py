from pathlib import Path

from PIL import Image

from django import forms
from django.contrib.postgres.fields import ArrayField
from django.db import models


## Model fields

class ChoiceArrayField(ArrayField):
    """
    From https://blogs.gnome.org/danni/2016/03/08/multiple-choice-using-djangos-postgres-arrayfield/
    A field that allows us to store an array of choices.

    Uses Django's postgres ArrayField and a MultipleChoiceField for its formfield.
    See also https://code.djangoproject.com/ticket/27704
    """
    widget = forms.CheckboxSelectMultiple

    def formfield(self, **kwargs):
        defaults = {
            'form_class': forms.TypedMultipleChoiceField,
            'coerce': self.base_field.to_python,
            'choices': self.base_field.choices,
            'widget': self.widget(attrs={'class': 'choicearray'}),
        }
        defaults.update(kwargs)
        # Skip our parent's formfield implementation completely as we don't
        # care for it.
        # pylint:disable=bad-super-call
        return super(ArrayField, self).formfield(**defaults)


class ImageFieldThumb(models.ImageField):
    thumb_dims = (400, 400)

    def pre_save(self, model_instance, add):
        file_ = super().pre_save(model_instance, add)
        if file_:
            self._check_thumb(file_)
        return file_

    def _check_thumb(self, img_field):
        path = Path(img_field.path)
        if not path.exists():
            return
        thumb_path = path.with_suffix(f'.thumb{path.suffix}')
        if self.storage.exists(thumb_path):
            return
        with Image.open(path) as img:
            if img.mode in ('RGBA', 'LA') and str(path).lower().endswith('jpg'):
                img = img.convert("RGB")
            img.thumbnail(self.thumb_dims, Image.ANTIALIAS)
            img.save(thumb_path)


## Form fields

class PickDateWidget(forms.DateInput):
    class Media:
        js = [
            'admin/js/core.js',
            'admin/js/calendar.js',
            # Include the Django 3.0 version with small customizations (removed today link).
            'DateTimeShortcuts.js',
        ]

    def __init__(self, attrs=None, **kwargs):
        attrs = {'class': 'vDateField', 'size': '10', **(attrs or {})}
        super().__init__(attrs=attrs, **kwargs)


class PickSplitDateTimeWidget(forms.SplitDateTimeWidget):
    def __init__(self, attrs=None):
        widgets = [
            PickDateWidget,
            forms.TimeInput(attrs={'class': 'TimeField', 'placeholder': '00:00'}, format='%H:%M')
        ]
        forms.MultiWidget.__init__(self, widgets, attrs)


class ListTextWidget(forms.TextInput):
    template_name = 'widgets/text_list.html'

    def __init__(self, data_list, attrs=None):
        self._list = data_list
        super().__init__(attrs=attrs)

    def get_context(self, name, *args):
        context = super().get_context(name, *args)
        context['widget']['attrs']['list'] = 'list__{}'.format(name)
        context['widget']['list'] = self._list
        return context
