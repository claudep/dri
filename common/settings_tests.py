import os

from .settings import *

SECRET_KEY = 'ForAutomatedTestsC9g'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME' : 'ci_test',
        'USER' : 'postgres',
        'PASSWORD': '',
        'HOST' : 'postgres',
        'PORT' : '5432',
    }
}
CACHES['default']['LOCATION'] = 'memcached:11211'
