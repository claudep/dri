import random
import re
import string
from datetime import date, datetime
from pathlib import Path

from django import template
from django.contrib.admin.templatetags.admin_list import _boolean_icon
from django.core.exceptions import FieldDoesNotExist, ObjectDoesNotExist
from django.db.models import DateField, DateTimeField
from django.db.models.fields.files import FieldFile
from django.template.defaultfilters import linebreaksbr
from django.urls import reverse
from django.utils import timezone
from django.utils.dateformat import format as dateformat
from django.utils.html import escape, format_html, format_html_join
from django.utils.safestring import mark_safe
from django.utils.text import Truncator, slugify

from common.fields import ChoiceArrayField
from common.utils import icon_url as common_icon_url
from person.models import User

register = template.Library()


@register.simple_tag(takes_context=True)
def top_tab(context, url, text):
    return format_html(
        '<p class="tablabel {}"><a href="{}">{}</a></p>',
        'active' if context['request'].path == url else '',
        url + '?f=a' if text == 'Contacts' else url,
        text
    )


@register.simple_tag(takes_context=True)
def page_url(context, page_num):
    """Build the URL in a pagination section."""
    qs = context['request'].GET.copy()
    qs['page'] = page_num
    return '{}?{}'.format(context['request'].path, qs.urlencode())


@register.filter
def icon_url(file_name):
    return common_icon_url(file_name)


@register.filter
def format_field(obj, field_name, date_fmt='d.m.Y'):
    """Get the value from an object field and show it nicely."""
    if hasattr(obj, 'get_%s_display' % field_name):
        value = getattr(obj, 'get_%s_display' % field_name)()
        if field_name == 'etat':
            value = format_html('<span class="{}">{}</span>', obj.etat, value)
    else:
        try:
            field = obj._meta.get_field(field_name)
        except FieldDoesNotExist:
            field = None
        if isinstance(field, ChoiceArrayField):
            values = getattr(obj, field_name) or []
            return format_html_join(
                " ", '<span class="{}selected">{} {}</span>',
                (('' if key in values else 'un', '☑' if key in values else '☐', verb)
                for key, verb in field.base_field.choices)
            )
        else:
            value = getattr(obj, field_name, None)
        if isinstance(value, (date, datetime)):
            value = dateformat(
                timezone.localtime(value) if isinstance(value, datetime) else value,
                date_fmt
            )
        elif value is True:
            value = 'Oui'
        elif value is False:
            value = 'Non'
        elif callable(value):
            if hasattr(value, 'all'):
                value = ', '.join(str(v) for v in value.all())
            else:
                value = value()
        elif isinstance(value, User):
            value = value.get_full_name()
        elif isinstance(value, str) and '\n' in value:
            value = mark_safe('<br>'.join(escape(value).split('\n')))
        elif isinstance(value, FieldFile):
            value = format_html('<a href="{}">{}</a>', value.url, value.name.split('/')[-1])
        elif hasattr(value, 'get_absolute_url'):
            value = format_html('<a href="{}">{}</a>', value.get_absolute_url(), str(value))
    if value in (None, ''):
        value = mark_safe('<span class="empty">-</span>')
    return value


@register.filter(is_safe=True)
def field_in_list(obj, field_name):
    """
    Display an object field name from the list template.
    The field_name can be followed by:
        h: show hour for datetime field
        w<num>: truncate the value to <num> words
        files: display object attachments as floating div
    """
    fname, *args = field_name.split(':')
    value = format_field(obj, fname, date_fmt='d.m.Y H:i' if 'h' in args else 'd.m.Y')
    trunc_len = 80
    if any(a.startswith('w') for a in args):
        trunc_len = int([a for a in args if a.startswith('w')][0][1:])
    if '<br><br>' in str(value):
        # Prevent multiple successive line breaks in list mode
        value = re.sub('<br>(<br>)+', '<br>', value)
    content = Truncator(value).words(trunc_len, truncate='\xa0…\xa0')
    was_truncated = isinstance(value, str) and content.endswith('\xa0…\xa0')
    if was_truncated:
        content = (
            f'<div class="text-partial">{content}'
            f' <span class="button-discrete text-exp" title="Afficher le texte entier">+</span></div>'
            f'<div hidden class="text-full">{value}</div>'
        )
    result = mark_safe(content)
    if 'files' in args:
        result = mark_safe('%s %s' % (fichiers_icons(obj), result))
    if 'modif_by' in args and getattr(obj, 'latest_history', None):
        result = mark_safe('%s %s' % (result, '<br><em>par %s</em>' % obj.latest_history.get_user()))
    return result


@register.inclusion_tag('form_head.html', takes_context=True)
def form_head(context, ajax=True):
    return {'action': context['request'].get_full_path(), 'form': context['form'], 'ajax': ajax}


@register.simple_tag(takes_context=True)
def display_field(context, field_name, *args, unit=None):
    """
    Display either a field value or a field form from a detail or edit template.
    args can be:
     - keep-ws: apply linebreaksbr to value
     - as_tr: display label and field in a <tr><th><td> line
     - as_div: display label and field in a <div>-style wrapper
     - unwrapped: prevent wrapping in <div class="field-content"></div>
     - inline: add an 'inline' class to the wrapper div
    """
    is_form = 'form' in context and field_name in context['form'].fields
    if is_form:
        bfield = context['form'][field_name]
        obj = context['form'].instance
        value = format_html('{}{}', bfield.errors, bfield) if bfield.errors else str(bfield)
    else:
        obj = context['object']
        value = format_field(obj, field_name)
        if 'keep-ws' in args:
            value = linebreaksbr(value)
    if 'as_tr' in args or 'as_div' in args:
        if 'as_tr' in args:
            template = '<tr><th>{}</th><td><div class="field-content">{}</div></td></tr>'
        else:
            template = '<div class="field-header">{}</div> <div class="field-content">{}</div>'
        if is_form:
            label = context['form'][field_name].label_tag()
        else:
            label = ''
            try:
                field = obj._meta.get_field(field_name)
            except FieldDoesNotExist:
                field = None
            else:
                label = field.verbose_name
        return format_html(template, label, value)
    elif not is_form and 'unwrapped' not in args:
        return format_html(
            '<div class="field-content{}">{} {}</div>',
            ' inline' if 'inline' in args else '', value, unit or ''
        )
    return value


@register.simple_tag(takes_context=True)
def display_label(context, field_name, *args):
    obj = context['form'].instance if 'form' in context else context['object']
    field = obj._meta.get_field(field_name)
    return format_html('<label for="id_{}">{}</label>', field_name, field.verbose_name)


@register.simple_tag(takes_context=True)
def display_impotence(context, field_name, *args):
    is_form = 'form' in context
    impotence = context['impotence']
    label = impotence._meta.get_field(field_name).verbose_name
    if 'form' in context:
        return format_html(
            '<tr><td>{label}</td><td>{boole}</td><td>{quand}</td><td>{aide}</td><td>{precision}</td></tr>',
            label=label, boole=context['form'].impotence_form[field_name],
            quand=context['form'].impotence_form['%s_quand' % field_name],
            aide=context['form'].impotence_form['%s_aide' % field_name],
            precision=context['form'].impotence_form['%s_precisions' % field_name],
        )
    else:
        if getattr(impotence, field_name):
            return format_html(
                '<tr><td>{label}</td><td>Oui</td><td>{quand}</td><td>{aide}</td><td>{precision}</td></tr>',
                label=label, quand=getattr(impotence, '%s_quand' % field_name),
                aide=getattr(impotence, '%s_aide' % field_name), precision=getattr(impotence, '%s_precisions' % field_name)
            )
        else:
            return format_html('<tr><td>{label}</td><td>Non</td></tr>', label=label)


@register.filter
def ftype(ffield):
    return ffield.field.__class__.__name__


@register.filter
def is_date(instance, field_name):
    try:
        return (
            field_name in ('date_admission', 'date_depart') or
            isinstance(instance._meta.get_field(field_name), DateField)
        )
    except FieldDoesNotExist:
        return False


@register.filter
def strip_suffix(field_name):
    return field_name.split(':')[0]


@register.filter(is_safe=True)
def strip_colon(label):
    return label.replace(':', '')


@register.filter
def sort_format(instance, field_name):
    """Output a data-sort attribute depending on the field type."""
    field_name = field_name.split(':')[0]
    try:
        fmeta = instance._meta.get_field(field_name)
    except FieldDoesNotExist:
        return ''
    else:
        if isinstance(fmeta, DateTimeField):
            # Checking first DateTimeField is important as it is also DateField instance
            value = getattr(instance, field_name)
            if value:
                return format_html(' data-sort="{}"', dateformat(value, 'Y-m-d-H-i'))
        elif isinstance(fmeta, DateField):
            value = getattr(instance, field_name)
            if value:
                return format_html(' data-sort="{}"', dateformat(value, 'Y-m-d'))
        return ''


@register.filter
def short_visa(user):
    if isinstance(user, User):
        return format_html('<span title="{}">{}</span>', user.get_full_name(), user.initiales)
    return user or '-'


@register.filter
def full_visa(user):
    return user.get_full_name() if isinstance(user, User) else (user or '-')


@register.filter
def user_classes(user):
    # Using all as probably prefetch_related
    names = [g.name for g in user.groups.all()]
    return ' '.join([
        'FHMN' if 'FHMN' in names else '', 'FHNE' if 'FHNE' in names else '',
        slugify(user.secteur) if user.secteur else ''
    ])


@register.filter
def has_group(user, group_name):
    return group_name in user.groups.values_list('name', flat=True)


@register.filter
def bool_as_x(value):
    return 'X' if value else ''


@register.filter
def bool_as_icon(value):
    return _boolean_icon(value)


@register.filter
def bool_as_icon_true(value):
    if value is True:
        return _boolean_icon(value)
    return ''


@register.filter
def duration_as_hours(duration):
    if duration is None:
        return ''
    seconds = int(duration.total_seconds())
    hours = seconds // 3600
    minutes = (seconds % 3600) // 60
    return '{}h{:02d}'.format(hours, minutes)


@register.filter
def get_value(instance, field_name):
    if field_name in ['demande_admission', 'bilan_independance']:
        value = getattr(instance, field_name)
        if value:
            return format_html('<a href="{}">{}</a>', value.get_absolute_url(), _boolean_icon(True))
        return _boolean_icon(False)

    if ':' in field_name:
        field_name = field_name.split(':')[0]
    val = instance
    # Allow access to 'onetonemodel.field'
    for part in field_name.split('.'):
        if hasattr(val, 'get_%s_display' % field_name):
            return getattr(val, 'get_%s_display' % field_name)()
        try:
            val = getattr(val, part)
        except ObjectDoesNotExist:
            val = None
            break
    if val is None:
        return '-'
    elif val is True or val is False:
        return 'Oui' if val else 'Non'
    return val


@register.filter
def can_edit(obj, user):
    return obj.can_edit(user)


@register.simple_tag(takes_context=True)
def form_foot(context):
    if 'form' in context:
        label = "Enregistrer" if context['form'].instance.pk else "Créer"
        return format_html(
            '<div class="form-buttons"><button>{}</button> <button class="cancel">Annuler</button></div>\n'
            '</form>',
            label
        )
    return ''


@register.filter
def view_url(tabitem, pers_pk):
    return reverse(tabitem['view_name'], args=[pers_pk] + tabitem.get('view_args', []))


@register.filter
def as_tr(form_field):
    return format_html(
        '<tr><td class="label">{}</td><td>{}</td></tr>', form_field.label_tag(), form_field
    )


def fichiers_icons(obj):
    """Render obj attachments as file icons in a div."""
    if not hasattr(obj, 'fichiers'):
        return ''
    fichiers = [f for f in obj.fichiers.all()]  # Hopefully prefetched
    if not fichiers:
        return ''
    return mark_safe('<div class="attachments">%s</div>' % (
        format_html_join(
            " ", '<a href="{}" class="{}" target="{}"><img class="ficon" src="{}" title="{}"></a>',
            ((f.fichier.url, f.is_image and 'image' or '', f.is_image and '' or '_béank', f.icon_url(), f.title())
            for f in fichiers)
        )
    ))


@register.simple_tag
def random_str(length):
    return ''.join(random.choices(string.ascii_lowercase, k=length))


@register.filter
def thumbnail(photo):
    path = Path(photo.url)
    return path.with_suffix(f'.thumb{path.suffix}')
