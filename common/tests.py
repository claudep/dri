from datetime import date, datetime, timedelta

from django.contrib import auth
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils.timezone import make_aware

from common.templatetags import fhutils
from person.models import Person, User


class LoginTests(TestCase):
    def test_login_protected(self):
        response = self.client.get(reverse('login'))
        self.assertContains(response, '<input type="submit" value="Se connecter">', html=True)
        response = self.client.get(reverse('login'), REMOTE_ADDR='100.100.1.1')
        self.assertContains(response, 'vous ne pouvez pas vous connecter')
        response = self.client.get(reverse('login'), REMOTE_ADDR='2a02:120b:2c7e:28b0:f994:86ae:3229:495a')
        self.assertContains(response, 'vous ne pouvez pas vous connecter')

    def test_login_expired(self):
        user = User.objects.create_user(username='jill', email='j@example.org', password='blabla')
        response = self.client.post(reverse('login'), data={'username': 'jill', 'password': 'blabla'})
        auth.get_user(self.client)
        self.assertTrue(user.is_authenticated)
        self.client.logout()

        user.expiration = '2018-04-04'
        user.save()
        response = self.client.post(reverse('login'), data={'username': 'jill', 'password': 'blabla'})
        self.assertContains(response, "Désolé, votre compte a expiré")

        user.expiration = date.today() + timedelta(days=2)
        user.save()
        response = self.client.post(reverse('login'), data={'username': 'jill', 'password': 'blabla'})
        auth.get_user(self.client)
        self.assertTrue(user.is_authenticated)


class OtherTests(TestCase):
    def test_field_in_list(self):
        pers = Person.objects.create(
            nom='Smith', prenom='John', situation='fhmn_int', date_naissance=date(1986, 2, 12),
            updated=make_aware(datetime(2019, 10, 4, 12, 34)),
            origine='Un texte un peu     plus long que les autres',
        )
        self.assertEqual(fhutils.field_in_list(pers, 'nom'), 'Smith')
        self.assertEqual(fhutils.field_in_list(pers, 'date_naissance'), '12.02.1986')
        self.assertEqual(fhutils.field_in_list(pers, 'updated'), '04.10.2019')
        self.assertEqual(fhutils.field_in_list(pers, 'updated:h'), '04.10.2019 12:34')
        self.assertEqual(
            fhutils.field_in_list(pers, 'origine'),
            'Un texte un peu plus long que les autres'
        )
        self.assertHTMLEqual(
            fhutils.field_in_list(pers, 'origine:w3'),
            '<div class="text-partial">Un texte un …  <span class="button-discrete text-exp" '
            'title="Afficher le texte entier">+</span></div>'
            '<div hidden class="text-full">Un texte un peu     plus long que les autres</div>'
        )
