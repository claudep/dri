import os

from django.templatetags.static import static


IMAGE_EXTS = ['.jpg', '.jpeg', '.png', '.tif', '.tiff', '.gif']


def icon_url(file_name):
    ext = os.path.splitext(file_name)[1].lower()
    icon = 'master'  # default
    if ext in IMAGE_EXTS:
        icon = 'image'
    elif ext == '.pdf':
        icon = 'pdf'
    elif ext in ('.xls', '.xlsx', '.ods', '.csv'):
        icon = 'tsv'
    elif ext == '.odt':
        icon = 'odt'
    elif ext in ('.doc', '.docx'):
        icon = 'docx'
    return static(f'ficons/{icon}.svg')


def is_ajax(request):
    return request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'
