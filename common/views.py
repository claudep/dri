from io import BytesIO
from ipaddress import ip_address
from tempfile import NamedTemporaryFile

from openpyxl import Workbook
from openpyxl.cell import WriteOnlyCell
from openpyxl.styles import Font

from django.apps import apps
from django.conf import settings
from django.contrib.admindocs.views import ModelDetailView, ModelIndexView
from django.contrib.auth.forms import AuthenticationForm as DjangoAuthForm
from django.contrib.auth.views import LoginView as AuthLoginView
from django.core.exceptions import ValidationError
from django.http import FileResponse, HttpResponse
from django.utils.safestring import mark_safe
from django.utils.text import slugify
from django.views.generic import UpdateView, View


class AuthenticationForm(DjangoAuthForm):
        def confirm_login_allowed(self, user):
            if user.has_expired():
                raise ValidationError("Désolé, votre compte a expiré.")
            # super() is checking the is_active field
            return super().confirm_login_allowed(user)


class LoginView(AuthLoginView):
    form_class = AuthenticationForm
    template_name = 'login.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ip = ip_address(self.request.META.get('REMOTE_ADDR'))
        if any(ip in net for net in settings.AUTHORIZED_NETWORKS):
            return context
        context['form'] = ''
        context['errornote'] = mark_safe(
            'Désolé, vous ne pouvez pas vous connecter depuis un poste en '
            'dehors du réseau de Foyer Handicap.<br><br>'
            '<span class="ipaddr">Votre adresse IP: %s</span>' % ip
        )
        return context


class DriModelIndexView(ModelIndexView):
    template_name = 'documentation/model_index.html'

    def dispatch(self, request, *args, **kwargs):
        # Short-circuit parent views decorated with staff_member_required
        return self.get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['models'] = [m._meta for m in apps.get_models() if not m._meta.app_config.name.startswith('django')]
        return context


class DriModelDetailView(ModelDetailView):
    template_name = 'documentation/model_detail.html'

    def dispatch(self, request, *args, **kwargs):
        # Short-circuit parent views decorated with staff_member_required
        return self.get(request, *args, **kwargs)


class FormErrorsHeaderMixin:
    """Used for AJAX response to signal form redisplay to the callback."""
    def form_invalid(self, form):
        response = super().form_invalid(form)
        response["X-FormHasErrors"] = 'true'
        return response


class CreateUpdateView(UpdateView):
    """Mix generic Create and Update views."""
    is_create = False

    def get_object(self):
        return None if self.is_create else super().get_object()


class BaseFileExportView(View):
    def get(self, request, *args, **kwargs):
        return self.render_to_response()


class BasePDFView(BaseFileExportView):
    pdf_class = None

    def pdf_context(self):
        return {}

    def render_to_response(self, **kwargs):
        pdf_doc = self.pdf_class(**self.pdf_context())
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % slugify(pdf_doc.title)
        pdf_doc.produce_pdf(response)
        return response


class BaseXLSView(BaseFileExportView):
    sheet_name = 'None'
    col_widths = []

    def get_headers(self):
        return []

    def date_cell(self, date_val, strip_time=False):
        if not date_val:
            return ''
        cell = WriteOnlyCell(self.ws, value=date_val.date() if strip_time else date_val)
        cell.number_format = 'dd.mm.yy'
        return cell

    def write_line(self, line):
        raise NotImplementedError
        #self.ws.append([])

    def get_filename(self):
        return 'export.xlsx'

    def render_to_response(self, **kwargs):
        self.wb = Workbook(write_only=True)
        self.ws = self.wb.create_sheet(self.sheet_name)
        self.bold = Font(bold=True)
        for idx, width in enumerate(self.col_widths):
            self.ws.column_dimensions[chr(65 + idx)].width = width
        # Write header
        head_cells = [WriteOnlyCell(self.ws, value=h) for h in self.get_headers()]
        for cell in head_cells:
            cell.font = self.bold
        self.ws.append(head_cells)
        # Write main content
        for line in self.get_queryset():
            self.write_line(line)
        with NamedTemporaryFile() as tmp:
            self.wb.save(tmp.name)
            tmp.seek(0)
            stream = BytesIO(tmp.read())
        return FileResponse(stream, filename=self.get_filename(), as_attachment=True)
