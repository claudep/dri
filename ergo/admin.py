from django.contrib import admin

from .models import ActionErgo, BilanErgo, MoyenAuxiliaire, MoyenAuxiliaireType, ObservationErgo


@admin.register(BilanErgo)
class BilanErgoAdmin(admin.ModelAdmin):
    list_display = ['person', 'date_bilan', 'createur']


admin.site.register(ActionErgo)
admin.site.register(MoyenAuxiliaire)
admin.site.register(MoyenAuxiliaireType)
admin.site.register(ObservationErgo)
