from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('person', '__first__'),
        ('ergo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MoyenAuxiliaireType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=50, unique=True, verbose_name='Type de moyen auxiliaire')),
            ],
        ),
        migrations.CreateModel(
            name='MoyenAuxiliaire',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(blank=True, null=True, verbose_name='Date')),
                ('date_octroi', models.DateField(blank=True, null=True, verbose_name="Date d'octroi")),
                ('decision_ai', models.CharField(blank=True, max_length=30, verbose_name='Décision AI')),
                ('financement', models.CharField(blank=True, max_length=50, verbose_name='Financement')),
                ('fournisseur', models.TextField(blank=True, verbose_name='Fournisseur')),
                ('description', models.TextField(blank=True, verbose_name='Description')),
                ('reparateur', models.TextField(blank=True, verbose_name='Réparateur')),
                ('remarques', models.TextField(blank=True, verbose_name='Remarques')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='person.Person')),
                ('type_m', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='ergo.MoyenAuxiliaireType', verbose_name="Type de moyen")),
            ],
        ),
        migrations.DeleteModel(
            name='MoyenAuxiliaireImport',
        ),
    ]
