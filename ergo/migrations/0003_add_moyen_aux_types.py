from django.db import migrations

def add_types(apps, schema_editor):
    MoyenAuxiliaireType = apps.get_model('ergo', 'MoyenAuxiliaireType')
    MoyenAuxiliaireType.objects.bulk_create([
        MoyenAuxiliaireType(nom='Attelles'),
        MoyenAuxiliaireType(nom='Autres 1'),
        MoyenAuxiliaireType(nom='Autres 2'),
        MoyenAuxiliaireType(nom='Autres 3'),
        MoyenAuxiliaireType(nom='Autres activités'),
        MoyenAuxiliaireType(nom='Communication'),
        MoyenAuxiliaireType(nom='Contrôle environnement'),
        MoyenAuxiliaireType(nom='FRE'),
        MoyenAuxiliaireType(nom='FRE coussins housse'),
        MoyenAuxiliaireType(nom='FRE positionnement'),
        MoyenAuxiliaireType(nom='FRM'),
        MoyenAuxiliaireType(nom='FRM coussins housses'),
        MoyenAuxiliaireType(nom='FRM positionnement'),
        MoyenAuxiliaireType(nom='Moy aux déplacement'),
        MoyenAuxiliaireType(nom='Moy aux Visuels 1'),
        MoyenAuxiliaireType(nom='Moy aux Visuels 2'),
        MoyenAuxiliaireType(nom='Ordinateur'),
        MoyenAuxiliaireType(nom='Repas'),
        MoyenAuxiliaireType(nom='Toilette'),
        MoyenAuxiliaireType(nom='Transfert'),
    ])


class Migration(migrations.Migration):

    dependencies = [
        ('ergo', '0002_moyen_aux_struct'),
    ]

    operations = [migrations.RunPython(add_types)]
