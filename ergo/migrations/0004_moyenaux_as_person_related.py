from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ergo', '0003_add_moyen_aux_types'),
    ]

    operations = [
        migrations.AddField(
            model_name='moyenauxiliaire',
            name='createur',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur'),
        ),
        migrations.AddField(
            model_name='moyenauxiliaire',
            name='visa',
            field=models.CharField(blank=True, max_length=25, verbose_name='Visa'),
        ),
    ]
