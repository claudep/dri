from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ergo', '0004_moyenaux_as_person_related'),
    ]

    operations = [
        migrations.AddField(
            model_name='moyenauxiliaire',
            name='date_fin',
            field=models.DateField(blank=True, null=True, verbose_name='Fin d’utilisation',
            help_text="Quand la date de fin est dans le passé, le moyen passe automatiquement dans les archives"),
        ),
    ]
