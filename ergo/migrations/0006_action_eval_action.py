from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ergo', '0005_moyenauxiliaire_date_fin'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActionAction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('action', models.ForeignKey(on_delete=models.deletion.CASCADE, to='ergo.ActionErgo')),
                ('createur', models.ForeignKey(on_delete=models.deletion.PROTECT, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
                ('date', models.DateTimeField(verbose_name='Date')),
                ('texte', models.TextField(verbose_name='Action')),
            ],
            options={
                'verbose_name': "Action d'action",
                'verbose_name_plural': "Action d'action",
            },
        ),
        migrations.CreateModel(
            name='ActionEval',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('action', models.ForeignKey(on_delete=models.deletion.CASCADE, to='ergo.ActionErgo')),
                ('createur', models.ForeignKey(on_delete=models.deletion.PROTECT, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
                ('date', models.DateTimeField(verbose_name='Date')),
                ('texte', models.TextField(verbose_name='Évaluation')),
            ],
            options={
                'verbose_name': "Évaluation d'action",
                'verbose_name_plural': "Évaluations d'action",
            },
        ),
        migrations.AlterModelOptions(
            name='actionergo',
            options={'verbose_name': 'Action ergo', 'verbose_name_plural': 'Actions ergo'},
        ),
        migrations.RenameField(
            model_name='actionergo',
            old_name='actions',
            new_name='actions_old',
        ),
        migrations.RenameField(
            model_name='actionergo',
            old_name='evaluations',
            new_name='evaluations_old',
        ),
    ]
