from datetime import date

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.urls import reverse

from person.models import ActionBase, Attachment, Person, PersonRelatedModel, User


class ActionErgo(ActionBase):
    class Meta:
        verbose_name = "Action ergo"
        verbose_name_plural = "Actions ergo"

    _view_name_base = 'actionergo'
    edit_perm = 'ergo.change_actionergo'

    def __str__(self):
        return "Action ergo pour %s (%s)" % (self.person, self.date)


class ActionEval(models.Model):
    action = models.ForeignKey(ActionErgo, on_delete=models.CASCADE)
    date = models.DateTimeField("Date")
    createur = models.ForeignKey(
        User, on_delete=models.PROTECT, verbose_name='Créateur', related_name='+'
    )
    texte = models.TextField("Évaluation")

    class Meta:
        verbose_name = "Évaluation d'action"
        verbose_name_plural = "Évaluations d'action"

    def __str__(self):
        return "Évaluation d'action pour %s (%s)" % (self.action.person, self.date)


class ActionAction(models.Model):
    action = models.ForeignKey(ActionErgo, on_delete=models.CASCADE)
    date = models.DateTimeField("Date")
    createur = models.ForeignKey(
        User, on_delete=models.PROTECT, verbose_name='Créateur', related_name='+'
    )
    texte = models.TextField("Action")

    class Meta:
        verbose_name = "Action d'action"
        verbose_name_plural = "Action d'action"

    def __str__(self):
        return "Action d'action pour %s (%s)" % (self.action.person, self.date)


class BilanErgo(PersonRelatedModel):
    date_bilan = models.DateField("Date du bilan")
    referent = models.CharField("Référent", max_length=50, blank=True)
    horaires = models.TextField("Cadre et horaires", blank=True)
    symptomes = models.TextField("Symptômes principaux", blank=True)
    rappel_objectifs = models.TextField("Rappel des objectifs", blank=True)
    resultats = models.TextField("Résultats", blank=True)
    objectifs = models.TextField("Nouveaux objectifs", blank=True)
    commentaires = models.TextField("Commentaires et propositions", blank=True)

    _view_name_base = 'bilanergo'
    list_display = ['date_bilan', 'auteur']

    def __str__(self):
        return "Bilan ergo pour %s (%s)" % (self.person, self.date)


class ObservationErgo(PersonRelatedModel):
    # TODO: liste de valeurs sans contrainte
    rubrique = models.CharField("Rubrique", max_length=100, blank=True)
    observation = models.TextField("Observation", blank=True)
    intervenant = models.CharField("Intervenant", max_length=50, blank=True)
    fichiers = GenericRelation(Attachment)

    _view_name_base = 'obsergo'
    list_display = ['date', 'rubrique', 'observation:w80:files', 'intervenant', 'auteur']

    def __str__(self):
        return "Observation ergo pour %s (%s)" % (self.person, self.date)

    def can_edit(self, user):
        return user.is_superuser or (user == self.createur and self.date.date() == date.today())


class MoyenAuxiliaireType(models.Model):
    nom = models.CharField("Type de moyen auxiliaire", max_length=50, unique=True)

    def __str__(self):
        return self.nom


class MoyenAuxiliaire(PersonRelatedModel):
    type_m = models.ForeignKey(MoyenAuxiliaireType, on_delete=models.PROTECT, verbose_name="Type de moyen")
    date_octroi = models.DateField("Date d'octroi", null=True, blank=True)
    decision_ai = models.CharField("Décision AI", max_length=30, blank=True)
    financement = models.CharField("Financement", max_length=50, blank=True)
    # Evaluer utilisation de contact
    fournisseur = models.TextField("Fournisseur", blank=True)
    description = models.TextField("Description", blank=True)
    # Evaluer utilisation de contact
    reparateur = models.TextField("Réparateur", blank=True)
    remarques = models.TextField("Remarques", blank=True)
    date_fin = models.DateField(
        "Fin d’utilisation", null=True, blank=True,
        help_text="Quand la date de fin est dans le passé, le moyen passe automatiquement dans les archives"
    )

    _view_name_base = 'moyensaux'
    list_display = [
        'type_m', 'date_octroi', 'description', 'fournisseur', 'reparateur', 'financement',
        'decision_ai', 'remarques'
    ]

    def __str__(self):
        return "Moyen auxiliaire '%s' pour %s (%s)" % (self.type_m, self.person, self.date)

    def can_edit(self, user):
        return user.has_perm('ergo.change_moyenauxiliaire')
