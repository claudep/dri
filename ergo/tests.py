from datetime import date

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse

from person.models import Contact
from person.tests import ModelBaseTextMixin
from .models import ActionErgo, BilanErgo, MoyenAuxiliaire, MoyenAuxiliaireType, ObservationErgo


class ActionErgoTests(ModelBaseTextMixin, TestCase):
    ModelClass = ActionErgo
    model_id = 'actionergo'
    custom_data = {'problemes': "Des problèmes…"}

    def test_add_action_evaluation(self):
        act = ActionErgo.objects.create(
            person=self.person, problemes="Des problèmes…", objectifs="Des objectifs…"
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('actionergo-detail', args=[self.person.pk, act.pk]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        addeval_url = reverse('actionergo-addeval', args=[self.person.pk, act.pk])
        addaction_url = reverse('actionergo-addaction', args=[self.person.pk, act.pk])
        self.assertContains(
            response,
            '<img class="icon-standalone" src="/static/img/add.svg" '
            'data-url="%s" onclick="addDynamicRow(this, \'evaluations\')" '
            'title="Ajouter une évaluation">' % addeval_url,
            html=True
        )
        self.assertContains(
            response,
            '<img class="icon-standalone" src="/static/img/add.svg" '
            'data-url="%s" onclick="addDynamicRow(this, \'actions\')" '
            'title="Ajouter une action">' % addaction_url,
            html=True
        )
        response = self.client.post(addeval_url, data={'texte': 'Une évaluation'})
        self.assertEqual(act.evaluations.first().texte, 'Une évaluation')
        response = self.client.post(addaction_url, data={'texte': 'Une action'})
        self.assertEqual(act.actions.first().texte, 'Une action')


class BilanErgoTests(ModelBaseTextMixin, TestCase):
    ModelClass = BilanErgo
    model_id = 'bilanergo'
    custom_data = {'date_bilan': '2019-03-04', 'symptomes': "Des symptômes…"}

    def test_initial_referent(self):
        self.client.force_login(self.user)
        url = reverse('%s-new' % self.model_id, args=[self.person.pk])
        self.person.repondant_ergo = Contact.objects.create(nom='Ergo', prenom='Jeanne')
        self.person.repondant_ergo_uaj = Contact.objects.create(nom='UAJ', prenom='Hervé')
        self.person.save()
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(
            response,
            '<input type="text" name="referent" value="Ergo Jeanne - UAJ Hervé" maxlength="50" id="id_referent">',
            html=True
        )


class MoyenAuxiliaireTests(ModelBaseTextMixin, TestCase):
    ModelClass = MoyenAuxiliaire
    model_id = 'moyensaux'

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user.user_permissions.add(Permission.objects.get(codename='change_moyenauxiliaire'))

    @property
    def custom_data(self):
        return {'decision_ai': "No 45634", 'type_m': MoyenAuxiliaireType.objects.first()}

    def test_model_list(self):
        self.client.force_login(self.user)
        new_obj = MoyenAuxiliaire.objects.create(person=self.person, **self.custom_data)
        # Archived item
        arch_obj = MoyenAuxiliaire.objects.create(
            person=self.person, date_fin=date(2017, 12, 12), **self.custom_data
        )
        list_url = reverse('moyensaux-list', args=[self.person.pk])
        response = self.client.get(list_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(
            response,
            '<a class="AjaxLink" href="%s">' % reverse('%s-detail' % self.model_id, args=[self.person.pk, new_obj.pk]),
        )
        self.assertEqual(len(response.context['object_list']), 1)
        response = self.client.get(list_url + '?view=archives', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertEqual(response.context['object_list'][0], arch_obj)


class ObservationErgoTests(ModelBaseTextMixin, TestCase):
    ModelClass = ObservationErgo
    model_id = 'obsergo'
    custom_data = {'observation': "Une observation…"}
