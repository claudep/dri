from django.urls import path

from person.views import ActionAddItemView, ActionCloseView
from . import views

urlpatterns = [
    path('actionergo/', views.ActionErgoListView.as_view(), name='actionergo-list'),
    path('actionergo/<int:obj_pk>/', views.ActionErgoDetailView.as_view(), name='actionergo-detail'),
    path('actionergo/<int:obj_pk>/edit/', views.ActionErgoEditView.as_view(), name='actionergo-edit'),
    path('actionergo/new/', views.ActionErgoEditView.as_view(), name='actionergo-new'),
    path('actionergo/<int:obj_pk>/addeval/', ActionAddItemView.as_view(_model='ergo.ActionEval'),
        name='actionergo-addeval'),
    path('actionergo/<int:obj_pk>/addaction/', ActionAddItemView.as_view(_model='ergo.ActionAction'),
        name='actionergo-addaction'),
    path('actionergo/<int:obj_pk>/close/', ActionCloseView.as_view(_model='ergo.ActionErgo'),
        name='actionergo-closeaction'),

    path('bilanergo/', views.BilanErgoListView.as_view(), name='bilanergo-list'),
    path('bilanergo/<int:obj_pk>/', views.BilanErgoDetailView.as_view(), name='bilanergo-detail'),
    path('bilanergo/<int:obj_pk>/edit/', views.BilanErgoEditView.as_view(), name='bilanergo-edit'),
    path('bilanergo/new/', views.BilanErgoEditView.as_view(), name='bilanergo-new'),

    path('obsergo/', views.ObservationErgoListView.as_view(), name='obsergo-list'),
    path('obsergo/<int:obj_pk>/', views.ObservationErgoDetailView.as_view(), name='obsergo-detail'),
    path('obsergo/<int:obj_pk>/edit/', views.ObservationErgoEditView.as_view(), name='obsergo-edit'),
    path('obsergo/new/', views.ObservationErgoEditView.as_view(), name='obsergo-new'),

    path('moyensaux/', views.MoyensAuxListView.as_view(), name='moyensaux-list'),
    path('moyensaux/<int:obj_pk>/', views.MoyensAuxDetailView.as_view(), name='moyensaux-detail'),
    path('moyensaux/<int:obj_pk>/edit/', views.MoyensAuxEditView.as_view(), name='moyensaux-edit'),
    path('moyensaux/new/', views.MoyensAuxEditView.as_view(), name='moyensaux-new'),
]
