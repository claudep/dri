from django.urls import reverse

from person.models import Person
from person.views import (
    BaseActionEditView, BaseActionDetailView, BaseDetailView, BaseListView, BaseEditView
)

from .models import ActionErgo, BilanErgo, ObservationErgo, MoyenAuxiliaire


class ActionErgoBaseView:
    model = ActionErgo
    action_title = 'Actions ergo'


class ActionErgoListView(ActionErgoBaseView, BaseListView):
    new_form_label = 'Créer nouvelle action ergo'


class ActionErgoDetailView(ActionErgoBaseView, BaseActionDetailView):
    pass


class ActionErgoEditView(ActionErgoBaseView, BaseActionEditView):
    template_name = 'base-action.html'


class BilanErgoBaseView:
    model = BilanErgo


class BilanErgoListView(BilanErgoBaseView, BaseListView):
    new_form_label = 'Créer nouvelle fiche bilan ergo - UAJ'


class BilanErgoDetailView(BilanErgoBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class BilanErgoEditView(BilanErgoBaseView, BaseEditView):
    def get_initial(self):
        initial = super().get_initial()
        if self.is_create:
            person = Person.objects.get(pk=initial['person'])
            initial['referent'] = ' - '.join(
                str(pers) for pers in [person.repondant_ergo, person.repondant_ergo_uaj] if pers
            )
        return initial


class ObservationErgoBaseView:
    model = ObservationErgo


class ObservationErgoListView(ObservationErgoBaseView, BaseListView):
    new_form_label = 'Créer nouvelle observation ergo'
    search_fields = ['rubrique', 'observation']


class ObservationErgoDetailView(ObservationErgoBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class ObservationErgoEditView(ObservationErgoBaseView, BaseEditView):
    pass


class MoyensAuxBaseView:
    model = MoyenAuxiliaire


class MoyensAuxListView(MoyensAuxBaseView, BaseListView):
    new_form_label = 'Créer nouveau moyen auxiliaire'
    template_name = 'ergo/moyensaux-list.html'

    def get_queryset(self):
        self.is_archives = self.request.GET.get('view') == 'archives'
        self.has_archives = not self.is_archives and super().get_queryset().filter(date_fin__isnull=False).exists()
        if self.is_archives:
            return super().get_queryset().filter(date_fin__isnull=False).order_by('-date_fin')
        else:
            return super().get_queryset().filter(date_fin__isnull=True).order_by('type_m')

    def get_fields_from_list_display(self, model):
        field_names = super().get_fields_from_list_display(model)
        if self.is_archives:
            field_names.append(('date_fin', 'Fin d’utilisation'))
        return field_names


class MoyensAuxDetailView(MoyensAuxBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class MoyensAuxEditView(MoyensAuxBaseView, BaseEditView):
    return_to_list = True
