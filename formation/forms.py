from datetime import timedelta

from django import forms

from common.fields import PickDateWidget
from .models import Formation


class HMDurationField(forms.DurationField):
    """A duration field taking HH:MM as input."""

    def to_python(self, value):
        if value in self.empty_values or isinstance(value, timedelta):
            return super().to_python(value)
        value += ':00'  # Simulate seconds
        return super().to_python(value)

    def prepare_value(self, value):
        value = super().prepare_value(value)
        if value:
            return value[:5]  # Remove seconds
        return value


class FormationEditForm(forms.ModelForm):
    class Meta:
        model = Formation
        fields = '__all__'
        field_classes = {
            'duree': HMDurationField,
        }
        widgets = {
            'user': forms.HiddenInput,
            'date': PickDateWidget,
        }
