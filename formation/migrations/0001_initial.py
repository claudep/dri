from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Formation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titre', models.CharField(max_length=200, verbose_name='Titre')),
                ('institut', models.CharField(blank=True, max_length=100, verbose_name='Institut de formation')),
                ('date', models.DateField(verbose_name='Date')),
                ('duree', models.DurationField(blank=True, null=True, verbose_name='Durée')),
                ('cours_qualite', models.BooleanField(default=False, verbose_name='Cours qualité')),
                ('cours_securite', models.BooleanField(default=False, verbose_name='Cours sécurité')),
                ('user', models.ForeignKey(on_delete=models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
