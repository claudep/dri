from django.db import models

from person.models import User


class Formation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    titre = models.CharField('Titre', max_length=200)
    institut = models.CharField('Institut de formation', max_length=100, blank=True)
    date = models.DateField('Date')
    duree = models.DurationField('Durée', null=True, blank=True)
    cours_qualite = models.BooleanField("Cours qualité", default=False)
    cours_securite = models.BooleanField("Cours sécurité", default=False)

    def __str__(self):
        return "Formation {} pour {} ({})".format(self.titre, self.user, self.date)
