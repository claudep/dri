from django.urls import path

from . import views

urlpatterns = [
    path('', views.FormationListView.as_view(), name='formations-list'),
    path('add/<int:pk>/', views.FormationEditView.as_view(is_create=True), name='formation-add'),
    path('<int:pk>/edit/', views.FormationEditView.as_view(), name='formation-edit'),
    path('<int:pk>/delete/', views.FormationDeleteView.as_view(), name='formation-delete'),
    path('export/', views.FormationExportView.as_view(), name='formations-export'),
]
