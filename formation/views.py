from datetime import date
from io import BytesIO
from tempfile import NamedTemporaryFile

from openpyxl import Workbook
from openpyxl.cell import WriteOnlyCell
from openpyxl.styles import Font

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Prefetch
from django.http import FileResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.text import slugify
from django.views.generic import DeleteView, ListView

from common.views import CreateUpdateView
from person.models import SECTEUR_CHOICES, User
from person.views import BaseXLSView
from .forms import FormationEditForm
from .models import Formation


class FormationListView(PermissionRequiredMixin, ListView):
    permission_required = 'formation.view_formation'
    model = Formation
    template_name = 'formation_list.html'

    def get_queryset(self):
        return User.objects.real_users().prefetch_related(
            Prefetch('formation_set', queryset=Formation.objects.order_by('-date'))
        ).prefetch_related('groups').order_by('last_name', 'first_name')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'can_edit': self.request.user.has_perm('formation.change_formation'),
            'secteurs': sorted(dict(SECTEUR_CHOICES).keys()),
            'annees': reversed(range(2007, date.today().year + 1)),
        })
        return context


class FormationEditView(PermissionRequiredMixin, SuccessMessageMixin, CreateUpdateView):
    permission_required = 'formation.change_formation'
    model = Formation
    form_class = FormationEditForm
    template_name = 'formation_edit.html'
    success_message = "La formation %(titre)s a bien été enregistrée"
    success_url = reverse_lazy('formations-list')

    def get_initial(self):
        initial = super().get_initial()
        if self.is_create:
            initial['user'] = self.kwargs['pk']
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if context['form'].instance.pk:
            context['person'] = context['form'].instance.user
        else:
            context['person'] = get_object_or_404(User, pk=self.kwargs['pk'])
        return context


class FormationDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'formation.delete_formation'
    model = Formation
    success_url = reverse_lazy('formations-list')
    success_message = "La formation %(titre)s a été supprimée"

    def delete(self, request, *args, **kwargs):
        msg = self.success_message % {'titre': self.get_object().titre}
        response = super().delete(request, *args, **kwargs)
        messages.success(request, msg)
        return response


class FormationExportView(BaseXLSView):
    sheet_name = "Formations"
    col_widths = [28, 28, 15, 15, 50, 40]

    def get_queryset(self):
        groups = []
        if self.request.GET.get('persfhne'):
            groups.append('FHNE')
        if self.request.GET.get('persfhmn'):
            groups.append('FHMN')
        query = Formation.objects.exclude(user__archived=True
            ).filter(user__groups__name__in=groups).order_by('user__last_name')
        if self.request.GET.get('secteur'):
            query = query.filter(user__secteur=unslugify(SECTEUR_CHOICES, self.request.GET.get('secteur')))
        if self.request.GET.get('annee'):
            query = query.filter(date__year=int(self.request.GET.get('annee')))
        return query

    def get_headers(self):
        return ['Nom', 'Secteur', 'Date', 'Durée', 'Cours', 'Institut de formation']

    def write_line(self, formation):
        dur = WriteOnlyCell(self.ws, value=formation.duree)
        dur.number_format = '[hh]:mm'
        self.ws.append([
            formation.user.nom_prenom,
            formation.user.secteur,
            formation.date,
            dur,
            formation.titre,
            formation.institut,
        ])

    def get_filename(self):
        return 'FH_formations_{}.xlsx'.format(date.strftime(date.today(), '%Y-%m-%d'))


def unslugify(choices, value):
    return {slugify(v): v for v in dict(choices).keys()}.get(value)
