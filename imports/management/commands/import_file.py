import csv
import re
from collections import Counter, OrderedDict
from datetime import date, datetime
from pathlib import Path
import xml.etree.ElementTree as ET

from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.db.models import Q
from django.utils.module_loading import import_string
from django.utils.timezone import get_current_timezone, make_aware

from person.models import Contact, Person, HistoireDeVie, Relation, Role
from besoins.models import RecueilBesoins
from bilanannuel.models import BilanAnnuel, BilanAnnuelEntree, SuiviAccompagnement, SuiviItem, SuiviCategorie
from ergo.models import MoyenAuxiliaire, MoyenAuxiliaireType
from soins.models import Medication, MedicationItem
from imports.mappings import main_mapping, MAPPINGS

ns = {'fm': 'http://www.filemaker.com/fmpxmlresult'}


def bilan_complet(instance, line):
    # Concatenate p1 to p14 fields to a single texte field.
    txt = ''
    for idx in range(1, 15):
        fname = 'Bilan neuropsy complet::Bilan neuropsy texte P%d' % idx
        if line[fname]:
            if txt:
                txt += '\n'
            txt += line[fname]
    instance.texte = txt

custom_handling = {
    'BilanNeuropsyComplet': bilan_complet,
}


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('imp_file')
        parser.add_argument('-o', '--output', default='db')

    def handle(self, *args, **options):
        file_param = Path(options['imp_file'])
        if file_param.is_dir():
            # Look first for the main resident file, then import the related data
            files = {f.name: f for f in file_param.iterdir() if f.name.endswith('xml')}
            if not 'residents.xml' in files:
                raise CommandError("Looks like residents.xml is not in this directory")
            with transaction.atomic():
                if Person.objects.count() > 10:
                    self.stdout.write("Résidents déjà importés")
                else:
                    self.stdout.write("Importation de la table des résidents")
                    self.import_file(files['residents.xml'], options['output'])
                for _file in sorted(files.values()):
                    if _file.name in ('residents.xml', 'contacts.xml'):
                        continue  # Already imported, or not imported at all
                    self.stdout.write("Importation du fichier %s" % _file.name)
                    self.import_file(_file, options['output'])
        elif file_param.is_file():
            with transaction.atomic():
                self.import_file(file_param, options['output'])
        else:
            raise CommandError("%s isn't an existing dir or file" % options['imp_file'])

    def import_file(self, imp_file, output):
        with open(str(imp_file)) as fh:
            # Some non-text chars pollute some xml files
            contents = fh.read().translate(
                {ord(i): None for i in '\x00\x02\x04\x05\x06\x0c\x0f\x10\x12\x13\x14\x1c\x1d\x1f'}
            )
        root = ET.fromstring(contents)
        headers = []
        meta = root.find('fm:METADATA', ns)
        for field in meta:
            headers.append(field.get('NAME'))

        results = root.find('fm:RESULTSET', ns)
        output_file = None
        errors = []
        if 'residents.xml' in str(imp_file):
            mapping = main_mapping
            model = Person
        elif output == 'csv':
            mapping = {}
        else:
            mapping = MAPPINGS.get(imp_file.name)
            if not mapping:
                raise ValueError("mapping à définir pour %s (%s)" % (imp_file.name, headers))
            model = import_string(mapping['__model__'])
        line_multivalued = mapping.get('__multivalued__', True)
        for res_idx, row in enumerate(results, start=1):
            line = OrderedDict()
            self.stdout.write('.', ending='')
            for col_idx, col in enumerate(row):
                datas = col.findall('fm:DATA', ns)
                if len(datas) > 1:
                    line[headers[col_idx]] = [d.text for d in datas]
                elif len(datas) == 1:
                    line[headers[col_idx]] = datas[0].text
                else:
                    line[headers[col_idx]] = None
            if output == 'csv':
                if output_file is None:
                    # create file, out put headers
                    csv_file = open(str(imp_file).replace('.xml', '.csv'), 'w')
                    output_file = csv.writer(csv_file)
                    output_file.writerow(headers)
                output_file.writerow(line.values())
            else:               
                if 'residents.xml' in str(imp_file):
                    self.import_resident(line, res_idx, model, mapping)
                else:
                    try:
                        if model.__name__ == 'SignesVitaux':
                            import_signes_vitaux(line, res_idx, model, mapping)
                        elif model.__name__ == 'StatusUrinaire':
                            import_status_urinaire(line, res_idx, model, mapping)
                        elif model.__name__ == 'MoyenAuxiliaire':
                            import_moyens_aux(line, res_idx, model, mapping)
                        elif model.__name__ == 'MedicationItem':
                            import_medication(line, res_idx, model, mapping)
                        elif model.__name__ == 'SuiviAccompagnement':
                            import_suivis(line, res_idx, model, mapping)
                        elif model.__name__ == 'BilanAnnuel':
                            import_bilanannuel(line, res_idx, model, mapping)
                        elif 'Atelier' in model.__name__ and str(imp_file).endswith('2.xml'):
                            import_atelier_items(line, res_idx, model, mapping)
                        elif line_multivalued:
                            # Each cell contains a liste of values
                            self.import_data_multi(line, res_idx, model, mapping)
                        else:
                            self.import_data(line, res_idx, model, mapping)
                    except Exception as err:
                        #raise
                        if err.__class__.__name__ == 'BdbQuit':
                            raise
                        errors.append(err)
                        continue
        if errors:
            raise Exception(errors)
        if output != 'csv' and model.__name__ == 'MedicationItem':
            finalize_medication()
        if output_file is not None:
            csv_file.close()

    def import_resident(self, line, line_no, model, mapping):
        conv_mappings = {
            'etat_civil': {
                'Célibataire': 'celib',
                'Marié': 'marie',
                'Marié(e)': 'marie',
                'Mariée': 'marie',
                'Veuf': 'veuf',
                'Veuf(ve)': 'veuf',
                'Veuve': 'veuf',
                'Divorcée': 'divorce',
                'Divorcé': 'divorce',
                'Divorcé(e)': 'divorce',
                'Séparé': 'separe',
                'Séparée': 'separe',
            },
            'situation': {v: k for k, v in Person.SITUATION_CHOICES},
            'hors_canton': {v: k for k, v in Person.HORS_CANTON_CHOICES},
            'comportement': {v: k for k, v in RecueilBesoins.COMPORT_CHOICES},
            'communication': {v: k for k, v in RecueilBesoins.COMMUNIC_CHOICES},
            'collaboration': {v: k for k, v in RecueilBesoins.COLLAB_CHOICES},
            'communiquer_vue': {v: k for k, v in RecueilBesoins.VUE_CHOICES},
            'securite_risque_errance': {v: k for k, v in RecueilBesoins.RISQUE_CHOICES},
            'securite_semainier': {v: k for k, v in RecueilBesoins.SEMAINIER_CHOICES},
        }
        conv_mappings['hors_canton'].update({'NON': '', 'Ne': '', 'Non': ''})
        conv_mappings['securite_risque_errance']['Elevé'] = 'eleve'
        conv_mappings['securite_risque_chute'] = conv_mappings['securite_risque_errance']

        def split_target(target):
            if '.' in target:
                mod, field = target.split('.')
                try:
                    model = getattr(pers, mod)
                except ObjectDoesNotExist:
                    remote_model = pers._meta.get_field(mod).remote_field.model
                    instance = remote_model(person=pers)
                    model = instance
            else:
                model, field = pers, target
            return model, field

        def set_value(target, value):
            target_model, target_field = split_target(target)
            value = convert_value(target_model, target_field, value, mappings=conv_mappings)
            setattr(target_model, target_field, value)

        if line['Nom'] is None:
            self.stderr.write("Skipping line %s as Nom is empty" % line_no)
            return
        # We need to create the object here so that One2One fields creation succeeds.
        pers = Person.objects.create(fm_id=int(line['IdRésident']))
        keys_to_ignore = ['id']
        for key, value in line.items():
            if key in keys_to_ignore or value is None:
                continue
            if isinstance(value, list):
                if not any(value):
                    continue
            else:
                if key == "No AVS AI" and len(value) > 16:
                    value = value.replace("- AI OK", '').replace(" - AI en c", '').replace("  - attente", '')
                value = value.strip()
                if value == '':
                    continue
            if key not in mapping:
                if key == "somme des tc ouvertes":
                    # Exception temporaire
                    continue
                raise Exception("Unable to find key '%s' to add value '%s'" % (key, value))
            target_field = mapping[key]
            if target_field == '<AD HOC HANDLING>' or target_field == '<NOT IMPORTED>':
                # This field is specifically handled later
                continue
            elif target_field == '<ALWAYS EMPTY>':
                raise ValueError("Value '%s' in a non-imported field %s" % (value, key))

            if isinstance(target_field, dict):
                # Split list of strings into various boolean fields
                values = value.splitlines()
                for val in values:
                    set_value(target_field[val], True)
            elif isinstance(target_field, list):
                for idx, val in enumerate(value):
                    if val is not None:
                        set_value(target_field[idx], val)
            else:
                set_value(target_field, value)

        if pers.actif and pers.cause_depart == "Non applicable":
            pers.cause_depart = ''
        try:
            pers.recueilbesoins.full_clean()
        except RecueilBesoins.DoesNotExist:
            pass
        else:
            pers.recueilbesoins.save()
        try:
            pers.histoiredevie.full_clean()
        except HistoireDeVie.DoesNotExist:
            pass
        else:
            pers.histoiredevie.save()
        pers.full_clean()
        pers.save()

        # Create relations
        # We do not differentiate between internal and external relations yet
        relations_keys = [
            "Relations nom prénom", "Relations adresse", "Relations email",
            "Relations natel", "Relations téléphone", "Relations liens"
        ]
        relations_externes_keys = [
            "Relations externes nom prénom", "Relations externes adresse",
            "Relations externes email", "Relations externes natel",
            "Relations externes téléphone", "Relations externes liens"
        ]
        for fieldset in (relations_keys, relations_externes_keys):
            for relation in zip(*(line[key] for key in fieldset)):
                if not any(relation):
                    continue
                nom = relation[0] or '?' 
                contact = get_contact(
                    nom, addr=relation[1], email=relation[2], tel=relation[4],
                    natel=relation[3]
                )
                nom_role = strip(relation[5])
                if nom_role:
                    role, _ = Role.objects.get_or_create(
                        nom__iexact=nom_role, defaults={'nom': nom_role}
                    )
                else:
                    role = None
                Relation.objects.create(person=pers, contact=contact, role=role)

        # Répondant officiel
        if line["Répondant officiel nom"]:
            civ, nom, prenom = split_nom_prenom(line["Répondant officiel nom"])
            if nom == 'Elle-même' or nom == 'Lui-même':
                pers.repondant_off_self = True
            else:
                contact = get_contact(
                    line["Répondant officiel nom"], addr=line["Répondant officiel adresse"],
                    email=line["Répondant officiel email"], tel=line["Répondant officiel téléphone"]
                )
                pers.repondant_off = contact
                pers.repondant_off_role = strip(line["Répondant officiel type relation"]) or ''
        # Autres répondants
        if line["Répondant animation"]:
            pers.repondant_anim = get_contact(line["Répondant animation"])
        if line["Répondant physiothérapie"]:
            pers.repondant_physio = get_contact(line["Répondant physiothérapie"])
        if line["Répondant soins"] and line["Répondant soins"] != '1- ':
            try:
                rep1, rep2 = [s.strip() for s in re.split(
                    '2[-:]',
                    line["Répondant soins"].replace('"', '').replace('1-', '').replace('1:', '')
                )]
            except Exception:
                pers.repondant_soins1 = get_contact(line["Répondant soins"].strip())
            else:
                pers.repondant_soins1 = get_contact(rep1)
                pers.repondant_soins2 = get_contact(rep2)
        if line["Répondant ergothérapie prénom nom"]:
            pers.repondant_ergo = get_contact(line["Répondant ergothérapie prénom nom"])

        # Médecins
        if line["Médecin dentiste"]:
            contact = get_contact(
                line["Médecin dentiste"], email=line["Médecin dentiste email"], tel=line["Médecin dentiste téléphone"]
            )
            pers.dentiste = contact
        if line["Médecin traitant nom"]:
            contact = get_contact(
                line["Médecin traitant nom"], addr=line["Médecin traitant adresse"],
                email=line["Médecin traitant email"], tel=line["Médecin traitant téléphone"],
                natel=line["Médecin traitant natel"],
            )
            pers.medecin_traitant = contact
        if line["Médecin spécialiste"]:
            # Ajout sous forme de relation pour avoir plusieurs spécialistes possibles
            contact = get_contact(
                line["Médecin spécialiste"],
                email=line["Médecin spécialiste email"], tel=line["Médecin spécialiste téléphone"],
            )
            role, _ = Role.objects.get_or_create(
                nom=strip(line["Médecin spécialiste liste"]) or 'Médecin spécialiste'
            )
            Relation.objects.create(person=pers, contact=contact, role=role)

        # Actes journaliers
        # Si tous les champs sont vides, ignorer
        vide = all(line[fld] == '' for fld in [
            "Actes journaliers actes journaliers", "Actes journaliers descriptions", "Actes journaliers objectifs soins"
        ])
        if not vide:
            from soins.models import ActesJournaliers
            aj = ActesJournaliers.objects.create(person=pers)
            if line["Actes journaliers date de mise à jour"]:
                aj.date = convert_value(ActesJournaliers, 'date', line["Actes journaliers date de mise à jour"])
            aj.visa = line["Actes journaliers visa"] or ''
            if line["Actes journaliers date prochain bilan"]:
                aj.prochain_bilan = convert_value(ActesJournaliers, 'prochain_bilan', line["Actes journaliers date prochain bilan"])
            aj.objectifs = line["Actes journaliers objectifs soins"] or ''
            aj.actes = (line["Actes journaliers actes journaliers"] or '') + (line["Actes journaliers actes journaliers 2"] or '')
            aj.descriptions = (line["Actes journaliers descriptions"] or '') + (line["Actes journaliers descriptions 2"] or '')
            aj.actes_r = (line["Actes journaliers R"] or '') + (line["Actes journaliers R 2"] or '')
            aj.actes_p = (line["Actes journaliers P"] or '') + (line["Actes journaliers P 2"] or '')
            aj.save()

        pers.full_clean()
        pers.save()

    def import_data(self, line, line_no, model, mapping):
        """Generic import method for tables other than residents.xml."""
        if all(v is None for v in line.values()):
            # Completely empty line
            return

        instance = model()
        for field_name, value in mapping.get('__forced__', {}).items():
            setattr(instance, field_name, value)
        exceptions = mapping.get('__exceptions__', {})
        for key, value in line.items():
            if value is None:
                continue
            if key not in mapping:
                raise Exception("Unable to find key '%s' to add value '%s'" % (key, value))
            target_field = mapping[key]
            if target_field in ('<NOT IMPORTED>', '<AD HOC HANDLING>'):
                continue
            if isinstance(value, list):
                if not any(value):
                    continue
            else:
                value = value.strip()
                if value == '':
                    continue
            if value in exceptions.get(key, {}).keys():
                value = exceptions[key][value]
            if target_field == 'person_id':
                instance.person = find_person_from_value(value)
            else:
                setattr(instance, target_field, convert_value(model, target_field, value))
        if model.__name__ in custom_handling:
            custom_handling[model.__name__](instance, line)
        instance.full_clean()
        instance.save()

    def import_data_multi(self, line, line_no, model, mapping):
        """Each line has multiple values that should be splitted in several instances."""
        if all(v is None for v in line.values()):
            # Completely empty line
            return
        instances = []
        exceptions = mapping.get('__exceptions__', {})
        for key, value in line.items():
            if not isinstance(value, list):
                # Sometimes inside a multivalued file, one line is monovalued!!
                value = [value]
            if not instances:
                instances = [model() for item in value]
            target_field = mapping[key]
            if target_field == '<NOT IMPORTED>':
                continue
            if target_field.startswith('<HOUR FOR:'):
                # Take the hour values to set to the specified date field
                target_date_field = target_field.split(':')[1].replace('>', '')
                for idx, item in enumerate(value):
                    if item in exceptions.get(key, {}).keys():
                        item = exceptions[key][item]
                    if item is None or getattr(instances[idx], target_date_field) is None:
                        continue
                    m = re.match('00:00:(\d\d)\.(\d{1,2})', item)
                    if m:
                        # Interpret wrong input like '00:00:17.45' as '17:45:00'
                        hours, minutes = map(int, m.groups())
                        seconds = 0
                    else:
                        try:
                            hours, minutes, seconds = map(int, item.split(':'))
                        except ValueError as err:
                            raise ValueError('%s, input was %s (for field %s)' % (err, item, target_field))
                    if hours == 24:
                        hours = 0
                    try:
                        setattr(
                            instances[idx], target_date_field,
                            getattr(instances[idx], target_date_field).replace(hour=hours, minute=minutes, second=seconds)
                        )
                    except (ValueError, AttributeError) as err:
                        raise ValueError('%s, input was %s' % (err, item))
            else:
                target_field, *tags = target_field.split(':')
                no_stripping = 'nostrip' in tags
                for idx, item in enumerate(value):
                    if item in exceptions.get(key, {}).keys():
                        item = exceptions[key][item]
                    if item is None or (not no_stripping and strip(item) == ''):
                        continue
                    if target_field == 'person_id':
                        instances[idx].person = Person.objects.get(fm_id=item)
                    else:
                        setattr(
                            instances[idx], target_field,
                            convert_value(model, target_field, item if no_stripping else strip(item))
                        )

        for instance in instances:
            assert instance.id is None
            # If only the person_id is filled, it is considered empty and ignored
            for field in instance._meta.get_fields():
                if getattr(instance, field.name, None) not in ('', None) and field.name != 'person':
                    break
            else:
                #print("Ignored following instance: %s" % instance.__dict__)
                continue
            if model.__name__ == 'Medication':
                try:
                    instance.validate_unique()
                except ValidationError:
                    print("Une fiche de médication à double a été ignorée (%s, %s)). Veuillez vérifier." % (
                        instance.person, instance.date_prescription
                    ))
                    continue

            instance.full_clean()
            instance.save()


day_counter = Counter()

def import_signes_vitaux(line, line_no, model, mapping):
    def get_month(key):
        """Extract month num from column name"""
        nums = "".join([c for c in key if c.isdigit()])
        return 1 if nums == '' else int(nums)

    exceptions = mapping.get('__exceptions__', {})
    id_sheet = line['Id Signes vitaux']
    day_counter[id_sheet] += 1
    id_resid = line['Id résident']
    date_sheet = line.get('Signes vitaux date feuille')
    year = convert_value(model, 'date', strip(date_sheet)).year
    day = day_counter[id_sheet]
    person = Person.objects.get(fm_id=id_resid[0] if isinstance(id_resid, list) else id_resid)
    for key, values in line.items():
        value = strip(values[0])
        if value in exceptions.get(key, {}).keys():
            value = exceptions[key][value]
        if value is None:
            continue
        target_field = mapping[key]
        if value in ('couché', 'assis'):
            continue
        if target_field in ('<NOT IMPORTED>', '<AD HOC HANDLING>'):
            continue
        value = value.replace('°', '')
        month = get_month(key)
        try:
            date_built = date(year=year, month=month, day=day)
        except ValueError:
            if day == 31:
                day = 30
            date_built = date(year=year, month=month, day=day)
        instance, created = model.objects.get_or_create(person=person, date=date_built, defaults={'t_a': ''})
        if getattr(instance, target_field):
            print("sheet %s: Already a value for this field??" % id_sheet)
            continue
        else:
            setattr(instance, target_field, convert_value(model, target_field, value))
        instance.save()


def import_status_urinaire(line, line_no, model, mapping):
    mapping = (
        ('Bilirubine:', 'bilirubine'), ('Cétones:', 'cetones'), ('Densité 1', 'densite'),
        ('Erythrocytes', 'erythrocytes'), ('Glucose:', 'glucose'), ('Hémoglobine:', 'hemoglobine'),
        ('Leucocytes:', 'Leucocytes'), ('Nitrites:', 'nitrites'), ('PH:', 'ph'), ('Protéines:', 'protéines'),
        ('Urobilinogène:', 'urobilinogene')
    )
    def empty(vals):
        return all([val is None for val in vals])

    if all([empty(line[col]) for col, _ in mapping]):
        return
    person = Person.objects.get(fm_id=line['Id résident'])
    instance = model(person=person, date=convert_date(line['Status urinaire date saisie 1'][0]))
    for col, field_name in mapping:
        setattr(instance, field_name, (line[col][0] or '').strip())
    instance.full_clean()
    instance.save()


atelier_counter = Counter()

atelier_items = [
    ('item_comport_msp', 'item_comport_msp_rem'),
    ('item_comport_coll', 'item_comport_coll_rem'),
    ('item_productivite', 'item_productivite_rem'),
    ('item_interch', 'item_interch_rem'),
    ('item_qualite', 'item_qualite_rem'),
    ('item_effort', 'item_effort_rem'),
    ('item_horaire', 'item_horaire_rem'), 
    ('item_initiatives', 'item_initiatives_rem'),
]

def import_atelier_items(line, line_no, model, mapping):
    fmap = {
        'AtelierCrea1': ('Id atelier créa1', 'Atelier créa1 date de mise à jour',
                         'créa1 item 1 à 8', 'créa1 item 1 à 8 commentaire'),
        'AtelierCrea2': ('Id atelier créa2 fhmn', 'Atelier créa2 fhmn date',
                         'Atelier créa2 item 1 à 8', 'Atelier créa2 item 1 à 8 commentaire'),
        'AtelierCuisine': ('Id atelier cuisine fhmn', 'Atelier cuisine date mise à jour',
                           'Cuisine item 1 à 8', 'Cuisine item 1 à 8 commentaire'),
        'AtelierInfo': ('Id atelier info FHMN', 'Atelier info FHMN date',
                        'Atelier info item 1 à 8', 'Atelier info item 1 à 8 commentaire'),
    }
    col_id_atelier, col_date_maj, col_item, col_item_comm = fmap[model.__name__]
    id_atelier = line[col_id_atelier]
    id_person = line['Id résident']
    date_maj = convert_date(line[col_date_maj])
    item_note = line[col_item][0]
    item_comm = line[col_item_comm][0] or ''

    item_idx = atelier_counter[id_atelier]
    try:
        atelier = model.objects.get(person__fm_id=id_person, date__date=date_maj)
    except model.MultipleObjectsReturned:
        raise
    except model.DoesNotExist:
        return
    setattr(atelier, atelier_items[item_idx][0], item_note)
    setattr(atelier, atelier_items[item_idx][1], item_comm)
    atelier.save(update_fields=[atelier_items[item_idx][0], atelier_items[item_idx][1]])
    atelier_counter[id_atelier] += 1


def convert_date(value, force_time=False):
    if value is None or value.strip() == '':
        return None
    value = value.replace('\t', '').replace(',', '.')
    try:
        day, month, year = value.split('.')
    except ValueError:
        print("Unable to convert '%s'" % value)
        raise
    day, month, year = int(day), int(month), int(year)
    if day > 2000 and year < 32:
        # Date sometimes entered as yyyy.mm.dd
        day, month, year = year, month, day
    if year < 100:
        year += 2000 if year < 30 else 1900
    if force_time:
        return datetime(
            year=year, month=month, day=day, hour=12, tzinfo=get_current_timezone()
        )
    else:
        return date(year=year, month=month, day=day)


def convert_value(model, field_name, value, mappings=None):
    field = model._meta.get_field(field_name)
    ftype = field.get_internal_type()
    if ftype in ('DateField', 'DateTimeField'):
        try:
            value = convert_date(value, force_time=(ftype == 'DateTimeField'))
        except ValueError as err:
            raise ValueError('%s, input was: %s (for field %s)' % (err, value, field_name))
    elif ftype == 'BooleanField':
        if not isinstance(value, bool):
            value = {'oui': True, 'non': False}.get(value.lower())
    elif ftype == 'DecimalField':
        value = value.replace(',', '.')
    elif ftype == 'ChoiceArrayField':
        mapping = {
            v: k for k, v in model._meta.get_field(field_name).base_field.choices
        } if mappings is None else mappings[field_name]
        values = value.splitlines()
        try:
            value = [mapping[v] for v in values]
        except KeyError:
            if not all([v in mapping.values() for v in values]):
                raise ValueError("Une des valeurs '%s' non présente dans le tableau %s" % (value, field_name))
            # Values are already using the choices key
            value = values
    elif mappings and field_name in mappings.keys():
        if value not in mappings[field_name]:
            raise ValueError("Valeur '%s' inconnue pour le champ %s" % (value, field_name))
        value = mappings[field_name][value]
    elif getattr(field, 'choices', False):
        possible_values = {v.lower(): k for k, v in field.choices}
        possible_values.update({k.lower(): k for k, _ in field.choices})
        if 'Élevé' in possible_values:
            possible_values['Elevé'] = possible_values['Élevé']
        if value.lower() not in possible_values:
            raise ValueError("Valeur '%s' inconnue pour le champ %s" % (value, field_name))
        value = possible_values[value.lower()]
    return value


def import_moyens_aux(line, line_no, model, mapping):
    instances = {}
    field_map = {
        "Date d'octroi": 'date_octroi',
        'Décision AI': 'decision_ai',
        'Description': 'description',
        'divers 1': 'remarques',
        'Financement': 'financement',
        'Fournisseur': 'fournisseur',
        'Réparateur': 'reparateur',
    }
    def ksplit(key):
        k = key.split('::')[1]
        for p1 in field_map.keys():
            if k.startswith(p1):
                part1 = p1
                part2 = k[len(p1) + 1:]
                return part1, part2
        raise Exception("Unable to split the key %s" % key)

    date = person = None
    for key, value in line.items():
        if mapping[key] in ('<NOT IMPORTED>', '<ALWAYS EMPTY>') or not value:
            continue
        if key == "Moyens auxiliaires::Date de mise a jour":
            date = convert_value(model, 'date', value)
            continue
        if key == "Moyens auxiliaires::Id Résident":
            person = Person.objects.get(fm_id=value)
            continue
        part1, part2 = ksplit(key)
        if part2 not in instances:
            instances[part2] = MoyenAuxiliaire(type_m=MoyenAuxiliaireType.objects.get(nom=part2))
        field_name = field_map[part1]
        setattr(
            instances[part2], field_name,
            convert_value(model, field_name, value)
        )

    for instance in instances.values():
        instance.date = date
        instance.person = person
        instance.full_clean()
        instance.save()


def import_bilanannuel(line, line_no, model, mapping):
    instances = None
    entries = []
    key_map = {
        'animation_musico': 'animation',
        'ateliers_uaj': 'ateliers',
        'ergo': 'ergo',
        'medecin': 'medecin',
        'objectifs_globaux': 'objectifs',
        'physio': 'physio',
        'resident': 'resident',
        'soins': 'soins',
    }
    libelle_map = {
        'bilan annuel': 'annuel',
        'Bilan anuel': 'annuel',
        'Bilan annuel': 'annuel',
        'Bilan à 9 mois': '9mois',
        'Réseau - NJ': 'interm',
        'bilan à 3 mois': '3mois',
        'Bilan à 3 mois': '3mois',
        'Bilan intermédiaire': 'interm',
        'Bilan à 1 mois': '1mois',
        'Bilan 1 mois': '1mois',
        'Séjour UAT': 'uat',
        'Bilan UAT': 'uat',
        'Bilan séjour UAT': 'uat',
        'bilan UAT': 'uat',
    }
    for key, value in line.items():
        if mapping[key] in ('<NOT IMPORTED>', '<ALWAYS EMPTY>') or not value:
            continue
        if not isinstance(value, list):
            value = [value]
        if not instances:
            instances = [BilanAnnuel() for item in value]
        target_field = mapping[key]
        for idx, item in enumerate(value):
            if target_field == 'person_id':
                instances[idx].person = Person.objects.get(fm_id=item)
            elif target_field == 'libelle':
                if item and item.strip():
                    instances[idx].libelle = libelle_map[item.strip()]
            elif target_field in key_map:
                if item is not None and item != '':
                    entries.append(
                        BilanAnnuelEntree(bilan=instances[idx], secteur=key_map[target_field], contenu=item)
                    )
            else:
                setattr(
                    instances[idx], target_field,
                    convert_value(model, target_field, item)
                )
    if instances:
        for instance in instances:
            if instance.visa is None:
                instance.visa = ''
            if instance.libelle is None:
                instance.libelle = ''
            if instance.participants is None:
                instance.participants = ''
            instance.date = make_aware(datetime(
                instance.date_bilan.year, instance.date_bilan.month, instance.date_bilan.day, 12, 0
            ))
            instance.full_clean()
            instance.save()
        for entry in entries:
            entry.bilan_id = entry.bilan.pk
            entry.save()


def import_medication(line, line_no, model, mapping):
    if line['Medics réguliers'][0] is None and line['Medics réserve'][0] is None:
        # Ignore lines without product name
        return

    # Find parent Medication
    person = Person.objects.get(fm_id=line['Id résident'])

    if line['Date prescription'] is None:
        # One resident has this case for an old medication, ignore
        return
    else:
        medic, _ = Medication.objects.get_or_create(
            person=person,
            date_prescription=convert_date(line['Date prescription']),
            defaults={'impr_etiquette': True}
        )

    exceptions = mapping.get('__exceptions__', {})
    # Variables to manage duplicates
    produit_seen = set()
    reserve_seen = set()
    for idx, produit in enumerate(line['Medics réguliers']):
        produit = produit.strip() if produit is not None else produit
        if produit and produit not in produit_seen:
            date_deb = line['Medics réguliers date début'][idx]
            date_fin = line['Medics réguliers date fin'][idx]
            if date_deb in exceptions['Medics réguliers date début'].keys():
                date_deb = exceptions['Medics réguliers date début'][date_deb]
            if date_fin in exceptions['Medics réguliers date fin'].keys():
                date_fin = exceptions['Medics réguliers date fin'][date_fin]
            if date_fin and '?' in date_fin:
                date_fin = date_fin.replace('?', '')
                if date_fin == '':
                    date_fin = None
            soir = line['Medics réguliers soir'][idx] or ''
            if soir == '1 stop le 11.07.17':
                soir = '1'
                date_fin = '11.7.17'
            item = MedicationItem(
                medication=medic, produit=produit, reserve=False, order=idx,
                date_debut=convert_date(date_deb),
                visa_debut=line['Medics réguliers visa début'][idx] or '',
                date_fin=convert_date(date_fin),
                visa_fin=line['Medics réguliers visa fin'][idx] or '',
                matin=line['Medics réguliers matin'][idx] or '',
                midi=line['Medics réguliers midi'][idx] or '',
                soir=soir,
                nuit=line['Medics réguliers nuit'][idx] or '',
            )
            item.full_clean()
            item.save()
            produit_seen.add(produit)

        if idx < len(line['Medics réserve']) and line['Medics réserve'][idx] is not None:
            reserve = line['Medics réserve'][idx].strip()
            if reserve and reserve not in reserve_seen:
                date_deb = line['Medics réserve date'][idx]
                date_fin = line['Medics réserve date fin'][idx]
                if date_deb in exceptions['Medics réserve date'].keys():
                    date_deb = exceptions['Medics réserve date'][date_deb]
                if date_fin in exceptions['Medics réserve date fin'].keys():
                    date_fin = exceptions['Medics réserve date fin'][date_fin]
                item = MedicationItem(
                    medication=medic, produit=reserve, reserve=True,
                    date_debut=convert_date(date_deb),
                    visa_debut=line['Medics réserve visa'][idx] or '',
                    date_fin=convert_date(date_fin),
                    visa_fin=line['Medics réserve visa fin'][idx] or '',
                )
                item.full_clean()
                item.save()
                reserve_seen.add(reserve)

def finalize_medication():
    # Archive all medication item for older medications (except the most current one)
    for pers in Person.objects.all():
        all_medications = pers.medication_set.all().order_by('date_prescription')
        prev_medic = None
        for medication in all_medications:
            if prev_medic:
                prev_medic.medicationitem_set.filter(date_fin__isnull=True).update(date_fin=medication.date_prescription)
            prev_medic = medication


ids_suivis = {}

def import_suivis(line, line_no, model, mapping):
    if not line['Id résident']:
        return
    line_id = int(line["Id Suivi d'accompagnement"])
    if line_id not in ids_suivis:
        ids_suivis[line_id] = SuiviAccompagnement.objects.create(
            person=Person.objects.get(fm_id=line['Id résident']),
            objectifs=line['Objectifs principaux'] or '',
            date=convert_date(line["Suivi d'accompagnement date"], force_time=True),
        )
    suivi = ids_suivis[line_id]
    categs = {
        'Accomplissement': [
            'acc', 'Actions Accomplissement', 'Evaluation Accomplissement', 'Qui Accomplissement',
            'Dépendant accomplissement', 'Partiellement indépendant accomplissement', 'Indépendant accomplissement'
        ],
        'Besoins physiologiques': [
            'phys', 'Actions Besoins physiologiques', 'Evaluation physiologiques', 'Qui Besoins physiologiques',
            'Dépendant physiologiques', 'Partiellement indépendant physiologiques', 'Indépendant physiologiques'
        ],
        'Besoins sécuritaires': [
            'secu', 'Actions Besoins sécuritaires', 'Evaluation sécuritaires', 'Qui Besoins sécuritaires',
            'Dépendant sécuritaire', 'Partiellement indépendant sécuritaire', 'Indépendant sécuritaire'
        ],
        'Besoins sociaux': [
            'soc', 'Actions Besoins sociaux', 'Evaluation sociaux', 'Qui Besoins sociaux',
            'Dépendant sociaux', 'Partiellement indépendant sociaux', 'Indépendant sociaux'
        ],
        'Estime de soi': [
            'estime', 'Actions Estime de soi', 'Evaluation Estime de soi', 'Qui Estime de soi',
            'Dépendant estime de soi', 'Partiellement indépendant estime de soi', 'Indépendant estime de soi'
        ],
    }

    def clean_autonomy(val):
        # X to M is arbitrary
        return val.strip().upper().replace('X', 'M') if val else ''

    for categ, fields in categs.items():
        for idx, item in enumerate(line[categ]):
            famille = fields[0]
            if item is None:
                continue
            try:
                categorie = SuiviCategorie.objects.get(famille=famille, nom=item)
            except SuiviCategorie.DoesNotExist:
                # Auto-create categories when needed
                categorie = SuiviCategorie.objects.create(famille=famille, nom=item)
            instance = SuiviItem(
                suivi=suivi,
                categorie=categorie,
                independant=clean_autonomy(line[fields[6]][0]),
                partiel=clean_autonomy(line[fields[5]][0]),
                dependant=clean_autonomy(line[fields[4]][0]),
                qui=line[fields[3]][0] or '',
                actions=line[fields[1]][0] or '',
                evaluation=line[fields[2]][0] or '',
            )
            instance.full_clean()
            instance.save()


def get_contact(nom, addr=None, tel=None, natel=None, email=None):
    civil, nom, prenom = split_nom_prenom(nom)
    email, telephone, portable = strip(email) or '', strip(tel) or '', strip(natel) or ''
    contact, created = Contact.objects.get_or_create(
        nom=nom, prenom=prenom, adresse=strip(addr) or '',
        defaults={
            'civilite': civil, 'email': email, 'telephone': telephone, 'portable': portable,
        }
    )
    if not created:
        for field_name in ('email', 'telephone', 'portable'):
            received_val = eval(field_name)
            existing_val = getattr(contact, field_name)
            if received_val:
                if not existing_val:
                    setattr(contact, field_name, received_val)
                    contact.save()
            elif received_val and existing_val != received_val:
                print("Warning, contact %s mismatch (%s vs %s)" % (field_name, existing_val, received_val))
    return contact


def find_person_from_value(value):
    if value == '' or 'gresset' in value.lower():
        return None
    try:
        value = int(value)
    except ValueError:
        nom1, nom2 = value.split(maxsplit=1)
        pers = Person.objects.get(
            Q(nom__unaccent__iexact=nom1, prenom__unaccent__iexact=nom2) |
            Q(nom__unaccent__iexact=nom2, prenom__unaccent__iexact=nom1) |
            Q(nom__unaccent__iexact=nom1, prenom__istartswith=nom2[:2])
        )
    else:
        pers = Person.objects.get(fm_id=value)
    return pers


def strip(value):
    return value.strip() if value is not None else value

prenoms = {
    'Alain', 'Aline', 'Alexia', 'André', 'Anick', 'Aude', 'Beat', 'Bertrand', 'Brigitte', 'Carine',
    'Carole', 'Caroline', 'Catherine', 'Cécile', 'Chantal', 'Chloé', 'Christèle', 'Christine',
    'Cindy', 'Claude', 'Cyril', 'Daisy', 'Daniel', 'Delphine', 'Denise', 'Dominique', 'Elene',
    'Elisabeth', 'Eloïse', 'Esther', 'Fatemeh', 'Fernanda', 'Fernande', 'Florent', 'François',
    'Françoise', 'Frédéric', 'Gabrielle', 'Gérard', 'Gilbert', 'Gilles', 'Grégory', 'Héléna',
    'Ines', 'Iris', 'Isabelle', 'Jean', 'Jean-Jacques', 'Jean-Marie', 'Jean-Pierre', 'Joanie',
    'Joël', 'Joëlle', 'Joelle', 'Josée', 'Josette', 'Josiane', 'Juerg', 'Julien', 'Karim',
    'Karine', 'Laurent', 'Léa', 'Lorenz', 'Magali', 'Manuela', 'Marc', 'Marc-Antoine',
    'Marcelle', 'Marc-Eugène', 'Marco', 'Marie-Cécile', 'Marie-Claire', 'Martha', 'Maya',
    'Mélanie', 'Michael', 'Michel', 'Michelle', 'Micky', 'Mireille', 'Monika', 'Nicolas',
    'Nicole', 'Noélie', 'Odile', 'Pascale', 'Patrice', 'Pauline', 'Pierre-Alain',
    'Pierre-François', 'Pierre-Yves', 'P.-Y.', 'Raphael', 'Sarah', 'Sophia', 'Sophie',
    'Stéphane', 'Suzanne', 'Sybile', 'Typhelle', 'Valérie', 'Vincent', 'Violaine', 'Virginie',
    'W.', 'Yolande', 'Yves', 'Yves-Robert', 'Yves-Roger', 'Yvette', 'Z.',
}

def split_nom_prenom(value):
    value = strip(value)
    civil = ''
    if value.startswith(('Dr ', 'Dr. ', 'Dresse ', 'Drsse ', 'Mme ', 'M. ', 'Me ')):
        civil, value = value.split(' ', maxsplit=1)
    nom_prenom = value.split(' ', maxsplit=1)
    if len(nom_prenom) == 2:
        nom, prenom = nom_prenom[0], nom_prenom[1]
    else:
        nom, prenom = nom_prenom[0], ''
    if nom and prenom and prenom in prenoms:
        nom, prenom = prenom, nom
    return civil, nom, prenom
