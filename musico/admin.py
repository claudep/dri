from django.contrib import admin

from .models import BilanMusico, Musico, ObservationMusico

admin.site.register(BilanMusico)
admin.site.register(Musico)
admin.site.register(ObservationMusico)
