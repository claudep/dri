# Generated by Django 2.1.1 on 2018-10-27 14:50

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('person', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BilanMusico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(blank=True, null=True, verbose_name='Date')),
                ('visa', models.CharField(blank=True, max_length=25, verbose_name='Visa')),
                ('participants', models.CharField(blank=True, max_length=70, verbose_name='Participants')),
                ('rappel_objectifs', models.TextField(blank=True, verbose_name='Rappel des objectifs')),
                ('situation', models.TextField(blank=True, verbose_name='Point de situation')),
                ('commentaires', models.TextField(blank=True, verbose_name='Commentaires et propositions')),
                ('objectifs', models.TextField(blank=True, verbose_name='Objectifs')),
                ('deroulement', models.TextField(blank=True, verbose_name='Déroulement de l’entretien')),
                ('createur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='person.Person')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Musico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(blank=True, null=True, verbose_name='Date')),
                ('visa', models.CharField(blank=True, max_length=25, verbose_name='Visa')),
                ('activites', models.TextField(blank=True, verbose_name='Activités')),
                ('ressources', models.TextField(blank=True, verbose_name='Ressources')),
                ('difficultes', models.TextField(blank=True, verbose_name='Difficultés')),
                ('objectifs', models.TextField(blank=True, verbose_name='Objectifs')),
                ('createur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='person.Person')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ObservationMusico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(blank=True, null=True, verbose_name='Date')),
                ('visa', models.CharField(blank=True, max_length=25, verbose_name='Visa')),
                ('date_fermee', models.DateField(blank=True, null=True, verbose_name='Date fermeture')),
                ('etat', models.CharField(choices=[('o', 'Ouverte'), ('f', 'Fermée')], default='o', max_length=1, verbose_name='État')),
                ('observations', models.TextField(blank=True, verbose_name='Observations')),
                ('createur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='person.Person')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
