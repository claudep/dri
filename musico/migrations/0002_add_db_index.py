from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('musico', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bilanmusico',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='musico',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='observationmusico',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
    ]
