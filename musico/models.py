from datetime import date

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from person.models import Attachment, Person, PersonRelatedModel


class BilanMusico(PersonRelatedModel):
    date_bilan = models.DateField("Date du bilan")
    participants = models.CharField("Participants", max_length=70, blank=True)
    rappel_objectifs = models.TextField("Rappel des objectifs", blank=True)
    situation = models.TextField("Point de situation", blank=True)
    commentaires = models.TextField("Commentaires et propositions", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)
    deroulement = models.TextField("Déroulement de l’entretien", blank=True)

    _view_name_base = 'bilanmusico'
    list_display = ['date', 'auteur']

    def __str__(self):
        return "Bilan musico pour %s (%s)" % (self.person, self.date)


class Musico(PersonRelatedModel):
    activites = models.TextField("Activités", blank=True)
    ressources = models.TextField("Ressources", blank=True)
    difficultes = models.TextField("Difficultés", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)

    _view_name_base = 'musico'
    list_display = ['date', 'auteur']

    def __str__(self):
        return "Fiche musico pour %s (%s)" % (self.person, self.date)


class ObservationMusico(PersonRelatedModel):
    ETAT_CHOICES = (
        ('o', 'Ouverte'),
        ('f', 'Fermée'),
    )
    date_fermee = models.DateField("Date fermeture", null=True, blank=True)
    etat = models.CharField("État", max_length=1, choices=ETAT_CHOICES, default='o')
    observations = models.TextField("Observations")
    fichiers = GenericRelation(Attachment)

    _view_name_base = 'obsmusico'
    list_display = ['date', 'etat', 'observations:files', 'auteur']

    def __str__(self):
        return "Observation musico pour %s (%s)" % (self.person, self.date)

    def can_edit(self, user):
        return user.is_superuser or (user == self.createur and self.date.date() == date.today())

    @property
    def closed(self):
        return self.etat == 'f'
