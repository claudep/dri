from django.test import TestCase

from person.tests import ModelBaseTextMixin
from .models import BilanMusico, Musico, ObservationMusico


class MusicoTests(ModelBaseTextMixin, TestCase):
    ModelClass = Musico
    model_id = 'musico'
    custom_data = {'activites': "Des choses…"}


class BilanMusicoTests(ModelBaseTextMixin, TestCase):
    ModelClass = BilanMusico
    model_id = 'bilanmusico'
    custom_data = {'date_bilan': '2019-06-06', 'situation': "Des choses…"}


class ObservationMusicoTests(ModelBaseTextMixin, TestCase):
    ModelClass = ObservationMusico
    model_id = 'obsmusico'
    custom_data = {'etat': 'o', 'observations': "Des choses…"}
