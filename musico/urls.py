from django.urls import path

from . import views

urlpatterns = [
    path('bilan/', views.BilanMusicoListView.as_view(), name='bilanmusico-list'),
    path('bilan/<int:obj_pk>/', views.BilanMusicoDetailView.as_view(), name='bilanmusico-detail'),
    path('bilan/<int:obj_pk>/edit/', views.BilanMusicoEditView.as_view(), name='bilanmusico-edit'),
    path('bilan/new/', views.BilanMusicoEditView.as_view(), name='bilanmusico-new'),

    path('musico/', views.MusicoListView.as_view(), name='musico-list'),
    path('musico/<int:obj_pk>/', views.MusicoDetailView.as_view(), name='musico-detail'),
    path('musico/<int:obj_pk>/edit/', views.MusicoEditView.as_view(), name='musico-edit'),
    path('musico/new/', views.MusicoEditView.as_view(), name='musico-new'),

    path('obsmusico/', views.ObservationMusicoListView.as_view(), name='obsmusico-list'),
    path('obsmusico/<int:obj_pk>/', views.ObservationMusicoDetailView.as_view(), name='obsmusico-detail'),
    path('obsmusico/<int:obj_pk>/edit/', views.ObservationMusicoEditView.as_view(), name='obsmusico-edit'),
    path('obsmusico/new/', views.ObservationMusicoEditView.as_view(), name='obsmusico-new'),
]
