from person.views import BaseDetailView, BaseListView, BaseEditView

from .models import BilanMusico, Musico, ObservationMusico


class BilanMusicoBaseView:
    model = BilanMusico


class BilanMusicoListView(BilanMusicoBaseView, BaseListView):
    new_form_label = 'Créer nouvelle fiche bilan musico'


class BilanMusicoDetailView(BilanMusicoBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'musico/bilanmusico-detail.html'


class BilanMusicoEditView(BilanMusicoBaseView, BaseEditView):
    pass


class MusicoBaseView:
    model = Musico


class MusicoListView(MusicoBaseView, BaseListView):
    new_form_label = 'Créer nouvelle fiche musico'


class MusicoDetailView(MusicoBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class MusicoEditView(MusicoBaseView, BaseEditView):
    pass


class ObservationMusicoBaseView:
    model = ObservationMusico


class ObservationMusicoListView(ObservationMusicoBaseView, BaseListView):
    new_form_label = 'Créer nouvelle fiche observation musico'


class ObservationMusicoDetailView(ObservationMusicoBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class ObservationMusicoEditView(ObservationMusicoBaseView, BaseEditView):
    pass
