from django.contrib import admin

from .models import BilanNeuropsyAnnuel, BilanNeuropsyComplet, ObservationNeuropsy

admin.site.register(BilanNeuropsyAnnuel)
admin.site.register(BilanNeuropsyComplet)
admin.site.register(ObservationNeuropsy)
