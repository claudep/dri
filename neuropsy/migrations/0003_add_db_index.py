from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('neuropsy', '0002_replace_pnum_by_texte'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bilanneuropsyannuel',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='bilanneuropsycomplet',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='observationneuropsy',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
    ]
