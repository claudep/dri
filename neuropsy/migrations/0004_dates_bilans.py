from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('neuropsy', '0003_add_db_index'),
    ]

    operations = [
        migrations.AddField(
            model_name='bilanneuropsyannuel',
            name='date_bilan',
            field=models.DateField(null=True, verbose_name='Date du bilan'),
        ),
        migrations.AddField(
            model_name='bilanneuropsycomplet',
            name='date_bilan',
            field=models.DateField(null=True, verbose_name='Date du bilan'),
        ),
    ]
