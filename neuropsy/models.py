from datetime import date

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from person.models import Attachment, Person, PersonRelatedModel


class BilanNeuropsyAnnuel(PersonRelatedModel):
    date_bilan = models.DateField("Date du bilan")
    referent = models.CharField("Référent", max_length=50, blank=True)
    situation = models.TextField("Situation actuelle", blank=True)
    evaluation = models.TextField("Résumé de l'évaluation", blank=True)
    prise_en_charge = models.TextField("Résumé de la prise en charge", blank=True)
    objectifs = models.TextField("Objectifs et délais", blank=True)
    # Toujours vide et pas dans l'interface ?
    deroulement = models.TextField("Déroulement de l'entretien", blank=True)

    _view_name_base = 'bilanpsyaanuel'
    list_display = ['date_bilan', 'auteur']

    def __str__(self):
        return "Bilan neuropsy annuel pour %s (%s)" % (self.person, self.date_bilan)


class BilanNeuropsyComplet(PersonRelatedModel):
    date_bilan = models.DateField("Date du bilan")
    texte = models.TextField("Bilan complet", blank=True)

    _view_name_base = 'bilanpsycomplet'
    list_display = ['date_bilan', 'auteur']

    def __str__(self):
        return "Bilan neuropsy complet pour %s (%s)" % (self.person, self.date_bilan)


class ObservationNeuropsy(PersonRelatedModel):
    observation = models.TextField("Observation")
    fichiers = GenericRelation(Attachment)

    _view_name_base = 'obsneuropsy'
    list_display = ['date:h', 'observation:w80:files', 'auteur']

    def __str__(self):
        return "Observation neuropsy pour %s (%s)" % (self.person, self.date)

    def can_edit(self, user):
        return user.is_superuser or (user == self.createur and self.date.date() == date.today())
