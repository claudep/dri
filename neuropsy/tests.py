from django.test import TestCase

from person.tests import ModelBaseTextMixin
from .models import BilanNeuropsyAnnuel, BilanNeuropsyComplet, ObservationNeuropsy



class BilanNeuropsyAnnuelTests(ModelBaseTextMixin, TestCase):
    ModelClass = BilanNeuropsyAnnuel
    model_id = 'bilanpsyaanuel'
    custom_data = {'date_bilan': '2019-03-04', 'situation': "Une situation…"}


class BilanNeuropsyCompletTests(ModelBaseTextMixin, TestCase):
    ModelClass = BilanNeuropsyComplet
    model_id = 'bilanpsycomplet'
    custom_data = {'date_bilan': '2019-03-04', 'texte': "Text bilan…"}


class ObservationNeuropsyTests(ModelBaseTextMixin, TestCase):
    ModelClass = ObservationNeuropsy
    model_id = 'obsneuropsy'
    custom_data = {'observation': "Une observation…"}
