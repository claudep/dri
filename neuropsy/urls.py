from django.urls import path

from . import views

urlpatterns = [
    path('bilanann/', views.BilanNeuropsyAnnuelListView.as_view(), name='bilanpsyaanuel-list'),
    path('bilanann/<int:obj_pk>/', views.BilanNeuropsyAnnuelDetailView.as_view(), name='bilanpsyaanuel-detail'),
    path('bilanann/<int:obj_pk>/edit/', views.BilanNeuropsyAnnuelEditView.as_view(), name='bilanpsyaanuel-edit'),
    path('bilanann/new/', views.BilanNeuropsyAnnuelEditView.as_view(), name='bilanpsyaanuel-new'),

    path('bilancompl/', views.BilanNeuropsyCompletListView.as_view(), name='bilanpsycomplet-list'),
    path('bilancompl/<int:obj_pk>/', views.BilanNeuropsyCompletDetailView.as_view(), name='bilanpsycomplet-detail'),
    path('bilancompl/<int:obj_pk>/edit/', views.BilanNeuropsyCompletEditView.as_view(), name='bilanpsycomplet-edit'),
    path('bilancompl/new/', views.BilanNeuropsyCompletEditView.as_view(), name='bilanpsycomplet-new'),

    path('obspsy/', views.ObservationNeuropsyListView.as_view(), name='obsneuropsy-list'),
    path('obspsy/<int:obj_pk>/', views.ObservationNeuropsyDetailView.as_view(), name='obsneuropsy-detail'),
    path('obspsy/<int:obj_pk>/edit/', views.ObservationNeuropsyEditView.as_view(), name='obsneuropsy-edit'),
    path('obspsy/new/', views.ObservationNeuropsyEditView.as_view(), name='obsneuropsy-new'),
]
