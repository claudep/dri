from person.views import BaseDetailView, BaseListView, BaseEditView

from .models import BilanNeuropsyAnnuel, BilanNeuropsyComplet, ObservationNeuropsy


class BilanNeuropsyAnnuelBaseView:
    model = BilanNeuropsyAnnuel


class BilanNeuropsyAnnuelListView(BilanNeuropsyAnnuelBaseView, BaseListView):
    new_form_label = 'Créer nouvelle fiche bilan annuel'


class BilanNeuropsyAnnuelDetailView(BilanNeuropsyAnnuelBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class BilanNeuropsyAnnuelEditView(BilanNeuropsyAnnuelBaseView, BaseEditView):
    pass


class BilanNeuropsyCompletBaseView:
    model = BilanNeuropsyComplet


class BilanNeuropsyCompletListView(BilanNeuropsyCompletBaseView, BaseListView):
    new_form_label = 'Créer nouveau bilan complet'


class BilanNeuropsyCompletDetailView(BilanNeuropsyCompletBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class BilanNeuropsyCompletEditView(BilanNeuropsyCompletBaseView, BaseEditView):
    pass


class ObservationNeuropsyBaseView:
    model = ObservationNeuropsy


class ObservationNeuropsyListView(ObservationNeuropsyBaseView, BaseListView):
    new_form_label = 'Créer nouvelle observation'


class ObservationNeuropsyDetailView(ObservationNeuropsyBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class ObservationNeuropsyEditView(ObservationNeuropsyBaseView, BaseEditView):
    pass
