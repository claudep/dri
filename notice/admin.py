from django.contrib import admin

from .models import NoticeSubscription

admin.site.register(NoticeSubscription)
