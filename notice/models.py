from django.db import models

from common.fields import ChoiceArrayField
from person.models import Person, User


class NoticeSubscription(models.Model):
    EVENTS = (
        ('regimes', 'Modifications de la liste des régimes'),
    )
    SITUATIONS = (
        ('fhmn_int', 'Interne FHMN'),
        ('fhmn_ext', 'Externe FHMN'),
        ('fhne_int', 'Interne FHNE'),
        ('fhne_ext', 'Externe FHNE'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    event = models.CharField(max_length=20, choices=EVENTS)
    situations = ChoiceArrayField(
        models.CharField(max_length=15, choices=Person.SITUATION_CHOICES),
        verbose_name="Situations"
    )

    def __str__(self):
        return "{} pour {} ({})".format(self.get_event_display(), self.user, self.situations)

    def get_situations_display(self):
        return ", ".join(dict(self.SITUATIONS)[site] for site in self.situations)

    @classmethod
    def email_for_event(cls, event, person):
        return NoticeSubscription.objects.filter(event=event, situations__contains=[person.situation]
            ).values_list('user__email', flat=True)
