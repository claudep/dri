from django.core import mail
from django.test import TestCase
from django.urls import reverse

from person.models import Person, User
from .models import NoticeSubscription


class NoticeTests(TestCase):
    def test_regime_notice(self):
        user = User.objects.create(username='jill', first_name='Jill', last_name='Martin', email='jill@example.org')
        pers_mn = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        pers_ne = Person.objects.create(nom='Blair', prenom='Tony', situation='fhne_int')
        NoticeSubscription.objects.create(event='regimes', user=user, situations=['fhmn_int'])

        self.client.force_login(user)
        post_data = {
            'person': pers_mn.pk, 'diagnostic': "Blah", 'problemes': 'Blih', 'allergies': 'Bloh',
        }
        response = self.client.post(reverse('nourrir-edit', args=[pers_mn.pk]), data=post_data)
        # No value related to the notice changed
        self.assertEqual(len(mail.outbox), 0)
        post_data.update({'naimepas': 'Le boudin', 'regimes': 'Sans sel'})
        response = self.client.post(reverse('nourrir-edit', args=[pers_mn.pk]), data=post_data)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, '[DRI] Changement liste des régimes')
        self.assertEqual(mail.outbox[0].body, '''Madame, Monsieur,
Une modification du contenu de la liste des régimes a été effectuée pour le résident Smith John:
  - Régimes: Sans sel
  - N'aime pas: Le boudin

Liste à jour: http://testserver/infos/fhmn_int/regimes/

Ceci est un message automatique, ne pas répondre.'''
        )
