from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Permission

from .models import (
    ActionIS, Attachment, Contact, Document, EntreeSortie, HistoireDeVie,
    Modification, Person, Relation, RepartitionTaches, Role, SoutienSocial, User
)


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    search_fields = ('nom', 'prenom')
    list_display = ('nom', 'prenom', 'npa', 'localite', 'email', 'actif')
    list_filter = ('actif',)
    ordering = ('nom', 'prenom')


@admin.register(Modification)
class ModificationAdmin(admin.ModelAdmin):
    list_display = ('date', 'person', 'precision', 'rubrique')
    list_filter = ('manual', 'fhne', 'fhmn')
    search_fields = ('person__nom', 'precision')
    ordering = ('-date',)


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    search_fields = ('nom', 'prenom')
    list_display = ('nom', 'prenom', 'date_naissance', 'actif', 'situation')
    autocomplete_fields = (
        'repondant_ergo', 'repondant_ergo_uaj', 'repondant_physio',
        'repondant_soins1', 'repondant_soins2', 'repondant_anim',
        'repondant_off', 'repondant_ther', 'medecin_traitant', 'dentiste',
    )
    ordering = ('nom', 'prenom')


@admin.register(EntreeSortie)
class EntreeSortieAdmin(admin.ModelAdmin):
    search_fields = ('person__nom', 'motif')
    list_display = ('person', 'date', 'entree_sortie', 'motif')
    list_filter = ('entree_sortie',)


@admin.register(Relation)
class RelationAdmin(admin.ModelAdmin):
    search_fields = ('person__nom', 'contact__nom', 'role__nom')
    list_display = ('person', 'contact', 'role', 'note')
    autocomplete_fields = ('contact',)


@admin.register(User)
class FHUserAdmin(UserAdmin):
    list_display = (
        'username', 'email', 'first_name', 'last_name', 'is_active', 'archived', 'last_login'
    )
    list_filter = ('is_active', 'archived', 'groups')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fieldsets[1][1]['fields'] = (
            'first_name', 'last_name', 'email', 'secteur', 'fonction', 'infos_a_lire', 'test', 'expiration', 'archived'
        )


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    search_fields = ('name', 'codename')
    list_display = ('name', 'codename')


admin.site.register(ActionIS)
admin.site.register(Attachment)
admin.site.register(Document)
admin.site.register(HistoireDeVie)
admin.site.register(RepartitionTaches)
admin.site.register(Role)
admin.site.register(SoutienSocial)
