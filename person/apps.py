from django.apps import AppConfig


class PersonConfig(AppConfig):
    name = 'person'
    verbose_name = 'Résidents'

    def ready(self):
        from django.db.models import CharField
        from django.db.models.functions import Length
        CharField.register_lookup(Length)
