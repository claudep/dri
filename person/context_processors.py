from person.models import Modification


def site_messages(request):
    if not request.user.is_authenticated or not request.user.infos_a_lire:
        return {}
    return {
        'modif_messages': Modification.objects.in_bulk(request.user.infos_a_lire.split(',')).values(),
    }
        
