import operator
from functools import reduce

from django import forms
from django.apps import apps
from django.contrib.contenttypes.forms import BaseGenericInlineFormSet, generic_inlineformset_factory
from django.db import transaction
from django.db.models import Q
from django.utils import timezone
from django.utils.functional import cached_property, classproperty

from besoins.models import BesoinsAlimentaires, Impotence, RecueilBesoins
from common.fields import PickDateWidget, PickSplitDateTimeWidget
from soins.models import DiagnosticMedical
from .models import (
    Attachment, Contact, EntreeSortie, Modification, Person, Relation,
    RepartitionTaches, User
)


class ContactAutocompleteWidget(forms.Select):
    def __init__(self, attrs=None):
        super().__init__(attrs={'class': 'contact-choice'})


class UserChoiceField(forms.ModelChoiceField):
    def __init__(self, **kwargs):
        kwargs['queryset'] = User.objects.exclude(archived=True).order_by('last_name', 'first_name')
        super().__init__(**kwargs)
        self.widget.attrs['class'] = 'select-style'

    def label_from_instance(self, obj):
        return "%s %s" % (obj.last_name or obj.username, obj.first_name)


class UtilsMixin:
    def changed_labels(self):
        return [self.fields[f].label for f in self.changed_data]


class PersonDetailForm(UtilsMixin, forms.ModelForm):
    class Meta:
        model = Person
        fields = [
            'nom', 'prenom', 'date_naissance', 'date_demande', 'prioritaire',
            'no_chambre', 'photo'
        ]
        widgets = {
            'date_naissance': PickDateWidget,
            'date_demande': PickDateWidget,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if kwargs['instance'] and kwargs['instance'].date_admission:
            del self.fields['date_demande']
        if not kwargs['instance']:
            del self.fields['no_chambre']

    def save(self, **kwargs):
        is_new = self.instance.pk is None
        obj = super().save(**kwargs)
        if is_new:
            # Create one-to-one models
            BesoinsAlimentaires.objects.create(person=obj)
            RecueilBesoins.objects.create(person=obj)
            Impotence.objects.create(person=obj)
            DiagnosticMedical.objects.create(person=obj)
        return obj


class ContactForm(forms.ModelForm):
    foruser = forms.ModelChoiceField(
        queryset=User.objects.filter(archived=False), required=False, widget=forms.HiddenInput
    )
    class Meta:
        model = Contact
        exclude = ['actif']

    def save(self, **kwargs):
        contact = super().save(**kwargs)
        if self.cleaned_data.get('foruser'):
            self.cleaned_data['foruser'].contact = contact
            self.cleaned_data['foruser'].save()
        return contact


class AdmissionForm(UtilsMixin, forms.ModelForm):
    class Meta:
        model = Person
        fields = [
            'civilite', 'etat_civil', 'origine', 'langue_mat', 'taille',
            'profession', 'confession', 'pratiquant', 'provenance', 'repondant_off',
            'repondant_off_self', 'repondant_ther', 'domicile_rue',
            'domicile_loc', 'domicile_tel', 'portable', 'dom_legal_rue', 'dom_legal_loc',
            'hors_canton', 'medecin_traitant', 'dentiste', 'referent1', 'referent2',
            'repondant_ergo', 'repondant_ergo_uaj', 'repondant_physio', 'repondant_atelier',
            'repondant_soins1', 'repondant_soins2', 'repondant_anim',
            'no_dossier_comptable', 'caisse_mal_acc', 'no_assure_lamal', 'caisse_mal_acc_compl',
            'no_assure_compl', 'assurance_rc', 'assurance_menage', 'no_avs', 'pc',
            'degre_plaisir', 'no_plaisir', 'date_plaisir',
        ]
        field_classes = {
            'referent1': UserChoiceField,
            'referent2': UserChoiceField,
        }
        widgets = {
            'date_plaisir': PickDateWidget,
        }

    def __init__(self, *args, user=None, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            if isinstance(field, forms.ModelChoiceField) and field.queryset.model == Contact:
                # Used by the select2 JS to set autocomplete
                field.widget.attrs['class'] = 'contact-choice'
        if not user.has_perm('person.plaisir_edition'):
            del self.fields['degre_plaisir']
            del self.fields['no_plaisir']
            del self.fields['date_plaisir']


class EntreeSortieForm(forms.ModelForm):
    situation = forms.ChoiceField(
        choices=Person.SITUATION_CHOICES, widget=forms.RadioSelect, required=False
    )

    class Meta:
        model = EntreeSortie
        exclude = ['person']
        field_classes = {'date': forms.SplitDateTimeField}
        widgets = {
            'date': PickSplitDateTimeWidget,
            'entree_sortie': forms.RadioSelect,
        }
        labels = {'entree_sortie': ''}

    def __init__(self, *args, last_es=None, **kwargs):
        super().__init__(*args, **kwargs)
        # Limit entree_sortie choices depending on current state
        all_choices = self.fields['entree_sortie'].choices

        if self.instance.pk:
            # Existing instance can not modify entree_sortie
            self.fields['entree_sortie'].choices = [c for c in all_choices if c[0] == self.instance.entree_sortie]
            del self.fields['situation']
        else:
            avail_choices = EntreeSortie.available_choices(last_es)
            self.fields['entree_sortie'].choices = [c for c in all_choices if c[0] in avail_choices]
            if avail_choices.isdisjoint({'a', 'at'}):
                del self.fields['situation']

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('entree_sortie') in {'a', 'at'} and not cleaned_data.get('situation'):
            raise forms.ValidationError("Vous devez indiquer la situation dans le cas d’une arrivée.")

    def save(self, **kwargs):
        instance = super().save(**kwargs)
        person = instance.person
        if 'situation' in self.fields:
            person.situation = self.cleaned_data['situation']
            person.save()
        elif person.situation and instance.entree_sortie != EntreeSortie.DEPART:
            instance.situation = person.situation
            instance.save()
        if instance.entree_sortie in {EntreeSortie.DEPART, EntreeSortie.DEPART_TEMPORAIRE}:
            person.actif = False
            if instance.entree_sortie == EntreeSortie.DEPART:
                person.situation = ''
            person.save()
        elif instance.entree_sortie in {EntreeSortie.ARRIVEE, EntreeSortie.ARRIVEE_TEMPORAIRE}:
            person.actif = True
            person.save()
        return instance


class RangementForm(UtilsMixin, forms.ModelForm):
    class Meta:
        model = RepartitionTaches
        fields = [
            'decisions_apa', 'carte_ident', 'permis_sejour', 'certif_am', 'contrats_rc',
            'carte_avs', 'carte_ai_cff', 'bons_taxi', 'doc_autres_txt', 'doc_autres',
        ]


class RepartitionForm(UtilsMixin, forms.ModelForm):
    class Meta:
        model = RepartitionTaches

        @classproperty
        def fields(self):
            return [
                f.name for f in self.model._meta.get_fields()
                if f.name not in RangementForm._meta.fields and f.name not in ['id', 'date', 'person']
            ]


class RelationForm(forms.ModelForm):
    class Meta:
        model = Relation
        fields = '__all__'
        widgets = {
            'person': forms.HiddenInput,
        }

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super().__init__(*args,**kwargs)
        if self.instance.pk:
            del self.fields['contact']
        else:
            self.fields['contact'].widget.attrs['class'] = 'contact-choice'
        if not user.has_perm('person.change_person'):
            del self.fields['proche_sig']


class ModificationForm(forms.ModelForm):
    GROUPS_CHOICES = (
        ('tous', 'Tout le personnel FH'),
        ('fhmn', 'Personnel FHMN'),
        ('fhne', 'Personnel FHNE'),
        ('fhmn-soins', 'Personnel soignant FHMN'),
        ('fhne-soins', 'Personnel soignant FHNE'),
    )
    precision = forms.CharField(label="Information", widget=forms.Textarea, required=True)
    groupes_cible = forms.MultipleChoiceField(
        label="Demander conf. de lecture par", choices=GROUPS_CHOICES,
        widget=forms.CheckboxSelectMultiple(attrs={'class': 'nobullets'}),
        required=False,
    )

    class Meta:
        model = Modification
        fields = ['date', 'precision']
        field_classes = {'date': forms.SplitDateTimeField}
        widgets = {'date': PickSplitDateTimeWidget}

    def __init__(self, *args, location=None, user=None, **kwargs):
        super().__init__(*args, **kwargs)
        if kwargs['instance'] is None and user and user.has_perm('person.info_to_group'):
            self.fields['groupes_cible'].choices = [
                gr for gr in self.GROUPS_CHOICES if gr[0].startswith(location) or gr[0] == 'tous'
            ]
        else:
            del self.fields['groupes_cible']

    def save(self, **kwargs):
        groupes_cible = self.cleaned_data.get('groupes_cible')
        if groupes_cible and 'tous' in groupes_cible:
            self.instance.fhmn = True
            self.instance.fhne = True
        obj = super().save(**kwargs)
        if groupes_cible:
            soins_groups = {'Infirmiers', 'Infirmiers-chefs', 'Soignants'}
            # Save the obj id to each target person infos_a_lire field.
            with transaction.atomic():
                if 'tous' in groupes_cible:
                    users = User.objects.real_users().select_for_update()
                elif 'fhmn' in groupes_cible:
                    users = User.objects.by_site('fhmn').real_users().select_for_update()
                elif 'fhne' in groupes_cible:
                    users = User.objects.by_site('fhne').real_users().select_for_update()
                elif 'fhmn-soins' in groupes_cible:
                    users = [
                        user for user in User.objects.by_site('fhmn').real_users().select_for_update().prefetch_related('groups')
                        if set(g.name for g in user.groups.all()) & soins_groups
                    ]
                elif 'fhne-soins' in groupes_cible:
                    users = [
                        user for user in User.objects.by_site('fhne').real_users().select_for_update().prefetch_related('groups')
                        if set(g.name for g in user.groups.all()) & soins_groups
                    ]
                users = list(users)
                for user in users:
                    user.infos_a_lire = ','.join((user.infos_a_lire.split(',') if user.infos_a_lire else []) + [str(obj.pk)])
                User.objects.bulk_update(users, ['infos_a_lire'])
        return obj


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            'username', 'first_name', 'last_name', 'email', 'secteur', 'resp_secteur',
            'fonction', 'contact', 'is_active', 'test', 'archived', 'groups', 'expiration',
        ]
        widgets = {
            'contact': ContactAutocompleteWidget,
            'groups': forms.CheckboxSelectMultiple(attrs={'class': 'nobullets'}),
            'expiration': PickDateWidget,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groups'].queryset = self.fields['groups'].queryset.order_by('name')


class FilterForm(forms.Form):
    text = forms.CharField(label="Filtrer", max_length=150, required=False)

    def __init__(self, search_fields, *args, **kwargs):
        self.search_fields = search_fields
        super().__init__(*args, **kwargs)

    def search(self, base_qs):
        terms = [t for t in self.cleaned_data['text'].split() if t]
        if terms:
            q_list = []
            for field_name in self.search_fields:
                q_list.extend([Q(**{'%s__unaccent__icontains' % field_name: term}) for term in terms])
            return base_qs.filter(reduce(operator.or_, q_list))
        return base_qs


class ModifFilterForm(forms.Form):
    """A filtering form for modification lists."""
    DOMAIN_CHOICES = (
        ('', 'Tous'), ('person', 'Données'), ('bilanannuel', 'Bilan annuel'),
        ('soins', 'Soins'), ('ergo', 'Ergo'), ('uaj', 'UAJ'), ('physio', 'Physio'),
        ('neuropsy', 'Neuropsy'), ('ateliers', 'Ateliers'), ('animation', 'Animation'),
        ('musico', 'Musico'),
    )
    domaine = forms.ChoiceField(choices=DOMAIN_CHOICES, required=False)
    manual_only = forms.BooleanField(label="Seul. infos manuelles", required=False)

    def __init__(self, data=None, manual_only=False, **kwargs):
        self.manual_only_default = manual_only
        # Exclude 'page' so form is not considered bound with only page param.
        data = {k: v for k, v in data.items() if k != 'page'} if data is not None else None
        if not data:
            kwargs['initial'] = {'manual_only': manual_only}
        super().__init__(data=data or None, **kwargs)

    def apply(self, queryset, manual_default=False):
        if self.is_bound and self.is_valid():
            if self.cleaned_data['manual_only']:
                queryset = queryset.filter(manual=True)
            dom = self.cleaned_data['domaine']
            if dom:
                if dom == 'person':
                    qfilter = Q(rubrique__startswith='person.') | Q(rubrique__startswith='besoins.')
                else:
                    qfilter = Q(rubrique__startswith=dom + '.')
                queryset = queryset.filter(qfilter)
        elif self.manual_only_default:
            queryset = queryset.filter(manual=True)
        return queryset


class InstanceSearchForm(forms.Form):
    date_du = forms.DateField(label="Du", widget=PickDateWidget, required=False)
    date_au = forms.DateField(label="Au", widget=PickDateWidget, required=False)
    qui = UserChoiceField(label="Par", required=False)
    texte = forms.CharField(label="Texte", required=False)

    def __init__(self, *args, location=None, **kwargs):
        self.location = location
        super().__init__(*args, **kwargs)
        self.fields['instance'] = forms.ChoiceField(
            label="Rechercher dans",
            choices=[('', '----')] + [
                (mod._meta.label, mod._meta.verbose_name) for mod in self.get_search_models()
            ]
        )

    def get_search_models(self):
        for app in apps.get_app_configs():
            for model in app.get_models():
                if getattr(model, 'searchable', False):
                    yield model

    @cached_property
    def model(self):
        app_label, model_name = self.cleaned_data['instance'].split('.')
        return apps.get_app_config(app_label).get_model(model_name)

    def search(self):
        qs = self.model.objects.filter(person__situation__startswith=self.location[:4]).order_by('-date')
        if self.cleaned_data['qui']:
            qs = qs.filter(createur=self.cleaned_data['qui'])
        if self.cleaned_data['date_du']:
            qs = qs.filter(date__date__gte=self.cleaned_data['date_du'])
        if self.cleaned_data['date_au']:
            qs = qs.filter(date__date__lte=self.cleaned_data['date_au'])
        if self.cleaned_data['texte']:
            q_filter = Q()
            for field_name in self.model.searchable['txt']:
                q_filter |= Q(**{'%s__unaccent__icontains' % field_name: self.cleaned_data['texte']})
            qs = qs.filter(q_filter)
        return qs[:50]


class ContactMergeForm(forms.Form):
    merge_with = forms.ModelChoiceField(
        queryset=Contact.objects.all(), widget=ContactAutocompleteWidget
    )


class AttachmentForm(forms.ModelForm):
    class Meta:
        model = Attachment
        fields = ['fichier', 'titre']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            del self.fields['fichier']


class AttachmentFormSetBase(BaseGenericInlineFormSet):
    def save_new(self, form, **kwargs):
        # Populate 'forced' fields
        form.instance.createur = self.instance.createur
        form.instance.date = timezone.now()
        super().save_new(form, **kwargs)


class AttachmentFormMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        AttachmentFormSet = generic_inlineformset_factory(
            Attachment, form=AttachmentForm, formset=AttachmentFormSetBase, extra=1, can_delete=True
        )
        self.attach_forms = AttachmentFormSet(
            data=kwargs.get('data'), files=kwargs.get('files'), instance=self.instance, prefix='attachments'
        )

    def is_valid(self):
        return all([super().is_valid(), self.attach_forms.is_valid()])

    def has_changed(self):
        return any([super().has_changed(), self.attach_forms.has_changed()])

    def save(self, **kwargs):
        instance = super().save(**kwargs)
        self.attach_forms.save(**kwargs)
        return instance


class AttachmentModelForm(AttachmentFormMixin, forms.ModelForm):
    """A class to pass as form attribute to modelform_factory."""
    pass
