# Generated by Django 2.1.1 on 2018-10-27 14:50

import common.fields
from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone

from ..models import RepartitionTaches


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
                'verbose_name_plural': 'users',
                'verbose_name': 'user',
            },
        ),
        migrations.CreateModel(
            name='ActionIS',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(blank=True, null=True, verbose_name='Date ouverte')),
                ('date_fermee', models.DateField(blank=True, null=True, verbose_name='Date fermeture')),
                ('visa_ouverture', models.CharField(blank=True, max_length=35, verbose_name='Visa ouverture')),
                ('visa_fermeture', models.CharField(blank=True, max_length=25, verbose_name='Visa fermeture')),
                ('etat', models.CharField(choices=[('o', 'Ouverte'), ('f', 'Fermée')], default='o', max_length=1, verbose_name='État')),
                ('delai', models.DateField(blank=True, null=True, verbose_name='Délai')),
                ('secteur', models.CharField(blank=True, max_length=20, verbose_name='Secteur')),
                ('nature', models.CharField(blank=True, choices=[('chron', 'Chronique'), ('ponct', 'Ponctuelle')], max_length=5, verbose_name='Nature')),
                ('probleme', models.TextField(blank=True, verbose_name='Problèmes')),
                ('objectifs', models.TextField(blank=True, verbose_name='Objectifs')),
                ('actions', models.TextField(blank=True, verbose_name='Actions')),
                ('evaluations', models.TextField(blank=True, verbose_name='Évaluations')),
                ('createur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
            ],
            options={
                'verbose_name_plural': 'Actions intersecteurs',
                'verbose_name': 'Action intersecteurs',
            },
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('civilite', models.CharField(blank=True, max_length=12, verbose_name='Civilité')),
                ('nom', models.CharField(max_length=50)),
                ('prenom', models.CharField(blank=True, max_length=50, verbose_name='Prénom')),
                ('adresse', models.CharField(blank=True, max_length=150, verbose_name='Adresse')),
                ('npa', models.CharField(blank=True, max_length=5, verbose_name='NPA')),
                ('localite', models.CharField(blank=True, max_length=50, verbose_name='Localité')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='Courriel')),
                ('telephone', models.CharField(blank=True, max_length=60, verbose_name='Téléphone')),
                ('portable', models.CharField(blank=True, max_length=60, verbose_name='Natel')),
            ],
            options={'ordering': ('nom', 'prenom')},
        ),
        migrations.CreateModel(
            name='HistoireDeVie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('updated', models.DateTimeField(blank=True, null=True, verbose_name='Date de mise à jour')),
                ('histoire', models.TextField(blank=True, verbose_name='Histoire de vie')),
                ('descr_biologiques', models.TextField(blank=True, verbose_name='Descriptions biologiques')),
                ('descr_psychologiques', models.TextField(blank=True, verbose_name='Descriptions psychologiques')),
                ('descr_sociales', models.TextField(blank=True, verbose_name='Descriptions sociales')),
                ('creativite', models.TextField(blank=True, verbose_name='Créativité')),
                ('dispo_anticipees', models.TextField(blank=True, verbose_name='Dispositions anticipées')),
                ('formation', models.TextField(blank=True, verbose_name='Éducation formation')),
                ('evenements', models.TextField(blank=True, verbose_name='Événements marquants')),
                ('vie_sociale', models.TextField(blank=True, verbose_name='Famille vie sociale')),
                ('filiation', models.TextField(blank=True, verbose_name='Filiation')),
                ('lieux_habit', models.TextField(blank=True, verbose_name="Lieux d'habitation")),
                ('metier', models.TextField(blank=True, verbose_name='Métier')),
                ('spiritualite', models.TextField(blank=True, verbose_name='Spritualité')),
                ('vocation', models.TextField(blank=True, verbose_name='Vocation')),
                ('visa', models.CharField(blank=True, max_length=15, verbose_name='Visa')),
            ],
            options={
                'verbose_name_plural': 'Histoires de vie',
                'verbose_name': 'Histoire de vie',
            },
        ),
        migrations.CreateModel(
            name='Modification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('person_str', models.CharField(blank=True, max_length=25)),
                ('date', models.DateField(blank=True, null=True, verbose_name='Date')),
                ('visa', models.CharField(blank=True, max_length=25, verbose_name='Visa')),
                ('fhmn', models.BooleanField(default=False)),
                ('fhne', models.BooleanField(default=False)),
                ('precision', models.TextField(blank=True, verbose_name='Précision')),
                ('rubrique', models.TextField(blank=True, verbose_name='Rubrique')),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fm_id', models.IntegerField(unique=True, verbose_name='FileMaker id')),
                ('updated', models.DateTimeField(blank=True, null=True, verbose_name='Date de modification')),
                ('actif', models.BooleanField(default=True)),
                ('prenom', models.CharField(max_length=40, verbose_name='Prénom')),
                ('nom', models.CharField(max_length=40, verbose_name='Nom')),
                ('civilite', models.CharField(blank=True, choices=[('Madame', 'Madame'), ('Monsieur', 'Monsieur')], max_length=10, verbose_name='Civilité')),
                ('date_naissance', models.DateField(blank=True, null=True, verbose_name='Date de naissance')),
                ('lieu_naissance', models.CharField(blank=True, max_length=50, verbose_name='Lieu de naissance')),
                ('etat_civil', models.CharField(blank=True, choices=[('celib', 'Célibataire'), ('marie', 'Marié-e'), ('separe', 'Séparé'), ('divorce', 'Divorcé-e'), ('veuf', 'Veuf-ve')], max_length=10, verbose_name='État-civil')),
                ('origine', models.CharField(blank=True, max_length=50, verbose_name='Origine')),
                ('hors_canton', models.CharField(blank=True, choices=[('be', 'BE'), ('fr', 'FR'), ('ge', 'GE'), ('ju', 'JU'), ('vd', 'VD'), ('vs', 'VS'), ('f', 'F')], max_length=5, verbose_name='Hors canton')),
                ('langue_mat', models.CharField(blank=True, max_length=20, verbose_name='Langue maternelle')),
                ('langue_pref', models.CharField(blank=True, max_length=20, verbose_name='Langue préférée')),
                ('domicile_rue', models.CharField(blank=True, max_length=80, verbose_name='Domicile de fait, rue')),
                ('domicile_loc', models.CharField(blank=True, max_length=50, verbose_name='Domicile de fait, npa et localité')),
                ('domicile_tel', models.CharField(blank=True, max_length=40, verbose_name='Domicile de fait, téléphone')),
                ('dom_legal_rue', models.CharField(blank=True, max_length=80, verbose_name='Domicile légal, rue')),
                ('dom_legal_loc', models.CharField(blank=True, max_length=50, verbose_name='Domicile légal, npa et localité')),
                ('situation', models.CharField(blank=True, choices=[('fhmn_int', 'Interne FHMN'), ('fhmn_ext', 'Externe FHMN'), ('fhne_int', 'Interne FHNE'), ('fhne_ext', 'Externe FHNE')], max_length=15, verbose_name='Situation')),
                ('no_dossier_comptable', models.CharField(blank=True, max_length=10, verbose_name='N° dossier comptable')),
                ('no_chambre', models.CharField(blank=True, max_length=20, verbose_name='N° chambre')),
                ('profession', models.CharField(blank=True, max_length=50, verbose_name='Profession')),
                ('provenance', models.CharField(blank=True, max_length=50, verbose_name='Provenance')),
                ('confession', models.CharField(blank=True, max_length=50, verbose_name='Confession')),
                ('pratiquant', models.BooleanField(default=False)),
                ('date_admission', models.DateField(blank=True, null=True, verbose_name='Date d’admission')),
                ('date_depart', models.DateField(blank=True, null=True, verbose_name='Date de départ')),
                ('cause_depart', models.TextField(blank=True, verbose_name='Cause du départ')),
                ('directives_anticipees', models.BooleanField(default=False, verbose_name='Directives anticipées')),
                ('date_directives_anticipees', models.DateField(blank=True, null=True, verbose_name='Date des directives anticipées')),
                ('fichier_directives_anticipees', models.FileField(blank=True, upload_to='directives_anticipees', verbose_name='Fichier des directives anticipées')),
                ('no_avs', models.CharField(blank=True, max_length=16, verbose_name='N° AVS')),
                ('pc', models.BooleanField(default=False, verbose_name='PC')),
                ('caisse_mal_acc', models.CharField(blank=True, max_length=60, verbose_name='Caisse maladie')),
                ('no_assure_lamal', models.CharField(blank=True, max_length=50, verbose_name="Numéro d’assuré")),
                ('caisse_mal_acc_compl', models.CharField(blank=True, max_length=80, verbose_name='Caisse maladie compl')),
                ('no_assure_compl', models.CharField(blank=True, max_length=50, verbose_name="Numéro d’assuré maladie compl")),
                ('caisse_ai_acc', models.CharField(blank=True, max_length=50, verbose_name='Assurance accident')),
                ('assurance_menage', models.CharField(blank=True, max_length=50, verbose_name='Assurance ménage')),
                ('assurance_rc', models.CharField(blank=True, max_length=50, verbose_name='Assurance RC')),
                ('referent1', models.CharField(blank=True, max_length=50, verbose_name='Référent 1')),
                ('referent2', models.CharField(blank=True, max_length=50, verbose_name='Référent 2')),
                ('repondant_off_self', models.BooleanField(default=False, verbose_name='Résident est répondant')),
                ('repondant_off_role', models.CharField(blank=True, max_length=50, verbose_name='Rôle du répondant officiel')),
                ('no_plaisir', models.CharField(blank=True, max_length=15, verbose_name='Numéro plaisir')),
                ('date_plaisir', models.DateField(blank=True, null=True, verbose_name='Date plaisir')),
                ('degre_plaisir', models.CharField(blank=True, max_length=8, verbose_name='Degré de plaisir')),
                ('dentiste', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='person.Contact', verbose_name='Dentiste')),
                ('medecin_traitant', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='person.Contact', verbose_name='Médecin traitant')),
                ('repondant_anim', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='person.Contact', verbose_name='Répondant animation')),
                ('repondant_ergo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='person.Contact', verbose_name='Répondant ergo')),
                ('repondant_off', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='person.Contact', verbose_name='Répondant officiel')),
                ('repondant_physio', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='person.Contact', verbose_name='Répondant physio')),
                ('repondant_soins1', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='person.Contact', verbose_name='Répondant soins')),
                ('repondant_soins2', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='person.Contact',
                verbose_name='Répondant soins (2)')),
            ],
            options={
                'db_table': 'person',
                'verbose_name': 'Résident',
            },
        ),
        migrations.CreateModel(
            name='Relation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contact', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='person.Contact')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='relations', to='person.Person')),
            ],
        ),
        migrations.CreateModel(
            name='RepartitionTaches',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(blank=True, null=True, verbose_name='Date de mise à jour')),
                ('achats_produits', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Achats divers produits de toilette etc')),
                ('achats_vetements', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Vêtements')),
                ('achats_autres', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Autres')),
                ('achats_autres_txt', models.CharField(blank=True, max_length=100, verbose_name='Autres achats texte')),
                ('boite_aux_lettres', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Gestion de la boîte aux lettres')),
                ('assurance', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Assurance documents officiels etc')),
                ('paiements', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Paiement des factures')),
                ('argent_poche', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Gestion argent de poche')),
                ('comm_bons_taxi', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Commande des bons taxi')),
                ('gestion_autres', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Autres')),
                ('gestion_autres_txt', models.CharField(blank=True, max_length=100, verbose_name='Autres administratif texte')),
                ('organisation_vac', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Organisation des vacances')),
                ('prep_bagages', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Préparation des bagages et des documents vacances')),
                ('vac_autres', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Autres')),
                ('vac_autres_txt', models.CharField(blank=True, max_length=100, verbose_name='Autres vacances texte')),
                ('medicaux', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Médicaux')),
                ('physio_ext', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Physiothérapie extérieure')),
                ('ergo_ext', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Ergothérapie extérieure')),
                ('dentiste', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Dentiste devis etc')),
                ('lunettes', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Lunettes de vue')),
                ('coiffeur', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Coiffeur pédicure')),
                ('reserv_taxi', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Réservations annulations du taxi')),
                ('mobilier', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Mobilier décoration')),
                ('orga_studio', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Organisation du studio et rangements')),
                ('studio_autres', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Autres studio')),
                ('studio_autres_txt', models.CharField(blank=True, max_length=100, verbose_name='Autres studio texte')),
                ('prep_camp', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Préparation des bagages et des documents camps')),
                ('camp_autres', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.REPART_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Autres camp')),
                ('camp_autres_txt', models.CharField(blank=True, max_length=100, verbose_name='Autres camps texte')),
                ('decisions_apa', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.RANGEMENTS_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name="Décisions de l’Autorité de protection de l’adulte")),
                ('carte_ident', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.RANGEMENTS_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name="Carte d’identité")),
                ('permis_sejour', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.RANGEMENTS_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Permis de séjour attestations domicile')),
                ('certif_am', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.RANGEMENTS_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name="Certificat d’assurance maladie")),
                ('contrats_rc', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.RANGEMENTS_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name="Contrats d’assurances RC ménage")),
                ('carte_avs', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.RANGEMENTS_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Carte AVS')),
                ('carte_ai_cff', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.RANGEMENTS_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Carte de légitimation AI Pro Inf voyage CFF')),
                ('bons_taxi', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.RANGEMENTS_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Bons taxi')),
                ('doc_autres', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=RepartitionTaches.RANGEMENTS_CHOICES, max_length=2), blank=True, null=True, size=None, verbose_name='Autres documents')),
                ('doc_autres_txt', models.CharField(blank=True, max_length=100, verbose_name='Autres document texte')),
                ('person', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='person.Person')),
            ],
            options={
                'verbose_name_plural': 'Répartitions de tâches',
                'verbose_name': 'Répartition de tâches',
            },
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=30, unique=True)),
            ],
            options={
                'ordering': ['nom'],
                'verbose_name': 'Rôle',
            },
        ),
        migrations.CreateModel(
            name='SoutienSocial',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(blank=True, null=True, verbose_name='Date')),
                ('visa', models.CharField(blank=True, max_length=25, verbose_name='Visa')),
                ('decisions', models.TextField(blank=True, verbose_name='Décisions p.v.')),
                ('intervenants', models.TextField(blank=True, verbose_name='Intervenants')),
                ('motifs', models.TextField(blank=True, verbose_name='Motifs raisons')),
                ('createur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='person.Person')),
            ],
            options={
                'verbose_name_plural': 'Réseaux',
                'verbose_name': 'Réseau',
            },
        ),
        migrations.AddField(
            model_name='relation',
            name='role',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='person.Role'),
        ),
        migrations.AddField(
            model_name='modification',
            name='person',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='person.Person'),
        ),
        migrations.AddField(
            model_name='histoiredevie',
            name='person',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='person.Person'),
        ),
        migrations.AddField(
            model_name='actionis',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='person.Person'),
        ),
    ]
