from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0002_unaccent_ext'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='fm_id',
            field=models.IntegerField(null=True, unique=True, verbose_name='FileMaker id'),
        ),
    ]
