from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0003_person_fmid_nullable'),
    ]

    operations = [
        migrations.RenameField(
            model_name='actionis',
            old_name='probleme',
            new_name='problemes',
        ),
    ]
