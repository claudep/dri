from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0004_pluralize_ais_problem'),
    ]

    operations = [
        migrations.AlterField(
            model_name='role',
            name='nom',
            field=models.CharField(max_length=50, unique=True),
        ),
    ]
