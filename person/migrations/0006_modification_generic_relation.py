from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('person', '0005_extend_role_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='modification',
            name='date',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Date'),
        ),
        migrations.AddField(
            model_name='modification',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Auteur'),
        ),
        migrations.AddField(
            model_name='modification',
            name='content_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.CASCADE, to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='modification',
            name='object_id',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
