from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0006_modification_generic_relation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actionis',
            name='date',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Date'),
        ),
    ]
