from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0008_document'),
    ]

    operations = [
        migrations.AddField(
            model_name='modification',
            name='manual',
            field=models.BooleanField(default=False),
        ),
    ]
