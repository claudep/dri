from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0010_relation_note'),
    ]

    operations = [
        migrations.RenameField(
            model_name='actionis',
            old_name='evaluations',
            new_name='evaluations_old',
        ),
        migrations.CreateModel(
            name='ActionEval',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(verbose_name='Date')),
                ('evaluation', models.TextField(verbose_name='Évaluation')),
                ('action', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='person.ActionIS')),
                ('createur', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
            ],
            options={
                'verbose_name': "Évaluation d'action",
                'verbose_name_plural': "Évaluations d'action",
            },
        ),
    ]
