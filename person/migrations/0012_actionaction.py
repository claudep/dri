from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0011_actioneval'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActionAction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('action', models.ForeignKey(on_delete=models.deletion.CASCADE, to='person.ActionIS')),
                ('createur', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
                ('date', models.DateTimeField(verbose_name='Date')),
                ('texte', models.TextField(verbose_name='Action')),
            ],
            options={
                'verbose_name_plural': "Action d'action",
                'verbose_name': "Action d'action",
            },
        ),
        migrations.RenameField(
            model_name='actioneval',
            old_name='evaluation',
            new_name='texte',
        ),
        migrations.RenameField(
            model_name='actionis',
            old_name='actions',
            new_name='actions_old',
        ),
    ]
