from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0012_actionaction'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='person',
            options={'permissions': (('access_fhmn', 'Voir résidents FHMN'), ('access_fhne', 'Voir résidents FHNE')), 'verbose_name': 'Résident'},
        ),
    ]
