from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0013_fhmn_fhne_perms'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='fichier_directives_anticipees',
        ),
    ]
