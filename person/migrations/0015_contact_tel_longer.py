from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0014_remove_person_fichier_directives_anticipees'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='portable',
            field=models.CharField(blank=True, max_length=80, verbose_name='Natel'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='telephone',
            field=models.CharField(blank=True, max_length=80, verbose_name='Téléphone'),
        ),
    ]
