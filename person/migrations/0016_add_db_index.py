from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0015_contact_tel_longer'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actionis',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='nom',
            field=models.CharField(db_index=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='modification',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='soutiensocial',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
    ]
