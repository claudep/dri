from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0016_add_db_index'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='fax',
            field=models.CharField(blank=True, max_length=30, verbose_name='Fax'),
        ),
    ]
