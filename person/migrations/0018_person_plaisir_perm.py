from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0017_contact_fax'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='person',
            options={'permissions': (('access_fhmn', 'Voir résidents FHMN'), ('access_fhne', 'Voir résidents FHNE'), ('plaisir_edition', 'Édition et liste des données plaisir')), 'verbose_name': 'Résident'},
        ),
    ]
