from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0020_document_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='repondant_ther',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, related_name='+', to='person.Contact', verbose_name='Répondant thérapeutique'),
        ),
    ]
