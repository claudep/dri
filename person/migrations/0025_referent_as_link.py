from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0024_person_portable'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='referent1_u',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='person',
            name='referent2_u',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
    ]
