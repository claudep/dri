from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0025_referent_as_link'),
    ]

    operations = [
        migrations.RenameField(
            model_name='person',
            old_name='referent1',
            new_name='referent1_str',
        ),
        migrations.RenameField(
            model_name='person',
            old_name='referent2',
            new_name='referent2_str',
        ),
        migrations.RenameField(
            model_name='person',
            old_name='referent1_u',
            new_name='referent1',
        ),
        migrations.RenameField(
            model_name='person',
            old_name='referent2_u',
            new_name='referent2',
        ),
    ]
