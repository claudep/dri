from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0026_rename_referent_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='fonction',
            field=models.CharField(blank=True, max_length=50, verbose_name='Fonction'),
        ),
        migrations.AlterField(
            model_name='person',
            name='referent1',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Référent 1'),
        ),
        migrations.AlterField(
            model_name='person',
            name='referent2',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Référent 2'),
        ),
    ]
