from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0027_add_contact_fonction'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='actif',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='user',
            name='expiration',
            field=models.DateField(blank=True, null=True, verbose_name="Date d'expiration du compte"),
        ),
    ]
