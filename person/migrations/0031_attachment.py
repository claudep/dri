from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('person', '0030_more_user_fields'),
    ]

    operations = [
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField()),
                ('fichier', models.FileField(upload_to='attachments', verbose_name='Fichier')),
                ('titre', models.CharField(blank=True, max_length=200, verbose_name='Titre')),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(on_delete=models.deletion.CASCADE, to='contenttypes.ContentType')),
                ('createur', models.ForeignKey(on_delete=models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Créateur')),
            ],
        ),
    ]
