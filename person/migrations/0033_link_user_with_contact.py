from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0032_histvie_date_createur'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='contact',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='person.Contact'),
        ),
    ]
