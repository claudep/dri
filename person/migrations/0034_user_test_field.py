from django.db import migrations, models
import person.models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0033_link_user_with_contact'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='test',
            field=models.BooleanField(default=False, help_text="Ce compte n'apparaît pas dans certaines listes comme les formations ou la liste du personnel", verbose_name='Compte test, externe ou provisoire'),
        ),
    ]
