from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0034_user_test_field'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='repondant_atelier',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, related_name='+', to='person.Contact', verbose_name='Répondant atelier'),
        ),
    ]
