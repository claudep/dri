from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0035_person_repondant_atlier'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='person',
            options={'permissions': (('access_fhmn', 'Voir résidents FHMN'), ('access_fhne', 'Voir résidents FHNE'), ('plaisir_edition', 'Édition et liste des données plaisir'), ('liste_attente', 'Consulter la liste d’attente')), 'verbose_name': 'Résident'},
        ),
    ]
