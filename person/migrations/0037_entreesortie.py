from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0036_perm_liste_attente'),
    ]

    operations = [
        migrations.CreateModel(
            name='EntreeSortie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField()),
                ('entree_sortie', models.CharField(choices=[('a', 'Arrivée dans l’institution'), ('s', 'Sortie temporaire'), ('r', 'Retour de sortie'), ('d', 'Départ de l’institution')], max_length=1)),
                ('motif', models.CharField(blank=True, max_length=150)),
                ('person', models.ForeignKey(on_delete=models.deletion.CASCADE, to='person.Person')),
            ],
            options={
                'ordering': ['-date'],
            },
        ),
    ]
