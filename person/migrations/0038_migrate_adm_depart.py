from django.db import migrations


def migrate_adm_depart(apps, schema_editor):
    Person = apps.get_model('person', 'Person')
    EntreeSortie = apps.get_model('person', 'EntreeSortie')
    for pers in Person.objects.all():
        if pers.date_admission:
            EntreeSortie.objects.create(person=pers, date=pers.date_admission, entree_sortie='a')
        if pers.date_depart:
            EntreeSortie.objects.create(person=pers, date=pers.date_depart, entree_sortie='d', motif=pers.cause_depart)
    

class Migration(migrations.Migration):

    dependencies = [
        ('person', '0037_entreesortie'),
    ]

    operations = [migrations.RunPython(migrate_adm_depart, migrations.RunPython.noop)]
