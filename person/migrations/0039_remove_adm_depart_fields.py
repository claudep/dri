from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0038_migrate_adm_depart'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='cause_depart',
        ),
        migrations.RemoveField(
            model_name='person',
            name='date_admission',
        ),
        migrations.RemoveField(
            model_name='person',
            name='date_depart',
        ),
    ]
