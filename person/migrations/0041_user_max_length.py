from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0040_user_infos_a_lire'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='first_name',
            field=models.CharField(blank=True, max_length=150, verbose_name='first name'),
        ),
    ]
