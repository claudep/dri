from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0041_user_max_length'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='fm_id',
        ),
    ]
