from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0042_remove_person_fm_id'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='modification',
            options={'permissions': (('info_to_group', 'Autoriser à publier une info pour certains groupes'),)},
        ),
    ]
