from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0043_modification_perm'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='modification',
            name='person_str',
        ),
    ]
