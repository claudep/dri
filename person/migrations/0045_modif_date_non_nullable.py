from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0044_remove_modification_person_str'),
    ]

    operations = [
        migrations.AlterField(
            model_name='modification',
            name='date',
            field=models.DateTimeField(db_index=True, verbose_name='Date'),
        ),
    ]
