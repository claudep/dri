from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0045_modif_date_non_nullable'),
    ]

    operations = [
        migrations.AddField(
            model_name='relation',
            name='proche_sig',
            field=models.BooleanField(default=False, verbose_name='Proche signifiant'),
        ),
    ]
