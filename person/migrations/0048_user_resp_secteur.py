from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0047_champs_demande'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='resp_secteur',
            field=models.BooleanField(default=False, verbose_name='Responsable de secteur'),
        ),
    ]
