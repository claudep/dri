from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0048_user_resp_secteur'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entreesortie',
            name='entree_sortie',
            field=models.CharField(choices=[('a', 'Arrivée dans l’institution'), ('s', 'Sortie temporaire'), ('r', 'Retour de sortie'), ('d', 'Départ de l’institution'), ('at', 'Arrivée séjour temporaire (UAT…)'), ('dt', 'Départ séjour temporaire')], max_length=2),
        ),
    ]
