from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0049_entreesortie_codes'),
    ]

    operations = [
        migrations.AddField(
            model_name='entreesortie',
            name='situation',
            field=models.CharField(blank=True, choices=[('fhmn_int', 'Interne FHMN'), ('fhmn_ext', 'Externe FHMN'), ('fhne_int', 'Interne FHNE'), ('fhne_ext', 'Externe FHNE')], max_length=15),
        ),
    ]
