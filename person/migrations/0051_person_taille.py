from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0050_entreesortie_situation'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='taille',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name="Taille"),
        ),
    ]
