import common.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0051_person_taille'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='photo',
            field=common.fields.ImageFieldThumb(blank=True, upload_to='photos', verbose_name='Photo'),
        ),
    ]
