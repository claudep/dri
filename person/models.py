import os
from datetime import date

from django.conf import settings
from django.contrib.auth.models import AbstractUser, UserManager as AuthUserManager
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Count, Max, OuterRef, Q, Subquery
from django.db.models.functions import Trunc
from django.urls import reverse
from django.utils import timezone

from common.fields import ChoiceArrayField, ImageFieldThumb
from common.utils import IMAGE_EXTS, icon_url

SITUATION_ADRESSES = {
    'fhmn': 'Rue des Moulins 22, 2300 La Chaux-de-Fonds, tél. 032 967 7300, fax 032 967 7301',
    'fhne': 'Rue de la Maladière 33, 2000 Neuchâtel, tél. 032 7200 300, fax 032 7200 400',
}


SECTEUR_CHOICES = (
    ('Administration', 'Administration'),
    ('Animation', 'Animation'),
    ('Ateliers', 'Ateliers'),
    ('Collaborateurs externes', 'Collaborateurs externes'),
    ('Cuisine', 'Cuisine'),
    ('Réadaptation', 'Réadaptation'),
    ('Soins infirmiers', 'Soins infirmiers'),
    ('Technique-Entretien', 'Technique-Entretien'),
)


class UserQuerySet(models.QuerySet):
    def real_users(self):
        return self.exclude(archived=True).exclude(test=True
            ).filter(Q(expiration__isnull=True) | Q(expiration__lt=date.today()))


class UserManager(AuthUserManager):
    use_in_migrations = False

    def by_site(self, site):
        """Return all users having perms for the specific `site` (fhmn or fhne)"""
        return self.with_perm(f'person.access_{site}')


class User(AbstractUser):
    """AUTH_USER_MODEL for this project."""
    secteur = models.CharField("Secteur", max_length=25, choices=SECTEUR_CHOICES, blank=True)
    resp_secteur = models.BooleanField("Responsable de secteur", default=False)
    fonction = models.CharField("Fonction", max_length=60, blank=True)
    contact = models.OneToOneField("Contact", on_delete=models.SET_NULL, null=True, blank=True)
    infos_a_lire = models.TextField(blank=True, help_text="List d’ids Modification, séparés par virgule")
    test = models.BooleanField(
        "Compte test, externe ou provisoire", default=False,
        help_text="Ce compte n'apparaît pas dans certaines listes comme les formations ou la liste du personnel"
    )
    expiration = models.DateField("Date d'expiration du compte", blank=True, null=True)
    archived = models.BooleanField("Ancien-ne employé-e", default=False)

    objects = UserManager.from_queryset(UserQuerySet)()

    def __str__(self):
        return self.get_full_name() or self.username

    def has_expired(self):
        return self.expiration is not None and self.expiration < date.today()

    @property
    def nom_prenom(self):
        return ' '.join([self.last_name, self.first_name])

    @property
    def initiales(self):
        try:
            return self.first_name[0] + self.last_name[0]
        except IndexError:
            return self.username

    def get_sites(self):
        sites = []
        if self.has_perm('person.access_fhmn'):
            sites.append('fhmn')
        if self.has_perm('person.access_fhne'):
            sites.append('fhne')
        return sites


class Contact(models.Model):
    civilite = models.CharField("Civilité", max_length=12, blank=True)
    nom = models.CharField(max_length=50, db_index=True)
    prenom = models.CharField("Prénom", max_length=50, blank=True)
    adresse = models.CharField("Adresse", max_length=150, blank=True)
    npa = models.CharField("NPA", max_length=5, blank=True)
    localite = models.CharField("Localité", max_length=50, blank=True)
    email = models.EmailField("Courriel", blank=True)
    fonction = models.CharField("Fonction", max_length=50, blank=True)
    # Fields a bit long to cope with imported values, might be shrinked later.
    telephone = models.CharField("Téléphone", max_length=80, blank=True)
    portable = models.CharField("Natel", max_length=80, blank=True)
    fax = models.CharField("Fax", max_length=30, blank=True)
    actif = models.BooleanField(default=True)

    class Meta:
        ordering = ('nom', 'prenom')

    def __str__(self):
        nom = " ".join(n for n in [self.nom, self.prenom] if n)
        if self.civilite.startswith(('Dr', 'Me')):
            nom = " ".join([self.civilite, nom])
        return nom

    def get_absolute_url(self):
        return reverse('contact-detail', args=[self.pk])

    def adresse_complete(self):
        """Adresse sur une ligne: 'rue, npa localité'."""
        adr = ' '.join(i for i in [self.npa, self.localite] if i)
        if self.adresse:
            adr = ', '.join(i for i in [self.adresse, adr] if i)
        return adr

    def all_relations(self):
        """Build a list of all contact relations"""
        relations = []
        contact_fields = [
            'dentiste', 'medecin_traitant', 'repondant_anim', 'repondant_ergo',
            'repondant_ergo_uaj', 'repondant_off', 'repondant_physio',
            'repondant_soins1', 'repondant_soins2', 'repondant_atelier',
        ]
        # Create in-memory Relation instances for 'contact' person fields
        q_filter = Q()
        for fld in contact_fields:
            q_filter |= Q(**{fld: self})
        for pers in Person.objects.filter(q_filter):
            role = ''
            relation = Relation(person=pers, contact=self)
            for fld in contact_fields:
                if getattr(pers, '%s_id' % fld) == self.pk:
                    role = pers._meta.get_field(fld).verbose_name
                    relation.role = Role(nom=role)
                    relation._pers_field = fld
                    break
            relations.append(relation)
        return sorted(
            relations + list(self.relation_set.select_related('person').all()),
            key=lambda r: (not r.person.actif, r.person.nom)
        )


class Role(models.Model):
    nom = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.nom

    class Meta:
        verbose_name = 'Rôle'
        ordering = ['nom']


class PersonManager(models.Manager):
    def create(self, **kwargs):
        entree_date = kwargs.pop('entree_date', timezone.now())
        person = super().create(**kwargs)
        if entree_date is not None:
            EntreeSortie.objects.create(
                person=person, entree_sortie='a', date=entree_date, situation=person.situation,
            )
        return person


class Person(models.Model):
    CIVILITE_CHOICES = (
        ('Madame', 'Madame'),
        ('Monsieur', 'Monsieur'),
    )
    ETAT_CIVIL_CHOICES = (
        ('celib', 'Célibataire'),
        ('marie', 'Marié-e'),
        ('separe', 'Séparé'),
        ('divorce', 'Divorcé-e'),
        ('veuf', 'Veuf-ve'),
    )
    SITUATION_CHOICES = (
        ('fhmn_int', 'Interne FHMN'),
        ('fhmn_ext', 'Externe FHMN'),
        ('fhne_int', 'Interne FHNE'),
        ('fhne_ext', 'Externe FHNE'),
    )
    HORS_CANTON_CHOICES = (
        ('be', 'BE'),
        ('fr', 'FR'),
        ('ge', 'GE'),
        ('ju', 'JU'),
        ('vd', 'VD'),
        ('vs', 'VS'),
        ('f', 'F'),  # France (dével. après migration)
    )

    #created
    updated = models.DateTimeField("Date de modification", null=True, blank=True)
    actif = models.BooleanField(default=True)
    prenom = models.CharField('Prénom', max_length=40)
    nom = models.CharField('Nom', max_length=40)
    photo = ImageFieldThumb('Photo', upload_to='photos', blank=True)
    civilite = models.CharField("Civilité", max_length=10, choices=CIVILITE_CHOICES, blank=True)
    date_naissance = models.DateField("Date de naissance", null=True, blank=True)
    lieu_naissance = models.CharField("Lieu de naissance", max_length=50, blank=True)
    etat_civil = models.CharField("État-civil", max_length=10, choices=ETAT_CIVIL_CHOICES, blank=True)
    origine = models.CharField("Origine", max_length=50, blank=True)
    hors_canton = models.CharField("Hors canton", max_length=5, choices=HORS_CANTON_CHOICES, blank=True)
    taille = models.PositiveSmallIntegerField("Taille", null=True, blank=True)
    langue_mat = models.CharField("Langue maternelle", max_length=20, blank=True)
    langue_pref = models.CharField("Langue préférée", max_length=20, blank=True)
    domicile_rue = models.CharField("Domicile de fait, rue", max_length=80, blank=True)
    domicile_loc = models.CharField("Domicile de fait, npa et localité", max_length=50, blank=True)
    domicile_tel = models.CharField("Domicile de fait, téléphone", max_length=40, blank=True)
    portable = models.CharField("Tél. portable", max_length=20, blank=True)
    dom_legal_rue = models.CharField("Domicile légal, rue", max_length=80, blank=True)
    dom_legal_loc = models.CharField("Domicile légal, npa et localité", max_length=50, blank=True)

    # Champs pour liste d'attente
    date_demande = models.DateField("Date de la demande", null=True, blank=True)
    prioritaire = models.BooleanField("Prioritaire", default=False)

    situation = models.CharField("Situation", max_length=15, choices=SITUATION_CHOICES, blank=True)
    no_dossier_comptable = models.CharField("N° dossier comptable", max_length=10, blank=True)
    no_chambre = models.CharField("N° chambre", max_length=20, blank=True)
    profession = models.CharField("Profession", max_length=50, blank=True)
    provenance = models.CharField("Provenance", max_length=50, blank=True)
    confession = models.CharField("Confession", max_length=50, blank=True)
    pratiquant = models.BooleanField(default=False)

    directives_anticipees = models.BooleanField("Directives anticipées", default=False)
    date_directives_anticipees = models.DateField("Date des directives anticipées", blank=True, null=True)

    # Assurances
    no_avs = models.CharField("N° AVS", max_length=16, blank=True)
    pc = models.BooleanField("PC", default=False)
    caisse_mal_acc = models.CharField("Caisse maladie", max_length=60, blank=True)
    no_assure_lamal = models.CharField("Numéro d’assuré", max_length=50, blank=True)
    caisse_mal_acc_compl = models.CharField("Caisse maladie compl", max_length=80, blank=True)
    no_assure_compl = models.CharField("Numéro d’assuré maladie compl", max_length=50, blank=True)
    caisse_ai_acc = models.CharField("Assurance accident", max_length=50, blank=True)
    assurance_menage = models.CharField("Assurance ménage", max_length=50, blank=True)
    assurance_rc = models.CharField("Assurance RC", max_length=50, blank=True)

    # Old field kept for history:
    referent1_str = models.CharField("Référent 1", max_length=50, blank=True)
    referent1 = models.ForeignKey(
        User, null=True, blank=True, on_delete=models.PROTECT, related_name='+',
        verbose_name="Référent 1"
    )
    # Old field kept for history:
    referent2_str = models.CharField("Référent 2", max_length=50, blank=True)
    referent2 = models.ForeignKey(
        User, null=True, blank=True, on_delete=models.PROTECT, related_name='+',
        verbose_name="Référent 2"
    )
    repondant_off = models.ForeignKey(
        Contact, null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Répondant officiel',
        related_name='+'
    )
    repondant_off_self = models.BooleanField("Résident est répondant", default=False)
    repondant_off_role = models.CharField("Rôle du répondant officiel", max_length=50, blank=True)
    repondant_ther = models.ForeignKey(
        Contact, null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Répondant thérapeutique',
        related_name='+'
    )
    repondant_anim = models.ForeignKey(
        Contact, null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Répondant animation',
        related_name='+'
    )
    repondant_physio = models.ForeignKey(
        Contact, null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Répondant physio',
        related_name='+'
    )
    repondant_ergo = models.ForeignKey(
        Contact, null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Répondant ergo',
        related_name='+'
    )
    repondant_ergo_uaj = models.ForeignKey(
        Contact, null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Répondant ergo (UAJ)',
        related_name='+'
    )
    repondant_atelier = models.ForeignKey(
        Contact, null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Répondant atelier',
        related_name='+'
    )
    repondant_soins1 = models.ForeignKey(
        Contact, null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Répondant soins',
        related_name='+'
    )
    repondant_soins2 = models.ForeignKey(
        Contact, null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Répondant soins (2)',
        related_name='+'
    )
    medecin_traitant = models.ForeignKey(
        Contact, null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Médecin traitant',
        related_name='+'
    )
    dentiste = models.ForeignKey(
        Contact, null=True, blank=True, on_delete=models.SET_NULL, verbose_name='Dentiste',
        related_name='+'
    )

    no_plaisir = models.CharField("Numéro plaisir", max_length=15, blank=True)
    date_plaisir = models.DateField("Date plaisir", blank=True, null=True)
    degre_plaisir = models.CharField("Degré de plaisir", max_length=8, blank=True)

    objects = PersonManager()

    class Meta:
        verbose_name = 'Résident'
        db_table = 'person'
        permissions = (
            ("access_fhmn", "Voir résidents FHMN"),
            ("access_fhne", "Voir résidents FHNE"),
            ("plaisir_edition", "Édition et liste des données plaisir"),
            ("liste_attente", "Consulter la liste d’attente"),
        )

    def __str__(self):
        return "%s %s" % (self.nom, self.prenom)

    def get_absolute_url(self):
        return reverse('person-detail', args=[self.pk])

    def can_view(self, user):
        if self.situation.startswith('fhmn'):
            return user.has_perm('person.access_fhmn')
        if self.situation.startswith('fhne'):
            return user.has_perm('person.access_fhne')
        return True

    def can_edit(self, user):
        return user.has_perm('person.change_person')

    @classmethod
    def en_attente(self):
        return Person.objects.filter(actif=True).annotate(
            latest_es_code=Subquery(
                EntreeSortie.objects.filter(person=OuterRef('pk')).order_by('-date').values('entree_sortie')[:1]
            )
        ).filter(
            Q(latest_es_code=None) | Q(latest_es_code__in=[
                EntreeSortie.DEPART, EntreeSortie.ARRIVEE_TEMPORAIRE, EntreeSortie.DEPART_TEMPORAIRE
            ])
        ).select_related('medecin_traitant', 'repondant_off').order_by('nom', 'prenom')

    @property
    def date_admission(self):
        es = self.entreesortie_set.filter(entree_sortie='a').first()
        return es.date if es else None

    @property
    def demande_admission(self):
        # Query all() to take advantage of possible prefetch
        return next(iter([doc for doc in self.document_set.all() if doc.type_doc == 'adm-demande']), None)

    @property
    def bilan_independance(self):
        # Query all() to take advantage of possible prefetch
        return next(iter([doc for doc in self.document_set.all() if doc.type_doc == 'adm-bilan']), None)

    @property
    def date_depart(self):
        es = self.entreesortie_set.first()  # due to EntreeSortie default '-date' sorting
        return es.date if es and es.entree_sortie == 'd' else None

    @property
    def cause_depart(self):
        es = self.entreesortie_set.first()
        return es.motif if es and es.entree_sortie == 'd' else ''

    @property
    def derniere_situation(self):
        if self.situation:
            return self.situation
        try:
            return self.entreesortie_set.exclude(situation="").latest('date').situation
        except EntreeSortie.DoesNotExist:
            return ''

    @property
    def repondant_off_str(self):
        if self.repondant_off_self:
            return 'Lui-même' if self.civilite == 'Monsieur' else 'Elle-même'
        else:
            return str(self.repondant_off) if self.repondant_off else ''

    @property
    def directives_anticipees_fichier(self):
        return self.document_set.filter(type_doc='directives').first()

    @property
    def modifications(self):
        return Modification.objects.filter(person=self)


class EntreeSortie(models.Model):
    ARRIVEE = 'a'
    DEPART = 'd'
    SORTIE_TEMPORAIRE = 's'
    RETOUR_SORTIE = 'r'
    ARRIVEE_TEMPORAIRE = 'at'
    DEPART_TEMPORAIRE = 'dt'

    ES_CHOICES = (
        (ARRIVEE, 'Arrivée dans l’institution'),
        (SORTIE_TEMPORAIRE, 'Sortie temporaire'),
        (RETOUR_SORTIE, 'Retour de sortie'),
        (DEPART, 'Départ de l’institution'),
        (ARRIVEE_TEMPORAIRE, 'Arrivée séjour temporaire (UAT…)'),
        (DEPART_TEMPORAIRE, 'Départ séjour temporaire'),
    )
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    date = models.DateTimeField()
    entree_sortie = models.CharField(max_length=2, choices=ES_CHOICES)
    situation = models.CharField(max_length=15, choices=Person.SITUATION_CHOICES, blank=True)
    motif = models.CharField(max_length=150, blank=True)

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return '{}: {}'.format(self.date, self.get_entree_sortie_display())

    @classmethod
    def available_choices(cls, current_es):
        """Return possible choices depending on current one"""
        es_code = current_es.entree_sortie if current_es else None
        return {
            None: {'a', 'at'},
            'a': {'s', 'd'},
            's': {'r', 'd'},
            'r': {'s', 'd'},
            'd': {'a', 'at'},
            'at': {'a', 'dt'},
            'dt': {'a', 'at'},
        }.get(es_code)

    def can_edit(self, user):
        return self.person.can_edit(user)

    def can_delete(self, user):
        return self.can_edit(user) and self == self.person.entreesortie_set.latest('date')

    def is_inside(self):
        return self.entree_sortie in ['a', 'at', 'r']


class Relation(models.Model):
    """
    Modèle de liaison entre personne et contact pour définir un rôle (facultatif)
    à la relation.
    """
    person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='relations')
    contact = models.ForeignKey(Contact, on_delete=models.PROTECT)
    role = models.ForeignKey(Role, on_delete=models.PROTECT, null=True, blank=True)
    note = models.CharField("Note", max_length=150, blank=True)
    proche_sig = models.BooleanField("Proche signifiant", default=False)

    def __str__(self):
        return 'Relation «{}» est «{}» pour «{}»'.format(self.contact, self.role, self.person)


class ObjectHistoryMixin:
    @classmethod
    def prefetch_object_history(cls, obj_list):
        ct = ContentType.objects.get_for_model(cls)
        modifs = Modification.objects.filter(
            content_type__pk=ct.pk, object_id__in=[obj.pk for obj in obj_list]
        ).order_by('date')
        modifs_dict = {modif.object_id: modif for modif in modifs}
        for obj in obj_list:
            obj.latest_history = modifs_dict.get(obj.pk)

    def object_history(self, as_qs=False, trunc_at_min=False, part=None):
        ct = ContentType.objects.get_for_model(self)
        modifs = Modification.objects.filter(
            content_type__pk=ct.pk, object_id=self.pk
        )
        if trunc_at_min:
            modifs = modifs.annotate(
                quand=Trunc('date', 'minute')
            ).distinct('quand', 'person').order_by('-quand', 'person')
        else:
            modifs = modifs.order_by('-date')
        if as_qs:
            return modifs
        modifs = list(modifs)
        if getattr(self, 'date', None) and (not modifs or self.date < modifs[-1].date):
            # Append probable creation modif 
            modifs.append(Modification(
                person=self.person, date=self.date,
                user=getattr(self, 'createur', None), visa=getattr(self, 'auteur', None),
                precision="Création de la fiche"
            ))
        return modifs

    def latest_history(self, **kwargs):
        if self.pk:
            history = self.object_history(**kwargs)
            return history[0] if history else None

    def object_history_url(self, third_arg=None):
        if self.pk:
            ct = ContentType.objects.get_for_model(self)
            return reverse('object-history', args=[ct.pk, self.pk] + ([third_arg] if third_arg else []))

    def delete(self, **kwargs):
        self.object_history(as_qs=True).delete()
        return super().delete(**kwargs)


class PersonRelatedModel(ObjectHistoryMixin, models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    # Après la migration, deviendra blank=False avec une valeur par défaut?
    date = models.DateTimeField("Date", null=True, blank=True, db_index=True)
    # nullable en raison de l'existant
    createur = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Créateur')
    # visa est un champ importé (ne devrait plus être rempli ni modifié).
    visa = models.CharField("Visa", max_length=25, blank=True)

    # A base name on which -list/-detail/-new suffixes will be appended to reach CRUD views.
    _view_name_base = ''
    edit_perm = None

    class Meta:
        abstract=True

    def get_absolute_url(self):
        if not self._view_name_base:
            raise NotImplementedError("model %s is missing a _view_name_base attribute" % self.__class__)
        return reverse('%s-detail' % self._view_name_base, args=[self.person_id, self.pk])

    def delete(self, **kwargs):
        # Explicit deletion of generic foreign key objects
        try:
            for att in self.fichiers.all():
                att.delete()
        except AttributeError:
            pass
        return super().delete(**kwargs)

    @property
    def auteur(self):
        return (self.createur.get_full_name() or self.createur.username) if self.createur_id else (self.visa if self.visa else '-')

    def can_edit(self, user):
        return (
            user.is_superuser or user == self.createur or
            (self.edit_perm and user.has_perm(self.edit_perm))
        )

    def classes(self):
        return ['closed'] if getattr(self, 'closed', False) else []


class HistoireDeVie(ObjectHistoryMixin, models.Model):
    person = models.OneToOneField(Person, on_delete=models.CASCADE)
    date = models.DateTimeField("Date de mise à jour", null=True, blank=True)
    createur = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Créateur')
    visa = models.CharField("Visa", max_length=15, blank=True)
    histoire = models.TextField("Histoire de vie", blank=True)
    descr_biologiques = models.TextField("Descriptions biologiques", blank=True)
    descr_psychologiques = models.TextField("Descriptions psychologiques", blank=True)
    descr_sociales = models.TextField("Descriptions sociales", blank=True)

    _view_name_base = 'histoirevie'

    class Meta:
        verbose_name = 'Histoire de vie'
        verbose_name_plural = 'Histoires de vie'

    def __str__(self):
        return "Histoire de vie de %s" % self.person

    @property
    def auteur(self):
        return (self.createur.get_full_name() or self.createur.username) if self.createur_id else (self.visa if self.visa else '-')

    def get_absolute_url(self):
        return reverse('histoirevie-detail', args=[self.person_id])

    def can_edit(self, user):
        return user.has_perm('person.change_person') or user.has_perm('person.change_histoiredevie')


class RepartitionTaches(ObjectHistoryMixin, models.Model):
    REPART_CHOICES = (
        ('R', 'R'),  # Résident
        ('F', 'F'),  # Famille
        ('C', 'C'),  # Curateur
        ('S', 'S'),  # Soins
        ('E', 'E'),  # Ergo
        ('An', 'An'),  # Animation
        ('Ad', 'Ad'),  # Ad
    )
    RANGEMENTS_CHOICES = (
        ('R', 'R'),  # Résident
        ('F', 'F'),  # Famille
        ('C', 'C'),  # Curateur
        ('TS', 'TS'),  # Tiroir Soins
        ('CA', 'CA'),  # Classeur résident administration
        ('An', 'An'),  # Animation
    )
    person = models.OneToOneField(Person, on_delete=models.CASCADE)
    date = models.DateTimeField("Date de mise à jour", null=True, blank=True)
    # Achats
    achats_produits = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Achats divers produits de toilette etc", blank=True, null=True,
    )
    achats_vetements = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Vêtements", blank=True, null=True,
    )
    achats_autres = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Autres", blank=True, null=True,
    )
    achats_autres_txt = models.CharField("Autres achats texte", max_length=100, blank=True)
    # Gestion administrative
    boite_aux_lettres = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Gestion de la boîte aux lettres", blank=True, null=True,
    )
    assurance = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Assurance documents officiels etc", blank=True, null=True,
    )
    paiements = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Paiement des factures", blank=True, null=True,
    )
    argent_poche = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Gestion argent de poche", blank=True, null=True,
    )
    comm_bons_taxi = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Commande des bons taxi", blank=True, null=True,
    )
    gestion_autres = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Autres", blank=True, null=True,
    )
    gestion_autres_txt = models.CharField("Autres administratif texte", max_length=100, blank=True)
    # Vacances
    organisation_vac = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Organisation des vacances", blank=True, null=True,
    )
    prep_bagages = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Préparation des bagages et des documents vacances", blank=True, null=True,
    )
    vac_autres = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Autres", blank=True, null=True,
    )
    vac_autres_txt = models.CharField("Autres vacances texte", max_length=100, blank=True)
    # Gestion des rendez-vous
    medicaux = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Médicaux", blank=True, null=True,
    )
    physio_ext = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Physiothérapie extérieure", blank=True, null=True,
    )
    ergo_ext = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Ergothérapie extérieure", blank=True, null=True,
    )
    dentiste = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Dentiste devis etc", blank=True, null=True,
    )
    lunettes = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Lunettes de vue", blank=True, null=True,
    )
    coiffeur = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Coiffeur pédicure", blank=True, null=True,
    )
    reserv_taxi = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Réservations annulations du taxi", blank=True, null=True,
    )
    # Studio
    mobilier = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Mobilier décoration", blank=True, null=True,
    )
    orga_studio = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Organisation du studio et rangements", blank=True, null=True,
    )
    studio_autres = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Autres studio", blank=True, null=True,
    )
    studio_autres_txt = models.CharField("Autres studio texte", max_length=100, blank=True)
    # Camps
    prep_camp = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Préparation des bagages et des documents camps", blank=True, null=True,
    )
    camp_autres = ChoiceArrayField(
        models.CharField(max_length=2, choices=REPART_CHOICES, blank=True),
        verbose_name="Autres camp", blank=True, null=True,
    )
    camp_autres_txt = models.CharField("Autres camps texte", max_length=100, blank=True)
    # Rangement documents
    decisions_apa = ChoiceArrayField(
        models.CharField(max_length=2, choices=RANGEMENTS_CHOICES, blank=True),
        verbose_name="Décisions de l’Autorité de protection de l’adulte", blank=True, null=True,
    )
    carte_ident = ChoiceArrayField(
        models.CharField(max_length=2, choices=RANGEMENTS_CHOICES, blank=True),
        verbose_name="Carte d’identité", blank=True, null=True,
    )
    permis_sejour = ChoiceArrayField(
        models.CharField(max_length=2, choices=RANGEMENTS_CHOICES, blank=True),
        verbose_name="Permis de séjour attestations domicile", blank=True, null=True,
    )
    certif_am = ChoiceArrayField(
        models.CharField(max_length=2, choices=RANGEMENTS_CHOICES, blank=True),
        verbose_name="Certificat d’assurance maladie", blank=True, null=True,
    )
    contrats_rc = ChoiceArrayField(
        models.CharField(max_length=2, choices=RANGEMENTS_CHOICES, blank=True),
        verbose_name="Contrats d’assurances RC ménage", blank=True, null=True,
    )
    carte_avs = ChoiceArrayField(
        models.CharField(max_length=2, choices=RANGEMENTS_CHOICES, blank=True),
        verbose_name="Carte AVS", blank=True, null=True,
    )
    carte_ai_cff = ChoiceArrayField(
        models.CharField(max_length=2, choices=RANGEMENTS_CHOICES, blank=True),
        verbose_name="Carte de légitimation AI Pro Inf voyage CFF", blank=True, null=True,
    )
    bons_taxi = ChoiceArrayField(
        models.CharField(max_length=2, choices=RANGEMENTS_CHOICES, blank=True),
        verbose_name="Bons taxi", blank=True, null=True,
    )
    doc_autres = ChoiceArrayField(
        models.CharField(max_length=2, choices=RANGEMENTS_CHOICES, blank=True),
        verbose_name="Autres documents", blank=True, null=True,
    )
    doc_autres_txt = models.CharField("Autres document texte", max_length=100, blank=True)

    _view_name_base = 'repartition'

    class Meta:
        verbose_name = 'Répartition de tâches'
        verbose_name_plural = 'Répartitions de tâches'

    def __str__(self):
        return "Répartition des tâches pour %s (%s)" % (self.person, self.date)

    def can_edit(self, user):
        return user.has_perm('person.change_person')

    def get_absolute_url(self):
        return reverse('person-repartition', args=[self.person_id])

    # Compatibilité avec ObjectHistoryMixin
    @property
    def createur(self):
        return None
    auteur = createur


class Document(models.Model):
    DOC_CHOICES = (
        ('adm-demande', 'Admission: demande'),
        ('adm-bilan', 'Admission: bilan d’indépendance'),
        ('adm-autre', 'Admission: autre document'),
        ('directives', 'Directives anticipées'),
        ('contention', 'Moyens de contention'),
        ('autre', 'Autre document'),
    )
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    date = models.DateTimeField("Date")
    createur = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Créateur')
    type_doc = models.CharField(
        "Type de document", max_length=15, choices=DOC_CHOICES,
        help_text='Les directives anticipées ne sont visibles que pour le personnel soignant'
    )
    doc = models.FileField(upload_to='documents', verbose_name="Fichier")
    title = models.CharField("Titre", max_length=150, blank=True)
    date_doc = models.DateField("Date du document", null=True)

    _view_name_base = 'documents'

    def __str__(self):
        return f"«{self.title}» «{self.get_type_doc_display()}» pour {self.person} ({self.type_doc} - {self.date})"

    def get_absolute_url(self):
        return self.doc.url

    @property
    def titre(self):
        return self.title if self.title else os.path.basename(self.doc.path)

    def pdf_path(self):
        # Create a PDF with image if doc is an image
        if self.doc.path.lower().endswith('.pdf'):
            return self.doc.path
        from .pdf_output import img_to_pdf
        return img_to_pdf(self.doc.path)


class SoutienSocial(PersonRelatedModel):
    intervenants = models.TextField("Intervenants", blank=True)
    motifs = models.TextField("Motifs raisons", blank=True)
    decisions = models.TextField("Décisions p.v.", blank=True)

    _view_name_base = 'soutiensocial'
    list_display = ['date', 'motifs', 'intervenants', 'auteur']
    searchable = {'txt': ['motifs', 'intervenants', 'decisions']}

    class Meta:
        verbose_name = 'Réseau'
        verbose_name_plural = 'Réseaux'

    def __str__(self):
        return "Soutien social pour %s (%s)" % (self.person, self.date)


class ActionBase(PersonRelatedModel):
    ETAT_CHOICES = (
        ('o', 'Ouverte'),
        ('f', 'Fermée'),
    )
    etat = models.CharField("État", max_length=1, choices=ETAT_CHOICES, default='o')
    date_fermee = models.DateField("Date fermeture", null=True, blank=True)
    problemes = models.TextField("Problèmes", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)
    # Uniquement pour anciennes actions importées, plus d'édition.
    actions_old = models.TextField("Actions", blank=True)
    evaluations_old = models.TextField("Évaluations", blank=True)

    class Meta:
        abstract = True

    list_display = ['date', 'etat', 'problemes', 'objectifs', 'auteur', 'date_fermee']

    @property
    def closed(self):
        return self.etat == 'f'

    @property
    def actions(self):
        return self.actionaction_set.order_by('-date')

    @property
    def evaluations(self):
        return self.actioneval_set.order_by('-date')


class ActionIS(ActionBase):
    NATURE_CHOICES = (
        ('chron', 'Chronique'),
        ('ponct', 'Ponctuelle'),
    )
    visa = None
    visa_ouverture = models.CharField("Visa ouverture", max_length=35, blank=True)
    visa_fermeture = models.CharField("Visa fermeture", max_length=25, blank=True)
    delai = models.DateField("Délai", null=True, blank=True)
    secteur = models.CharField("Secteur", max_length=20, blank=True)
    nature = models.CharField("Nature", max_length=5, choices=NATURE_CHOICES, blank=True)

    _view_name_base = 'ais'
    edit_perm = 'person.change_actionis'
    list_display = [
        'date', 'delai', 'nature', 'secteur', 'problemes', 'objectifs', 'auteur', 'date_fermee'
    ]

    class Meta:
        verbose_name = 'Action intersecteurs'
        verbose_name_plural = 'Actions intersecteurs'

    def __str__(self):
        return "Action inter-secteurs pour %s (%s)" % (self.person, self.date)

    def get_absolute_url(self):
        return reverse('ais-detail', args=[self.person_id, self.pk])

    @property
    def auteur(self):
        return self.createur.get_full_name() if self.createur_id else (self.visa_ouverture if self.visa_ouverture else '-')


class ActionEval(models.Model):
    action = models.ForeignKey(ActionIS, on_delete=models.CASCADE)
    date = models.DateTimeField("Date")
    createur = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Créateur')
    texte = models.TextField("Évaluation")

    class Meta:
        verbose_name = "Évaluation d'action"
        verbose_name_plural = "Évaluations d'action"

    def __str__(self):
        return "Évaluation d'AIS pour %s (%s)" % (self.action.person, self.date)


class ActionAction(models.Model):
    action = models.ForeignKey(ActionIS, on_delete=models.CASCADE)
    date = models.DateTimeField("Date")
    createur = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Créateur')
    texte = models.TextField("Action")

    class Meta:
        verbose_name = "Action d'action"
        verbose_name_plural = "Action d'action"

    def __str__(self):
        return "Action d'AIS pour %s (%s)" % (self.action.person, self.date)


class Modification(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, null=True, blank=True)
    date = models.DateTimeField("Date", db_index=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Auteur')
    visa = models.CharField("Visa", max_length=25, blank=True)
    # Two fields used when modif not related to a Person instance.
    fhmn = models.BooleanField(default=False)
    fhne = models.BooleanField(default=False)
    precision = models.TextField("Précision", blank=True)
    rubrique = models.TextField("Rubrique", blank=True)
    manual = models.BooleanField(default=False)
    # Generic relation to changed instances (nullable because of legacy entries)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey()

    class Meta:
        permissions = (
            ("info_to_group", "Autoriser à publier une info pour certains groupes"),
        )

    def __str__(self):
        return "Modification pour %s (%s)" % (self.person, self.date)

    def __lt__(self, other):
        return self.date < other.date

    @classmethod
    def save_modification(cls, instance, user, created=False, msg=None):
        if isinstance(instance, (Person, PersonRelatedModel)) or hasattr(instance, 'person'):
            if msg is None:
                if created:
                    msg = "Ajout d’une fiche «%s»" % instance._meta.verbose_name
                else:
                    msg = "Modification d’une fiche «%s»" % instance._meta.verbose_name
            return Modification.objects.create(
                person=instance if isinstance(instance, Person) else instance.person,
                date=timezone.now(),
                user=user,
                precision=msg,
                rubrique='%s.%s' % (instance._meta.app_label, instance._meta.model_name),
                content_object=None if isinstance(instance, Person) else instance
            )

    def can_edit(self, user):
        return self.manual and (self.user == user or user.has_perm('person.change_modification'))

    def can_delete(self, user):
        return self.can_edit(user)

    @property
    def is_future(self):
        return self.date is not None and self.date > timezone.now()

    @property
    def target_url(self):
        try:
            obj = self.content_object
        except AttributeError:
            return None
        return obj.get_absolute_url() if obj else None

    def get_person(self):
        return self.person if self.person_id else ''

    def get_user(self):
        return self.user if self.user_id else self.visa


class Attachment(models.Model):
    fichier = models.FileField(upload_to='attachments', verbose_name="Fichier")
    titre = models.CharField("Titre", max_length=200, blank=True)
    createur = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Créateur')
    date = models.DateTimeField()
    # Generic relation fields
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return "Fichier %s" % self.fichier.name

    def delete(self, **kwargs):
        os.unlink(self.fichier.path)
        super().delete(**kwargs)

    def title(self):
        return self.titre or os.path.basename(self.fichier.name)

    @property
    def is_image(self):
        return os.path.splitext(self.fichier.name)[1].lower() in IMAGE_EXTS

    def icon_url(self):
        return icon_url(self.fichier.name)
