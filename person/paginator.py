from itertools import chain

from django.core.paginator import Paginator
from django.utils import timezone


class MixedPaginator(Paginator):
    """
    A Paginator where some `addit_class` objects are mixed in the main
    object list. The additional class must have date and person fields.
    """ 
    addit_class = None

    def page(self, number):
        if self.addit_class is None:
            return super().page(number)
        number = self.validate_number(number)
        bottom = (number - 1) * self.per_page
        top = bottom + self.per_page
        if top + self.orphans >= self.count:
            top = self.count
        obj_list = list(self.object_list[bottom:top])
        if obj_list:
            to_date = self.object_list[bottom - 1].date if bottom > 0 else timezone.now()
            from_date = obj_list[-1].date
            addit_res = self.addit_class.objects.filter(person=obj_list[0].person, date__range=[from_date, to_date])
            obj_list = sorted(
                chain(obj_list, addit_res), key=lambda instance: instance.date, reverse=True
            )
        return self._get_page(obj_list, number, self)
