from datetime import date

from reportlab.platypus import Image, Paragraph, SimpleDocTemplate, Spacer, Table, TableStyle
from reportlab.platypus.flowables import HRFlowable
from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import cm
from reportlab.lib.utils import ImageReader
from reportlab.pdfbase.pdfmetrics import registerFont, stringWidth
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas

from django.contrib.staticfiles.finders import find
from django.core.exceptions import ObjectDoesNotExist
from django.utils.dateformat import format

from besoins.models import RecueilBesoins
from person.models import SITUATION_ADRESSES


class PageNumCanvas(canvas.Canvas):
    """A special canvas to be able to draw the total page number in the footer."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pages = []

    def showPage(self):
        self._pages.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        page_count = len(self._pages)
        for page in self._pages:
            self.__dict__.update(page)
            self.draw_page_number(page_count)
            canvas.Canvas.showPage(self)
        super().save()

    def draw_page_number(self, page_count):
        self.setFont("Helvetica", 9)
        self.drawRightString(self._pagesize[0] - 1.6*cm, 1.4*cm, "p. %s/%s" % (self._pageNumber, page_count))


class HeaderFooterMixin:
    logo_height = 2.6 * cm

    def draw_logo_header(self, canvas, doc, top_margin=1.5*cm):
        canvas.saveState()
        canvas.drawImage(
            find('img/fh-logo.png'), 1.5*cm, doc.height - top_margin,
            width=self.logo_height * 0.79, height=self.logo_height, mask=None
        )
        canvas.restoreState()

    def draw_footer_address(self, canvas, doc, loc):
        canvas.saveState()
        canvas.setFont("Helvetica", 9)
        canvas.drawString(
            self.doc.leftMargin, 1.4*cm,
            'Foyer Handicap, %s' % SITUATION_ADRESSES.get(loc, '-')
        )
        canvas.restoreState()


class StyleMixin:
    def __init__(self, base_font='Helvetica', **params):
        if base_font == 'Liberation Sans Narrow':
            registerFont(
                TTFont('Liberation Sans Narrow', find('fonts/LiberationSansNarrow-Regular.ttf'))
            )
        self.styles = getSampleStyleSheet()
        self.style_title = ParagraphStyle(
            name='Title', fontSize=16, leading=18, leftIndent=50, alignment=TA_CENTER,
            fontName='Helvetica-Bold' if base_font=='Helvetica' else base_font
        )
        self.style_n = ParagraphStyle('normal', fontName=base_font, fontSize=12, leading=14, spaceAfter=0.5*cm)
        super().__init__(**params)


class PersonPDFModel(StyleMixin, HeaderFooterMixin):
    def __init__(self, **params):
        self.person = params.pop('person')
        super().__init__(**params)

    @property
    def is_landscape(self):
        return self.doc.pagesize[0] > self.doc.pagesize[1]

    def draw_first_header_footer(self, canvas, doc):
        self.draw_logo_header(canvas, doc)
        self.draw_footer_address(canvas, doc, self.person.situation[:4])

    def draw_later_header_footer(self, canvas, doc):
        self.draw_footer_address(canvas, doc, self.person.situation[:4])

    def person_header(self, story):
        style_t = ParagraphStyle('table', parent=self.style_n, leading=16)
        table_data = [
            [Paragraph('<b>{}</b><br/>{}<br/>{}'.format(
                ' '.join([self.person.prenom, self.person.nom]),
                self.person.domicile_rue,
                self.person.domicile_loc,
             ), style=style_t),
             Paragraph(
                '<b>Né-e le :</b> {}<br/><b>Médecin traitant :</b> {}<br/>'
                '<b>À Foyer handicap depuis :</b> {}'.format(
                    format(self.person.date_naissance, 'd.m.Y') if self.person.date_naissance else '?',
                    self.person.medecin_traitant,
                    format(self.person.date_admission, 'd.m.Y') if self.person.date_admission else '?',
                 ), style=style_t
             ),
            ],
        ]
        col_widths = [7.1*cm, 8.3*cm]
        if self.person.photo:
            try:
                img = ImageReader(self.person.photo.path)
            except IOError:
                pass
            else:
                width, height = img.getSize()
                width, height = _max_image_dim(width, height, max_w=2.3 * cm, max_h=2.9 * cm)
                table_data[0].append(Image(self.person.photo.path, width, height))
                col_widths = [5.5*cm, 7.6*cm, 2.4*cm]
        story.append(Paragraph(self.title, self.style_title))
        story.append(Table(table_data, colWidths=col_widths, style=TableStyle(
            (('BOX', (0, 0), (-1, -1), 1, colors.toColor('rgb(220,220,220)')),
            )
        ), hAlign='CENTER' if self.is_landscape else 'RIGHT', spaceBefore=0.6*cm, spaceAfter=0.6*cm))


class RegimesList(HeaderFooterMixin, StyleMixin):
    title = 'Liste des régimes des résidents'
    logo_height = 2 * cm

    def __init__(self, **params):
        self.persons = params['persons']
        self.title = self.title + ' • ' + format(date.today(), 'd.m.Y')
        super().__init__(base_font='Liberation Sans Narrow')

    def draw_header_footer(self, canvas, doc):
        self.draw_logo_header(canvas, doc, top_margin=0.2*cm)

    def produce_pdf(self, response):
        self.doc = SimpleDocTemplate(response, title=self.title, pagesize=A4,
            leftMargin=1.5*cm, rightMargin=1*cm, topMargin=1.8*cm, bottomMargin=1*cm)
        style_table = ParagraphStyle('table', fontName='Liberation Sans Narrow', fontSize=9, leading=10)
        style_table_red = ParagraphStyle('red', parent=style_table, textColor=colors.red)
        style_table_center = ParagraphStyle('table2', parent=style_table, alignment=TA_CENTER)
        story = []
        story.append(Paragraph(self.title, style=self.style_title))
        story.append(Spacer(1, 0.5*cm))
        table_data = [
            ['Nom et prénom', 'Régimes', 'Portions', 'Moyens auxiliaires', 'Compléments alimentaires', "N'aime pas"],
        ]
        table_style = [
            ('BACKGROUND', (0, 0), (-1, 0), colors.toColor('rgb(220,220,220)')),
            ('FONTNAME', (0, 0), (-1, -1), style_table.fontName),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
            ('LEFTPADDING', (0, 1), (-1, -1), 2),
            ('RIGHTPADDING', (0, 1), (-1, -1), 2),
            ('TOPPADDING', (0, 1), (-1, -1), 1),
            ('BOTTOMPADDING', (0, 1), (-1, -1), 1),
            ('VALIGN', (0, 1), (-1, -1), 'TOP'),
        ]

        for pers in self.persons:
            try:
                bes = pers.besoinsalimentaires
            except ObjectDoesNotExist:
                bes = None
            table_data.append([
                Paragraph(" ".join([pers.nom, pers.prenom]), style=style_table),
                Paragraph(bes.regimes if bes else '', style=style_table),
                Paragraph(bes.portions if bes else '', style=style_table_center),
                Paragraph(bes.moyens_aux if bes else '', style=style_table),
                Paragraph(bes.complements if bes else '', style=style_table),
                Paragraph(bes.naimepas if bes else '', style=style_table),
            ])
            if bes and bes.allergies:
                table_data.append(['', Paragraph(f'Allergies: {bes.allergies}', style=style_table_red)])
                line_idx = len(table_data) - 1
                table_style.append(('LINEABOVE', (0, line_idx), (-1, line_idx), 0.25, colors.lightgrey)),
                table_style.append(('SPAN', (1, line_idx),(-1, line_idx)))

        story.append(Table(
            table_data,
            colWidths = [3.9*cm, 2*cm, 1.4*cm, 3*cm, 4.2*cm, 4.2*cm],
            style=TableStyle(table_style)
        ))
        self.doc.build(story, onFirstPage=self.draw_header_footer)


class DegresPlaisirList(HeaderFooterMixin, StyleMixin):
    title = 'Liste des degrés plaisir'

    def __init__(self, **params):
        self.persons = params['persons']
        self.title = self.title + ' • ' + format(date.today(), 'd.m.Y')
        super().__init__()

    def draw_header_footer(self, canvas, doc):
        self.draw_logo_header(canvas, doc)
        self.draw_footer_address(canvas, doc, self.persons[0].situation[:4])

    def produce_pdf(self, response):
        self.doc = SimpleDocTemplate(response, title=self.title, pagesize=A4,
            leftMargin=1.5*cm, rightMargin=1*cm, topMargin=1.8*cm, bottomMargin=1*cm)
        style_table = ParagraphStyle('table', fontSize=11, leading=13)

        story = []
        story.append(Paragraph(self.title, style=self.style_title))
        story.append(Spacer(1, 0.5*cm))
        table_data = [
            ['Nom et prénom', 'N° plaisir', 'Degré\nplaisir', 'Date plaisir'],
        ]
        for pers in self.persons:
            table_data.append([
                " ".join([pers.nom, pers.prenom]),
                Paragraph(pers.no_plaisir, style=style_table),
                Paragraph(pers.degre_plaisir, style=style_table),
                Paragraph(format(pers.date_plaisir, 'd.m.Y') if pers.date_plaisir else '-', style=style_table)
            ])
        table_style = [
            ('BACKGROUND', (0, 0), (-1, 0), colors.toColor('rgb(220,220,220)')),
            ('FONTNAME', (0, 0), (-1, -1), style_table.fontName),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
            ('LEFTPADDING', (0, 1), (-1, -1), 2),
            ('RIGHTPADDING', (0, 1), (-1, -1), 2),
            ('TOPPADDING', (0, 1), (-1, -1), 1),
            ('BOTTOMPADDING', (0, 1), (-1, -1), 1),
        ]
        story.append(Table(
            table_data,
            colWidths = [5.5*cm, 4*cm, 1.7*cm, 2.5*cm],
            style=TableStyle(table_style)
        ))
        self.doc.build(story, onFirstPage=self.draw_header_footer)


class TransfertPDF(HeaderFooterMixin, StyleMixin):
    title = 'Feuille de transfert'

    def __init__(self, **params):
        self.person = params['person']
        super().__init__()

    def draw_header_footer(self, canvas, doc):
        self.draw_logo_header(canvas, doc)
        self.draw_footer_address(canvas, doc, self.person.situation[:4])

    def produce_pdf(self, response):
        self.doc = SimpleDocTemplate(response, title=self.title, pagesize=A4,
            leftMargin=2*cm, rightMargin=1.5*cm, topMargin=1*cm, bottomMargin=1.7*cm)
        story = []
        style_sub = ParagraphStyle(
            'subtitle', parent=self.style_n, fontSize=14, spaceAfter=0.1 * cm, spaceBefore=0.8 * cm
        )
        style_n = ParagraphStyle('norm', parent=self.style_n, spaceAfter=0.1 * cm)
        story.extend([
            Spacer(1, 1 * cm),
            Paragraph(self.title, style=self.style_title),
            Spacer(1, 0.7 * cm),
            Paragraph('PATIENT(E)', style_sub),
            HRFlowable(width='100%', color=colors.black),
            Spacer(1, 0.3 * cm),
        ])
        for value in (
                '<b>Prénom et nom:</b> {} {}'.format(self.person.prenom, self.person.nom),
                '<b>N° AVS:</b> {}'.format(self.person.no_avs),
                '<b>Date de naissance:</b> {}'.format(
                    format(self.person.date_naissance, 'd.m.Y') if self.person.date_naissance else '?'
                ),
                '<b>Caisse maladie:</b> {}'.format(self.person.caisse_mal_acc),
                '<b>N° assuré:</b> {}'.format(self.person.no_assure_lamal),
                '<b>Personne répondante:</b> {}'.format(self.person.repondant_off),
                '<b>Médecin traitant:</b> {}'.format(self.person.medecin_traitant or '-'),
                '<b>Admission à Foyer Handicap:</b> {}'.format(
                    format(self.person.date_admission, 'd.m.Y')if self.person.date_admission else '?'
                ),
            ):
            story.append(Paragraph(value, style_n))
        story.extend([
            Paragraph('DIAGNOSTIC MÉDICAL', style_sub),
            HRFlowable(width='100%', color=colors.black),
            Spacer(1, 0.3 * cm),
            Paragraph('cf. Fiche annexe «Diagnostic médical»', style_n),
        ])
        story.extend([
            Paragraph('MÉDICATIONS', style_sub),
            HRFlowable(width='100%', color=colors.black),
            Spacer(1, 0.3 * cm),
            Paragraph('cf. Fiche annexe «Médication»', style_n),
        ])
        story.extend([
            Paragraph('SOINS INFIRMIERS DE BASE', style_sub),
            HRFlowable(width='100%', color=colors.black),
            Spacer(1, 0.3 * cm),
            Paragraph('cf. Fiche annexe «Carte de vie»', style_n),
            Spacer(1, 0.6 * cm),
            HRFlowable(width='100%', color=colors.darkgray, dash=(1, 2)),
            Spacer(1, 0.6 * cm),
            HRFlowable(width='100%', color=colors.darkgray, dash=(1, 2)),
        ])
        story.extend([
            Paragraph('RELATIONS', style_sub),
            HRFlowable(width='100%', color=colors.black),
            Spacer(1, 0.3 * cm),
            Paragraph('cf. Fiche annexe «Tableau des relations»', style_n),
        ])
        story.extend([
            Spacer(1, 2 * cm),
            Paragraph('INDICATIONS PARTICULIÈRES:', style_n),
            Spacer(1, 0.6 * cm),
            HRFlowable(width='100%', color=colors.darkgray, dash=(1, 2)),
            Spacer(1, 0.6 * cm),
            HRFlowable(width='100%', color=colors.darkgray, dash=(1, 2)),
            Spacer(1, 0.6 * cm),
            HRFlowable(width='100%', color=colors.darkgray, dash=(1, 2)),
            Spacer(1, 1 * cm),
            Paragraph('Lieu et date: ' + ('.' * 53) + 'Signature: '  + ('.' * 53), style_n)
        ])
        self.doc.build(story, onFirstPage=self.draw_header_footer)


class RelationsPDF(PersonPDFModel):
    title = "Tableau des relations"

    def produce_pdf(self, response):
        self.doc = SimpleDocTemplate(response, title=self.title, pagesize=landscape(A4),
            leftMargin=2*cm, rightMargin=1.5*cm, topMargin=1*cm, bottomMargin=1.7*cm)
        story = []
        self.person_header(story)
        style_t = ParagraphStyle('table', parent=self.style_n, fontSize=9, leading=11, spaceAfter=0)
        table_data = [['Nom, prénom', 'Adresse', 'Lien', 'Tél.', 'Mobile', 'Courriel', 'Note']]
        table_data.extend([
            [Paragraph(txt, style=style_t) for txt in [
                str(rel.contact), rel.contact.adresse_complete(), str(rel.role),
                rel.contact.telephone, rel.contact.portable, rel.contact.email,
                rel.note]
            ]
            for rel in self.person.relations.all()
        ])
        story.append(Table(table_data, style=TableStyle((
            ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('LEFTPADDING', (0, 0), (-1, -1), 2),
            ('RIGHTPADDING', (0, 0), (-1, -1), 2),
            ('TOPPADDING', (0, 0), (-1, -1), 2),
            ('BOTTOMPADDING', (0, 0), (-1, -1), 2),
            ('VALIGN', (0, 1), (-1, -1), 'TOP'),
        ))))
        self.doc.build(story, onFirstPage=self.draw_first_header_footer)


class CarteDeViePDF:
    title = 'Carte de vie'
    no_padding_styles = [
        ('LEFTPADDING', (0, 0), (-1, -1), 0),
        ('RIGHTPADDING', (0, 0), (-1, -1), 0),
        ('TOPPADDING', (0, 0), (-1, -1), 0),
        ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
    ]
    line1_style = [
        ('BACKGROUND', (0, 0), (-1, 0), colors.toColor('rgb(255,218,124)')),
        ('VALIGN', (0, 0), (-1, 0), 'MIDDLE'),
        ('ALIGN', (1, 0), (1, 0), 'RIGHT'),
        ('FONTSIZE', (0, 0), (-1, 0), 15),
        ('LEADING', (0, 0), (-1, 0), 17),
        ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
        ('BOX', (0, 0), (-1, 0), 0.25, colors.black),
    ]
    data_style = no_padding_styles + [
        ('VALIGN', (0, 0), (0, -1), 'TOP'),
        ('FONTSIZE', (0, 0), (-1, -1), 9),
        ('FONTNAME', (0, 0), (0, -1), 'Helvetica-Bold'),
    ]

    def __init__(self, **params):
        self.person = params['person']
        self.besoins, _ = RecueilBesoins.objects.get_or_create(person=self.person)
        try:
            self.besoins_alim = self.person.besoinsalimentaires
        except ObjectDoesNotExist:
            self.besoins_alim = None
        dates = [
            d for d in [
                self.besoins.carte_vie_maj,
                self.besoins_alim.date.date() if self.besoins_alim and self.besoins_alim.date else None
            ] if d is not None
        ]
        if dates:
            last_modif = max(dates)
        else:
            last_modif = date.today()
        self.last_modif_date = format(last_modif, 'd.m.Y')
        # Useful to display checkboxes chars (unavail. in Helvetica)
        registerFont(TTFont('DejaVuSans', find('fonts/DejaVuSans.ttf')))
        self.style_n = ParagraphStyle('normal', fontSize=9)
        self.style_b = ParagraphStyle('bold', fontSize=9, fontName='Helvetica-Bold')

    def subtitle_style(self, line):
        return [
            ('BACKGROUND', (0, line), (-1, line), colors.yellow),
            ('FONTSIZE', (0, line), (-1, line), 13),
            ('LEADING', (0, line), (-1, line), 15),
            ('FONTNAME', (0, line), (-1, line), 'Helvetica-Bold'),
        ]

    def produce_pdf(self, response):
        doc = SimpleDocTemplate(response, title=self.title, pagesize=A4,
            leftMargin=1*cm, rightMargin=1*cm, topMargin=1*cm, bottomMargin=1*cm)
        story = []
        story.append(Table(
            [[self.carte1_table(), '', self.carte2_table()],
             ['', '', ''],
             [self.carte3_table(), '', self.carte4_table()]
            ],
            colWidths=[9.6*cm, 0.4*cm, 9.6*cm],
            rowHeights=[13.2*cm, 0.6*cm, 13.2*cm],
            style=TableStyle([
                ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                ('BOX', (0, 0), (0, 0), 1, colors.black),
                ('BOX', (2, 0), (2, 0), 1, colors.black),
                ('BOX', (0, 2), (0, 2), 1, colors.black),
                ('BOX', (2, 2), (2, 2), 1, colors.black),
            ] + self.no_padding_styles)
        ))
        doc.build(story)

    def produce_carte(self, response, num=1):
        self.last_modif_date = ''
        doc = SimpleDocTemplate(response, title=self.title, pagesize=(A4[0]/2, A4[1]/2),
            leftMargin=1*cm, rightMargin=1*cm, topMargin=1*cm, bottomMargin=1*cm)
        story = [getattr(self, f'carte{num}_table')()]
        doc.build(story)

    def carte1_table(self):
        """Carte 1"""
        besoins = self.besoins
        text_width_hyg = 7.9*cm
        text_width_vet = 7.1*cm
        c1_table = Table(
            [['CARTE DE VIE', self.last_modif_date],
             ['Soins d’hygiène'], [Table(
                [['Toilette:', text_limit(besoins.hygiene_toilette, text_width_hyg)],
                 ['Douche:', text_limit(besoins.hygiene_bain_douche, text_width_hyg)],
                 ['Cheveux:', text_limit(besoins.hygiene_cheveux, text_width_hyg)],
                 ['Dents:', text_limit(besoins.hygiene_dents, text_width_hyg)],
                 ['Rasage:', text_limit(besoins.hygiene_coiffer, text_width_hyg)],
                 ['Peau:', text_limit(besoins.hygiene_peau, text_width_hyg)],
                 ['Divers:', text_limit(besoins.hygiene_divers, text_width_hyg)]
                ],
                colWidths=[1.5*cm, text_width_hyg],
                style=TableStyle(self.data_style),
             )],
             ['Se vêtir'], [Table(
                [['Habillage:', text_limit(besoins.vetir_habillage, text_width_vet, lines=2)],
                 ['Déshabillage:', text_limit(besoins.vetir_deshabillage, text_width_vet, lines=2)],
                 ['Divers:', text_limit(besoins.vetir_divers, text_width_vet, lines=2)],
                ],
                colWidths=[2.2*cm, text_width_vet],
                style=TableStyle(self.data_style),
             )],
             ['Sécurité'], [Table(
                [['Risque de chute:', ' ,'.join(besoins.securite_risque_chute or [])],
                 ['Risque d’errance:', ' ,'.join(besoins.securite_risque_errance or [])],
                 [Paragraph('Agressivité:  %s' % bool_to_cb(besoins.securite_risque_agr), style=self.style_b),
                  Paragraph('MRSA résistant:  %s' % bool_to_cb(besoins.securite_mrsa_resist), style=self.style_b)],
                 ['Contentions:', Paragraph(besoins.securite_contentions, style=self.style_n)],
                 ['Commentaires:', Paragraph(besoins.securite_commentaires, style=self.style_n)],
                ],
                colWidths=[3*cm, 6.3*cm],
                style=TableStyle(self.data_style),
             )],
            ],
            colWidths=[6.3*cm, 3.3*cm],
            style=TableStyle(
                self.line1_style + self.subtitle_style(1) + self.subtitle_style(3) +
                self.subtitle_style(5)
            )
        )
        return c1_table

    def carte2_table(self):
        """Carte 2"""
        besoins = self.besoins
        date_naissance = self.person.date_naissance
        c2_table = Table([
                ['CARTE DE VIE', self.last_modif_date],
                ['Résident', 'Référents'],
                [Paragraph(' '.join([self.person.prenom, self.person.nom]) + '<br/>' +
                 format(date_naissance, 'd.m.Y') if date_naissance else '', self.style_b),
                 '\n'.join([
                    p.get_full_name()
                    for p in [self.person.referent1, self.person.referent2]
                    if p is not None
                 ])
                ],
                ['Se mouvoir'],
                [Paragraph(
                    '<b>Problème de motricité:</b> ' +
                    bool_to_cb(besoins.motricite_probleme) + '<br/>' +
                    '<b>Préciser:</b> ' + besoins.motricite_probleme_prec + '<br/>' +
                    '<b>Déplacements:</b><br/>' + besoins.motricite_deplacements + '<br/>' +
                    '<b>Transferts:</b><br/>' + besoins.motricite_transferts + '<br/>' +
                    '<b>Se mobiliser:</b><br/>' + besoins.motricite_mobiliser,
                    style=self.style_n
                )],
            ],
            colWidths=[4.8*cm, 4.8*cm],
            style=TableStyle(self.line1_style + [
                ('BACKGROUND', (0, 1), (-1, 2), colors.toColor('rgb(168,208,248)')),
                ('FONTNAME', (0, 2), (-1, 2), 'Helvetica-Bold'),
                ('VALIGN', (0, 2), (-1, 2), 'TOP'),
            ] + self.subtitle_style(3) + [
                ('SPAN', (0, 4),(-1, -1)),
            ])
        )
        return c2_table

    def carte3_table(self):
        """Carte 3"""
        besoins = self.besoins
        text_width_elim = 7.1*cm
        text_width_comm = 6.8*cm
        c3_table = Table([
                ['CARTE DE VIE', self.last_modif_date],
                ['Éliminer'],
                [Table(
                    [['Incontinence:', text_limit(besoins.eliminer_incontinence, text_width_elim)],
                     ['Transferts:', text_limit(besoins.eliminer_transferts, text_width_elim)],
                     ['Moy. aux.:', text_limit(besoins.eliminer_moyens, text_width_elim)],
                     ['Médications:', text_limit(besoins.eliminer_medication, text_width_elim)],
                     ['Protections:', text_limit(besoins.eliminer_protections, text_width_elim)],
                     ['Divers:', text_limit(besoins.eliminer_divers, text_width_elim)]
                    ],
                    colWidths=[2.2*cm, text_width_elim],
                    style=TableStyle(self.data_style),
                 )],
                ['Respirer'],
                [Table([
                    [Paragraph(bool_to_cb(besoins.respirer_tabagisme) + ' Tabagisme', style=self.style_n),
                     Paragraph(bool_to_cb(besoins.respirer_bronches) + ' Encombrements bronchiques', style=self.style_n)],
                    [Paragraph(bool_to_cb(besoins.respirer_dyspnee) + ' Dyspnée', style=self.style_n),
                     Paragraph(bool_to_cb(besoins.respirer_toux) + ' Toux, expectorations', style=self.style_n)],
                    [Paragraph(bool_to_cb(besoins.respirer_aerosol) + ' Aérosol', style=self.style_n),
                     Paragraph(bool_to_cb(besoins.respirer_oxygene) + ' Oxygène', style=self.style_n)],
                    [Paragraph(besoins.respirer_commentaires, style=self.style_n)],
                ], colWidths=[2.2*cm, text_width_elim],
                style=TableStyle(self.no_padding_styles + [
                    ('VALIGN', (0, 0), (0, -1), 'TOP'),
                    ('FONTSIZE', (0, 0), (-1, -1), 9),
                    ('SPAN', (0, 3), (1, 3)),
                ]))],
                ['Communiquer'],
                [Table(
                    [['Général:', text_limit(besoins.communiquer_general, text_width_comm, lines=3)],
                     ['Ouïe:', text_limit(besoins.communiquer_ouie, text_width_comm, lines=3)],
                     ['Langage:', text_limit(besoins.communiquer_langage, text_width_comm, lines=3)],
                     ['Commentaires:', text_limit(besoins.communiquer_commentaires, text_width_comm, lines=3)],
                    ],
                    colWidths=[2.5*cm, text_width_comm],
                    style=TableStyle(self.data_style),
                 )],
            ],
            colWidths=[6.3*cm, 3.3*cm],
            style=TableStyle(
                self.line1_style + self.subtitle_style(1) + self.subtitle_style(3) +
                self.subtitle_style(5) + [
            ])
        )
        return c3_table

    def carte4_table(self):
        """Carte 4"""
        text_width_alim1 = 7.5*cm
        text_width_alim2 = 3*cm
        text_width_repo = 7.7*cm
        table_data = [
            ['CARTE DE VIE', self.last_modif_date],
            ['Alimentation'],
        ]
        if self.besoins_alim is not None:
            table_data.extend([
                 [Table(
                    [['Problèmes:', text_limit(self.besoins_alim.problemes, text_width_alim1)],
                     ['Allerg. ali.:', text_limit(self.besoins_alim.allergies, text_width_alim1)]],
                    colWidths=[2.2*cm, text_width_alim1],
                    style=TableStyle(self.data_style + [('TEXTCOLOR', (0, 1), (-1, 1), colors.red)])
                 )],
                 [Table(
                    [['Régimes:', text_limit(self.besoins_alim.regimes, text_width_alim2),
                      'Portions:', text_limit(self.besoins_alim.portions, text_width_alim2)],
                     ['Prot. dent.:', text_limit(self.besoins_alim.protheses, text_width_alim2),
                      'Aide ali.:', text_limit(self.besoins_alim.aide_alim, text_width_alim2)],
                     ['Aide hydr.:', text_limit(self.besoins_alim.aide_hydr, text_width_alim2),
                      'Moy. aux.:', text_limit(self.besoins_alim.moyens_aux, text_width_alim2)],
                     ['Compl ali.:', text_limit(self.besoins_alim.complements, 7.5*cm)],
                    ],
                    colWidths=[1.7*cm, text_width_alim2, 1.6*cm, text_width_alim2],
                    style=TableStyle(self.data_style + [
                        ('SPAN', (1, 3), (-1, 3)),
                        ('FONTNAME', (2, 0), (2, -1), 'Helvetica-Bold'),
                    ])
                 )],
                 [Table(
                    [[Paragraph('<font name="DejaVuSans">☺</font>', style=self.style_b), text_limit(self.besoins_alim.desirs, 3.2*cm),
                      Paragraph('<font name="DejaVuSans">☹</font>', style=self.style_b), text_limit(self.besoins_alim.naimepas, 3.5*cm)],
                     ['Boiss.:', text_limit(self.besoins_alim.boissons, 3.2*cm),
                      'Alim.:', text_limit(self.besoins_alim.aliments, 3.5*cm)],
                     ['Acc.:', text_limit(self.besoins_alim.accompagnements, 3.2*cm),
                      'M. gra.:', text_limit(self.besoins_alim.mat_grasses, 3.5*cm)],
                     ['Divers:', text_limit(self.besoins_alim.divers, 8.2*cm)],
                    ],
                    colWidths=[1.1*cm, 3.2*cm, 1.2*cm, 3.5*cm],
                    style=TableStyle(self.data_style + [
                        ('SPAN', (1, 3), (-1, 3)),
                        ('FONTNAME', (2, 0), (2, -1), 'Helvetica-Bold'),
                    ])
                 )],
            ])
        else:
            table_data.extend([['-']])
        besoins = self.besoins
        table_data.extend([
             ['Se reposer'],
             [Table(
                [['Sieste:', text_limit(besoins.reposer_sieste, text_width_repo, lines=3)],
                 ['Lit:', text_limit(besoins.reposer_lit, text_width_repo, lines=3)],
                 ['Matelas:', text_limit(besoins.reposer_matelas, text_width_repo, lines=3)],
                 ['Divers:', text_limit(besoins.reposer_divers, text_width_repo, lines=3)],
                ],
                colWidths=[1.5*cm, text_width_repo],
                style=TableStyle(self.data_style),
             )],
        ])
        reposer_index = 5 if self.besoins_alim else 3
        c4_table = Table(
            table_data,
            colWidths=[6.3*cm, 3.3*cm],
            style=TableStyle(self.line1_style + self.subtitle_style(1) + self.subtitle_style(reposer_index))
        )
        return c4_table


def text_limit(text, width, font_size=9, lines=1):
    """Limit text to width (in cm) in max lines."""
    if lines > 1:
        pstyle = ParagraphStyle('base', fontSize=font_size)
        par = Paragraph(text, style=pstyle)
        par.width = width
        frag = par.breakLines(width)
        if len(frag.lines) > lines:
            # count words for shown lines
            num_words = sum([len(line[1]) for idx, line in enumerate(frag.lines) if idx < lines])
            text = " ".join(text.split()[:num_words])
        return Paragraph(text, style=pstyle)
    while stringWidth(text, 'Helvetica', font_size) > width:
        text = " ".join(text.split()[:-1])  # remove a word
    return text


def bool_to_cb(boolean):
    return '<font name="DejaVuSans">%s</font>' % ('☒' if boolean is True else '☐',)


def img_to_pdf(img_path):
    """Print an image in a single-page PDF."""
    from io import BytesIO
    buff = BytesIO()
    doc = SimpleDocTemplate(
        buff, pagesize=A4,
        leftMargin=1.5*cm, rightMargin=1*cm, topMargin=1.5*cm, bottomMargin=1*cm
    )
    max_width = doc.width - doc.leftMargin - doc.rightMargin
    img = ImageReader(img_path)
    orig_width, height = img.getSize()
    aspect = height / orig_width
    story = [Image(img_path, max_width, height=max_width * aspect)]
    doc.build(story)
    return buff


def _max_image_dim(current_w, current_h, max_w, max_h):
    shrink_w = shrink_h = 1
    if current_w > max_w:
        shrink_w = max_w/float(current_w)
    if max_h is not None and current_h > max_h:
        shrink_h = max_h/float(current_h)
    shrink = min([shrink_h, shrink_w])
    return (shrink*current_w, shrink*current_h)
