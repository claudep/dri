'use strict';

var debug = false,
    activeTab = null;

document.addEventListener('DOMContentLoaded', function () {
    attachHandlers(document);
    window.addEventListener('popstate', function(ev) {
        if (ev.state === null) {
            // We are back to the initial page before first push state.
            var targetInput = document.querySelector('input[id="tab-1"]');
            var currentInput = null;
        } else {
            var targetInput = document.querySelector('input[id="' + ev.state[ev.state.inputName] + '"]');
            var currentInput = document.querySelector('input[name=' + ev.state.inputName + ']:checked');
        }
        if (targetInput != currentInput) {
            targetInput.checked = true;
            targetInput.focus();
            var div = targetInput.nextElementSibling.nextElementSibling;
            loadURL(div.dataset.url, div);
        } else if (ev.state && ev.state.panelURL) {
            // Same tab but different URL
            var div = targetInput.nextElementSibling.nextElementSibling;
            loadURL(ev.state.panelURL, div);
        }
        console.log('popstate fired, event state: ' + ev.state);
        console.log('current checked tabs0: ' + document.querySelector('input[name=tabs0]:checked').id);
    });
    window.addEventListener("beforeunload", checkFormOpen, false);
    const span = document.getElementById("modalClose");
    span.onclick = function() {
        parentTag(this, 'div').style.display = "none";
    }
    // Hide message after click on "J'ai lu"
    const modifMsgDiv = document.getElementById("modif_messages");
    if (modifMsgDiv) {
        const modifForms = modifMsgDiv.querySelectorAll("form");
        for (let i = 0; i < modifForms.length; i++) {
            modifForms[i].addEventListener('submit', function (ev) {
                ev.preventDefault();
                const frm = this;
                submitAjaxForm(this, null, function(xhr) {
                    frm.style.display = 'none';
                });
            });
        }
    }
    // Detect obsolete browsers on login
    if (!('Promise' in window)) {
        document.getElementById('login-form').style.display = 'none';
        document.getElementById('unsupported').style.display = 'block';
    }
});

function attachHandlers(node) {
    var elements = node.querySelectorAll("input.tabinput");
    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', checkFormOpen);
        elements[i].addEventListener('change', activateTab);
    }
    var links = node.querySelectorAll("a.AjaxLink");
    for (var i = 0; i < links.length; i++) {
        links[i].addEventListener('click', displayPage);
    }
    // Attachment images
    var imgLinks = node.querySelectorAll("a.image");
    for (var i = 0; i < imgLinks.length; i++) {
        imgLinks[i].addEventListener('click', showImage);
    }
    // Attachment file deletion
    var delAttachLinks = node.querySelectorAll("img.deleteAttachment");
    for (var i = 0; i < delAttachLinks.length; i++) {
        delAttachLinks[i].addEventListener('click', deleteAttachment);
    }
    var buttons = node.querySelectorAll(".linkButton");  // button or images
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', loadPageHandler);
    }
    var forms = node.querySelectorAll("form.AjaxForm");
    for (var i = 0; i < forms.length; i++) {
        forms[i].addEventListener('submit', submitFormHandler);
    }
    var cancels = node.querySelectorAll("button.cancel");
    for (var i = 0; i < cancels.length; i++) {
        cancels[i].addEventListener('click', backContent);
    }
    var confirms = node.querySelectorAll("button.confirm");
    for (var i = 0; i < confirms.length; i++) {
        confirms[i].addEventListener('click', buttonConfirm);
    }
    var immediateInputs = node.querySelectorAll(".immediate-submit");
    for (var i = 0; i < immediateInputs.length; i++) {
        immediateInputs[i].addEventListener('change', ev => {
            if (ev.target.nodeName == "FORM") ev.target.submit();
            else ev.target.form.submit();
        });
    }
    var histSwitchers = node.querySelectorAll("img.history-switcher");
    for (var i = 0; i < histSwitchers.length; i++) {
        histSwitchers[i].addEventListener('click', switchHistory);
    }
    var expButton = node.querySelectorAll("span.text-exp");
    for (var i = 0; i < expButton.length; i++) {
        expButton[i].addEventListener('click', expandText);
    }
    var printBtn = document.getElementById('print-med-label');
    if (printBtn) {
        printBtn.addEventListener('click', printMedLabel);
    }
    var viewLineImgs = node.querySelectorAll("img.view_actej_line");
    for (var i = 0; i < viewLineImgs.length; i++) {
        viewLineImgs[i].addEventListener('click', showThisElement);
    }
    // agenda
    var categSelect = document.getElementById('id_category');
    if (categSelect) {
        categSelect.addEventListener('change', setColorInput);
    }
    var calSelectors = node.querySelectorAll("input[name='cal_display']");
    for (var i = 0; i < calSelectors.length; i++) {
        calSelectors[i].addEventListener('change', loadPageHandler);
    }
    // autosizing textarea
    autosize(document.querySelectorAll('textarea'));
    // Table sort headers
    var tables = node.querySelectorAll("table.tablesort");
    for (var i = 0; i < tables.length; i++) {
        new Tablesort(tables[i]);
    }
    // Manually sortable tables
    var sortables = node.querySelectorAll("tbody.sortable");
    for (var i = 0; i < sortables.length; i++) {
        Sortable.create(sortables[i], {handle: '.handler', onEnd: function (evt) {
            var form = parentTag(evt.to, 'form');
            if (form.dataset.sorturl) {
                // Compute an ordered list of items ids and post them.
                let trs = evt.to.querySelectorAll('tr');
                const ids = [];
                trs.forEach(function(item){
                    ids.push(item.dataset.pk);
                });
                // Post sorted items to sort URL
                form.action = form.dataset.sorturl;
                submitAjaxForm(form, {'item_ids': ids.join(',')}, function() {});
            } else {
                // Update values of the order inputs
                var orders = evt.to.querySelectorAll('input[name$=order]');
                for (var i = 0; i < orders.length; i++) {
                    // if (!isFormEmpty())the line is not empty
                    orders[i].value = i + 1;
                }
            }
        }});
    }
    // Date pickers
    if (typeof DateTimeShortcuts !== 'undefined') {
        DateTimeShortcuts.init();
    }
    // jQuery required for select2
    if (typeof jQuery !== 'undefined') {
        $('.contact-choice').select2({
            allowClear: true,
            placeholder: '----',
            ajax: {
                url: '/contacts/query/',
                delay: 250,
                dataType: 'json'
            },
            language: {
                noResults: function() {
                    return 'pas de résultats';
                },
                inputTooShort: function (args) {
                    var remainingChars = args.minimum - args.input.length;
                    return 'Saisissez au moins ' + remainingChars + ' caractère' + ((remainingChars > 1) ? 's' : '');
                },
                searching: function () {
                    return 'Recherche en cours…';
                }
            },
            minimumInputLength: 2
        });
        $("select.dynamic-autocomplete").select2({
          tags: true
        });
    }
}

function checkFormOpen(ev) {
    var openForms = document.getElementsByClassName('AjaxForm object-edition');
    if (openForms.length) {
        alert("Vous devez confirmer ou annuler le formulaire en cours avant de pouvoir changer d'onglet.");
        ev.preventDefault();
        var inputName = ev.target.name;
        var previousSelected = document.querySelector("input[name=" + inputName + "]:checked");
        if (previousSelected) previousSelected.checked = true;
        return false;
    }
}

function activateTab(ev) {
    /* When clicking a tab (which is a radio), load the matching url */
    if (!this.checked) return;
    var div = this.nextElementSibling.nextElementSibling;
    loadURL(div.dataset.url, div);
    var data = {inputName: this.name, panelURL: div.dataset.url};
    data[this.name] = document.querySelector('input[name=' + this.name + ']:checked').id;
    history.pushState(data, this.id, div.dataset.url);
    activeTab = this.id.substring(4);  // id starts with 'tab-'
}


function hasClass(node, selector) {
    var className = " " + selector + " ";
    if ((" " + node.className + " ").replace(/[\n\t]/g, " ").indexOf(className) > -1 ) {
        return true;
    }
    return false;
}

function isVisible(element) {
    return element.offsetWidth > 0 && element.offsetHeight > 0;
}

function parentPanel(node) {
    while(node) {
        if (node.tagName == 'DIV' && hasClass(node, 'panel')) return node;
        node = node.parentNode;
    }
}

function parentTag(node, tag) {
    while(node) {
        if (node.tagName == tag.toUpperCase()) return node;
        node = node.parentNode;
    }
}

function showImage(ev) {
    var modal = document.getElementById('imgModal'); /* Present in base.html */
    var imgTag = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    ev.preventDefault();
    modal.style.display = "block";
    imgTag.src = this.href;
    captionText.innerHTML = this.textContent;
}

function displayPage(ev) {
    ev.preventDefault();
    // 'this' is a <a> whose href target will be pushed in the link panel.
    var panel = parentPanel(this);
    loadURL(this.href, panel);
    history.pushState({}, this.textContent, this.href);
}

function loadPageHandler(ev) {
    var parent = parentPanel(this);
    ev.preventDefault();
    if (typeof parent !== 'undefined') {
        parent.previousContent = parent.innerHTML;
        loadURL(this.href || this.dataset.url, parent);
    } else {
        window.location = this.dataset.url;
    }
}

function selectTab(tabList, finalUrl) {
    var tabInput = document.getElementById('tab-' + tabList[0]);
    if (tabInput) {
        // A bit similar than activateTab, but no history.
        tabInput.checked = true;
        var panelDiv = tabInput.nextElementSibling.nextElementSibling;
        loadURL(panelDiv.dataset.url, panelDiv).then(resp => {
            if (tabList.length > 1) selectTab(tabList.slice(1), finalUrl);
            else {
                activeTab = tabList[0];
                if (finalUrl.length && finalUrl != 'None' && finalUrl != panelDiv.dataset.url) {
                    loadURL(finalUrl, panelDiv);
                }
            }
        });
    } else {
        console.log("Unable to find a tab with id 'tab-" + tabList[0] + "'");
        var wdiv = document.querySelector('div.centered:not([hidden])');
        if (wdiv) {
            wdiv.textContent = 'Désolé, ce contenu n’est pas accessible ou vous n’avez pas les permissions nécessaires.';
        }
    }
}

function switchHistory(ev) {
    // Show or hide object history
    var histDiv = document.getElementById(this.dataset.target);
    if (isVisible(histDiv)) {
        histDiv.style.display = 'none';
    } else {
        if (histDiv.dataset.url) {
            loadURL(histDiv.dataset.url, histDiv).then(resp => {
                histDiv.style.display = 'block';
            });
        } else {
            histDiv.style.display = 'block';
        }
    }
}

function backContent(ev) {
    // Go back to previous content after clicking on Cancel
    ev.preventDefault();
    var parent = parentPanel(this);
    if (typeof parent !== 'undefined' && parent.previousContent !== undefined) {
        parent.innerHTML = parent.previousContent;
        parent.previousContent = undefined;
        attachHandlers(parent);
    } else {
        history.back();
    }
}

function submitFormHandler(ev) {
    ev.preventDefault();
    if (this.dataset.parent == 'panel') {
        var inside = parentPanel(this);
    } else {
        var inside = this.parentNode;
    }
    submitAjaxForm(this, null, function(xhr) {
        inside.innerHTML = xhr.responseText;
        inside.dataset.currentURL = xhr.responseURL;
        attachHandlers(inside);
    });
}

function submitAjaxForm(form, optData, callback) {
    var formAction = document.activeElement.getAttribute('formaction') || form.getAttribute('action');
    if (form.method.toLowerCase() == 'get') {
        var parent = parentPanel(form);
        var qString = new URLSearchParams(new FormData(form)).toString();
        return loadURL(formAction + '?' + qString, parent);
    }
    var csrftoken = form.elements["csrfmiddlewaretoken"].value,
        xhr = new XMLHttpRequest(),
        formData = new FormData(form);
    if (optData) {
        for (var key in optData) {
            formData.append(key, optData[key]);
        }
    }

    xhr.open('POST', formAction, true);
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.addEventListener('readystatechange', function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                callback(xhr);
            } else {
                alert("Désolé, une erreur s'est produite lors de l'enregistrement des données. Les administrateurs ont été averti et vont corriger le problème au plus vite.");
            }
        }
    });
    xhr.send(formData);
    return false;
}

function loadURL(url, inside) {
    var errMsg = "Désolé, une erreur s'est produite en essayant de charger ce lien. Les administrateurs ont été averti et vont corriger le problème au plus vite."
    if (debug) console.log('Loading ' + url + ' inside ' + inside);

    return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

        xhr.onload = function() {
            if (xhr.status == 200) {
                inside.innerHTML = xhr.responseText;
                inside.dataset.currentURL = url;
                attachHandlers(inside);
                // Autoload the first panel content if any
                var panels = inside.getElementsByClassName('panel');
                if (panels.length && panels[0].dataset.autoload == 'true') {
                    loadURL(panels[0].dataset.url, panels[0]);
                }
                // Resolve the promise with the response text
                resolve(xhr.response);
            }
            else {
                reject(Error(xhr.statusText));
            }
        };

        // Handle network errors
        xhr.onerror = function() {
            reject(Error("Network Error"));
        };

        xhr.send();
    }).catch(err => {
        alert(errMsg);
    });
}

function buttonConfirm(ev) {
    if (this.dataset.confirm && !confirm(this.dataset.confirm)) {
        ev.preventDefault();
        return false;
    }
}

function expandText(ev) {
    var fullText = this.parentNode.nextSibling;
    if (fullText) {
        fullText.removeAttribute('hidden');
        this.parentNode.setAttribute('hidden', true);
    }
}

function printMedLabel(ev) {
    if (this.dataset.askconfirm == 'false') return true;
    ev.preventDefault();
    var discard = confirm(
        "Imprimez-vous pour remplacer l’ancienne étiquette ?\n Sinon, cliquez sur annuler pour simplement afficher l’étiquette imprimable."
    )
    this.form.elements.discard.value = discard ? '1' : '0';
    if (discard) {
        document.getElementById("not-printed-note").style.display = 'none';
    }
    this.form.submit();
}

function setColorInput(ev) {
    // Setting color in the agenda event edit page.
    var colWidget = document.getElementById('id_color');
    if (colWidget) {
        var colMap = JSON.parse(document.getElementById('categ-color-map').textContent);
        colWidget.value = colMap[this.value];
    }
}

/* An eye icon on actesjournaliers tables history can be clicked to show archived lines
 * after the current line (second click hides it again).
 */
function showThisElement(ev) {
    ev.preventDefault();
    var thisPk = this.dataset.pk;
    var existing = document.querySelector("[data-archived='" + thisPk + "']");
    if (existing) {
        existing.remove();
    } else {
        var url = this.dataset.url;
        var thisTable = parentTag(this, 'table');
        var thisLine = parentTag(this, 'tr');
        var newRow = thisTable.insertRow(thisLine.rowIndex + 1);
        loadURL(url, newRow).then(resp => {
            newRow.classList.add('archived');
            newRow.dataset.archived = thisPk;
        });
    }
}

/* Functions to dynamically insert an item in a table (e.g. relations) */
function addDynamicRow(img, tableId) {
    if (typeof tableId !== 'undefined') {
        var table = document.getElementById(tableId);
    } else var table = parentTag(img, 'table');
    // hide the + tr line
    var plusRow = table.querySelector('tr.plus-line');
    plusRow.style.display = 'none';
    // insert a new tr line with the form and save/cancel buttons
    var row = table.insertRow(plusRow.rowIndex);
    row.className = 'form-line';
    loadURL(img.dataset.url, row);
}

function cancelRow(ev, img) {
    // Show hidden next row, remove the form row(s), redisplay a tr.plus-line, if any.
    ev.preventDefault();
    var thisTr = parentTag(img, 'tr');
    var table = parentTag(thisTr, 'table');
    var nextTr = thisTr.nextSibling;
    if (nextTr) nextTr.style.display = 'table-row';
    removeFormRows(thisTr);
    var plusRow = table.querySelector('tr.plus-line');
    if (plusRow) plusRow.style.display = 'table-row';
}

function removeFormRows(btnTr) {
    if (btnTr.previousSibling && btnTr.previousSibling.className == 'form-line') {
        // this is the case when the button line is a separate row under the form row.
        btnTr.parentNode.removeChild(btnTr.previousSibling);
    }
    btnTr.parentNode.removeChild(btnTr);
}

function saveRowForm(ev, btn) {
    ev.preventDefault();
    // Get parent form
    var form = parentTag(btn, 'form');
    form.action = btn.formaction || btn.formAction;
    submitAjaxForm(form, null, function(xhr) {
        var hasErrors = xhr.getResponseHeader('X-FormHasErrors');
        if (hasErrors == 'true') {
            // Redisplay form with error
            let tr = form.querySelector('tr.form-line')
            tr.innerHTML = xhr.responseText;
            attachHandlers(tr);
        } else {
            // reload the current panel
            let panel = parentPanel(btn);
            if (panel) {
                if (panel.dataset.currentURL) loadURL(panel.dataset.currentURL, panel);
                else loadURL(panel.dataset.url, panel);
            } else {
                let contentTr = parentTag(btn, 'tr').nextSibling;
                removeFormRows(parentTag(btn, 'tr'));
                // if response contains HTML, replace tr by new content
                contentTr.innerHTML = xhr.responseText;
                contentTr.style.display = 'table-row';
                contentTr.classList.add('highlighted');
                window.setTimeout(function() { contentTr.classList.remove('highlighted'); }, 500);
            }
        }
    });
}

function editRow(img, insertButtons=false) {
    // hide this row, insert a new form row after this
    // if insertButtons is true, create a second line with save/cancel buttons.
    const form = img.closest('form');
    if (img.closest('form').querySelectorAll('button[type="submit"]').length > 0) {
        alert("Il y a déjà une ligne en cours d’édition.")
        return;
    }
    var thisTr = parentTag(img, 'tr');
    var table = parentTag(thisTr, 'table');
    var numCols = thisTr.cells.length;
    var row = table.insertRow(thisTr.rowIndex);
    var url = img.dataset.url;
    row.className = 'form-line';
    loadURL(url, row).then(resp => {
        thisTr.style.display = 'none';
        if (insertButtons) {
            let btnRow = table.insertRow(thisTr.rowIndex);
            btnRow.className = 'form-line-buttons';
            let newCell = btnRow.insertCell(0);
            newCell.colSpan = numCols;
            var saveBtn = document.createElement("button");
            saveBtn.textContent = 'Enregistrer';
            saveBtn.formaction = url;
            saveBtn.onclick = function (ev) { saveRowForm(ev, saveBtn); }
            var cancelBtn = document.createElement("button");
            cancelBtn.textContent = 'Annuler';
            cancelBtn.onclick = function (ev) { cancelRow(ev, cancelBtn); }
            newCell.appendChild(saveBtn);
            newCell.appendChild(cancelBtn);
        }
    });
}

function deleteItem(img) {
    if (!confirm("Voulez-vous vraiment " + img.title.toLowerCase() + " ?")) return;
    var form = parentTag(img, 'form');
    form.action = img.dataset.url;
    submitAjaxForm(form, null, function(xhr) {
        img.parentNode.parentNode.remove();
    });
}

function deleteAttachment(ev) {
    ev.preventDefault();
    if (!confirm("Voulez-vous vraiment supprimer ce fichier ? (la suppression est immmédiate)")) return;

    var img = this,
        form = parentTag(this, 'form'),
        csrftoken = form.elements["csrfmiddlewaretoken"].value,
        action = this.dataset.delete,
        xhr = new XMLHttpRequest();

    xhr.open('POST', action, true);
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.addEventListener('readystatechange', function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                // Check delete cb because submitting the form needs that entry to be valid.
                parentTag(img, 'li').style.display = 'none';
                document.getElementById(img.dataset.deletecb).checked = true;
            } else if (xhr.status === 403) {
                alert("Désolé, vous n'avez pas les droits nécessaires pour supprimer ce fichier.");
            } else {
                alert("Désolé, une erreur s'est produite lors de la suppression du fichier. Les administrateurs ont été averti et vont corriger le problème au plus vite.");
            }
        }
    });
    xhr.send();
    return false;
}
