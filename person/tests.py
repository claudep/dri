import atexit
import shutil
import tempfile
from datetime import date, datetime, timedelta
from pathlib import Path

from django.conf import settings
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core import mail
from django.core.files import File
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, override_settings
from django.urls import resolve, reverse
from django.utils import timezone
from django.utils.dateformat import format as date_format

from besoins.models import BesoinsAlimentaires, RecueilBesoins
from .forms import EntreeSortieForm
from .models import (
    ActionIS, Attachment, Contact, Document, EntreeSortie, HistoireDeVie,
    Modification, Person, Relation, Role, SoutienSocial, User
)
from .views import GeneralJournalView

XLSX_CONTENT_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'

_temp_media = tempfile.mkdtemp()
atexit.register(shutil.rmtree, _temp_media)


@override_settings(MEDIA_ROOT=_temp_media)
class TempMediaTestCase(TestCase):
    """Redirect media dir to temp dir."""


class ModelBaseTextMixin:
    ModelClass = None
    model_id = None  # base view name
    pers_situation = 'fhmn_int'
    custom_data = {}  # data provided to the post method

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(username='jill', first_name='Jill', last_name='Martin')
        cls.person = Person.objects.create(
            nom='Smith', prenom='John', situation=cls.pers_situation,
            entree_date=timezone.now()-timedelta(days=180)
        )
        cls.person.referent1 = User.objects.create(username='karl', first_name='Karl', last_name='Referent')
        cls.person.save()

    def test_model_creation(self):
        self.client.force_login(self.user)
        url = reverse('%s-new' % self.model_id, args=[self.person.pk])
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(
            response, 
            '<form class="AjaxForm object-edition" method="post" enctype="multipart/form-data" action="{}">'.format(url)
        )
        self.assertNotContains(response, 'name="visa"')
        self.assertNotContains(response, 'name="createur"')
        self.assertNotContains(response, '<label for="id_person">Person :</label>')
        data = dict(
            {k: getattr(v, 'pk', v) for k, v in self.custom_data.items()},
            person=self.person.pk
        )
        if hasattr(response.context['form'], 'attach_forms'):
            data.update({
                'attachments-TOTAL_FORMS': 0,
                'attachments-INITIAL_FORMS': 0,
            })
        response = self.client.post(url, data=data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        query_data = {k: v for k, v in self.custom_data.items() if not isinstance(v, SimpleUploadedFile)}
        try:
            new_instance = self.ModelClass.objects.get(**query_data)
        except self.ModelClass.DoesNotExist:
            self.fail(response.context['form'].errors)
        view = resolve(url)
        if view.func.view_class.return_to_list:
            expected = reverse('%s-list' % self.model_id, args=[self.person.pk])
        else:
            expected = reverse('%s-detail' % self.model_id, args=[self.person.pk, new_instance.pk])
        self.assertRedirects(response, expected, fetch_redirect_response=False)
        self.assertEqual(new_instance.date.date() if isinstance(new_instance.date, datetime) else new_instance.date, date.today())
        self.assertEqual(new_instance.person, self.person)
        self.assertEqual(new_instance.auteur, 'Jill Martin')
        self.assertEqual(Modification.objects.filter(
            content_type=ContentType.objects.get_for_model(new_instance), object_id=new_instance.pk
        ).count(), 1)
        self.assertTrue(new_instance.can_edit(self.user))
        if 'Observation' in self.ModelClass.__name__:
            # No edition when observation is not from today
            new_instance.date = timezone.now() - timedelta(days=1)
            new_instance.save()
            self.assertFalse(new_instance.can_edit(self.user))

    def test_model_list(self):
        self.client.force_login(self.user)
        create_kwargs = {**self.custom_data, 'person': self.person, 'date': timezone.now()}
        new_obj = self.ModelClass.objects.create(**create_kwargs)
        response = self.client.get(
            reverse('%s-list' % self.model_id, args=[self.person.pk]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        # User has no special permissions, should not be able to add element.
        self.assertNotIn('new_form_url', response.context)
        self.assertContains(
            response,
            '<a class="AjaxLink" href="%s">' % reverse('%s-detail' % self.model_id, args=[self.person.pk, new_obj.pk]),
        )

    def test_model_detail(self):
        self.client.force_login(self.user)
        create_kwargs = {**self.custom_data, 'person': self.person, 'date': timezone.now()}
        new_obj = self.ModelClass.objects.create(**create_kwargs)
        response = self.client.get(
            reverse('%s-detail' % self.model_id, args=[self.person.pk, new_obj.pk]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertContains(
            response,
            '<button class="linkButton" data-url="{}">Retour à la liste</button>'.format(
                reverse('%s-list' % self.model_id, args=[self.person.pk])
            ),
            html=True,
        )


class PersonTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(username='jill', first_name='Jill', last_name='Martin', is_superuser=True)

    def test_person_list(self):
        pers = Person.objects.create(
            nom='Smith', prenom='John', situation='fhmn_int',
            entree_date= timezone.now()-timedelta(days=180)
        )
        pers2 = Person.objects.create(
            nom='Smith', prenom='Kate', situation='fhmn_int',
            entree_date=timezone.now()-timedelta(days=180)
        )
        EntreeSortie.objects.create(
            person=pers2, entree_sortie='d', date=timezone.now()-timedelta(days=120)
        )
        EntreeSortie.objects.create(
            person=pers2, entree_sortie='a', date=timezone.now()-timedelta(days=80)
        )
        pers_attente = Person.objects.create(
            nom='Dupond', prenom='Archibald', situation='fhmn_int',
            entree_date=timezone.now()-timedelta(days=180)
        )
        EntreeSortie.objects.create(
            person=pers_attente, entree_sortie='d', date=timezone.now()-timedelta(days=120)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('person-list', args=['fhmn_int']))
        self.assertContains(response, '<td><a href="/person/%d/">Smith John</a></td>' % pers.pk, html=True)
        self.assertContains(response, '<td><a href="/person/%d/">Smith Kate</a></td>' % pers2.pk, html=True)
        self.assertNotContains(response, 'Dupond Archibald')
        response = self.client.get(reverse('person-list', args=['fhmn_int']) + '?columns=regimes')
        self.assertContains(response, '<td><a href="/person/%d/">Smith John</a></td>' % pers.pk, html=True)

    def test_person_list_attente(self):
        pers = Person.objects.create(nom='Smith', prenom='John', entree_date=None)
        pers2 = Person.objects.create(nom='Smith', prenom='Kate', entree_date=timezone.now()-timedelta(days=180))
        EntreeSortie.objects.create(
            person=pers2, entree_sortie='d', date=timezone.now()-timedelta(days=80)
        )
        pers_active = Person.objects.create(
            nom='Dupond', prenom='Archibald', situation='fhmn_int',
            entree_date=timezone.now()-timedelta(days=180)
        )
        EntreeSortie.objects.create(
            person=pers_active, entree_sortie='d', date=timezone.now()-timedelta(days=120), situation='fhmn_int'
        )
        EntreeSortie.objects.create(
            person=pers_active, entree_sortie='a', date=timezone.now()-timedelta(days=80), situation='fhmn_int'
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('person-attente'))
        self.assertContains(response, '<td><a href="/person/%d/attente/">Smith John</a></td>' % pers.pk, html=True)
        self.assertContains(response, '<td><a href="/person/%d/attente/">Smith Kate</a></td>' % pers2.pk, html=True)
        self.assertNotContains(response, 'Dupond Archibald')

    def test_person_archive_unarchive(self):
        """From waiting list to archives and back."""
        pers = Person.objects.create(nom='Smith', prenom='John', entree_date=None)
        self.assertTrue(pers.actif)
        self.client.force_login(self.user)
        response = self.client.post(reverse('person-archiver', args=[pers.pk]))
        self.assertRedirects(response, pers.get_absolute_url())
        pers.refresh_from_db()
        self.assertFalse(pers.actif)
        response = self.client.post(reverse('person-desarchiver', args=[pers.pk]))
        self.assertRedirects(response, pers.get_absolute_url())
        pers.refresh_from_db()
        self.assertTrue(pers.actif)

    def test_person_list_proche_sig(self):
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        c1 = Contact.objects.create(nom='Obstgen', prenom='Mimi', telephone='032 333 33 33', portable='078 888 88 88')
        Relation.objects.create(person=pers, contact=c1, proche_sig=True)
        c2 = Contact.objects.create(nom='Würren', prenom='Jon')
        Relation.objects.create(person=pers, contact=c2, proche_sig=True)
        Relation.objects.create(
            person=pers, contact=Contact.objects.create(nom='Doe', prenom='Sally')
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('person-list', args=['fhmn_int']) + '?columns=signifiants')
        self.assertContains(
            response,
            f'<td><a href="/contacts/{c1.pk}/">Obstgen Mimi</a> 032 333 33 33, 078 888 88 88<br>'
            f'<a href="/contacts/{c2.pk}/">Würren Jon</a> </td>'
        )

    def test_person_list_edition(self):
        from .views import PERSON_LIST_COLUMN_SETS
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_ext')
        BesoinsAlimentaires.objects.create(person=pers)
        self.client.force_login(self.user)
        for list_key in PERSON_LIST_COLUMN_SETS.keys():
            if list_key == 'signifiants':
                continue  # Not editable for this list
            response = self.client.get(reverse('person-list-edit', args=[pers.pk, list_key]))
            self.assertContains(response, '<td>Smith John</td>', html=True)
        # Test POSTing for specific sets
        response = self.client.post(reverse('person-list-edit', args=[pers.pk, 'ass']), data={
            'no_avs': '756.67.45.23',
            'pc': 'on',
        })
        self.assertContains(response, '<td>756.67.45.23</td>')
        response = self.client.post(reverse('person-list-edit', args=[pers.pk, 'regimes']), data={
            'regimes': 'Strict',
            'portions': '1/2',
        })
        self.assertContains(response, '<td>1/2</td>', html=True)

    def test_person_list_attente_edition(self):
        pers = Person.objects.create(nom='Smith', prenom='John', entree_date=None)
        self.client.force_login(self.user)
        response = self.client.post(reverse('person-list-edit', args=[pers.pk, 'attente']), data={
            'date_naissance': '12.02.1972',
            'date_demande': '01.12.2020',
            'origine': 'Fr, Oron la ville',
            'medecin_traitant': '',
            'repondant_off': '',
        })
        self.assertContains(response, 'Oron la ville')
        pers.refresh_from_db()
        self.assertEqual(pers.date_naissance, date(1972, 2, 12))

    def test_person_details(self):
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_ext')
        self.client.force_login(self.user)
        response = self.client.get(reverse('person-detail', args=[pers.pk]))
        self.assertContains(
            response,
            '<tr><th>Nom</th><td class="nom"><div class="field-content">Smith</div></td></tr>',
            html=True
        )
        self.assertContains(
            response,
            '<a class="no-hover" href="%s"><img id="logo" src="/static/img/fh-logo.png"></a>' % (
                reverse('person-list', args=[pers.situation]),
            ),
            html=True
        )
        # Test warning during temporary absence
        EntreeSortie.objects.create(
            person=pers, entree_sortie=EntreeSortie.SORTIE_TEMPORAIRE, date=timezone.now(), motif='Hospitalisation'
        )
        response = self.client.get(reverse('person-detail', args=[pers.pk]))
        self.assertContains(response, 'Ce résident est actuellement absent (Hospitalisation)')

    def test_access_by_location(self):
        user = User.objects.create(username='karl', first_name='Karl', last_name='Martin')
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_ext')
        self.client.force_login(user)
        response = self.client.get(reverse('home'))
        self.assertNotContains(response, 'Internes FHMN')
        response = self.client.get(reverse('person-detail', args=[pers.pk]))
        self.assertEqual(response.status_code, 403)
        # Now try the same by adding the appropriate permission
        user.user_permissions.add(Permission.objects.get(codename='access_fhmn'))
        response = self.client.get(reverse('home'))
        self.assertContains(response, 'Internes FHMN')
        response = self.client.get(reverse('person-detail', args=[pers.pk]))
        self.assertEqual(response.status_code, 200)

    def test_person_new(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('person-new'))
        self.assertContains(response, '<input type="text" name="nom" maxlength="40" id="id_nom" required>', html=True)
        self.assertContains(response, '<input type="text" name="date_naissance" size="10" id="id_date_naissance" class="vDateField">', html=True)
        response = self.client.post(reverse('person-new'), data={
            'nom': 'Not',
            'prenom': 'Hugues',
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        pers = Person.objects.get(nom='Not')
        self.assertRedirects(response, reverse('person-detail-attente', args=[pers.pk]))
        self.assertIsNotNone(pers.besoinsalimentaires)
        self.assertIsNotNone(pers.impotence)
        self.assertIsNotNone(pers.recueilbesoins)
        self.assertIsNone(pers.date_admission)
        modif = pers.modifications.latest('date')
        self.assertEqual(modif.precision, 'Ajout d’un nouveau résident')

    def test_edit_details(self):
        pers = Person.objects.create(nom='Smith', prenom='John')
        old_date = pers.updated
        self.client.force_login(self.user)
        url = reverse('person-edit', args=[pers.pk])
        response = self.client.get(url)
        self.assertContains(
            response,
            '<tr><th>Nom</th><td class="nom"><input type="text" name="nom" value="Smith" required id="id_nom" maxlength="40"></td></tr>',
            html=True
        )
        with open(str(Path(__file__).parent / 'test_photo.jpg'), 'rb') as fh:
            response = self.client.post(url, data={
                'nom': 'Smith',
                'prenom': 'John',
                'no_chambre': '334',
                'photo': File(fh),
            })
        self.assertRedirects(response, reverse('person-detail-only', args=[pers.pk]))
        pers.refresh_from_db()
        self.assertEqual(pers.no_chambre, '334')
        # Check thumbnail was created
        self.assertTrue(Path(pers.photo.path.replace('.jpg', '.thumb.jpg')).exists())
        self.assertNotEqual(old_date, pers.updated)
        modif = pers.modifications.latest('date')
        self.assertEqual(modif.precision, 'Modification du résident Smith John (N° chambre, Photo)')

    def test_person_date_depart(self):
        pers = Person.objects.create(
            nom='Dupond', prenom='Archibald', situation='fhmn_int', actif=False,
            entree_date=timezone.now()-timedelta(days=180)
        )
        EntreeSortie.objects.create(
            person=pers, entree_sortie='d', date=timezone.now()-timedelta(days=120), situation='fhmn_int'
        )
        EntreeSortie.objects.create(
            person=pers, entree_sortie='a', date=timezone.now()-timedelta(days=80), situation='fhne_int'
        )
        forty_days_ago = timezone.now()-timedelta(days=40)
        EntreeSortie.objects.create(
            person=pers, entree_sortie='d', date=forty_days_ago, situation='fhne_int'
        )
        self.assertEqual(pers.date_depart, forty_days_ago)


    def test_person_archived_and_back(self):
        pers = Person.objects.create(
            nom='Smith', prenom='John', situation='fhmn_ext',
            entree_date=timezone.now()-timedelta(days=180)
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('journal-person-es-new', args=[pers.pk]), data={
            'date_0': date.today() - timedelta(days=10),
            'date_1': '14:30',
            'entree_sortie': 'd',
            'motif': 'Retour à domicile',
        })
        self.assertRedirects(response, reverse('journal-person-es', args=[pers.pk]), fetch_redirect_response=False)
        pers.refresh_from_db()
        self.assertFalse(pers.actif)
        self.assertEqual(pers.situation, '')
        es = pers.entreesortie_set.last()
        self.assertEqual(es.situation, 'fhmn_ext')
        # Now back
        response = self.client.post(reverse('journal-person-es-new', args=[pers.pk]), data={
            'date_0': date.today(),
            'date_1': '00:00',
            'entree_sortie': 'a',
            'motif': 'Retour à FH',
            'situation': 'fhmn_int',  # Mandatory for admissions
        })
        self.assertRedirects(response, reverse('journal-person-es', args=[pers.pk]), fetch_redirect_response=False)
        pers.refresh_from_db()
        self.assertIsNone(pers.date_depart)
        self.assertEqual(pers.cause_depart, '')
        self.assertTrue(pers.actif)
        self.assertEqual(pers.situation, 'fhmn_int')

    def test_entreesortie_form(self):
        form = EntreeSortieForm()
        self.assertEqual(
            form.fields['entree_sortie'].choices,
            [('a', 'Arrivée dans l’institution'), ('at', 'Arrivée séjour temporaire (UAT…)')]
        )
        self.assertIn('situation', form.fields)

        form = EntreeSortieForm(last_es=EntreeSortie(entree_sortie='a'))
        self.assertEqual(
            form.fields['entree_sortie'].choices,
            [('s', 'Sortie temporaire'), ('d', 'Départ de l’institution')]
        )
        self.assertNotIn('situation', form.fields)

    def test_entreesortie_delete(self):
        pers = Person.objects.create(
            nom='Smith', prenom='John', situation='fhmn_ext',
            entree_date=timezone.now()-timedelta(days=180)
        )
        es1 = pers.entreesortie_set.first()
        es2 = EntreeSortie.objects.create(
            person=pers, entree_sortie=EntreeSortie.DEPART, date=timezone.now()-timedelta(days=1)
        )
        # Only the last item can be deleted
        self.assertFalse(es1.can_delete(self.user))
        self.assertTrue(es2.can_delete(self.user))

    def test_entree_temporaire(self):
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int', entree_date=None)
        EntreeSortie.objects.create(
            person=pers, entree_sortie=EntreeSortie.ARRIVEE_TEMPORAIRE, date=timezone.now()-timedelta(days=180), situation='fhmn_int'
        )
        self.assertIsNone(pers.date_admission)
        # La personne est toujours en liste d'attente
        self.assertTrue(Person.en_attente().filter(pk=pers.pk).exists())
        # …mais apparaît aussi dans la liste des résidents internes
        self.client.force_login(self.user)
        response = self.client.get(reverse('person-list', args=['fhmn_int']))
        self.assertTrue(response.context['object_list'].filter(pk=pers.pk).exists())

    def test_autocomplete_query(self):
        Contact.objects.create(nom='Obstgen', prenom='Mimi')
        c2 = Contact.objects.create(nom='Würren', prenom='Jon')
        self.client.force_login(self.user)
        response = self.client.get('/contacts/query/?term=urr')
        self.assertEqual(response.json(), {'results': [{'id': c2.pk, 'text': 'Würren Jon'}]})

    def test_add_edit_delete_relation(self):
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_ext')
        contact = Contact.objects.create(nom='Obstgen', prenom='Mimi')
        role = Role.objects.create(nom='Soeur')
        self.client.force_login(self.user)
        response = self.client.post(reverse('person-relation-add', args=[pers.pk]), data={
            'person': pers.pk,
            'contact': contact.pk,
            'role': role.pk,
            'note': 'À vérifier',
        })
        self.assertEqual(response.content, b'OK')
        modif = pers.modifications.latest('date')
        self.assertEqual(modif.precision, 'Ajout d’une relation vers Obstgen Mimi')
        rel = pers.relations.first()
        self.assertEqual(rel.note, 'À vérifier')
        # Edit the relation
        response = self.client.post(reverse('person-relation-edit', args=[pers.pk, rel.pk]), data={
            'person': pers.pk,
            'role': role.pk,
            'note': 'Autre note',
            'proche_sig': 'on',
        })
        self.assertEqual(response.content, b'OK')
        rel.refresh_from_db()
        self.assertEqual(rel.note, 'Autre note')
        self.assertTrue(rel.proche_sig)
        # Delete the relation
        response = self.client.post(reverse('person-relation-delete', args=[pers.pk, rel.pk]))
        self.assertEqual(response.json(), {'results': 'OK'})
        modif = pers.modifications.latest('date')
        self.assertEqual(modif.precision, 'Suppression de la relation vers Obstgen Mimi')

    def test_add_delete_document(self):
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_ext')
        self.client.force_login(self.user)
        with open(str(Path(__file__).parent / 'test.pdf'), 'rb') as fh:
            response = self.client.post(reverse('documents-new', args=[pers.pk]), data={
                'person': pers.pk,
                'date_doc': '2017-03-24',
                'type_doc': 'directives',
                'doc': File(fh),
                'title': 'Doc title',
            })
        self.assertRedirects(response, reverse('documents-list', args=[pers.pk]), fetch_redirect_response=False)
        modif = pers.modifications.latest('date')
        doc = pers.document_set.first()
        self.assertEqual(modif.precision, 'Ajout d’un document «Directives anticipées»')
        self.assertEqual(modif.target_url, doc.doc.url)
        self.assertEqual(doc.createur, self.user)
        self.assertEqual(doc.titre, 'Doc title')
        # Ensure the document is not accessible anonymously
        self.client.logout()
        response = self.client.get(doc.doc.url)
        self.assertRedirects(response, reverse('login') + '?next=%s' % doc.doc.url)
        # Neither from an administrative user
        simple_user = User.objects.create(username='joe', first_name='Joe', last_name='Martin')
        self.client.force_login(simple_user)
        response = self.client.get(
            reverse('documents-list', args=[pers.pk]), HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(len(response.context['object_list']), 0)
        # Delete the document
        self.client.force_login(self.user)
        response = self.client.post(reverse('documents-delete', args=[pers.pk, doc.pk]))
        self.assertEqual(response.json(), {'results': 'OK'})
        modif = pers.modifications.latest('date')
        self.assertEqual(modif.precision, 'Suppression d’un document «Directives anticipées»')

    def test_edit_admission(self):
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_ext')
        old_date = pers.updated
        self.client.force_login(self.user)
        edit_url = reverse('person-admission-edit', args=[pers.pk])
        response = self.client.get(edit_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, '<input type="checkbox" name="pratiquant" id="id_pratiquant">')
        response = self.client.post(edit_url, data={'profession': 'Menuisier'}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertRedirects(response, reverse('person-admission', args=[pers.pk]), fetch_redirect_response=False)
        pers.refresh_from_db()
        self.assertNotEqual(old_date, pers.updated)
        modif = pers.modifications.latest('date')
        self.assertEqual(modif.precision, 'Modification des données d’admission (Profession)')
        # Plaisir fields can only be edited with a special permission
        user = User.objects.create(username='joe', first_name='Joe', last_name='Martin')
        user.user_permissions.add(
            Permission.objects.get(codename='change_person')
        )
        self.client.force_login(user)
        response = self.client.get(edit_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertNotIn('no_plaisir', response.context['form'].fields)
        user.user_permissions.add(
            Permission.objects.get(codename='plaisir_edition')
        )
        response = self.client.get(edit_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertIn('no_plaisir', response.context['form'].fields)

    def test_histoiredevie(self):
        pers = Person.objects.create(nom='Smith', prenom='John')
        self.client.force_login(self.user)
        detail_url = reverse('histoirevie-detail', args=[pers.pk])
        response = self.client.get(detail_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, '<th><label for="id_histoire">Histoire de vie</label></th>')
        # Edition
        edit_url = reverse('histoirevie-edit', args=[pers.pk])
        response = self.client.get(edit_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, '<label for="id_histoire">Histoire de vie :</label>')
        response = self.client.post(
            edit_url, data={'person': str(pers.pk), 'histoire': 'Son histoire…'},
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertRedirects(response, detail_url, fetch_redirect_response=False)

    def test_repartition_view(self):
        pers = Person.objects.create(nom='Smith', prenom='John')
        self.user.user_permissions.add(
            Permission.objects.get(codename='change_repartitiontaches')
        )
        self.client.force_login(self.user)
        detail_url = reverse('person-repartition', args=[pers.pk])
        response = self.client.get(detail_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, '<th>Gestion de la boîte aux lettres</th>')
        # Edition
        edit_url = reverse('person-repartition-edit', args=[pers.pk])
        response = self.client.get(edit_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(
            response,
            '<li><label for="id_boite_aux_lettres_0"><input type="checkbox" name="boite_aux_lettres" value="R" class="choicearray" id="id_boite_aux_lettres_0">R</label></li>',
            html=True
        )
        response = self.client.post(
            edit_url, data={'person': str(pers.pk), 'boite_aux_lettres': ['S', 'E']},
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertRedirects(response, detail_url, fetch_redirect_response=False)
        self.assertEqual(pers.repartitiontaches.boite_aux_lettres, ['S', 'E'])
        self.assertEqual(pers.repartitiontaches.date.date(), date.today())

    def test_rangements_view(self):
        pers = Person.objects.create(nom='Smith', prenom='John')
        self.user.user_permissions.add(
            Permission.objects.get(codename='change_repartitiontaches')
        )
        self.client.force_login(self.user)
        detail_url = reverse('person-rangement', args=[pers.pk])
        response = self.client.get(detail_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, '<th>Décisions de l’Autorité de protection de l’adulte</th>')
        # Edition
        edit_url = reverse('person-rangement-edit', args=[pers.pk])
        response = self.client.get(edit_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(
            response,
            '<li><label for="id_decisions_apa_0"><input type="checkbox" name="decisions_apa" value="R" class="choicearray" id="id_decisions_apa_0">R</label></li>',
            html=True
        )
        response = self.client.post(
            edit_url, data={'person': str(pers.pk), 'decisions_apa': ['C']},
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertRedirects(response, detail_url, fetch_redirect_response=False)
        self.assertEqual(pers.repartitiontaches.decisions_apa, ['C'])
        self.assertEqual(pers.repartitiontaches.date.date(), date.today())

    def test_search_global(self):
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        SoutienSocial.objects.create(person=pers, intervenants='Simon Jules Andrée', date=timezone.now())
        search_url = reverse('search', args=['fhmn'])
        self.client.force_login(self.user)
        response = self.client.get(search_url)
        self.assertNotIn('results', response.context)
        self.assertContains(response, '<option value="person.SoutienSocial">Réseau</option>')
        response = self.client.get(search_url, data={
            'instance': 'person.SoutienSocial',
            'date_du': '31.12.2018', 'date_au': '31.12.2028',
            'texte': 'Jules',
        })
        self.assertEqual(len(response.context['results']), 1)

    def test_relations_pdf(self):
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        contact = Contact.objects.create(nom='Obstgen', prenom='Mimi')
        Relation.objects.create(person=pers, contact=contact)
        self.client.force_login(self.user)
        response = self.client.get(reverse('person-relations-print', args=[pers.pk]))
        self.assertEqual(response.status_code, 200)
        # Can happen when on waiting list
        pers.situation = ''
        pers.save()
        response = self.client.get(reverse('person-relations-print', args=[pers.pk]))
        self.assertEqual(response.status_code, 200)

    def test_dossier_transfert(self):
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        with open(str(Path(__file__).parent / 'test.png'), 'rb') as fh:
            Document.objects.create(
                person=pers, date=timezone.now(), createur=self.user, type_doc='directives',
                doc=File(fh, name='test.png'), title="Dir. ant.",
            )
        self.client.force_login(self.user)
        response = self.client.get(reverse('person-transfert', args=[pers.pk]))
        self.assertEqual(response.status_code, 200)

    def test_regimes_list(self):
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        BesoinsAlimentaires.objects.create(person=pers, regimes='le régime de monsieur')
        Person.objects.create(nom='Smith', prenom='Jill', situation='fhmn_int')
        self.client.force_login(self.user)
        response = self.client.get(reverse('regimes-pdf', args=['fhmn_int']))
        self.assertEqual(response.status_code, 200)


class ModificationTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(username='jill', first_name='Jill', last_name='Martin', is_superuser=True)

    def test_modifications(self):
        """Edition to person objects automatically add Modification instances."""
        pers1 = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        pers2 = Person.objects.create(nom='Smith', prenom='Jill', situation='fhne_int')
        self.client.force_login(self.user)
        resp = self.client.post(reverse('person-edit', args=[pers1.pk]), data={
            'nom': 'Smith', 'prenom': 'John', 'situation': 'fhmn_int', 'no_chambre': '334'
        })
        resp = self.client.post(reverse('person-edit', args=[pers2.pk]), data={
            'nom': 'Smith', 'prenom': 'Jill', 'situation': 'fhne_int', 'no_chambre': '335'
        })
        # Add a 3rd one from another app
        Modification.objects.create(
            person=pers2,
            date=timezone.now(),
            user=self.user,
            precision='From other app',
            rubrique='soins.observationsoin',
            content_object=None,
        )
        # Add a general manual info
        Modification.objects.create(
            date=timezone.now() + timedelta(days=3), user=self.user, fhmn=True,
            precision="Whatever", manual=True
        )
        response = self.client.get(
            reverse('general-journal', args=['fhmn']), HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(len(response.context['object_list']), 1)
        modif1 = response.context['object_list'][0]
        self.assertTrue(modif1.manual)
        response = self.client.get(
            reverse('general-journal', args=['fhne']), HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(len(response.context['object_list']), 0)
        # With filter
        response = self.client.get(
            reverse('general-journal', args=['fhne']) + '?domaine=soins&manual_only=on',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(len(response.context['object_list']), 0)
        response = self.client.get(
            reverse('general-journal', args=['fhne']) + '?domaine=soins',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(len(response.context['object_list']), 1)

        # Person journal with filter
        journal_url = reverse('journal-person', args=[pers2.pk])
        response = self.client.get(journal_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(len(response.context['object_list']), 2)
        response = self.client.get(
            journal_url + '?domaine=soins', HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(len(response.context['object_list']), 1)
        # Force a second page
        Modification.objects.bulk_create([
            Modification(date=timezone.now(), user=self.user, precision='Modif', fhne=True, manual=True)
            for i in range(GeneralJournalView.paginate_by + 2)
        ])
        response = self.client.get(
            reverse('general-journal', args=['fhne']) + '?page=2', HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(len(response.context['object_list']), 2)

    def test_object_history(self):
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        soutien = SoutienSocial.objects.create(person=pers, intervenants='Moi')
        modif1 = Modification.save_modification(soutien, self.user, created=True)
        modif1.date = modif1.date - timedelta(hours=2)
        modif1.save()
        now = timezone.localtime(timezone.now())
        Modification.save_modification(soutien, self.user, created=False)
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('object-history', args=[ContentType.objects.get_for_model(soutien).pk, soutien.pk])
        )
        self.assertContains(
            response,
            'Ajout d’une fiche «Réseau», le %s par Jill Martin' % (
                date_format(timezone.localtime(modif1.date), 'j F Y H:i')
            ),
            html=True
        )
        self.assertContains(
            response,
            'Modification d’une fiche «Réseau», le %s par Jill Martin' % (
                date_format(now, 'j F Y H:i')
            ),
            html=True
        )

    def test_delete_byadmin_cascade(self):
        """Deleting an object (through the admin) also delete modifications."""
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        soutien = SoutienSocial.objects.create(person=pers, intervenants='Moi')
        Modification.save_modification(soutien, self.user, created=True, msg='New obj')
        modif = soutien.object_history()[0]
        superuser = User.objects.create_superuser(username='ruth', first_name='Ruth', last_name='Martin')
        self.client.force_login(superuser)
        response = self.client.post(
            reverse('admin:person_soutiensocial_delete', args=[soutien.pk]), {'post': 'yes'},
        )
        self.assertRedirects(response, reverse('admin:person_soutiensocial_changelist'))
        self.assertFalse(SoutienSocial.objects.filter(pk=soutien.pk).exists())
        self.assertFalse(Modification.objects.filter(pk=modif.pk).exists())

    def test_no_modification_if_form_not_changed(self):
        """
        Edit views based on BaseEditView don't generate Modifications
        if the form did not change.
        """
        self.client.force_login(self.user)
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        HistoireDeVie.objects.create(person=pers, date=timezone.now(), createur=self.user)
        response = self.client.post(
            reverse('histoirevie-edit', args=[pers.pk]), data={'histoire': ''}
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Modification.objects.filter(person=pers).count(), 0)

    def test_manual_modification(self):
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        self.client.force_login(self.user)
        response = self.client.post(reverse('journal-person-new', args=[pers.pk]), data={
            'date_0': '2018-12-12',
            'date_1': '12:30',
            'precision': "Modification manuelle."
        })
        self.assertRedirects(
            response, reverse('journal-person', args=[pers.pk]), fetch_redirect_response=False
        )
        modif = pers.modifications.latest('date')
        self.assertEqual(modif.date, timezone.make_aware(datetime(2018, 12, 12, 12, 30)))
        self.assertEqual(modif.user, self.user)
        self.assertTrue(modif.manual)
        self.assertEqual(modif.precision, 'Modification manuelle.')
        # Edit form
        response = self.client.get(reverse('journal-person-edit', args=[pers.pk, modif.pk]))
        self.assertContains(
            response,
            '<input type="text" name="date_1" required value="12:30" placeholder="00:00" class="TimeField" id="id_date_1">',
            html=True
        )

    def test_manual_general_modification(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('modifs-add', args=['fhmn']), data={
            'date_0': '2018-12-12',
            'date_1': '12:30',
            'precision': "Modification manuelle générale."
        }, follow=True, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        modif = Modification.objects.get(precision='Modification manuelle générale.')
        self.assertIn(modif, response.context['object_list'])
        self.assertEqual(modif.date, timezone.make_aware(datetime(2018, 12, 12, 12, 30)))
        self.assertEqual(modif.user, self.user)
        self.assertIsNone(modif.person)
        self.assertTrue(modif.manual)
        self.assertTrue(modif.fhmn)

    def test_manual_general_modification_recipients(self):
        g_fhmn = Group.objects.create(name='FHMN')
        g_fhmn.permissions.add(Permission.objects.get(codename='access_fhmn'))
        g_fhne = Group.objects.create(name='FHNE')
        g_fhne.permissions.add(Permission.objects.get(codename='access_fhne'))
        g_soins = Group.objects.create(name='Soignants')
        g_inf = Group.objects.create(name='Infirmiers-chefs')
        g_inf.permissions.add(Permission.objects.get(codename='info_to_group'))
        u_fhne_inact = User.objects.create(username='user1', first_name='Jill1', last_name='Martin', archived=True)
        u_fhne_inact.groups.add(g_fhne)
        u_fhne_soignant = User.objects.create(username='user2', first_name='Jill2', last_name='Martin')
        u_fhne_soignant.groups.add(g_fhne, g_soins)
        u_fhmn_secret = User.objects.create(username='user3', first_name='Jill3', last_name='Martin')
        u_fhmn_secret.groups.add(g_fhmn)
        u_fhmn_infchef = User.objects.create(username='pinterg', first_name='Jill4', last_name='Martin')
        u_fhmn_infchef.groups.add(g_fhmn, g_inf)

        self.client.force_login(u_fhmn_infchef)
        msg = {}
        for idx, destin in enumerate(['tous', 'fhmn', 'fhne', 'fhmn-soins', 'fhne-soins']):
            response = self.client.post(
                reverse('modifs-add', args=['fhmn' if (destin == 'tous' or destin.startswith('fhmn')) else 'fhne']),
                data={
                    'date_0': '2018-12-12',
                    'date_1': '12:30',
                    'groupes_cible': [destin],
                    'precision': "Modification manuelle générale %s." % idx,
                }, HTTP_X_REQUESTED_WITH='XMLHttpRequest'
            )
            if response.status_code == 200:
                self.fail(response.context['form'].errors)
            msg[destin] = Modification.objects.get(precision="Modification manuelle générale %s." % idx)
        self.assertTrue(msg['tous'].fhmn)
        self.assertTrue(msg['tous'].fhne)

        for user, dests in (
            (u_fhne_inact, []),
            (u_fhne_soignant, ['tous', 'fhne', 'fhne-soins']),
            (u_fhmn_secret, ['tous', 'fhmn']),
            (u_fhmn_infchef, ['tous', 'fhmn', 'fhmn-soins']),
        ):
            user.refresh_from_db()
            with self.subTest(user=user):
                self.assertEqual(
                    user.infos_a_lire,
                    ','.join(str(msg[dest].pk) for dest in dests)
                )

    def test_manual_general_modification_confirm_read(self):
        self.user.infos_a_lire = '85,34,256'
        self.user.save()
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('modifs-confirm-read', args=[34]), data={}, HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(response.json(), {'result': 'OK'})
        self.user.refresh_from_db()
        self.assertEqual(self.user.infos_a_lire, '85,256')
        # Ignore when no id matches
        self.user.infos_a_lire = ''
        self.user.save()
        response = self.client.post(
            reverse('modifs-confirm-read', args=[34]), data={}, HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(response.json(), {'result': 'OK'})


class ContactTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(username='jill', first_name='Jill', last_name='Martin')

    def test_create_contact(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('contact-new'), data={
            'nom': 'JJJ',
            'prenom': 'kkk',
            'localite': 'Neuchâtel',
        })
        self.assertRedirects(response, '/contacts/?f=j')
        contact = Contact.objects.get(nom='JJJ')
        self.assertTrue(contact.actif)

    def test_contact_list(self):
        Contact.objects.bulk_create([
            Contact(nom='Acker', prenom='Nathalie'),
            Contact(nom='Baumann', prenom='Eric', fonction='Dentiste'),
            Contact(nom='Curry', prenom='Blaise'),
            Contact(nom='Droz', prenom='Anne', fonction='Dentiste'),
        ])
        self.client.force_login(self.user)
        response = self.client.get(reverse('contact-list') + '?f=b')
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertEqual(response.context['object_list'][0].nom, 'Baumann')
        response = self.client.get(reverse('contact-list') + '?text=dent')
        self.assertEqual(len(response.context['object_list']), 2)
        self.assertEqual(response.context['object_list'][0].nom, 'Baumann')
        self.assertEqual(response.context['object_list'][1].nom, 'Droz')

    def test_create_contact_for_user(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('contact-new'), data={
            'nom': 'KKK',
            'prenom': 'lll',
            'localite': 'Neuchâtel',
            'foruser': self.user.pk,
        })
        self.user.refresh_from_db()
        self.assertEqual(self.user.contact.nom, 'KKK')

    def test_contact_detail(self):
        contact = Contact.objects.create(nom='Obstgen', prenom='Mimi')
        self.client.force_login(self.user)
        response = self.client.get(reverse('contact-detail', args=[contact.pk]))
        self.assertContains(response, 'Obstgen')

    def test_archive_contact(self):
        pers = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_ext')
        contact = Contact.objects.create(nom='Obstgen', prenom='Mimi')
        Relation.objects.create(person=pers, contact=contact)
        self.assertTrue(contact.actif)

        self.client.force_login(self.user)
        arch_url = reverse('contact-archive', args=[contact.pk])
        response = self.client.post(arch_url, data={}, follow=True)
        self.assertContains(response, "Impossible d’archiver")
        pers.actif = False
        pers.save()
        response = self.client.post(arch_url, data={}, follow=True)
        contact.refresh_from_db()
        self.assertFalse(contact.actif)


class ActionISTests(ModelBaseTextMixin, TestCase):
    ModelClass = ActionIS
    model_id = 'ais'
    custom_data = {'etat': 'o', 'problemes': "Des problèmes…"}

    def test_add_action_evaluation(self):
        act = ActionIS.objects.create(
            person=self.person, problemes="Des problèmes…", objectifs="Des objectifs…",
            createur=self.user,
        )
        self.client.force_login(self.user)
        get_url = reverse('ais-detail', args=[self.person.pk, act.pk])
        response = self.client.get(get_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        addeval_url = reverse('ais-addeval', args=[self.person.pk, act.pk])
        addaction_url = reverse('ais-addaction', args=[self.person.pk, act.pk])
        self.assertContains(
            response,
            '<img class="icon-standalone" src="/static/img/add.svg" '
            'data-url="%s" onclick="addDynamicRow(this, \'evaluations\')" '
            'title="Ajouter une évaluation">' % addeval_url,
            html=True
        )
        self.assertContains(
            response,
            '<img class="icon-standalone" src="/static/img/add.svg" '
            'data-url="%s" onclick="addDynamicRow(this, \'actions\')" '
            'title="Ajouter une action">' % addaction_url,
            html=True
        )
        response = self.client.post(addeval_url, data={'texte': 'Une évaluation'})
        self.assertEqual(act.evaluations.first().texte, 'Une évaluation')
        response = self.client.post(addaction_url, data={'texte': 'Une action'})
        self.assertEqual(act.actions.first().texte, 'Une action')

    def test_close_action(self):
        other_user = User.objects.create(username='capone', first_name='Al', last_name='Capone')
        act = ActionIS.objects.create(
            createur=self.user, person=self.person,
            problemes="Des problèmes…", objectifs="Des objectifs…"
        )
        self.assertEqual(act.etat, 'o')  # Open by default
        self.user.user_permissions.add(Permission.objects.get(codename='access_fhmn'))
        self.client.force_login(other_user)
        # Only the creator can close the action
        get_url = reverse('ais-detail', args=[self.person.pk, act.pk])
        response = self.client.get(get_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertNotContains(response, 'Fermer l’action</button>')

        self.client.force_login(self.user)
        response = self.client.get(get_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, 'Fermer l’action</button>')
        response = self.client.post(reverse('ais-closeaction', args=[self.person.pk, act.pk]))
        self.assertRedirects(response, act.get_absolute_url())
        act.refresh_from_db()
        self.assertEqual(act.etat, 'f')
        self.assertEqual(act.date_fermee, date.today())


class SoutienSocialTests(ModelBaseTextMixin, TestCase):
    ModelClass = SoutienSocial
    model_id = 'soutiensocial'
    custom_data = {'motifs': "Des motifs…"}

    def test_edit_soutiensocial(self):
        some_user = User.objects.create(
            username='someone', first_name='One', last_name='Some',
            email='someone@example.org',
        )
        sso = SoutienSocial.objects.create(person=self.person, date=timezone.now(), createur=some_user)
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('soutiensocial-edit', args=[self.person.pk, sso.pk]),
            data={'createur': str(self.user.pk), 'person': str(self.person.pk), 'motifs': "Autres motifs"},
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertRedirects(response, reverse('soutiensocial-detail', args=[self.person.pk, sso.pk]))
        sso.refresh_from_db()
        self.assertEqual(sso.createur, some_user)
        self.assertEqual(sso.motifs, "Autres motifs")


class UserTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(
            username='jill', first_name='Jill', last_name='Martin',
            email='user@example.org', is_superuser=True
        )

    def test_create_user(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('user-new'), data={
            'username': 'jjj',
            'last_name': 'JJJ',
            'first_name': 'kkk',
            'email': 'jjj@example.org',
            'groups': [],
            'is_active': True,
        })
        self.assertRedirects(response, reverse('user-list'))
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, '[DRI] Nouveau compte')
        self.assertEqual(mail.outbox[0].to, ['jjj@example.org'])
        mail.outbox = []
        # Email is optional (email sent to logged-in user)
        response = self.client.post(reverse('user-new'), data={
            'username': 'uuu',
            'last_name': 'aaa',
            'first_name': 'kkk',
            'email': '',
            'groups': [],
            'is_active': True,
        })
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.user.email])
        mail.outbox = []
        # If is_active is False, no email sent
        response = self.client.post(reverse('user-new'), data={
            'username': 'inact',
            'last_name': 'qqq',
            'first_name': 'jjj',
            'email': 'test@example.org',
            'groups': [],
            'is_active': False,
        })
        self.assertEqual(len(mail.outbox), 0)
        inactive_user = User.objects.get(username='inact')
        from django.contrib.auth.hashers import is_password_usable
        self.assertFalse(is_password_usable(inactive_user.password))

    def test_reinit_password(self):
        target_user = User.objects.create(
            username='evam', first_name='Eva', last_name='Martin',
            email='eva@example.org',
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('user-reinit-password', args=['evam']), data={})
        self.assertRedirects(response, reverse('user-list'))
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, '[DRI] Mot de passe réinitialisé')
        self.assertEqual(mail.outbox[0].to, ['eva@example.org'])

    def test_export_users(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('user-export'))
        self.assertEqual(response['Content-Type'], XLSX_CONTENT_TYPE)


class AttachmentTests(TempMediaTestCase):
    def setUp(self):
        self.user = User.objects.create(username='jill', first_name='Jill', last_name='Martin')
        self.person = Person.objects.create(
            nom='Smith', prenom='John', situation='fhmn_int',
        )

    def _create_obj_with_attachment(self):
        from soins.models import ObservationSoins

        date_now = timezone.now()
        obs = ObservationSoins.objects.create(
            person=self.person, date=date_now, observation='Des larmes', createur=self.user
        )
        test_path = Path(__file__).parent / 'test.pdf'
        with open(test_path, mode='rb') as fh:
            att = Attachment.objects.create(
                fichier=File(fh, name='attach.pdf'), titre='Titre', createur=self.user, date=date_now,
                content_object=obs
            )
        return att

    def test_delete_attachment(self):
        att = self._create_obj_with_attachment()
        att_file = Path(att.fichier.path)
        self.client.force_login(self.user)
        response = self.client.post(reverse('delete-attachment', args=[att.pk]), {})
        self.assertEqual(response.json()['result'], 'OK')
        self.assertFalse(att_file.exists())

    def test_delete_attachment_object(self):
        """Attachment (and file) is removed when removing from admin."""
        att = self._create_obj_with_attachment()
        obs = att.content_object
        att_file = Path(att.fichier.path)
        superuser = User.objects.create_superuser(username='ruth', first_name='Ruth', last_name='Martin')
        self.client.force_login(superuser)
        response = self.client.post(
            reverse('admin:soins_observationsoins_delete', args=[att.object_id]), {'post': 'yes'},
        )
        self.assertRedirects(response, reverse('admin:soins_observationsoins_changelist'))
        self.assertFalse(obs.__class__.objects.filter(pk=obs.pk).exists())
        self.assertFalse(Attachment.objects.filter(pk=att.pk).exists())
        self.assertFalse(att_file.exists())
