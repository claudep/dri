import os
from collections import OrderedDict
from copy import deepcopy
from datetime import date, datetime, timedelta
from io import BytesIO

from django import forms
from django.apps import apps
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.views import PasswordChangeView
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import FieldDoesNotExist, ObjectDoesNotExist, PermissionDenied
from django.core.mail import send_mail
from django.db import transaction
from django.db.models import Count, OuterRef, Prefetch, Q, Subquery
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import NoReverseMatch, resolve, reverse, reverse_lazy
from django.utils import timezone
from django.utils.html import format_html_join
from django.utils.safestring import mark_safe
from django.utils.text import slugify
from django.views.generic import (
    CreateView, DeleteView, DetailView, FormView, ListView, UpdateView,
    TemplateView, View
)

from agenda.models import Event
from common.fields import PickDateWidget
from common.utils import is_ajax
from common.views import BasePDFView, BaseXLSView, CreateUpdateView, FormErrorsHeaderMixin
from besoins.models import BesoinsAlimentaires
from soins.models import MedicationItem, VisiteMedicale
from .forms import (
    AdmissionForm, AttachmentModelForm, ContactForm, ContactMergeForm,
    EntreeSortieForm, FilterForm, InstanceSearchForm, ModificationForm,
    ModifFilterForm, PersonDetailForm, RangementForm, RelationForm,
    RepartitionForm, UserForm
)
from .models import (
    ActionIS, Attachment, Contact, Document, EntreeSortie,
    HistoireDeVie, Modification, Person, Relation, RepartitionTaches,
    SoutienSocial, User
)
from .pdf_output import (
    CarteDeViePDF, DegresPlaisirList, RegimesList, RelationsPDF, TransfertPDF
)


class TabStructure:
    ATELIERS_NE = ['ARIHANE', 'Atelier Poste']
    ATELIERS_CDF = ['Créa1', 'Créa2', 'Cuisine', 'Info FHMN']

    def __init__(self):
        self._TABS_BY_ID = {}
        self.walk_tabs(self._TABS, 1)

    def all(self):
        return self._TABS

    @classmethod
    def filter(self, user, tabs, situation):
        """Only ateliers is filtered currently by resident site."""
        tabs = deepcopy(tabs)
        if 'Données' in tabs:
            # At first level, user needs at least one perm by app to see the tab.
            for label, subtabs in tabs.items():
                if subtabs['id'] in ('donnees', 'journal'):
                    continue
                elif subtabs['id'] == 'ais':
                    if not user.has_perm('person.view_actionis'):
                        tabs['Actions inter-secteurs'] = {}
                elif 'perm' in subtabs and user.has_perm(subtabs['perm']):
                    continue
                elif not user.has_module_perms(subtabs['id']):
                    tabs[label] = {}
        elif 'tabs' in tabs and 'Actions ateliers' in tabs['tabs']:
            if situation.startswith('fhmn'):
                # Enlever ateliers ne
                for at in self.ATELIERS_NE:
                    del tabs['tabs'][at]
            else:
                # Enlever ateliers cdf
                for at in self.ATELIERS_CDF:
                    del tabs['tabs'][at]
        elif 'tabs' in tabs and 'Sécurité' in tabs['tabs']:
            if not user.has_perm('besoins.view_recueilbesoins'):
                # Deactivate all but besoins 'nourrir'
                for label in tabs['tabs']:
                    if label != 'Se nourrir':
                        tabs['tabs'][label] = {}
        else:
            # Check tab permissions
            for tab_name, tab_data in tabs['tabs'].items():
                if 'perm' in tab_data and not user.has_perm(tab_data['perm']):
                    tabs['tabs'][tab_name] = {}
        return tabs

    def get_by_id(self, tab_id):
        return self._TABS_BY_ID.get(tab_id)

    def get_tab_chain(self, tab_id):
        """Return a comma-separated list of hierarchical tabs ('soins,soins-divers')."""
        if not tab_id in self._TABS_BY_ID:
            return None
        tabs = tab_id
        # Going up to root
        parent_id = self._TABS_BY_ID[tab_id]['parent']
        while parent_id is not None:
            tabs = '%s,%s' % (parent_id, tabs)
            parent_id = self._TABS_BY_ID[parent_id]['parent']
        if 'tabs' in self._TABS_BY_ID[tab_id]:
            # Going down to leaf
            child_id = list(self._TABS_BY_ID[tab_id]['tabs'].values())[0]['id']
            while child_id is not None:
                tabs = '%s,%s' % (tabs, child_id)
                try:
                    child_id = list(self._TABS_BY_ID[child_id]['tabs'].values())[0]['id']
                except KeyError:
                    break
        return tabs

    #@classmethod
    def walk_tabs(self, node, level, parent_id=None):
        """Check IDs are unique, fill the _TABS_BY_ID dict."""
        for label, tab in node.items():
            if tab['id'] in self._TABS_BY_ID:
                raise Exception("Duplicate '%s' id in TABS" % tab['id'])
            self._TABS_BY_ID[tab['id']] = tab
            self._TABS_BY_ID[tab['id']]['level'] = level
            self._TABS_BY_ID[tab['id']]['parent'] = parent_id
            if 'tabs' in tab:
                self.walk_tabs(tab['tabs'], level + 1, tab['id'])


class PersonTabs(TabStructure):
    # First-level tab ids must match application names if possible (for permission checking)
    _TABS = OrderedDict([
        ('Données', {'id': 'donnees', 'tabs': OrderedDict([
            ('Admission', {'id': 'admission', 'view_name': 'person-admission'}),
            ('Relations', {'id': 'relations', 'view_name': 'person-relations'}),
            ('Histoire de vie', {'id': 'histoirevie', 'view_name': 'histoirevie-detail', 'perm': 'person.view_histoiredevie'}),
            ('Recueil données', {'id': 'recueil', 'perm': 'besoins.view_besoinsalimentaires', 'tabs': OrderedDict([
                ('Carte de vie', {'id': 'cartevie', 'view_name': 'cartedevie-recap'}),
                ('Sécurité', {'id': 'securite', 'view_name': 'besoins-detail', 'view_args': ['securite']}),
                ('Communiquer', {'id': 'communiquer', 'view_name': 'besoins-detail', 'view_args': ['communiquer']}),
                ('Respirer', {'id': 'respirer', 'view_name': 'besoins-detail', 'view_args': ['respirer']}),
                ('Hygiène', {'id': 'hygiene', 'view_name': 'besoins-detail', 'view_args': ['hygiene']}),
                ('Se vêtir', {'id': 'vetir', 'view_name': 'besoins-detail', 'view_args': ['vetir']}),
                ('Se nourrir', {'id': 'nourrir', 'view_name': 'nourrir-detail', 'view_args': []}),
                ('Éliminer', {'id': 'eliminer', 'view_name': 'besoins-detail', 'view_args': ['eliminer']}),
                ('Se mouvoir', {'id': 'mouvoir', 'view_name': 'besoins-detail', 'view_args': ['mouvoir']}),
                ('Se reposer', {'id': 'reposer', 'view_name': 'besoins-detail', 'view_args': ['reposer']}),
                ('S’occuper', {'id': 'soccuper', 'view_name': 'besoins-detail', 'view_args': ['soccuper']}),
                ('Apprendre', {'id': 'apprendre', 'view_name': 'besoins-detail', 'view_args': ['apprendre']}),
                ('Croyances', {'id': 'croyances', 'view_name': 'besoins-detail', 'view_args': ['croyances']}),
                ('Vivre sa sexualité', {'id': 'sexualite', 'view_name': 'besoins-detail', 'view_args': ['sexualite']}),
            ])}),
            ('Réseau', {'id': 'reseau', 'view_name': 'soutiensocial-list', 'perm': 'person.view_soutiensocial'}),
            ('Répartition des tâches', {'id': 'repartition', 'view_name': 'person-repartition', 'perm': 'person.view_repartitiontaches'}),
            ('Rangement documents', {'id': 'rangements', 'view_name': 'person-rangement', 'perm': 'person.view_repartitiontaches'}),
            ('e-Documents', {'id': 'edocuments', 'view_name': 'documents-list', 'perm': 'person.view_document'}),
        ])}),
        ('Journal', {'id': 'journal', 'tabs': OrderedDict([
            ('General', {'id': 'journalgen', 'view_name': 'journal-person'}),
            ('Agenda', {'id': 'agenda', 'view_name': 'person-agenda', 'perm': 'agenda.view_event'}),
            ('Entrées/Sorties', {'id': 'journales', 'view_name': 'journal-person-es'}),
        ])}),
        ('Actions inter-secteurs', {'id': 'ais', 'view_name': 'ais-list'}),
        ('Bilans', {'id': 'bilanannuel', 'tabs': OrderedDict([
            ('Bilans', {'id': 'bilanann', 'view_name': 'bilanannuel-list'}),
            ('Suivi d’accompagnement', {'id': 'suiviacc', 'view_name': 'suiviacc-list'}),
        ])}),
        ('Soins', {'id': 'soins', 'tabs': OrderedDict([
            ('Observations', {'id': 'obssoins', 'view_name': 'obssoins-list', 'perm': 'soins.view_observationsoins'}),
            ('Actions de soins', {'id': 'actionsoins', 'view_name': 'actionsoins-list', 'perm': 'soins.view_actionsoins'}),
            ('Soins divers', {'id': 'soins_divers', 'perm': 'soins.view_observationsoins', 'tabs': OrderedDict([
                ('Signes vitaux', {'id': 'signesvitaux', 'view_name': 'signesvitaux-list'}),
                ('Status urinaire', {'id': 'statusurin', 'view_name': 'statusurin-list'}),
                ('Soins divers', {'id': 'soinsdivers', 'view_name': 'soinsdivers-list'}),
                ('Sondages vésicaux', {'id': 'sondageves', 'view_name': 'sondageves-list'}),
            ])}),
            ('Actes journaliers', {'id': 'actesjourn', 'view_name': 'actesjourn-detail', 'perm': 'soins.view_actesjournaliers'}),
            ('Bilan soins', {'id': 'bilansoins', 'view_name': 'bilansoins-list', 'perm': 'soins.view_bilansoins'}),
            ('Visites médicales', {'id': 'visitemed', 'view_name': 'visitemed-list', 'perm': 'soins.view_visitemedicale'}),
            ('Diagnostic médical', {'id': 'diagmedic', 'view_name': 'diagmedic-list', 'perm': 'soins.view_diagnosticmedical'}),
            ('Médication', {'id': 'medication', 'view_name': 'medication-list', 'perm': 'soins.view_medication'}),
        ])}),
        ('Ergo', {'id': 'ergo', 'tabs': OrderedDict([
            ('Observations', {'id': 'obsergo', 'view_name': 'obsergo-list'}),
            ('Actions ergo', {'id': 'actionergo', 'view_name': 'actionergo-list'}),
            ('Moyens auxiliaires', {'id': 'moyensaux', 'view_name': 'moyensaux-list'}),
            ('Bilan Ergo - UAJ', {'id': 'bilanergo', 'view_name': 'bilanergo-list'}),
        ])}),
        ('UAJ', {'id': 'uaj', 'tabs': OrderedDict([
            ('Observations', {'id': 'obsuaj', 'view_name': 'obsuaj-list'}),
            ('UAJ Visite', {'id': 'visiteuaj', 'view_name': 'visiteuaj-list'}),
            ('Bilan UAJ externe', {'id': 'bilanuaj', 'view_name': 'bilanuaj-list'}),
        ])}),
        ('Physio', {'id': 'physio', 'tabs': OrderedDict([
            ('Observations', {'id': 'obsphysio', 'view_name': 'obsphysio-list'}),
            ('Bilan physio', {'id': 'bilanphysio', 'view_name': 'bilanphysio-list'}),
        ])}),
        ('Neuropsy', {'id': 'neuropsy', 'tabs': OrderedDict([
            ('Observations', {'id': 'obsneuropsy', 'view_name': 'obsneuropsy-list'}),
            ('Bilan neuropsy annuel', {'id': 'bilanpsyaanuel', 'view_name': 'bilanpsyaanuel-list'}),
            ('Bilan neuropsy complet', {'id': 'bilanpsycomplet', 'view_name': 'bilanpsycomplet-list'}),
        ])}),
        ('Animation', {'id': 'animation', 'tabs': OrderedDict([
            ('Observations', {'id': 'obsanim', 'view_name': 'obsanim-list'}),
            ('Bilan animation', {'id': 'bilananim', 'view_name': 'bilananim-list'}),
            ('Animation FHMN', {'id': 'animfhmn', 'view_name': 'animfhmn-list'}),
        ])}),
        ('Musico', {'id': 'musico', 'tabs': OrderedDict([
            ('Observations', {'id': 'obsmusico', 'view_name': 'obsmusico-list'}),
            ('Bilan musico', {'id': 'bilanmusico', 'view_name': 'bilanmusico-list'}),
            ('Musico', {'id': 'musico1', 'view_name': 'musico-list'}),
        ])}),
        ('At. com.', {
            'id': 'at_comm', 'view_name': 'at_comm-list', 'perm': 'ateliers.view_bilanatelier',
        }),
        ('Ateliers', {'id': 'ateliers', 'tabs': OrderedDict([
            ('Observations', {'id': 'obsatelier', 'view_name': 'obsatelier-list'}),
            ('Actions ateliers', {'id': 'actionatelier', 'view_name': 'actionatelier-list'}),
            # 'Bilan at. com.' merged into 'Bilan ateliers'
            ('Bilan ateliers', {'id': 'bilanatelier', 'view_name': 'bilanatelier-list'}),
            ('ARIHANE', {'id': 'at_arihane', 'view_name': 'at_arihane-list'}),
            ('Atelier Poste', {'id': 'at_poste', 'view_name': 'at_poste-list'}),
            ('Créa1', {'id': 'at_crea1', 'view_name': 'at_crea1-list'}),
            ('Créa2', {'id': 'at_crea2', 'view_name': 'at_crea2-list'}),
            ('Cuisine', {'id': 'at_cuisine', 'view_name': 'at_cuisine-list'}),
            ('Info FHMN', {'id': 'at_info', 'view_name': 'at_info-list'}),
        ])}),
    ])


class AttenteTabs(TabStructure):
    _TABS = OrderedDict([
        ('Données', {'id': 'donnees', 'tabs': OrderedDict([
            ('Admission', {'id': 'admission', 'view_name': 'person-admission'}),
            ('Relations', {'id': 'relations', 'view_name': 'person-relations'}),
            ('e-Documents', {'id': 'edocuments', 'view_name': 'documents-list', 'perm': 'person.view_document'}),
        ])}),
        ('Journal', {'id': 'journal', 'tabs': OrderedDict([
            ('General', {'id': 'journalgen', 'view_name': 'journal-person'}),
            ('Entrées/Sorties', {'id': 'journales', 'view_name': 'journal-person-es'}),
        ])}),
    ])


class SiteTabs(TabStructure):
    _TABS = OrderedDict([
        ('Journal', {'id': 'general-journal', 'view_name': 'general-journal'}),
        ('Soins', {'id': 'general-soins', 'view_name': 'general-soins'}),
        ('Agendas', {'id': 'general-agendas', 'view_name': 'general-agendas'}),
    ])

    def get_tab_chain(self, tab_id):
        if tab_id in {'qmed', 'visitegen-detail'}:
            tab_id = 'general-soins'
        return super().get_tab_chain(tab_id)


PERSON_TABS = PersonTabs()
ATTENTE_TABS = AttenteTabs()
SITE_TABS = SiteTabs()


class OnlyAjaxMixin:
    def get(self, request, *args, **kwargs):
        if not is_ajax(request):
            return self.get_nonajax(request, *args, **kwargs)
        return super().get(request, *args, **kwargs)

    def get_nonajax(self, request, *args, **kwargs):
        tab_id = getattr(self, 'tab_id', kwargs.get('tabid'))
        if tab_id is None:
            try:
                tab_id = self.model._view_name_base
            except AttributeError:
                pass
        if tab_id:
            tab_chain = PERSON_TABS.get_tab_chain(tab_id)
            if tab_chain:
                return PersonDetailView.as_view(
                    tabs=tab_chain, final_url=request.get_full_path()
                )(request, *args, **kwargs)
        return HttpResponseRedirect(reverse('person-detail', args=[kwargs['pk']]))


class LocationMixin:
    def dispatch(self, *args, **kwargs):
        if kwargs['location'] not in dict(Person.SITUATION_CHOICES):
            raise Http404("Situation non valable")
        return super().dispatch(*args, **kwargs)


class ListDisplayMixin:
    list_display = ['date', 'auteur']

    def get_fields_from_list_display(self, model):
        field_names = []
        display = getattr(model, 'list_display', self.list_display)
        for item in display:
            fname = item.split(':')[0]
            try:
                verbose = model._meta.get_field(fname).verbose_name
            except FieldDoesNotExist:
                verbose = fname.capitalize()
            field_names.append((item, verbose))
        return field_names


class CanEditMixin:
    """Check that current user has right to edit this object."""
    def dispatch(self, request, *args, **kwargs):
        if not self.get_object().can_edit(request.user):
            raise PermissionDenied("Désolé, vous n'êtes pas autorisé à modifier cet objet.")
        return super().dispatch(request, *args, **kwargs)


class BaseListView(OnlyAjaxMixin, ListDisplayMixin, ListView):
    template_name = 'base-list.html'
    new_form_label = ''
    list_title = ''
    paginate_by = 30
    # When search_fields is set, a filter form appears top right of the list.
    search_fields = ()

    def get_queryset(self):
        qs = super().get_queryset().filter(person__pk=self.kwargs['pk'])
        try:
            qs.model._meta.get_field('fichiers')
            qs.prefetch_related('fichiers')
        except FieldDoesNotExist:
            pass
        if not qs.ordered:
            qs = qs.order_by('-date')
        if self.search_fields:
            form_data = self.filter_data()
            if form_data:
                form = FilterForm(self.search_fields, data=form_data)
                if form.is_valid():
                    # Push the filter data in the session
                    self.request.session[self.request.META['PATH_INFO']] = form_data
                    return form.search(qs)
        return qs

    def filter_data(self):
        if 'text' in self.request.GET:
            return self.request.GET
        return self.request.session.get(self.request.META['PATH_INFO'])

    def can_add(self):
        perm_name = '{}.add_{}'.format(self.model._meta.app_label, self.model._meta.model_name)
        return self.request.user.has_perm(perm_name)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'field_names': self.get_fields_from_list_display(self.model),
            'is_ajax': True,
        })
        if self.can_add():
            context.update({
                'new_form_label': self.new_form_label,
                'new_form_url': reverse('%s-new' % self.model._view_name_base, args=[self.kwargs['pk']]),
            })
        if self.search_fields:
            context['filter_form'] = FilterForm(
                self.search_fields, data=self.filter_data()
            )
        return context

    def post(self, request, *args, **kwargs):
        """Filter list by search text."""
        return self.get(request, *args, **kwargs)


class BaseDetailView(OnlyAjaxMixin, DetailView):
    template_name = 'base-detail.html'
    modif_label = 'Modifier'

    def back_to_list_url(self):
        if self.model._view_name_base:
            try:
                return reverse('%s-list' % self.model._view_name_base, args=[self.kwargs['pk']])
            except NoReverseMatch:
                pass

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'field_names': [
                f.name for f in self.object._meta.get_fields()
                if f.name not in ('id', 'person', 'date', 'visa', 'createur') and not f.one_to_many
            ],
            'latest_history': self.object.latest_history(),
            'object_history_url': self.object.object_history_url(),
            'editable': self.object.can_edit(self.request.user),
            'is_form': False,
            'extend_base': BaseDetailView.template_name,
            'back': self.back_to_list_url(),
        })
        return context


class BaseEditView(OnlyAjaxMixin, CreateUpdateView):
    pk_url_kwarg = 'obj_pk'
    fields = None
    # visa: champ historique importé, plus édité
    exclude = ['visa', 'createur', 'date']
    template_name = 'base-edit.html'
    modif_msg = None
    return_to_list = False

    @property
    def is_create(self):
        return self.pk_url_kwarg not in self.kwargs

    def get_initial(self):
        initial = {'person': self.kwargs['pk']}
        if self.is_create:
            form_class = self.get_form_class()
            if 'rappel_objectifs' in form_class.base_fields:
                latest = self.model.objects.filter(person__pk=self.kwargs['pk']).order_by('-date').first()
                if latest is not None:
                    initial['rappel_objectifs'] = latest.objectifs
        return initial

    def get_exclude_fields(self):
        return self.exclude

    def get_form_class(self):
        if self.form_class:
            return super().get_form_class()
        kwargs = {'fields': self.fields} if self.fields is not None else {'exclude': self.get_exclude_fields()}
        widgets = {'person': forms.HiddenInput}
        for field in self.model._meta.get_fields():
            if field.__class__.__name__ == 'DateField':
                widgets[field.name] = PickDateWidget
        if hasattr(self.model, 'fichiers'):
            form_base = AttachmentModelForm
        else:
            form_base = forms.ModelForm  # The default
        return forms.modelform_factory(self.model, form=form_base, widgets=widgets, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({'is_form': True, 'extend_base': BaseEditView.template_name})
        return context

    def form_invalid(self, form):
        print(form.errors)
        return super().form_invalid(form)

    def form_valid(self, form, update_date=True):
        if not self.is_create and not form.has_changed():
            # Short-circuit save if nothing changed
            return HttpResponseRedirect(self.get_success_url())
        if hasattr(form.instance, 'date') and (not form.instance.date or update_date):
            form.instance.date = timezone.now()
        if self.is_create and hasattr(form.instance, 'createur_id'):
            form.instance.createur = self.request.user
        if (hasattr(form.instance, 'person_id') and not form.instance.person_id
                and 'person' not in form.cleaned_data):
            form.instance.person_id = self.kwargs['pk']
        with transaction.atomic():
            self.object = form.save()
            if self.modif_msg and hasattr(form, 'changed_labels'):
                msg = self.modif_msg.format(fields=', '.join(form.changed_labels()))
            else:
                msg = self.modif_msg
            Modification.save_modification(self.object, self.request.user, created=self.is_create, msg=msg)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        view_name = ('%s-list' if self.return_to_list else '%s-detail') % self.model._view_name_base
        if self.return_to_list:
            return reverse(view_name, args=[self.kwargs['pk']])
        else:
            try:
                return reverse(view_name, args=[self.kwargs['pk'], self.object.pk])
            except NoReverseMatch:
                # Views for one-to-one relations don't always need the object pk.
                return reverse(view_name, args=[self.kwargs['pk']])


class AttachmentDeleteView(View):
    def post(self, request, *args, **kwargs):
        attach = get_object_or_404(Attachment, pk=kwargs['pk'])
        if not attach.content_object.can_edit(request.user):
            raise PermissionDenied("Désolé, vous n'êtes pas autorisé à modifier cet objet.")
        attach.delete()
        return JsonResponse({'result': 'OK'})


class BaseActionEditView(BaseEditView):
    def get_exclude_fields(self):
        return super().get_exclude_fields() + [
            'actions_old', 'evaluations_old', 'etat', 'date_fermee'
        ]


class BaseActionDetailView(BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'base-action.html'

    def get_context_data(self, **kwargs):
        rev_args = [self.object.person.pk, self.object.pk]
        return dict(
            super().get_context_data(**kwargs),
            eval_url=reverse('%s-addeval' % self.model._view_name_base, args=rev_args),
            action_url=reverse('%s-addaction' % self.model._view_name_base, args=rev_args),
            close_url=reverse('%s-closeaction' % self.model._view_name_base, args=rev_args),
            can_close=self.object.can_edit(self.request.user),
        )


class BasePersonPDFView(BasePDFView):
    def pdf_context(self):
        return {'person': get_object_or_404(Person, pk=self.kwargs['pk'])}


class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        return dict(super().get_context_data(**kwargs), site_message=settings.SITE_MESSAGE)


class UserListView(PermissionRequiredMixin, ListView):
    model = User
    permission_required = 'person.view_user'
    template_name = 'user_list.html'
    archived = False

    def get_queryset(self):
        return super().get_queryset().exclude(archived=not self.archived
            ).prefetch_related('groups').order_by('last_name', 'first_name')


class UserExportView(PermissionRequiredMixin, BaseXLSView):
    sheet_name = "Personnel Foyer Handicap"
    col_widths = [20, 20, 20, 40, 25]
    permission_required = 'person.view_user'

    def get_queryset(self):
        return User.objects.real_users().order_by('last_name')

    def get_headers(self):
        return ['Nom', 'Prénom', 'Fonction', 'Adresse', 'N° tél.']

    def write_line(self, user):
        contact = user.contact
        self.ws.append([
            user.last_name,
            user.first_name,
            user.fonction,
            contact.adresse_complete() if contact else '',
            (contact.telephone or contact.portable) if contact else '',
        ])

    def get_filename(self):
        return 'Liste_personnel_FH_{}.xlsx'.format(date.strftime(date.today(), '%Y-%m-%d'))


class UserBaseEditView(PermissionRequiredMixin):
    model = User
    form_class = UserForm
    template_name = 'user_detail.html'
    permission_required = 'person.change_user'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'user' in context:
            # Prevent conflict with 'user' added in context by auth context processor.
            del context['user']
        context['is_form'] = True
        return context


class UserEditView(UserBaseEditView, UpdateView):
    slug_field = 'username'
    slug_url_kwarg = 'username'
    success_url = reverse_lazy('user-list')


class UserNewView(UserBaseEditView, CreateView):
    def form_valid(self, form):
        response = super().form_valid(form)
        # Set random password and send an email with info
        if self.object.is_active:
            pwd = User.objects.make_random_password()
            self.object.set_password(pwd)
            self.object.save()
            send_mail(
                '[DRI] Nouveau compte',
                'Madame, Monsieur,\n'
                'Un nouveau compte a été créé pour que vous puissiez accéder au '
                'Dossier Résident Informatisé de Foyer Handicap.\n'
                'Adresse du site : https://dri.foyerhandicap.ch\n'
                'Votre nom d’utilisateur-trice : {}\n'
                'Votre mot de passe : {}\n\n'
                'Ceci est un message automatique, ne pas répondre.'.format(self.object.username, pwd),
                'www-data@2xlibre.net',
                [self.object.email if self.object.email else self.request.user.email],
            )
        else:
            self.object.set_unusable_password()
            self.object.save()
        return response

    def get_success_url(self):
        return reverse('user-list')


class UserProfileView(DetailView):
    model = User
    template_name = 'user_detail.html'

    def get_object(self):
        user = get_object_or_404(User, username=self.kwargs['username'])
        # Only available for the user itself.
        if user != self.request.user and not self.request.user.has_perm('person.view_user'):
            raise Http404
        return user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'notices': self.object.noticesubscription_set.all(),
            'on_own_profile': self.object == self.request.user,
        })
        return context


class UserChangePasswordView(PasswordChangeView):
    template_name = 'user_change_password.html'
    title = 'Changement de mot de passe'

    def get_success_url(self):
        return reverse('user-profile', args=[self.kwargs['username']])


class UserReinitPasswordView(View):
    def post(self, request, *args, **kwargs):
        import random, string
        user = get_object_or_404(User, username=self.kwargs['username'])
        letters = string.ascii_lowercase
        random_pwd = ''.join(random.choice(letters) for i in range(10))
        user.set_password(random_pwd)
        user.save()
        messages.success(request, 'Le mot de passe de %s a été réinitialisé' % user)
        send_mail(
            '[DRI] Mot de passe réinitialisé',
            'Madame, Monsieur,\n'
            'Votre mot de passe pour le Dossier Résident Informatisé de Foyer Handicap a été réinitialisé.\n'
            'Adresse du site : https://dri.foyerhandicap.ch\n'
            'Votre nom d’utilisateur-trice : {}\n'
            'Nouveau mot de passe : {}\n\n'
            'Ceci est un message automatique, ne pas répondre.'.format(user.username, random_pwd),
            'www-data@2xlibre.net',
            [user.email if user.email else request.user.email],
        )
        return HttpResponseRedirect(reverse('user-list'))


TITLE_MAP = {
    'arch': 'Résidents archivés',
    'attente': 'Liste d’attente',
    'fhmn_int': 'Résidents FHMN internes',
    'fhmn_ext': 'Résidents FHMN externes',
    'fhne_int': 'Résidents FHNE internes',
    'fhne_ext': 'Résidents FHNE externes',
}

PERSON_LIST_COLUMN_SETS = OrderedDict([
    ('default', {
        'title': 'Données de base',
        'cols': [
            ('Date de naissance', 'date_naissance'),
            ('Date admission', 'date_admission'),
            ('Médecin traitant', 'medecin_traitant'),
            ('Répondant off.', 'repondant_off'),
            ('No chambre', 'no_chambre'),
        ],
        'modif_msg': "Modification des données d’admission ({fields})",
    }),
    ('attente', {
        'title': 'Liste d’attente',
        'cols': [
            ('Date de naissance', 'date_naissance'),
            ('Date de la demande', 'date_demande'),
            ('Dem. adm.', 'demande_admission'),
            ('Bilan ind.', 'bilan_independance'),
            ('Origine', 'origine'),
            ('Médecin traitant', 'medecin_traitant'),
            ('Répondant off.', 'repondant_off'),
            ('Prioritaire', 'prioritaire'),
        ],
        'modif_msg': "Modification des données d’admission ({fields})",
    }),
    ('arch', {
        'title': 'Données pour archives',
        'cols': [
            ('Date de naissance', 'date_naissance'),
            ('Date admission', 'date_admission'),
            ('Date de sortie', 'date_depart'),
            ('Cause de sortie', 'cause_depart'),
            ('Situation', 'situation'),
        ],
        'modif_msg': "Modification des données d’admission ({fields})",
    }),
    ('ass', {
        'title': 'Données d’assurance',
        'cols': [
            ('N° AVS', 'no_avs'),
            ('PC', 'pc'),
            ('Caisse LAMAL', 'caisse_mal_acc'),
            ('N° ass LAMAL', 'no_assure_lamal'),
            ('Caisse compl.', 'caisse_mal_acc_compl'),
            ('N° ass compl.', 'no_assure_compl'),
            ('Caisse AI acc.', 'caisse_ai_acc'),
            ('Assurance ménage', 'assurance_menage'),
            ('Assurance RC', 'assurance_rc'),
        ],
        'modif_msg': "Modification des données d’admission ({fields})",
    }),
    ('plaisir', {
        'title': 'Données plaisir',
        'cols': [
            ('N° plaisir', 'no_plaisir'),
            ('Degré plaisir', 'degre_plaisir'),
            ('Date plaisir', 'date_plaisir'),
        ],
        'pdf_url_name': 'degresplaisir-pdf',
        'modif_msg': "Modification des données plaisir ({fields})",
    }),
    ('signifiants', {
        'title': 'Proches signifiants',
        'cols': [('Proches signifiants', 'proches_signifiants')],
    }),
    ('repondants', {
        'title': 'Référents et répondants',
        'cols': [
            ('Référent 1', 'referent1'),
            ('Référent 2', 'referent2'),
            ('Répondant officiel', 'repondant_off_str'),
            ('Répondant thérapeutique', 'repondant_ther'),
            ('Répondant animation', 'repondant_anim'),
            ('Répondant physiothérapie', 'repondant_physio'),
            ('Répondant ergothérapie', 'repondant_ergo'),
            ('Répondant ergothérapie UAJ', 'repondant_ergo_uaj'),
            ('Répondant atelier', 'repondant_atelier'),
            ('Répondant soins', 'repondant_soins1'),
            ('Répondant soins 2', 'repondant_soins2'),
        ],
        'modif_msg': "Modification des données du résident ({fields})",
    }),
    ('regimes', {
        'title': 'Régimes alimentaires',
        'model': BesoinsAlimentaires,
        # Si cette liste change, modifier aussi NourrirEditView.form_valid.
        'cols': [
            ('Régimes', 'besoinsalimentaires.regimes'),
            ('Portions','besoinsalimentaires.portions'),
            ('Moyens auxiliaires','besoinsalimentaires.moyens_aux'),
            ('Compléments alimentaires','besoinsalimentaires.complements'),
            ('N’aime pas','besoinsalimentaires.naimepas'),
            ('Allergies', 'besoinsalimentaires.allergies'),
        ],
        'pdf_url_name': 'regimes-pdf',
        'modif_msg': "Modification des besoins alimentaires ({fields})",
    }),
])

class PersonListView(ListView):
    model = Person
    context_object_name = 'person_list'
    template_name = 'person_list.html'

    def dispatch(self, *args, **kwargs):
        if kwargs['location'] == 'attente' and not isinstance(self, PersonListAttenteView):
            return PersonListAttenteView.as_view()(self.request, *args, **kwargs)
        if not self.has_access():
            raise PermissionDenied("Désolé, vous n'êtes pas autorisé à voir cette liste de résidents.")
        return super().dispatch(*args, **kwargs)

    def has_access(self):
        location = self.kwargs['location']
        if location[:4] in ('fhmn', 'fhne'):
            return self.request.user.has_perm('person.access_%s' % location[:4])
        elif location == 'arch':
            return True
        return False

    def get_queryset(self):
        location = self.kwargs['location']
        qs = super().get_queryset().select_related('medecin_traitant', 'repondant_off').order_by('nom', 'prenom')
        if location == 'arch':
            qs = qs.filter(actif=False)
        else:
            qs = qs.filter(actif=True, situation=location).annotate(
                latest_es_code=Subquery(
                    EntreeSortie.objects.filter(person=OuterRef('pk')).order_by('-date').values('entree_sortie')[:1]
                )
            ).exclude(latest_es_code='d')
            # Beware: this exclude also excludes latest_es_code=None, but all active pers should have at least one

        if self.request.GET.get('columns', '') == 'regimes':
            qs = qs.select_related('besoinsalimentaires')
        elif self.request.GET.get('columns', '') == 'signifiants':
            qs = qs.prefetch_related(Prefetch(
                'relations',
                queryset=Relation.objects.order_by('contact__nom', 'contact__prenom').select_related('contact')
            ))
            qs = list(qs)
            for pers in qs:
                pers.proches_signifiants = format_html_join(
                    mark_safe('<br>'), '<a href="{}">{}</a> {}',
                    [
                        (rel.contact.get_absolute_url(), str(rel.contact),
                         ', '.join(num for num in [rel.contact.telephone, rel.contact.portable] if num))
                        for rel in pers.relations.all() if rel.proche_sig
                    ]
                )
                pers.can_edit = lambda p: False
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        location = self.kwargs['location']
        # Plaisir columns are protected with a special permission
        column_sets = PERSON_LIST_COLUMN_SETS.copy()
        column_set_name = self.request.GET.get(
            'columns', location if location in ['arch', 'attente'] else 'default'
        )
        if column_set_name == 'plaisir' and not self.request.user.has_perm('person.plaisir_edition'):
            raise PermissionDenied("Vous n’êtes pas autorisé·e à afficher ces données.")
        column_dict = column_sets.get(column_set_name, 'default')
        context.update({
            'title': TITLE_MAP.get(location),
            'headers': [c[0] for c in column_dict['cols']],
            'columns': [c[1] for c in column_dict['cols']],
            'col_choices': [
                (key, val['title']) for key, val in column_sets.items() if key not in ['arch', 'attente']
            ],
            'col_chosen': column_set_name,
            'person_detail_url_name': 'person-detail',
            'can_edit': context['person_list'][0].can_edit(self.request.user) if context['person_list'] else False,
            'pdf_url': (
                reverse(column_dict['pdf_url_name'], args=[location])
                if 'pdf_url_name' in column_dict else ''
            ),
            'new_button': False,
        })
        return context


class PersonListAttenteView(PersonListView):
    def has_access(self):
        return self.request.user.has_perm('person.liste_attente')

    def get_queryset(self):
        # Prefetch document_set for 'demande_admission' and 'bilan_independance' properties
        return Person.en_attente().prefetch_related('document_set')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'person_detail_url_name': 'person-detail-attente',
            'new_button': self.request.user.has_perm('person.add_person'),
        }


class PersonListLineEditView(FormErrorsHeaderMixin, UpdateView):
    """View to edit a single line in a person list view."""
    model = Person
    template_name = 'person_list_line.html'

    def dispatch(self, request, *args, **kwargs):
        self.col_dict = PERSON_LIST_COLUMN_SETS.get(self.kwargs['columns'])
        if not self.col_dict:
            raise Http404
        return super().dispatch(request, *args, **kwargs)

    def get_object(self):
        obj = super().get_object()
        if self.kwargs['columns'] == 'regimes':
            obj = obj.besoinsalimentaires
        return obj

    def get_fields(self, stripped=True):
        fields = [c[1] for c in self.col_dict['cols']]
        if stripped and fields[0].startswith('besoinsalimentaires'):
            fields = [f.replace('besoinsalimentaires.', '') for f in fields]
        return fields

    def get_form(self):
        self.fields = self.get_fields()
        target_model = self.col_dict.get('model', self.model)
        # Exclude properties for the form
        fields = [
            f for f in self.fields if (hasattr(target_model, f) and not isinstance(getattr(target_model, f), property))
        ]
        widgets = {}
        for fname in fields:
            if target_model._meta.get_field(fname).__class__.__name__ == 'DateField':
                widgets[fname] = PickDateWidget
        form_class = forms.modelform_factory(target_model, fields=fields, widgets=widgets)
        return form_class(**self.get_form_kwargs())

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['columns'] = self.fields
        if not 'person' in context:
            context['person'] = context['object'].person
        return context

    def form_valid(self, form):
        with transaction.atomic():
            obj = form.save()
            changed_labels = [c[0] for c in self.col_dict['cols'] if c[1] in form.changed_data]
            msg = self.col_dict['modif_msg'].format(fields=', '.join(changed_labels))
            Modification.save_modification(obj, self.request.user, msg=msg)

        return render(self.request, self.template_name, {
            'person': obj.person if hasattr(obj, 'person') else obj,
            'object': obj,
            'columns': self.get_fields(stripped=False),
            'col_chosen': self.kwargs['columns'],
            'can_edit': True,
            'person_detail_url_name': 'person-detail',  # FIXME: We don't here which sort of list this is...
        })


class PersonDetailView(DetailView):
    model = Person
    template_name = 'person_detail.html'
    tab_set = PERSON_TABS
    tabs = 'donnees,admission'
    # If the URL points to a specific instance, final_url will contain that URL.
    final_url = None
    modif_label = 'Modifier'

    def get_context_data(self, *args, **kwargs):
        person = self.object
        if not person.can_view(self.request.user):
            raise PermissionDenied("Désolé, vous n'êtes pas autorisé à voir ce résident.")
        context = super().get_context_data(*args, **kwargs)
        target_tab = self.request.GET.get('totab')
        if target_tab is None:
            active_tabs = self.tabs
        else:
            active_tabs = self.tab_set.get_tab_chain(target_tab)

        if person.actif is False:
            pers_qs = Person.objects.filter(actif=False)
            home_url = reverse('person-list', args=['arch'])
        else:
            pers_qs = Person.objects.filter(actif=True, situation=person.situation)
            if person.situation:
                home_url = reverse('person-list', args=[person.situation])
            else:
                home_url = reverse('home')
        pers_ids = list(pers_qs.order_by('nom', 'prenom').values_list('pk', flat=True))
        current_idx = pers_ids.index(person.pk)

        warnings = []
        if person.actif:
            last_es = person.entreesortie_set.first()
            if last_es and last_es.is_inside():
                try:
                    latest_bilan_date = person.bilanannuel_set.latest('date_bilan').date_bilan
                except ObjectDoesNotExist:
                    latest_bilan_date = person.date_admission.date() if person.date_admission else None
                months_since_last = int((date.today() - latest_bilan_date).days / 30.5) if latest_bilan_date else 0
                if months_since_last >= 5:
                    warnings.append(f"Le dernier bilan date de plus de {months_since_last} mois.")

            if last_es and last_es.entree_sortie == EntreeSortie.SORTIE_TEMPORAIRE:
                warnings.append(f"Ce résident est actuellement absent ({last_es.motif}).")

            if last_es and last_es.entree_sortie == EntreeSortie.ARRIVEE_TEMPORAIRE:
                warnings.append(f"Ce résident est en séjour temporaire.")

        context.update({
            'warnings': warnings,
            'home_url': home_url,
            'tabset': 'person',
            'tabs': TabStructure.filter(self.request.user, self.tab_set.all(), person.situation),
            'active_tabs': active_tabs,
            'prev_pers_pk': pers_ids[current_idx - 1] if current_idx > 0 else None,
            'next_pers_pk': pers_ids[current_idx + 1] if current_idx < (len(pers_ids) - 1) else None,
        })
        return context


class PersonDetailOnlyView(DetailView):
    model = Person
    template_name = 'person_detail_person.html'


class PersonDetailAttenteView(PersonDetailView):
    tab_set = ATTENTE_TABS

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'tabset': 'attente'}


class PersonPlaisirsListView(LocationMixin, BasePersonPDFView):
    pdf_class = DegresPlaisirList

    def pdf_context(self):
        return {
            'persons': Person.objects.filter(actif=True, situation=self.kwargs['location']
                ).order_by('nom', 'prenom')
        }


class PersonEditBaseView:
    model = Person
    form_class = PersonDetailForm

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save()
            created = isinstance(self, PersonNewView)
            msg = self.modif_msg if created else self.modif_msg % (self.object, ', '.join(form.changed_labels()))
            Modification.save_modification(self.object, self.request.user, created=created, msg=msg)
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['is_form'] = True
        context['ajax'] = is_ajax(self.request)
        return context


class PersonNewView(PersonEditBaseView, CreateView):
    template_name = 'person_new.html'
    modif_msg = "Ajout d’un nouveau résident"

    def get_success_url(self):
        messages.success(
            self.request,
            "Le nouveau résident a bien été créé et se trouve automatiquement en liste d’attente. "
            "Pour saisir une admission, veuillez passer dans l’onglet «Journal», sous-onglet «Entrées/Sorties»."
        )
        return reverse('person-detail-attente', args=[self.object.pk])


class PersonEditView(PersonEditBaseView, UpdateView):
    template_name = 'person_detail_person.html'
    modif_msg = "Modification du résident %s (%s)"

    def form_valid(self, form):
        form.instance.updated = timezone.now()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('person-detail-only', args=[self.kwargs['pk']])


class RelationListView(OnlyAjaxMixin, ListView):
    template_name = 'person_relations.html'
    tab_id = 'relations'

    def get_queryset(self):
        self.pers = get_object_or_404(Person, pk=self.kwargs['pk'])
        return self.pers.relations.all().select_related('contact').order_by(
            'contact__nom', 'contact__prenom'
        )

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context.update({
            'person': self.pers,
            'pdf_url': reverse('person-relations-print', args=[self.pers.pk]),
        })
        return context


class RelationsPrintView(BasePersonPDFView):
    pdf_class = RelationsPDF


class RelationBaseEditView(FormErrorsHeaderMixin):
    model = Relation
    form_class = RelationForm
    template_name = 'relation_edit.html'

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'user': self.request.user}

    def form_valid(self, form):
        with transaction.atomic():
            is_new = form.instance.pk is None
            self.object = form.save()
            if is_new:
                msg = 'Ajout d’une relation vers %s' % self.object.contact
            else:
                msg = 'Modification de la relation vers %s' % self.object.contact
            Modification.save_modification(self.object.person, self.request.user, msg=msg)
        # Nothing done with a redirect for now (refreshed client-side).
        return HttpResponse('OK')


class RelationAddView(RelationBaseEditView, CreateView):

    def get_initial(self):
        return dict(super().get_initial(), person=self.kwargs['pk'])


class RelationEditView(RelationBaseEditView, UpdateView):
    pk_url_kwarg = 'pk_rel'


class RelationDeleteView(View):
    def post(self, request, *args, **kwargs):
        rel = get_object_or_404(Relation, pk=self.kwargs['pk_rel'])
        with transaction.atomic():
            pers = rel.person
            msg = "Suppression de la relation vers %s" % rel.contact
            rel.delete()
            Modification.save_modification(pers, self.request.user, created=False, msg=msg)
        return JsonResponse({'results': 'OK'})


class ModifFilterBase(ListView):
    manual_only = True

    def get(self, request, *args, **kwargs):
        self.dfilter = ModifFilterForm(request.GET or None, manual_only=self.manual_only)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['filter_form'] = self.dfilter
        return context


class NextEventsMixin:
    event_category = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        today = date.today()
        start = timezone.make_aware(datetime(today.year, today.month, today.day, 0, 0))
        end = start + timedelta(days=1000)
        context['next_events'] = Event.objects.get_for_person(
            self.person, start, end, category=self.event_category
        )
        return context


class PersonModificationView(OnlyAjaxMixin, NextEventsMixin, ModifFilterBase):
    model = Modification
    manual_only = False
    template_name = 'person_journal.html'
    paginate_by = 20
    event_category = 'Soins'

    def get_queryset(self):
        self.person = Person.objects.get(pk=self.kwargs['pk'])
        qs = super().get_queryset().filter(person=self.person).order_by('-date')
        return self.dfilter.apply(qs)


class PersonModificationAddView(CreateView):
    form_class = ModificationForm
    template_name = 'base-edit.html'

    def dispatch(self, *args, **kwargs):
        self.person = get_object_or_404(Person, pk=self.kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def get_initial(self):
        return dict(super().get_initial(), date=timezone.now())

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'location': self.person.situation.split('_')[0],
        }

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.person = self.person
        form.instance.manual = True
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('journal-person', args=[self.object.person.pk])


class PersonModificationEditView(CanEditMixin, UpdateView):
    model = Modification
    pk_url_kwarg = 'obj_pk'
    form_class = ModificationForm
    template_name = 'base-edit.html'
    delete_url_name = 'journal-person-delete'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        if self.object.can_delete(self.request.user):
            context['delete_url'] = reverse(self.delete_url_name, kwargs=self.kwargs)
        return context

    def get_success_url(self):
        return reverse('journal-person', args=[self.object.person.pk])


class PersonModificationDeleteView(CanEditMixin, DeleteView):
    model = Modification
    pk_url_kwarg = 'obj_pk'

    def get_success_url(self):
        return reverse('journal-person', args=[self.object.person.pk])


class PersonEntreesSortiesView(OnlyAjaxMixin, ModifFilterBase):
    model = EntreeSortie
    template_name = 'person_journal_es.html'

    def dispatch(self, *args, **kwargs):
        self.person = get_object_or_404(Person, pk=self.kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'last_es': self.person.entreesortie_set.first()}

    def get_queryset(self):
        return super().get_queryset().filter(person=self.person)


class PersonEntreesSortiesAddView(CreateView):
    form_class = EntreeSortieForm
    template_name = 'base-edit.html'

    def dispatch(self, *args, **kwargs):
        self.person = get_object_or_404(Person, pk=self.kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def get_initial(self):
        return dict(super().get_initial(), date=timezone.now())

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'last_es': self.person.entreesortie_set.first()}

    def form_valid(self, form):
        form.instance.person = self.person
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('journal-person-es', args=[self.object.person.pk])


class PersonEntreesSortiesEditView(PersonModificationEditView):
    model = EntreeSortie
    form_class = EntreeSortieForm
    delete_url_name = 'journal-person-es-delete'

    def get_success_url(self):
        return reverse('journal-person-es', args=[self.object.person.pk])


class PersonEntreesSortiesDeleteView(CanEditMixin, DeleteView):
    model = EntreeSortie
    pk_url_kwarg = 'obj_pk'

    def get_success_url(self):
        return reverse('journal-person-es', args=[self.object.person.pk])


class PersonArchiveView(View):
    """
    Only for persons in waiting list, others should use the EntreeSortie workflow.
    """
    def post(self, request, *args, **kwargs):
        person = get_object_or_404(Person, pk=kwargs['pk'])
        # Do some perm checks
        if person.date_admission is not None:
            messages.error(request, "Pour archiver un résident, allez dans le journal des entrées/sorties.")
        elif not person.can_edit(request.user):
            messages.error(request, "Vous n'avez pas la permission de modifier cette personne.")
        else:
            person.actif = False
            person.save()
            messages.success(request, f"Le dossier de {person} a bien été placé dans les archives.")
        return HttpResponseRedirect(person.get_absolute_url())


class PersonUnarchiveView(View):
    """
    To move person from archives to waiting list.
    """
    def post(self, request, *args, **kwargs):
        person = get_object_or_404(Person, pk=kwargs['pk'])
        if person.actif:
            messages.error(request, "Ce résident n’est pas archivé.")
        elif not person.can_edit(request.user):
            messages.error(request, "Vous n'avez pas la permission de modifier cette personne.")
        else:
            person.actif = True
            if person.situation:
                person.situation = ''
            person.save()
            messages.success(request, f"Le dossier de {person} a bien été placé en liste d’attente.")
        return HttpResponseRedirect(person.get_absolute_url())


class GeneralView(TemplateView):
    template_name = 'general_base.html'
    tabs = 'general-journal'
    final_url = None

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context.update({
            'active_tabs': self.tabs,
            'location': self.kwargs['location'],
            'home_url': reverse('person-list', args=['%s_int' % self.kwargs['location']])
        })
        return context


class GeneralTabMixin:
    def get(self, request, *args, **kwargs):
        if not is_ajax(request):
            return self.get_nonajax(request, *args, **kwargs)
        location = self.kwargs['location']
        if location not in ('fhmn', 'fhne'):
            raise Http404("Uniquement valable pour fhmn ou fhne")
        if not request.user.has_perm('person.access_%s' % location):
            raise PermissionDenied("Désolé, vous n'êtes pas autorisé à afficher cette page.")
        return super().get(request, *args, **kwargs)

    def get_nonajax(self, request, *args, **kwargs):
        view_name = resolve(request.path).url_name
        tab_chain = SITE_TABS.get_tab_chain(view_name)
        if tab_chain:
            return GeneralView.as_view(tabs=tab_chain, final_url=request.path)(request, *args, **kwargs)
        return HttpResponseRedirect(reverse('general-base', args=[self.kwargs['location']]))


class GeneralJournalView(GeneralTabMixin, ModifFilterBase):
    """List all modification for a given situation."""
    model = Modification
    template_name = 'general_journal.html'
    paginate_by = 30

    def get_queryset(self):
        location = self.kwargs['location']
        modifs = super().get_queryset().filter(
            Q(person__situation__startswith=location) |
            Q(**{location: True})
        ).exclude(
            date__gt=timezone.now() + timedelta(days=7), person__isnull=False
        ).order_by('-date')
        return self.dfilter.apply(modifs)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context.update({
            'home_url': reverse('person-list', args=['%s_int' % self.kwargs['location']])
        })
        return context


class GeneralAgendasView(GeneralTabMixin, TemplateView):
    template_name = 'general_agendas.html'
    categ_slugs = ['physio', 'ergo']


class GeneralSoinsView(GeneralTabMixin, TemplateView):
    template_name = 'general_soins.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        visites = VisiteMedicale.open_visites(self.kwargs['location']).order_by('type_visite'
            ).values('type_visite').annotate(Count('id'))
        context.update({
            'med_for_validation': MedicationItem.a_valider_list(self.kwargs['location']).count(),
            'visites': {vm['type_visite']: vm['id__count'] for vm in visites},
        })
        return context


class ModificationAddView(CreateView):
    form_class = ModificationForm
    template_name = 'modification-edit.html'

    def get_initial(self):
        return dict(super().get_initial(), date=timezone.now())

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'location': self.kwargs['location'],
            'user': self.request.user,
        }

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.manual = True
        if self.kwargs['location'] == 'fhmn':
            form.instance.fhmn = True
        else:
            form.instance.fhne = True
        return super().form_valid(form)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title'] = 'Ajout d’une information générale'
        context['intro'] = (
            'Uniquement pour des informations non spécifiques à un résident. '
            'Allez dans l’onglet Journal du résident pour une information liée à un résident.'
        )
        return context

    def get_success_url(self):
        return reverse('general-journal', args=[self.kwargs['location']])


class ModificationEditView(CanEditMixin, UpdateView):
    model = Modification
    form_class = ModificationForm
    template_name = 'modification-edit.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title'] = 'Modification d’une information générale'
        context['delete_url'] = reverse('modifs-delete', kwargs=self.kwargs)
        return context

    def get_success_url(self):
        return reverse('general-journal', args=[self.kwargs['location']])


class ModificationDeleteView(CanEditMixin, DeleteView):
    model = Modification

    def get_success_url(self):
        return reverse('general-base', args=[self.kwargs['location']])


class ModificationConfirmReadView(View):
    def post(self, *args, **kwargs):
        infos = [int(pk) for pk in self.request.user.infos_a_lire.split(',') if pk]
        if self.kwargs['pk'] in infos:
            infos.remove(self.kwargs['pk'])
            self.request.user.infos_a_lire = ','.join(str(i) for i in infos)
            self.request.user.save()
        return JsonResponse({'result': 'OK'})


class ObjectHistoryView(TemplateView):
    template_name = 'object_history.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        obj = ContentType.objects.get_for_id(self.kwargs['ct_pk']
            ).get_object_for_this_type(pk=self.kwargs['obj_pk'])
        context['history'] = obj.object_history(
            as_qs=True, trunc_at_min=True, part=self.kwargs.get('part')
        )
        return context


class VisiteMedicaleView(GeneralTabMixin, ListView):
    model = VisiteMedicale
    template_name = 'visites_med_open.html'

    def get_queryset(self):
        if self.kwargs['vtyp'] not in dict(VisiteMedicale.TYPEV_CHOICES):
            raise Http404
        return self.model.open_visites(self.kwargs['location']).filter(
            type_visite=self.kwargs['vtyp']
        )

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context.update({
            'title': "Visites {} pour {}".format(
                {'med': 'médicales', 'psy': 'du psychiatre', 'ergo': 'ergo', 'physio': 'physio'}.get(self.kwargs['vtyp']),
                self.kwargs['location'].upper()
            ),
            'location': self.kwargs['location'],
            'vtyp': self.kwargs['vtyp'],
            'is_ajax': is_ajax(self.request),
        })
        return context


class MedicationItemsToValidateView(ListView):
    model = MedicationItem
    template_name = 'med_to_validate.html'

    def get_queryset(self):
        if self.kwargs['location'] not in ('fhmn', 'fhne'):
            raise Http404("Uniquement valable pour fhmn ou fhne")
        return MedicationItem.a_valider_list(self.kwargs['location']
            ).select_related('medication__person').order_by('-date_debut')


class ContactListView(ListView):
    model = Contact
    template_name = 'contact_list.html'
    search_fields = ['nom', 'prenom', 'fonction', 'localite']

    def dispatch(self, request, *args, **kwargs):
        if not request.GET.get('f') and not request.GET.get('text'):
            return HttpResponseRedirect(request.path + '?f=a')
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super().get_queryset().filter(actif=True)
        if self.request.GET.get('text'):
            form = FilterForm(self.search_fields, data=self.request.GET)
            if form.is_valid():
                qs = form.search(qs)
        elif self.request.GET.get('f'):
            qs = qs.filter(nom__istartswith=self.request.GET.get('f'))
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context.update({
            'on_contacts': True,
            'letters': 'abcdefghijklmnopqrstuvwxyz',
            'current': self.request.GET.get('f'),
            'filter_form': FilterForm(self.search_fields, data=self.request.GET),
        })
        return context


class ContactQueryView(View):
    """Reçoit les requêtes Ajax pour les menus déroulants 'autocomplete'."""
    def get(self, request, *args, **kwargs):
        term = request.GET.get('term')
        if term:
            query = Contact.objects.filter(
                Q(nom__unaccent__icontains=term) | Q(prenom__unaccent__icontains=term) |
                Q(fonction__unaccent__icontains=term)
            )
        else:
            query = Contact.objects.none()
        return JsonResponse({'results': [{'id': cont.pk, 'text': str(cont)} for cont in query]})


class ContactDetailView(DetailView):
    model = Contact
    template_name = 'contact_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        relations = self.object.all_relations()
        context.update({
            'editable': self.request.user.has_perm('person.change_contact'),
            'relations': relations,
            'rel_all_archived': all(not r.person.actif for r in relations),
        })
        if self.request.user.is_superuser:
            context['merge_form'] = ContactMergeForm()
        return context


class ContactBaseEditView:
    model = Contact
    form_class = ContactForm
    template_name = 'contact_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_form'] = True
        return context


class ContactEditView(ContactBaseEditView, UpdateView):
    pass


class ContactNewView(ContactBaseEditView, CreateView):
    def get_initial(self):
        initial = super().get_initial()
        foruser_id = self.request.GET.get('foruser')
        if foruser_id:
            try:
                user = User.objects.get(pk=int(foruser_id))
            except User.DoesNotExist:
                return initial
            initial.update({
                'nom': user.last_name,
                'prenom': user.first_name,
                'fonction': user.fonction,
                'foruser': user,
            })
        return initial

    def get_success_url(self):
        return reverse('contact-list') + '?f=%s' % self.object.nom[0].lower()


class ContactArchiveView(View):
    def post(self, request, *args, **kwargs):
        contact = get_object_or_404(Contact, pk=self.kwargs['pk'])
        if any(r.person.actif for r in contact.all_relations()):
            messages.error(request, "Impossible d’archiver un contact lié à des personnes actives.")
        else:
            contact.actif = False
            contact.save()
            messages.success(request, "Le contact «%s» a été archivé avec succès." % contact)
        return HttpResponseRedirect(reverse('contact-list') + '?f=%s' % contact.nom[0].lower())


class ContactDeleteView(DeleteView):
    model = Contact

    def get_success_url(self):
        return reverse('contact-list') + '?f=%s' % self.object.nom[0].lower()


class ContactMergeView(ContactEditView):
    def dispatch(self, *args, **kwargs):
        self.merge_with = self.request.GET.get('merge_with') or self.request.POST.get('merge_with')
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['compare_with'] = get_object_or_404(Contact, pk=self.merge_with)
        return context

    def form_valid(self, form):
        contact2 = Contact.objects.get(pk=self.merge_with)
        if contact2 == form.instance:
            form.add_error(None, "Il n'est pas permis de fusionner un contact avec lui-même")
            return self.form_invalid(form)
        # Save any modification to main contact
        super().form_valid(form)
        # Replace merged contact refs with main contact, then delete it.
        for rel in contact2.all_relations():
            if rel.pk:
                rel.contact = self.object
                rel.save()
            else:
                # A fake (unsaved) relation
                setattr(rel.person, rel._pers_field, self.object)
                rel.person.save()
        contact2.delete()
        return HttpResponseRedirect(reverse('contact-list') + '?f=%s' % self.object.nom[0].lower())


class AdmissionView(OnlyAjaxMixin, TemplateView):
    template_name = 'person_admission.html'
    tab_id = 'admission'
    modif_label = 'Modifier'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        person = Person.objects.prefetch_related('relations').get(pk=self.kwargs['pk'])
        context.update({
            'person': person,
            'object': person,
            'allergies': person.diagnosticmedical.allergies,
            'editable': person.can_edit(self.request.user),
            'archivable': person.actif and person.can_edit(self.request.user) and person.date_admission is None,
        })
        return context


class AdmissionEditView(BaseEditView):
    model = Person
    form_class = AdmissionForm
    template_name = 'person_admission.html'
    pk_url_kwarg = 'pk'
    modif_msg = "Modification des données d’admission ({fields})"

    def form_valid(self, form):
        form.instance.updated = timezone.now()
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['person'] = Person.objects.get(pk=self.kwargs['pk'])
        return context

    def get_success_url(self):
        return reverse('person-admission', args=[self.kwargs['pk']])


class TransfertPDFView(PermissionRequiredMixin, BasePDFView):
    pdf_class = TransfertPDF
    permission_required = 'soins.view_diagnosticmedical'

    def render_to_response(self, *args, **kwargs):
        from soins.pdf_output import DiagnosticPDF, MedicationLabel
        from pypdf import PdfMerger

        self.person = get_object_or_404(Person, pk=self.kwargs['pk'])
        pdf_merger = PdfMerger(strict=False)

        def merge_doc(pdf_doc):
            tampon = BytesIO()
            pdf_doc.produce_pdf(tampon)
            pdf_merger.append(tampon)

        # Feuille de transfert
        merge_doc(TransfertPDF(person=self.person))
        # Diagnostic médical
        try:
            diag = self.person.diagnosticmedical
        except ObjectDoesNotExist:
            pass
        else:
            merge_doc(DiagnosticPDF(person=self.person, diagnostic=diag))
        # Médication
        try:
            medic = self.person.medication_set.latest('date_prescription')
        except ObjectDoesNotExist:
            pass
        else:
            merge_doc(MedicationLabel(object=medic))
        # Carte de vie
        try:
            merge_doc(CarteDeViePDF(person=self.person))
        except ObjectDoesNotExist:
            pass
        # Relations
        merge_doc(RelationsPDF(person=self.person))
        # Directives anticipées
        directives = self.person.directives_anticipees_fichier
        if directives:
            pdf_merger.append(directives.pdf_path())

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="dossier_transfert_%s.pdf"' % slugify(self.person)
        pdf_merger.write(response)
        return response


class RepartitionTachesBaseView(DetailView):
    model = RepartitionTaches
    is_create = False

    def get_object(self):
        pers = Person.objects.get(pk=self.kwargs['pk'])
        try:
            return pers.repartitiontaches
        except RepartitionTaches.DoesNotExist:
            return RepartitionTaches(person=pers)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['person'] = self.object.person
        context['editable'] = self.request.user.has_perm('person.change_repartitiontaches')
        return context


class RepartitionView(RepartitionTachesBaseView, BaseDetailView):
    template_name = 'person_repartition.html'


class RepartitionEditView(RepartitionTachesBaseView, BaseEditView):
    form_class = RepartitionForm
    modif_msg = "Modification de la répartition ({fields})"
    template_name = 'person_repartition.html'

    def get_success_url(self):
        return reverse('person-repartition', args=[self.object.person.pk])


class RangementView(RepartitionTachesBaseView, BaseDetailView):
    template_name = 'person_rangements.html'
    tab_id = 'rangements'


class RangementEditView(RepartitionTachesBaseView, BaseEditView):
    form_class = RangementForm
    modif_msg = "Modification des rangements ({fields})"
    template_name = 'person_rangements.html'
    tab_id = 'rangements'

    def get_success_url(self):
        return reverse('person-rangement', args=[self.object.person.pk])


class DocumentsView(BaseListView):
    model = Document
    template_name = 'document_list.html'
    new_form_label = 'Ajouter nouveau document'
    list_display = ['date', 'date_doc', 'type_doc']
    tab_id = 'documents'

    def get_queryset(self):
        self.pers = get_object_or_404(Person, pk=self.kwargs['pk'])
        if self.request.user.has_perm('soins.view_medication'):
            qs = self.pers.document_set.all()
        else:
            qs = self.pers.document_set.exclude(type_doc='directives')
        return qs.order_by('-date_doc', 'title')


class DocumentEditView(BaseEditView):
    model = Document
    exclude = ['date', 'createur']
    pk_url_kwarg = 'pk_doc'
    return_to_list = True

    def form_valid(self, form):
        if self.is_create:
            self.modif_msg = "Ajout d’un document «%s»" % form.instance.get_type_doc_display()
        else:
            self.modif_msg = "Modification d’un document «%s»" % form.instance.get_type_doc_display()
        return super().form_valid(form, update_date=False)


class DocumentDeleteView(View):
    def post(self, request, *args, **kwargs):
        doc = get_object_or_404(Document, pk=self.kwargs['pk_doc'])
        with transaction.atomic():
            pers = doc.person
            msg = "Suppression d’un document «%s»" % doc.get_type_doc_display()
            doc.doc.delete(save=False)
            doc.delete()
            Modification.save_modification(pers, self.request.user, created=False, msg=msg)
        return JsonResponse({'results': 'OK'})


class TabContentView(OnlyAjaxMixin, TemplateView):
    @property
    def tab_set(self):
        return {'person': PERSON_TABS, 'attente': ATTENTE_TABS}.get(self.kwargs['tabset'])

    def get(self, request, *args, **kwargs):
        self.person = get_object_or_404(Person, pk=self.kwargs['pk'])
        self.tab = self.tab_set.get_by_id(kwargs['tabid'])
        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        if 'tabs' in self.tab:
            return ['subtabs.html']
        else:
            return [self.tab.get('template', 'base-detail.html')]
        
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context.update({
            'person': self.person,
            'tabset': self.kwargs['tabset'],
            'tab': TabStructure.filter(self.request.user, self.tab, self.person.situation),
        })
        return context


class AISBaseView:
    model = ActionIS
    tab_id = 'ais'


class AISListView(AISBaseView, BaseListView):
    new_form_label = 'Créer nouvelle action intersecteur'


class AISDetailView(AISBaseView, BaseActionDetailView):
    template_name = 'ais_detail.html'
    action_title = 'Action inter-secteurs'


class AISEditView(AISBaseView, BaseActionEditView):
    template_name = 'ais_detail.html'
    exclude = ['visa_ouverture', 'visa_fermeture']
    action_title = 'Action inter-secteurs'


class ActionAddItemView(FormErrorsHeaderMixin, CreateView):
    _model = ''
    fields = ['texte']
    template_name = 'ais_additem.html'

    @property
    def model(self):
        label, name = self._model.split('.')
        return apps.get_model(app_label=label, model_name=name)

    def form_valid(self, form):
        form.instance.action_id = self.kwargs['obj_pk']
        form.instance.date = timezone.now()
        form.instance.createur = self.request.user
        with transaction.atomic():
            self.object = form.save()
            msg = 'Ajout d’une %s sur une %s' % (
                form.fields['texte'].label.lower(),
                self.object.action._meta.verbose_name
            )
            Modification.save_modification(self.object.action, self.request.user, created=True, msg=msg)
        return HttpResponse('OK')


class ActionCloseView(View):
    _model = ''

    @property
    def model(self):
        label, name = self._model.split('.')
        return apps.get_model(app_label=label, model_name=name)

    def post(self, request, *args, **kwargs):
        action = get_object_or_404(self.model, pk=kwargs['obj_pk'])
        if not action.can_edit(request.user):
            raise PermissionDenied("Vous n’êtes pas autorisé à fermer cette action")
        action.etat = 'f'
        action.date_fermee = date.today()
        with transaction.atomic():
            action.save()
            msg = "Fermeture de l’action «%s»" % str(action)
            Modification.save_modification(action, request.user, msg=msg)
        return HttpResponseRedirect(action.get_absolute_url())


class SoutienSocialListView(BaseListView):
    model = SoutienSocial
    list_title = 'Soutien social et visites'
    new_form_label = 'Créer nouvelle fiche réseau'


class SoutienSocialDetailView(BaseDetailView):
    model = SoutienSocial
    pk_url_kwarg = 'obj_pk'
    tab_id = 'reseau'


class SoutienSocialEditView(BaseEditView):
    model = SoutienSocial


class HistoireVieMixin:
    model = HistoireDeVie

    def get_object(self):
        pers = Person.objects.get(pk=self.kwargs['pk'])
        try:
            return pers.histoiredevie
        except HistoireDeVie.DoesNotExist:
            return HistoireDeVie(person=pers)


class HistoireVieView(HistoireVieMixin, BaseDetailView):
    pass


class HistoireVieEditView(HistoireVieMixin, BaseEditView):
    view_name_base = 'histoirevie'
    modif_msg = "Modification de l’histoire de vie"
    exclude = BaseEditView.exclude + ['person']

    @property
    def is_create(self):
        return self.object.pk is None


class RegimesPdfView(LocationMixin, BasePDFView):
    pdf_class = RegimesList

    def pdf_context(self):
        return {
            'persons': Person.objects.filter(actif=True, situation=self.kwargs['location']
                ).select_related('besoinsalimentaires'
                ).order_by('nom', 'prenom')
        }


class SearchView(ListDisplayMixin, PermissionRequiredMixin, FormView):
    template_name = 'search.html'
    form_class = InstanceSearchForm
    permission_required = 'person.change_user'

    def get(self, request, *args, **kwargs):
        """Handle GET requests: instantiate a blank version of the form."""
        if 'instance' in self.request.GET:
            form = self.form_class(**self.get_form_kwargs(), data=self.request.GET)
            if form.is_valid():
                return self.render_to_response(self.get_context_data(
                    form=form,
                    results=form.search(),
                    field_names=self.get_fields_from_list_display(form.model),
                    is_ajax=False,
                    show_resident=True,
                ))
            else:
                return self.render_to_response(self.get_context_data(form=form, results=None))
        else:
            return self.render_to_response(self.get_context_data())

    def get_form_kwargs(self):
        return dict(super().get_form_kwargs(), location=self.kwargs['location'])

    def get_context_data(self, **kwargs):
        return super().get_context_data(
            **kwargs,
            location=self.kwargs['location'][:4].upper(),
        )
