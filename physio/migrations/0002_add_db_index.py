from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('physio', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bilanphysio',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='observationphysio',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
    ]
