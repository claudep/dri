from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('physio', '0002_add_db_index'),
    ]

    operations = [
        migrations.AddField(
            model_name='bilanphysio',
            name='date_bilan',
            field=models.DateField(null=True, verbose_name='Date du bilan'),
        ),
    ]
