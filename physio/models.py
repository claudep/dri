from datetime import date

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from person.models import Attachment, Person, PersonRelatedModel


class BilanPhysio(PersonRelatedModel):
    date_bilan = models.DateField("Date du bilan")
    referent = models.CharField("Personnes présentes", max_length=50, blank=True)
    rappel_objectifs = models.TextField("Rappel des objectifs", blank=True)
    resultats = models.TextField("Résultats", blank=True)
    deroulement = models.TextField("Déroulement de l'entretien", blank=True)
    commentaires = models.TextField("Commentaires et propositions", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)

    class Meta:
        verbose_name = "Bilan physio"
        verbose_name_plural = "Bilans physio"

    _view_name_base = 'bilanphysio'
    list_display = ['date_bilan', 'auteur']

    def __str__(self):
        return "Bilan physio pour %s (%s)" % (self.person, self.date_bilan)


class ObservationPhysio(PersonRelatedModel):
    intervenant = models.CharField("Intervenant", max_length=50, blank=True)
    observation = models.TextField("Observation", blank=True)
    fichiers = GenericRelation(Attachment)

    class Meta:
        verbose_name = "Observation physio"
        verbose_name_plural = "Observations physio"

    _view_name_base = 'obsphysio'
    list_display = ['date', 'observation:w50:files', 'intervenant', 'auteur']
    searchable = {'txt': ['observation']}

    def __str__(self):
        return "Observation physio pour %s (%s)" % (self.person, self.date)

    def can_edit(self, user):
        return user.is_superuser or (user == self.createur and self.date.date() == date.today())
