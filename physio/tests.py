from django.test import TestCase

from person.tests import ModelBaseTextMixin
from .models import BilanPhysio, ObservationPhysio


class BilanPhysioAnnuelTests(ModelBaseTextMixin, TestCase):
    ModelClass = BilanPhysio
    model_id = 'bilanphysio'
    custom_data = {'date_bilan': '2019-03-04', 'objectifs': "Des objectifs…"}


class ObservationPhysioCompletTests(ModelBaseTextMixin, TestCase):
    ModelClass = ObservationPhysio
    model_id = 'obsphysio'
    custom_data = {'observation': "Une observation…"}
