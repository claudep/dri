from django.urls import path

from . import views

urlpatterns = [
    path('bilan/', views.BilanPhysioListView.as_view(), name='bilanphysio-list'),
    path('bilan/<int:obj_pk>/', views.BilanPhysioDetailView.as_view(), name='bilanphysio-detail'),
    path('bilan/<int:obj_pk>/edit/', views.BilanPhysioEditView.as_view(), name='bilanphysio-edit'),
    path('bilan/new/', views.BilanPhysioEditView.as_view(), name='bilanphysio-new'),

    path('observation/', views.ObservationPhysioListView.as_view(), name='obsphysio-list'),
    path('observation/<int:obj_pk>/', views.ObservationPhysioDetailView.as_view(), name='obsphysio-detail'),
    path('observation/<int:obj_pk>/edit/', views.ObservationPhysioEditView.as_view(), name='obsphysio-edit'),
    path('observation/new/', views.ObservationPhysioEditView.as_view(), name='obsphysio-new'),
]
