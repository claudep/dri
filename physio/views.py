from person.views import BaseDetailView, BaseListView, BaseEditView

from .models import BilanPhysio, ObservationPhysio


class BilanPhysioBaseView:
    model = BilanPhysio


class BilanPhysioListView(BilanPhysioBaseView, BaseListView):
    new_form_label = 'Créer nouveau bilan physio'


class BilanPhysioDetailView(BilanPhysioBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class BilanPhysioEditView(BilanPhysioBaseView, BaseEditView):
    pass


class ObservationPhysioBaseView:
    model = ObservationPhysio


class ObservationPhysioListView(ObservationPhysioBaseView, BaseListView):
    new_form_label = 'Créer nouvelle observation physio'


class ObservationPhysioDetailView(ObservationPhysioBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class ObservationPhysioEditView(ObservationPhysioBaseView, BaseEditView):
    pass

