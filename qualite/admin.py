from django.contrib import admin

from .models import Categorie, CategConstat, Constat, Document
from .utils import convert_doc_to_pdf


class NeedConversionListFilter(admin.SimpleListFilter):
    title = 'nécessite conv. PDF'
    parameter_name = 'need_conv'

    def lookups(self, *args):
        return (
            ('1', 'Oui'),
            ('0', 'Non'),
        )

    def queryset(self, request, queryset):
        choice = self.value()
        need_conv = queryset.filter(pdf='', fichier__regex=r'\.(docx?|xlsx?|pptx?)$')
        if choice == '1':
            return need_conv
        elif choice == '0':
            return queryset.exclude(pk__in=need_conv)
        return queryset


class DocumentArchivedFilter(admin.SimpleListFilter):
    title = 'archivé'
    parameter_name = 'archive_for'

    def lookups(self, *args):
        return (
            ('1', 'Oui'),
            ('0', 'Non'),
        )

    def queryset(self, request, queryset):
        if self.value() is not None:
            return queryset.filter(archive_for__isnull=self.value()=='1')
        return queryset


@admin.register(Categorie)
class CategorieAdmin(admin.ModelAdmin):
    list_display = ['numero', 'titre', 'chapitre']
    list_filter = ['chapitre']
    ordering = ['numero']


@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    list_display = ['code', 'titre', 'site', 'date_doc']
    list_filter = [NeedConversionListFilter, DocumentArchivedFilter]
    actions = ['convert_to_pdf']
    search_fields = ['titre', 'numero', 'type_doc', 'categorie__numero']
    raw_id_fields = ['archive_for']

    def convert_to_pdf(modeladmin, request, queryset):
        count = 0
        for obj in queryset:
            success = convert_doc_to_pdf(request, obj)
            if success:
                count += 1
            else:
                break
        modeladmin.message_user(request, "%d documents ont été convertis" % count)
    convert_to_pdf.short_description = "Convertir les documents en PDF"


@admin.register(Constat)
class ConstatAdmin(admin.ModelAdmin):
    list_display = ['createur', 'date', 'site']
    search_fields = ['constat']
    list_filter = ['site']


admin.site.register(CategConstat)
