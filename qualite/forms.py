from django import forms
from django.contrib.postgres.search import SearchHeadline, SearchQuery, SearchVector
from django.utils import timezone

from common.fields import PickDateWidget, PickSplitDateTimeWidget
from person.models import Person, User
from .models import CategConstat, Constat, Document


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ['fichier', 'titre', 'categorie', 'type_doc', 'numero', 'site', 'date_doc', 'comment']
        widgets = {
            'date_doc': PickDateWidget,
        }

    def clean(self):
        cleaned_data = super().clean()
        site = cleaned_data['site']
        numero = cleaned_data['numero']
        if cleaned_data.get('type_doc') == 'MAQ' and numero == 0:
            return
        if site == 'commun' and len(str(numero)) != 3:
            self.add_error(
                'numero', forms.ValidationError("Les documents communs doivent avoir un numéro à 3 chiffres.")
            )
        if site in ('fhne', 'fhmn') and len(str(numero)) not in [1, 2]:
            self.add_error(
                'numero', forms.ValidationError("Les documents spécifiques à un site doivent avoir un numéro à 2 chiffres.")
            )
        if site == 'fhmn' and (numero % 2) == 0:
            self.add_error(
                'numero', forms.ValidationError("Les documents de Chaux-de-Fonds doivent avoir un numéro impair")
            )
        if site == 'fhne' and (numero % 2) == 1:
            self.add_error(
                'numero', forms.ValidationError("Les documents de Neuchâtel doivent avoir un numéro pair")
            )


class DocumentVersionForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ['fichier', 'titre', 'date_doc', 'comment']
        labels = {
            'fichier': 'Nouvelle version',
        }
        widgets = {
            'date_doc': PickDateWidget,
        }


class DocumentSearchForm(forms.Form):
    text = forms.CharField(
        label="", widget=forms.TextInput(attrs={'placeholder': 'Rechercher'}), required=False
    )

    def search(self):
        text_query = SearchQuery(self.cleaned_data['text'], config='french_unaccent')
        query = Document.objects.annotate(
            unaccent_text=SearchVector('text', config='french_unaccent')
        ).filter(
            archive_for__isnull=True,
            unaccent_text=text_query
        ).annotate(
            headline=SearchHeadline('text', text_query)
        )
        return query


class UserNomPrenomChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return ' '.join([obj.last_name.upper(), obj.first_name])


class ConstatAddForm(forms.ModelForm):
    class Meta:
        model = Constat
        fields = [
            'saisi_pour', 'saisi_pour_res', 'site', 'lieu', 'date_even', 'constat',
            'resident', 'mesures_imm', 'suggestions', 'fichier',
        ]
        field_classes = {
            'date_even': forms.SplitDateTimeField,
        }
        widgets = {
            'date_even': PickSplitDateTimeWidget,
        }

    def __init__(self, *args, user=None, **kwargs):
        super().__init__(*args, **kwargs)
        sites = user.get_sites()
        if len(sites) == 1:
            self.fields['site'].initial = sites[0]
            self.fields['site'].widget = forms.HiddenInput()
        else:
            self.fields['site'].widget = forms.RadioSelect(choices=self.fields['site'].widget.choices[1:])
        if len(sites) == 1:
            self.fields['saisi_pour'] = UserNomPrenomChoiceField(
                queryset=User.objects.by_site(sites[0]).real_users().order_by('last_name'),
                required=False
            )
            self.fields['saisi_pour_res'].queryset = Person.objects.filter(
                actif=True).filter(situation__startswith=sites[0]).order_by('nom')
            self.fields['resident'].queryset = Person.objects.filter(
                actif=True).filter(situation__startswith=sites[0]).order_by('nom')
        else:
            self.fields['saisi_pour'] = UserNomPrenomChoiceField(
                queryset=User.objects.real_users().order_by('last_name'),
                required=False
            )
            self.fields['saisi_pour_res'].queryset = Person.objects.filter(
                actif=True).order_by('nom')
            self.fields['resident'].queryset = Person.objects.filter(
                actif=True).order_by('nom')


class ConstatEditForm(forms.ModelForm):
    """Traitement du constat."""
    gravite = forms.ChoiceField(
        label="Gravité", choices=Constat.GraviteChoices.choices, widget=forms.RadioSelect,
        required=False
    )
    frequence = forms.ChoiceField(
        label="Fréquence", choices=Constat.FrequenceChoices.choices, widget=forms.RadioSelect,
        required=False
    )

    class Meta:
        model = Constat
        fields = [
            'categories', 'gravite', 'frequence', 'resident', 'responsables',
            'lien_doc', 'infos_compl', 'decision', 'suivi_en_cours',
        ]
        widgets = {
            'categories': forms.CheckboxSelectMultiple,
            'responsables': forms.CheckboxSelectMultiple,
        }

    def __init__(self, is_manager=None, **kwargs):
        super().__init__(**kwargs)
        self.is_manager = is_manager
        self.fields['resident'].queryset = self.fields['resident'].queryset.filter(
            actif=True, situation__startswith=self.instance.site
        ).order_by('nom', 'prenom')
        self.fields['lien_doc'].queryset = self.fields['lien_doc'].queryset.filter(
            archive_for__isnull=True, site__in=['commun', self.instance.site]
        ).order_by('type_doc', 'categorie__numero', 'numero')
        self.fields['responsables'].queryset = self.fields['responsables'].queryset.filter(
            is_active=True
        )
        if self.is_manager:
            self.fields['validate'] = forms.BooleanField(
                label="Valider définitivement le constat", required=False
            )

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('validate') and not all(
                cleaned_data[key] for key in ('gravite', 'frequence', 'decision')):
            raise forms.ValidationError(
                "La gravité, la fréquence et la décision sont obligatoires pour valider définitivement un constat"
            )

    def save(self, **kwargs):
        if self.is_manager and self.cleaned_data['validate']:
            self.instance.date_traite = timezone.now()
        return super().save(**kwargs)


class ConstatFilterForm(forms.Form):
    RESULTAT_CHOICES = (
        ('', '-------'),
        ('R', 'rouge'),
        ('J', 'jaune'),
        ('V', 'vert'),
    )
    date_du = forms.DateField(label="Du", widget=PickDateWidget, required=False)
    date_au = forms.DateField(label="Au", widget=PickDateWidget, required=False)
    categorie = forms.ModelChoiceField(
        label="Catégorie", queryset=CategConstat.objects.all(), required=False
    )
    resident = forms.ModelChoiceField(
        label="Résident", queryset=Person.objects.all().order_by('nom', 'prenom'),
        required=False,
    )
    resultat = forms.ChoiceField(
        label="Résultat", choices=RESULTAT_CHOICES, required=False
    )
    site = forms.ChoiceField(
        choices=(('', 'Tous'), ('fhmn', 'FHMN'), ('fhne', 'FHNE')), required=False
    )
    suivi = forms.BooleanField(label="Suivi en cours", required=False)

    def __init__(self, show_site=False, **kwargs):
        super().__init__(**kwargs)
        if not show_site:
            del self.fields['site']

    def apply(self, queryset):
        if self.cleaned_data['date_du']:
            queryset = queryset.filter(date_even__gte=self.cleaned_data['date_du'])
        if self.cleaned_data['date_au']:
            queryset = queryset.filter(date_even__lte=self.cleaned_data['date_au'])
        if self.cleaned_data['categorie']:
            queryset = queryset.filter(categories=self.cleaned_data['categorie'])
        if self.cleaned_data['resident']:
            queryset = queryset.filter(resident=self.cleaned_data['resident'])
        if self.cleaned_data.get('site'):
            queryset = queryset.filter(site=self.cleaned_data['site'])
        if self.cleaned_data['suivi']:
            queryset = queryset.filter(suivi_en_cours=True)
        if self.cleaned_data['resultat']:
            queryset = Constat.resultat_filter(queryset, self.cleaned_data['resultat'])
        return queryset
