import re
from pathlib import Path

from django.core.files import File
from django.core.management.base import BaseCommand

from qualite.models import Categorie, Document


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('imp_dir')

    def handle(self, *args, **options):
        dir_param = Path(options['imp_dir'])
        for child in dir_param.iterdir():
            m = re.match(r'([A-Z]+) (\d{2})\.(\d+) ([^\.]*)\..*', child.name)
            if not m:
                self.stdout.write("Unable to match file name {}".format(child.name))
                continue
            num = int(m.groups()[2])
            with child.open(mode='rb') as fh:
                Document.objects.create(
                    titre=m.groups()[3],
                    numero=num,
                    site='commun' if len(str(num)) == 3 else ('fhmn' if (num % 2) == 1 else 'fhne'),
                    fichier=File(fh, name=child.name),
                    type_doc=m.groups()[0],
                    categorie=Categorie.objects.get(numero=int(m.groups()[1])),
                )
            self.stdout.write("Le fichier {} a bien été importé".format(child.name))
