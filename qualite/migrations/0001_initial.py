from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Categorie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('chapitre', models.CharField(choices=[('00', '00 Manuel Qualité'), ('10', '10 Management'), ('20', '20 Ressources'), ('30', '30 Suivi accompagnement et production'), ('40', '40 Soutien'), ('50', '50 Analyse et amélioration')], max_length=2, verbose_name='Chapitre')),
                ('numero', models.PositiveSmallIntegerField(unique=True, verbose_name='Numéro')),
                ('titre', models.CharField(max_length=200, verbose_name='Titre')),
            ],
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titre', models.CharField(max_length=200, verbose_name='Titre')),
                ('numero', models.PositiveSmallIntegerField(verbose_name='Numéro')),
                ('site', models.CharField(choices=[('commun', 'Commun'), ('fhmn', 'FHMN'), ('fhne', 'FHNE')], default='commun', max_length=6, verbose_name='Site')),
                ('fichier', models.FileField(upload_to='qualite', verbose_name='Fichier')),
                ('date_doc', models.DateField(verbose_name='Date du document')),
                ('type_doc', models.CharField(choices=[('CL', 'Check-list'), ('FO', 'Formulaire'), ('IT', 'Instruction'), ('ORG', 'Organisation'), ('PQ', 'Procédure'), ('MAQ', 'Manuel qualité')], max_length=3, verbose_name='Type de document')),
                ('categorie', models.ForeignKey(on_delete=models.deletion.PROTECT, to='qualite.Categorie', verbose_name='Catégorie')),
            ],
        ),
    ]
