from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualite', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='pdf',
            field=models.FileField(blank=True, upload_to='qualite', verbose_name='Fichier PDF'),
        ),
    ]
