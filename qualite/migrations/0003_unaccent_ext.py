from django.db import migrations
from django.contrib.postgres.operations import UnaccentExtension


class Migration(migrations.Migration):

    dependencies = [
        ('qualite', '0002_document_pdf'),
    ]

    operations = [
        UnaccentExtension(),
    ]
