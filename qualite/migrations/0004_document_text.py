from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualite', '0003_unaccent_ext'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='text',
            field=models.TextField(blank=True),
        ),
        migrations.RunSQL("CREATE TEXT SEARCH CONFIGURATION french_unaccent( COPY = french );"),
        migrations.RunSQL("ALTER TEXT SEARCH CONFIGURATION french_unaccent ALTER MAPPING FOR hword, hword_part, word WITH unaccent, french_stem;"),
        migrations.RunSQL("CREATE INDEX text_idx ON qualite_document USING GIN (to_tsvector('french_unaccent', text));"),
    ]
