from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualite', '0005_constat'),
    ]

    operations = [
        migrations.AddField(
            model_name='constat',
            name='site',
            field=models.CharField(choices=[('fhmn', 'FHMN'), ('fhne', 'FHNE')], db_index=True, default='fhmn', max_length=4),
            preserve_default=False,
        ),
    ]
