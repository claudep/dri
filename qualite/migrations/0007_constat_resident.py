from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '__first__'),
        ('qualite', '0006_constat_site'),
    ]

    operations = [
        migrations.AddField(
            model_name='constat',
            name='resident',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='person.Person', verbose_name='Lien avec résident'),
        ),
    ]
