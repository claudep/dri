from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualite', '0007_constat_resident'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='archived',
            field=models.BooleanField(default=False),
        ),
    ]
