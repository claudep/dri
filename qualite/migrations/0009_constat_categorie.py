from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualite', '0008_document_archived'),
    ]

    operations = [
        migrations.CreateModel(
            name='CategConstat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=30, unique=True)),
            ],
            options={
                'verbose_name': 'Catégorie de constat',
                'verbose_name_plural': 'Catégories de constat',
                'ordering': ['nom'],
            },
        ),
        migrations.AddField(
            model_name='constat',
            name='categorie',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='qualite.CategConstat', verbose_name='Catégorie'),
        ),
    ]
