from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualite', '0009_constat_categorie'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='constat',
            name='categorie',
        ),
        migrations.AddField(
            model_name='constat',
            name='categories',
            field=models.ManyToManyField(blank=True, to='qualite.CategConstat', verbose_name='Catégories'),
        ),
        migrations.AddField(
            model_name='constat',
            name='date_even',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Date et heure de l’événement'),
        ),
        migrations.AlterField(
            model_name='constat',
            name='date',
            field=models.DateTimeField(db_index=True, verbose_name='Date de saisie'),
        ),
    ]
