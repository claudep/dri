from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualite', '0011_date_doc_nullable'),
    ]

    operations = [
        migrations.AddField(
            model_name='constat',
            name='lien_doc',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='qualite.Document', verbose_name='Document qualité à mettre à jour'),
        ),
    ]
