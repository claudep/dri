from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualite', '0012_constat_lien_doc'),
    ]

    operations = [
        migrations.AddField(
            model_name='constat',
            name='fichier',
            field=models.FileField(blank=True, upload_to='constats'),
        ),
    ]
