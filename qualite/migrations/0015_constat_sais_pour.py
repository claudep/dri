from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0041_user_max_length'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('qualite', '0014_remove_constat_desire_copie'),
    ]

    operations = [
        migrations.AddField(
            model_name='constat',
            name='saisi_pour',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Saisi pour'),
        ),
        migrations.AddField(
            model_name='constat',
            name='saisi_pour_res',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='person.person', verbose_name='Saisi pour résident'),
        ),
    ]
