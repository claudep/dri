from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualite', '0016_document_comment'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='archive_for',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.CASCADE, related_name='archives', to='qualite.document', verbose_name='Version archivée de'),
        ),
    ]
