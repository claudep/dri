from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualite', '0018_remove_document_archived'),
    ]

    operations = [
        migrations.AddField(
            model_name='constat',
            name='suivi_en_cours',
            field=models.BooleanField(default=False, verbose_name='Suivi en cours'),
        ),
    ]
