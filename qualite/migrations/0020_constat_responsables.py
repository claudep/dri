from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('qualite', '0019_constat_suivi_en_cours'),
    ]

    operations = [
        migrations.AddField(
            model_name='constat',
            name='responsables',
            field=models.ManyToManyField(blank=True, limit_choices_to={'resp_secteur': True}, related_name='_constat_responsables_+', to=settings.AUTH_USER_MODEL, verbose_name='Responsables secteurs'),
        ),
    ]
