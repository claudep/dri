import subprocess

from django.db import models

from common.utils import icon_url
from person.models import Person, User


class Categorie(models.Model):
    class ChapitreChoices(models.TextChoices):
        MAQ = '00', '00 Manuel Qualité'
        MANAGEMENT = '10', '10 Management'
        RESSOURCES = '20', '20 Ressources'
        SUIVI = '30', '30 Suivi accompagnement et production'
        SOUTIEN = '40', '40 Soutien'
        ANALYSE = '50', '50 Analyse et amélioration'

    chapitre = models.CharField("Chapitre", choices=ChapitreChoices.choices, max_length=2)
    numero = models.PositiveSmallIntegerField("Numéro", unique=True)
    titre = models.CharField("Titre", max_length=200)

    def __str__(self):
        return '{} {}'.format(self.numero, self.titre)


class Document(models.Model):
    class SiteChoices(models.TextChoices):
        COMMUN = 'commun', 'Commun'
        FHMN = 'fhmn', 'FHMN'
        FHNE = 'fhne', 'FHNE'

    class TypeDocChoices(models.TextChoices):
        CL = 'CL', 'Check-list'
        FO = 'FO', 'Formulaire'
        IT = 'IT', 'Instruction'
        ORG = 'ORG', 'Organisation'
        PQ = 'PQ', 'Procédure'
        MAQ = 'MAQ', 'Manuel qualité'

    titre = models.CharField("Titre", max_length=200)
    numero = models.PositiveSmallIntegerField("Numéro")
    site = models.CharField("Site", max_length=6, choices=SiteChoices.choices, default='commun')
    fichier = models.FileField(upload_to='qualite', verbose_name="Fichier")
    pdf = models.FileField(upload_to='qualite', verbose_name="Fichier PDF", blank=True)
    text = models.TextField(blank=True)
    # null=True so as import script can import without knowing the document date.
    date_doc = models.DateField("Date du document", null=True)
    type_doc = models.CharField("Type de document", max_length=3, choices=TypeDocChoices.choices)
    comment = models.TextField("Commentaire de version", blank=True)
    categorie = models.ForeignKey(Categorie, on_delete=models.PROTECT, verbose_name="Catégorie")
    archive_for = models.ForeignKey(
        'self', null=True, blank=True, on_delete=models.CASCADE, related_name='archives',
        verbose_name="Version archivée de"
    )

    def __str__(self):
        line = f'{self.code} {self.titre}'
        if self.archive_for_id:
            line = f'{line} (version archivée du {self.date_doc})'
        return line

    @property
    def code(self):
        if self.categorie.numero == 0:  # MAQ
            if self.numero == 0:
                return f'{self.type_doc} 000'
            return f'{self.type_doc} {self.numero:0>2}'
        return f'{self.type_doc} {self.categorie.numero}.{self.numero:0>2}'

    @property
    def icon_url(self):
        return icon_url(self.fichier.name)

    @property
    def pdf_url(self):
        """Return the PDF URL or None."""
        return self.pdf.url if self.pdf else None

    def extract_text(self):
        """
        Update the text column with the extracted text from fichier or pdf.
        Needs pdftotext from poppler-utils package.
        """
        if self.pdf:
            path = self.pdf.path
        elif self.fichier.path.lower().endswith('pdf'):
            path = self.fichier.path
        else:
            raise Exception("No PDF file found to extract text from.")
        result = subprocess.run(['pdftotext', path, '-'], capture_output=True)
        self.text = result.stdout.decode()
        self.save()


class CategConstat(models.Model):
    nom = models.CharField(max_length=30, unique=True)

    class Meta:
        ordering = ['nom']
        verbose_name = 'Catégorie de constat'
        verbose_name_plural = 'Catégories de constat'

    def __str__(self):
        return self.nom


class Constat(models.Model):
    """Formulaire FO 54.101"""

    class SiteChoices(models.TextChoices):
        FHMN = 'fhmn', 'FHMN'
        FHNE = 'fhne', 'FHNE'

    class GraviteChoices(models.TextChoices):
        TRES_GRAVE = 'tg', 'Très grave (4)'
        GRAVE = 'gr', 'Grave (3)'
        MOYEN = 'mo', 'Moyen (2)'
        FAIBLE = 'fa', 'Faible (1)'

    class FrequenceChoices(models.TextChoices):
        TRES_PROBABLE = 'tp', 'Très probable (4)'
        PROBABLE = 'pr', 'Probable (3)'
        IMPROBABLE = 'im', 'Improbable (2)'
        TRES_IMPROBABLE = 'ti', 'Très improbable (1)'

    createur = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Créateur')
    saisi_pour = models.ForeignKey(
        User, null=True, blank=True, related_name='+', on_delete=models.PROTECT, verbose_name='Saisi pour'
    )
    saisi_pour_res = models.ForeignKey(
        Person, null=True, blank=True, related_name='+', on_delete=models.SET_NULL, verbose_name="Saisi pour résident"
    )
    date = models.DateTimeField("Date de saisie", db_index=True)
    date_even = models.DateTimeField("Date et heure de l’événement", null=True, blank=True)
    lieu = models.CharField(max_length=80)
    site = models.CharField(max_length=4, choices=SiteChoices.choices, db_index=True)

    constat = models.TextField()
    mesures_imm = models.TextField("Mesures immédiates prises", blank=True)
    suggestions = models.TextField("Suggestions d'améliorations", blank=True)
    fichier = models.FileField(upload_to='constats', blank=True)
    # partie traitement
    categories = models.ManyToManyField(
        CategConstat, blank=True, verbose_name="Catégories"
    )
    gravite = models.CharField("Gravité", max_length=2, choices=GraviteChoices.choices, blank=True)
    frequence = models.CharField("Fréquence", max_length=2, choices=FrequenceChoices.choices, blank=True)
    resident = models.ForeignKey(
        Person, null=True, blank=True, on_delete=models.SET_NULL, verbose_name="Lien avec résident"
    )
    lien_doc = models.ForeignKey(
        Document, null=True, blank=True, on_delete=models.SET_NULL, verbose_name="Document qualité à mettre à jour"
    )
    responsables = models.ManyToManyField(
        User, blank=True, limit_choices_to={'resp_secteur': True}, verbose_name="Responsables secteurs", related_name="+"
    )
    infos_compl = models.TextField("Informations complémentaires du RDS concerné", blank=True)
    decision = models.TextField("Décision du briefing / conseil de direction", blank=True)
    date_traite = models.DateTimeField("Date de traitement", null=True, blank=True)
    traite_par = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, verbose_name='Traité par', related_name='+'
    )
    suivi_en_cours = models.BooleanField("Suivi en cours", default=False)

    def __str__(self):
        return "Constat du %s par %s" % (self.date, self.createur)

    def get_createur(self):
        cr = str(self.createur)
        if self.saisi_pour_id:
            cr += f' pour {self.saisi_pour}'
        if self.saisi_pour_res_id:
            cr += f' pour {self.saisi_pour_res.prenom} {self.saisi_pour_res.nom}'
        return cr

    @property
    def resultat(self):
        if not self.gravite or not self.frequence:
            return ''
        if self.gravite in ['tg', 'gr'] and self.frequence in ['pr', 'tp']:
            return 'R'
        elif self.gravite == 'fa' or (self.gravite == 'mo' and self.frequence in ['ti', 'im']) or (
                self.gravite == 'gr' and self.frequence == 'ti'):
            return 'V'
        else:
            return 'J'

    @classmethod
    def resultat_filter(cls, qs, resultat):
        if resultat == 'R':
            return qs.filter(gravite__in=['tg', 'gr'], frequence__in=['pr', 'tp'])
        if resultat == 'V':
            return qs.filter(
                models.Q(gravite='fa') |
                models.Q(gravite='mo', frequence__in=['ti', 'im']) |
                models.Q(gravite='gr', frequence='ti')
            )
        elif resultat == 'J':
            return qs.exclude(gravite__in=['tg', 'gr'], frequence__in=['pr', 'tp']
                ).exclude(gravite='fa'
                ).exclude(gravite='mo', frequence__in=['ti', 'im']
                ).exclude(gravite='gr', frequence='ti')
        return qs
