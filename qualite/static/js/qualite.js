function showHideDocs() {
    const siteCBs = document.querySelectorAll("input.sites, input[name=sites]");
    siteCBs.forEach(cb => {
        if (cb.checked) {
            document.querySelectorAll('li.' + cb.value).forEach(li => li.classList.remove('hidden'));
        } else {
            document.querySelectorAll('li.' + cb.value).forEach(li => li.classList.add('hidden'));
        }
    });
}

document.addEventListener('DOMContentLoaded', function () {
    // search.html/documents.html
    const siteCBs = document.querySelectorAll("input.sites, input[name=sites]");
    siteCBs.forEach(cb => {
        cb.addEventListener('click', showHideDocs);
    });
    showHideDocs();

    const histSwitchers = document.querySelectorAll("img.history-switcher");
    for (let i = 0; i < histSwitchers.length; i++) {
        histSwitchers[i].addEventListener('click', switchHistory);
    }
});
