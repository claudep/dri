from datetime import date, datetime
from pathlib import Path

from django.contrib.auth.models import Permission
from django.core import mail
from django.core.files import File
from django.test import TestCase
from django.urls import reverse
from django.utils.timezone import make_aware, now

from person.models import Person, User
from person.tests import TempMediaTestCase, XLSX_CONTENT_TYPE
from ..forms import DocumentForm
from ..models import Categorie, CategConstat, Constat, Document


class DocumentTests(TempMediaTestCase):
    fixtures = ['categories.json']

    def setUp(self):
        super().setUp()
        # create user
        self.fhmn_user = User.objects.create(
            username='evam', first_name='Eva', last_name='Martin',
        )
        self.fhmn_user.user_permissions.add(Permission.objects.get(codename='access_fhmn'))

    def test_document_code(self):
        doc = Document(type_doc='FO', categorie=Categorie.objects.get(numero=11), numero=134)
        self.assertEqual(doc.code, 'FO 11.134')
        doc.numero = 5
        self.assertEqual(doc.code, 'FO 11.05')

    def test_document_form(self):
        base_data = {
            'titre': 'Doc test', 'categorie': '1', 'type_doc': 'CL', 'date_doc': '2019-8-8',
            'fichier': 'chemin.docx',
        }
        pdf_file = Path(__file__).parent / 'test.pdf'
        with pdf_file.open(mode='rb') as fh:
            form = DocumentForm({**base_data, 'site': 'commun', 'numero': '23'}, files={'fichier': File(fh)})
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['numero'][0],
                'Les documents communs doivent avoir un numéro à 3 chiffres.'
            )
            form = DocumentForm({**base_data, 'site': 'fhmn', 'numero': '23'}, files={'fichier': File(fh)})
            self.assertTrue(form.is_valid())
            form = DocumentForm({**base_data, 'site': 'fhmn', 'numero': '03'}, files={'fichier': File(fh)})
            self.assertTrue(form.is_valid())
            form = DocumentForm({**base_data, 'site': 'fhne', 'numero': '03'}, files={'fichier': File(fh)})
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['numero'][0],
                'Les documents de Neuchâtel doivent avoir un numéro pair'
            )

    def test_document_visibility(self):
        Document.objects.bulk_create([
            Document(
                titre="Doc commun", numero=1, site='commun',
                fichier='qualite/doc1.docx', pdf='qualite/doc1.pdf',
                date_doc='2019-8-8',
                type_doc='CL', categorie=Categorie.objects.get(numero=12)
            ),
            Document(
                titre="Doc fhmn", numero=2, site='fhmn', fichier='qualite/doc2.pdf', date_doc='2019-8-8',
                type_doc='CL', categorie=Categorie.objects.get(numero=12)
            ),
            Document(
                titre="Doc fhne", numero=3, site='fhne', fichier='qualite/doc3.pdf', date_doc='2019-8-8',
                type_doc='CL', categorie=Categorie.objects.get(numero=12)
            ),
        ])
        self.client.force_login(self.fhmn_user)
        response = self.client.get(reverse('qualite-documents', args=['12']))
        self.assertNotContains(response, "qualite/doc1.docx")
        self.assertQuerysetEqual(
            response.context['docs'], ['Doc commun', 'Doc fhmn'], transform=lambda o: o.titre
        )
        # Responsable qualité peut voir les fichiers originaux
        qual_user = User.objects.create(
            username='quality', first_name='Janne', last_name='Qualité',
        )
        qual_user.user_permissions.add(Permission.objects.get(codename='add_document', content_type__app_label='qualite'))
        self.client.force_login(qual_user)
        response = self.client.get(reverse('qualite-documents', args=['12']))
        self.assertContains(response, "qualite/doc1.docx")

    def test_document_index_and_search(self):
        doc = Document.objects.create(titre="Doc commun", numero=1, site='commun', date_doc='2019-8-8',
            type_doc='CL', categorie=Categorie.objects.get(numero=12)
        )
        pdf_file = Path(__file__).parent / 'test.pdf'
        with pdf_file.open(mode='rb') as fh:
            doc.fichier.save('test.pdf', File(fh))
        doc.extract_text()

        # A document from FHNE should not be visible for fhmn_user
        Document.objects.create(
            titre="Doc fhne", numero=3, site='fhne', fichier='qualite/doc3.pdf', date_doc='2019-8-8',
            type_doc='CL', categorie=Categorie.objects.get(numero=12),
            text="Voici une adresse ainsi qu'un numéro de téléphone dans un document."
        )
        Document.objects.create(
            titre="Doc fhmn", numero=4, site='fhmn', fichier='qualite/doc3.pdf', date_doc='2019-8-8',
            type_doc='CL', categorie=Categorie.objects.get(numero=12),
            text="Ce document est spécifique FHMN."
        )
        self.client.force_login(self.fhmn_user)
        response = self.client.get(reverse('qualite-search') + '?text=document&sites=fhmn')
        self.assertEqual(len(response.context['results']), 1)
        response = self.client.get(reverse('qualite-search') + '?text=adresse')
        self.assertEqual(len(response.context['results']), 1)
        response = self.client.get(reverse('qualite-search') + '?text=telephone')
        self.assertEqual(len(response.context['results']), 1)
        response = self.client.get(reverse('qualite-search') + '?text=téléphone')
        self.assertEqual(len(response.context['results']), 1)
        self.assertContains(
            response,
            '<span class="file-icons"><a href="{}" title="Format original"><img class="ficon" '
            'src="/static/ficons/pdf.svg"></a></span>'.format(doc.fichier.url),
            html=True
        )
        self.assertContains(response, '<span>CL 12.01 Doc commun')

    def test_add_new_version(self):
        doc = Document.objects.create(titre="Doc commun", numero=1, site='commun', date_doc='2019-8-8',
            type_doc='CL', categorie=Categorie.objects.get(numero=12)
        )
        self.fhmn_user.user_permissions.add(
            Permission.objects.get(content_type__app_label='qualite', codename='add_document')
        )
        self.client.force_login(self.fhmn_user)
        response = self.client.get(reverse('qualite-addversion', args=[doc.pk]))
        self.assertContains(response, '<td>12 Sécurité - Santé au travail - Environnement</td>')
        pdf_file = Path(__file__).parent / 'test.pdf'
        with pdf_file.open(mode='rb') as fh:
            response = self.client.post(reverse('qualite-addversion', args=[doc.pk]), data={
                'fichier': File(fh, name='test.pdf'),
                'titre': 'Doc commun',
                'comment': 'Corrigé',
                'date_doc': '2020-01-12',
            })
        self.assertRedirects(response, reverse('qualite-documents', args=['12']))
        self.assertEqual(Document.objects.filter(titre='Doc commun', archive_for__isnull=False).count(), 1)
        doc_new = Document.objects.get(titre='Doc commun', archive_for=None)
        self.assertEqual(doc_new.numero, doc.numero)
        self.assertNotEqual(doc_new.date_doc, doc.date_doc)
        # Add a second version
        with pdf_file.open(mode='rb') as fh:
            response = self.client.post(reverse('qualite-addversion', args=[doc_new.pk]), data={
                'fichier': File(fh, name='test.pdf'),
                'titre': 'Doc commun',
                'comment': 'Corrigé 2',
                'date_doc': '2020-08-10',
            })
        doc_new2 = Document.objects.get(titre='Doc commun', archive_for=None)
        self.assertEqual(doc_new2.archives.all().count(), 2)

    def test_add_pdf_document(self):
        self.fhmn_user.user_permissions.add(
            Permission.objects.get(content_type__app_label='qualite', codename='add_document')
        )
        self.client.force_login(self.fhmn_user)
        pdf_file = Path(__file__).parent / 'test.pdf'
        categ = Categorie.objects.get(numero=12)
        with pdf_file.open(mode='rb') as fh:
            response = self.client.post(reverse('qualite-adddocument', args=['12']), data={
                'fichier': File(fh, name='test.pdf'),
                'titre': 'Fichier PDF',
                'categorie': categ.pk,
                'type_doc': 'CL',
                'numero': '111',
                'site': 'commun',
                'date_doc': '2020-01-12',
            })
        self.assertRedirects(response, reverse('qualite-documents', args=['12']))
        doc = Document.objects.get(titre='Fichier PDF')
        self.assertIn('Speed-Pizza', doc.text)


class ConstatTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        CategConstat.objects.create(nom='Chutes')
        CategConstat.objects.create(nom='Cuisine')

    def setUp(self):
        super().setUp()
        self.user = User.objects.create(
            username='evam', first_name='Eva', last_name='Martin', email='eva@example.org'
        )
        self.user.user_permissions.add(Permission.objects.get(codename='access_fhmn'))
        self.admin_user = User.objects.create(
            username='dirlo', first_name='Edith', last_name='Bar',
        )
        self.admin_user.user_permissions.add(
            Permission.objects.get(content_type__app_label='qualite', codename='change_constat'),
            Permission.objects.get(codename='access_fhmn'),
            Permission.objects.get(codename='access_fhne'),
        )
        self.manage_user = User.objects.create(
            username='qualite', first_name='Céline', last_name='Dupond',
        )
        self.manage_user.user_permissions.add(
            Permission.objects.get(codename='access_fhmn'),
            Permission.objects.get(content_type__app_label='qualite', codename='change_constat'),
            Permission.objects.get(content_type__app_label='qualite', codename='add_document')
        )

    def test_create_constat(self):
        person = Person.objects.create(
            nom='Smith', prenom='John', situation='fhmn_int',
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('qualite-addconstat'))
        self.assertContains(response, '<input type="hidden" name="site" value="fhmn" id="id_site">', html=True)
        self.assertContains(response, '<button type="submit">Enregistrer</button>')
        response = self.client.post(reverse('qualite-addconstat'), data={
            'lieu': 'Neuchâtel',
            'date_even_0': '12.2.2020', 'date_even_1': '12:30',
            'site': 'fhmn',
            'constat': 'Il n’y a rien qui va!\nYaka faukon.',
            'resident': person.pk,
            'mesures_imm': 'Aucune pour le moment',
        })
        self.assertRedirects(response, reverse('qualite-home'))
        constat = Constat.objects.first()
        self.assertEqual(constat.date.date(), date.today())
        self.assertEqual(constat.date_even, make_aware(datetime(2020, 2, 12, 12, 30)))
        self.assertEqual(constat.createur, self.user)
        self.assertEqual(constat.resident, person)

    def test_create_constat_for_user(self):
        """A quality manager can create a constat for another user."""
        self.client.force_login(self.manage_user)
        form_data = {
            'lieu': 'Neuchâtel',
            'date_even_0': '12.2.2020', 'date_even_1': '12:30',
            'site': 'fhmn',
            'constat': 'Il n’y a rien qui va!\nYaka faukon.',
        }
        response = self.client.post(reverse('qualite-addconstat'), data={
            **form_data, 'saisi_pour': self.user.pk
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('qualite-home'))
        constat = Constat.objects.first()
        self.assertEqual(constat.get_createur(), 'Céline Dupond pour Eva Martin')
        # Now for a resident
        person = Person.objects.create(
            nom='Smith', prenom='John', situation='fhmn_int',
        )
        response = self.client.post(reverse('qualite-addconstat'), data={
            **form_data, 'lieu': 'Petaouchnok', 'saisi_pour_res': person.pk
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('qualite-home'))
        constat = Constat.objects.get(lieu='Petaouchnok')
        self.assertEqual(constat.get_createur(), 'Céline Dupond pour John Smith')


    def test_list_constats(self):
        constats = Constat.objects.bulk_create([
            Constat(
                createur=self.user, date=now(), lieu='Ici', site='fhmn', constat='Test'
            ),
            Constat(
                createur=self.user, date=now(), lieu='Ici aussi', site='fhne', constat='Test FHNE'
            ),
            Constat(
                createur=self.user, date=now(), lieu='Ici encore', site='fhmn', constat='Test terminé',
                gravite='gr', frequence='pr', decision='OK', date_traite=now(), traite_par=self.manage_user
            ),
            Constat(
                createur=self.user, date=now(), lieu='Ici encore', site='fhmn', constat='Test terminé 2',
                gravite='mo', frequence='ti', decision='OK', date_traite=now(), traite_par=self.manage_user
            ),
        ])
        constats[-1].categories.set(list(CategConstat.objects.all()))

        self.client.force_login(self.manage_user)
        response = self.client.get(reverse('qualite-constats-pending'))
        self.assertEqual(len(response.context['constats_suspens']), 1)
        self.assertEqual(len(response.context['constats_traitement']), 0)
        response = self.client.get(reverse('qualite-constats'))
        self.assertEqual(len(response.context['object_list']), 2)
        self.assertContains(response, "Chutes, Cuisine")
        # Filtering
        self.client.force_login(self.admin_user)
        response = self.client.get(reverse('qualite-constats') + '?resultat=V')
        self.assertEqual(len(response.context['object_list']), 1)
        response = self.client.get(reverse('qualite-constats') + '?site=fhmn')
        self.assertEqual(len(response.context['object_list']), 2)
        response = self.client.get(reverse('qualite-constats') + '?site=fhne')
        self.assertEqual(len(response.context['object_list']), 0)

    def test_edit_constat(self):
        constat = Constat.objects.create(
            createur=self.user, date=now(), lieu='Ici', site='fhmn', constat='What?'
        )
        resp_secteur = User.objects.create(
            username='jdupond', first_name='Julie', last_name='Dupond', email="julie@example.org",
            resp_secteur=True
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('qualite-constat', args=[constat.pk]))
        self.assertContains(response, "What?")
        edit_url = reverse('qualite-editconstat', args=[constat.pk])
        response = self.client.get(edit_url)
        # Cannot edit if not authorized
        self.assertEqual(response.status_code, 403)

        self.client.force_login(self.admin_user)
        response = self.client.post(edit_url, data={
            'gravite': 'gr',
            'frequence': 'pr',
            'infos_compl': '',
            'decision': 'Attendre',
            'responsables': [resp_secteur.pk],
        })
        self.assertRedirects(response, reverse('qualite-constats-pending'))
        constat.refresh_from_db()
        self.assertIsNone(constat.date_traite)
        self.assertEqual(constat.traite_par, self.admin_user)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), ['julie@example.org'])

        # Only Quality manager can validate finally.
        self.client.force_login(self.manage_user)
        response = self.client.get(edit_url)
        self.assertContains(response, 'What?')
        response = self.client.post(edit_url, data={
            'gravite': 'gr',
            'frequence': 'pr',
            'infos_compl': '',
            'decision': 'Attendre',
            'responsables': [resp_secteur.pk],
            'validate': 'on',
        })
        self.assertRedirects(response, reverse('qualite-constats-pending'))
        self.assertEqual(len(mail.outbox), 1)
        constat.refresh_from_db()
        self.assertEqual(constat.date_traite.date(), date.today())
        # manage_user only validated, no change of traite_par
        self.assertEqual(constat.traite_par, self.admin_user)

    def test_edit_constat_suivi(self):
        constat_traite = Constat.objects.create(
            createur=self.user, date=now(), lieu='Ici encore', site='fhmn', constat='Test terminé',
            gravite='gr', frequence='pr',
            decision='OK', date_traite=now(), traite_par=self.manage_user
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('qualite-constat', args=[constat_traite.pk]))
        self.assertNotContains(response, 'id="id_suivi_en_cours"')
        response = self.client.post(
            reverse('qualite-change_suivi', args=[constat_traite.pk]),
            data={'suivi_en_cours': 'on'}
        )
        self.assertEqual(response.status_code, 403)

        self.client.force_login(self.manage_user)
        response = self.client.get(reverse('qualite-constat', args=[constat_traite.pk]))
        self.assertContains(response, 'id="id_suivi_en_cours"')
        response = self.client.post(
            reverse('qualite-change_suivi', args=[constat_traite.pk]),
            data={'suivi_en_cours': 'on'}
        )
        self.assertEqual(response.json(), {'result': 'OK'})
        constat_traite.refresh_from_db()
        self.assertTrue(constat_traite.suivi_en_cours)

    def test_export_constats(self):
        Constat.objects.create(
            createur=self.user, date=now(), lieu='Ici', site='fhmn', constat='Test'
        )
        c2 = Constat.objects.create(
            createur=self.user, date=now(), lieu='Ici encore', site='fhmn', constat='Test terminé',
            gravite='gr', frequence='pr',
            decision='OK', date_traite=now(), traite_par=self.manage_user
        )
        c2.categories.set([CategConstat.objects.get(nom='Chutes')])
        self.client.force_login(self.manage_user)
        response = self.client.get(reverse('qualite-constats-export'))
        self.assertEqual(response['Content-Type'], XLSX_CONTENT_TYPE)


class ConvertTests(TempMediaTestCase):
    """
    This test class is only activated on specific cicumstances,
    typically testing a cloudconvert api change.
    """

    def x_test_cloudconvert_api(self):
        from qualite.utils import convert_doc_to_pdf

        categ = Categorie.objects.create(numero=11)
        doc = Document(type_doc='FO', categorie=categ, numero=134)
        docx_file = Path(__file__).parent / 'sample.docx'
        with docx_file.open(mode='rb') as fh:
            doc.fichier.save('sample.docx', File(fh))

        convert_doc_to_pdf(None, doc, sandbox=True)
        self.assertIn('.pdf', str(doc.pdf))
        self.assertIn('Sample', doc.text)
