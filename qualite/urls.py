from django.urls import path

from . import views

urlpatterns = [
    path('', views.QualiteHomeView.as_view(), name='qualite-home'),
    path('search/', views.DocumentSearchView.as_view(), name='qualite-search'),
    path('documents/<int:no>/', views.DocumentListView.as_view(), name='qualite-documents'),
    path('documents/<int:no>/add/', views.DocumentAddEditView.as_view(), name='qualite-adddocument'),
    path('documents/<int:pk>/edit/', views.DocumentAddEditView.as_view(), name='qualite-editdocument'),
    path('documents/<int:pk>/online-edit/', views.DocumentOnlineEditView.as_view(), name='qualite-onlinedocument'),
    path('documents/<int:pk>/addversion/', views.DocumentAddVersionView.as_view(), name='qualite-addversion'),
    path('documents/<int:pk>/delete/', views.DocumentDeleteView.as_view(), name='qualite-deldocument'),
    path('documents/maq/',  views.DocumentListView.as_view(typ='maq'), name='qualite-documents-maq'),
    path('documents/tous/',  views.DocumentListView.as_view(typ='tous'), name='qualite-documents-tous'),
    # Constats
    path('constat/nouveau/', views.ConstatAddView.as_view(), name='qualite-addconstat'),
    path('constat/liste/suspens/', views.ConstatPendingListView.as_view(), name='qualite-constats-pending'),
    path('constat/liste/miens/', views.ConstatListView.as_view(selection='mine'), name='qualite-mes-constats'),
    path('constat/liste/traites/', views.ConstatListView.as_view(), name='qualite-constats'),
    path('constat/liste/export/', views.ConstatExportView.as_view(), name='qualite-constats-export'),
    path('constat/<int:pk>/', views.ConstatDetailView.as_view(), name='qualite-constat'),
    path('constat/<int:pk>/edition/', views.ConstatAddView.as_view(), name='qualite-editmonconstat'),
    path('constat/<int:pk>/traitement/', views.ConstatEditView.as_view(), name='qualite-editconstat'),
    path('constat/<int:pk>/suivi/', views.ConstatChangeSuiviView.as_view(), name='qualite-change_suivi'),
]
