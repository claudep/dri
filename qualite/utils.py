import tempfile
from pathlib import Path

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.mail import mail_admins


class UploadedFile:
    """
    A proxy file object with some API to make FileSystemStorage believe
    it's a TemporaryUploadedFile, so we never have to read the file in memory.
    """
    def __init__(self):
        # delete=False because the temp file will be moved as new model field location.
        self.file = tempfile.NamedTemporaryFile(suffix='.upload', dir=settings.FILE_UPLOAD_TEMP_DIR, delete=False)

    def temporary_file_path(self):
        """This is the API needed for FileSystemStorage to move the file"""
        return self.file.name

    def chunks(self, *args, **kwargs):
        pass


def _convert_doc_v2(request, doc, sandbox=False):
    import cloudconvert

    in_file = Path(doc.fichier.path)
    #file_ext = in_file.suffix.strip('.').lower()
    cloudconvert.configure(api_key=settings.CLOUDCONVERT_APIKEY, sandbox=sandbox)
    job = cloudconvert.Job.create(payload={
        'tasks': {
            'upload-my-file': {
                'operation': 'import/upload',
            },
            'convert-my-file': {
                'operation': 'convert',
                'input': 'upload-my-file',
                'output_format': 'pdf',
            },
            'export-my-file': {
                'operation': 'export/url',
                'input': 'convert-my-file',
            },
        }
    })
    if job.get('code') == 'UNAUTHENTICATED':
        raise Exception("Not authenticated with cloudconvert")
    upload_task = cloudconvert.Task.find(id=job['tasks'][0]['id'])
    cloudconvert.Task.upload(file_name=str(in_file), task=upload_task)
    res = cloudconvert.Job.wait(id=job['id'])
    export_task = [task for task in res["tasks"] if task.get("name") == 'export-my-file'][0]
    file_ = export_task.get("result").get("files")[0]

    local_file = UploadedFile()
    res = cloudconvert.download(filename=local_file.file.name, url=file_['url'])
    doc.pdf.save(in_file.stem + '.pdf', local_file)
    doc.extract_text()
    return True


def convert_doc_to_pdf(request, doc, sandbox=False):
    # See https://github.com/cloudconvert/cloudconvert-python
    try:
        return _convert_doc_v2(request, doc, sandbox=sandbox)
    except Exception as err:
        if sandbox:
            raise
        #except cloudconvert.exceptions.APIError: ?
        # Don't let any cloudconvert error block the view.
        mail_admins(
            '[FH] Cloudconvert error',
            'An error occurred while sending document {doc} to CloudConvert: {err}'.format(
                doc=doc, err=err
            )
        )
        return False
