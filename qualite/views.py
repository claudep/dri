import os

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.mail import send_mail
from django.db import transaction
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.views.generic import (
    CreateView, DeleteView, DetailView, ListView, TemplateView, UpdateView, View,
)

from common.views import BaseXLSView, CreateUpdateView
from wopi.views import WopiAccessMixin

from .forms import (
    ConstatAddForm, ConstatEditForm, ConstatFilterForm, DocumentForm,
    DocumentSearchForm, DocumentVersionForm,
)
from .models import Categorie, Constat, Document
from .utils import convert_doc_to_pdf


class ConstatsSuspensMixin:
    def get_constats_suspens_count(self):
        can_edit_constats = self.request.user.has_perm('qualite.change_constat')
        return Constat.objects.filter(
            site__in=self.request.user.get_sites(), date_traite__isnull=True
        ).count() if can_edit_constats else None

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'num_suspens': self.get_constats_suspens_count(),
        }


class QualiteHomeView(ConstatsSuspensMixin, TemplateView):
    template_name = 'qualite/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'search_form': DocumentSearchForm(),
        })
        return context


class DocumentSearchView(TemplateView):
    template_name = 'qualite/search.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sites = self.request.user.get_sites()
        results = None
        if self.request.GET:
            form = DocumentSearchForm(data=self.request.GET)
            if form.is_valid():
                results = form.search().filter(site__in=['commun'] + sites)
        if results is None:
            form = DocumentSearchForm()
        context.update({
            'sites': [ch for ch in Document.SiteChoices if ch.value in ['commun'] + sites],
            'form': form,
            'results': results,
        })
        return context


class DocumentListView(TemplateView):
    template_name = 'qualite/documents.html'
    typ = 'categ'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        sites = user.get_sites()
        doc_qs = Document.objects.filter(archive_for__isnull=True, site__in=['commun'] + sites
            ).select_related('categorie').order_by('type_doc', 'categorie__numero', 'numero')
        categ = None
        if self.typ == 'categ':
            categ = get_object_or_404(Categorie, numero=self.kwargs['no'])
            doc_qs = doc_qs.filter(categorie=categ)
            title = "Documents de la catégorie «{categorie}»".format(
                categorie=str(categ),
            )
        elif self.typ == 'tous':
            title = "Tous les documents qualité"
        elif self.typ == 'maq':
            doc_qs = doc_qs.filter(type_doc='MAQ')
            title = "Documents du manuel qualité"

        if user.has_perm('qualite.change_document'):
            doc_qs = doc_qs.prefetch_related('constat_set', 'archives')

        context.update({
            'no_categ': categ.numero if categ else 0,
            'title': title,
            'sites': [ch for ch in Document.SiteChoices if ch.value in ['commun'] + sites],
            'docs': doc_qs,
        })
        return context


class DocumentEditBaseMixin(PermissionRequiredMixin):
    model = Document
    template_name = 'qualite/document-form.html'
    permission_required = 'qualite.add_document'

    def form_valid(self, form):
        response = super().form_valid(form)
        if 'fichier' in form.changed_data:
            file_ext = os.path.splitext(form.instance.fichier.path)[1].strip('.').lower()
            if file_ext == 'pdf':
                self.object.extract_text()
            elif file_ext in ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx'] and settings.CLOUDCONVERT_APIKEY:
                convert_doc_to_pdf(self.request, form.instance)
        return response

    def get_success_url(self):
        return reverse('qualite-documents', args=[self.object.categorie.numero])


class DocumentAddEditView(DocumentEditBaseMixin, CreateUpdateView):
    form_class = DocumentForm

    @property
    def is_create(self):
        return 'pk' not in self.kwargs

    @property
    def title(self):
        return "Nouveau document" if self.is_create else "Édition d’un document"

    def get_initial(self):
        if self.is_create and self.kwargs['no'] != 0:
            return {
                **super().get_initial(),
                'categorie': get_object_or_404(Categorie, numero=self.kwargs['no'])
            }
        return super().get_initial()


class DocumentAddVersionView(DocumentEditBaseMixin, CreateView):
    form_class = DocumentVersionForm
    title = "Ajout d’une nouvelle version"

    def dispatch(self, *args, **kwargs):
        self.original = get_object_or_404(Document, pk=self.kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def get_initial(self):
        return {'titre': self.original.titre}

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'fixed_values': (
            ('Catégorie :', self.original.categorie),
            ('Type de document :', self.original.get_type_doc_display()),
            ('Numéro :', self.original.numero),
            ('Site :', self.original.get_site_display()),
            ('Dernière version :', self.original.date_doc),
        )}

    def form_valid(self, form):
        form.instance.categorie = self.original.categorie
        form.instance.type_doc = self.original.type_doc
        form.instance.numero = self.original.numero
        form.instance.site = self.original.site
        with transaction.atomic():
            response = super().form_valid(form)
            self.original.archive_for = form.instance
            Document.objects.filter(archive_for=self.original).update(archive_for=form.instance)
            self.original.save()
        return response


class DocumentOnlineEditView(PermissionRequiredMixin, WopiAccessMixin, View):
    template_name = 'online-editor.html'
    permission_required = 'qualite.add_document'

    def get(self, *args, **kwargs):
        doc = get_object_or_404(Document, pk=self.kwargs['pk'])
        url = self.get_wopi_access(doc, 'fichier')
        return HttpResponseRedirect(url)


class DocumentDeleteView(PermissionRequiredMixin, DeleteView):
    model = Document
    permission_required = 'qualite.delete_document'

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.success_url = reverse('qualite-documents', args=[self.object.categorie.numero])
        response = super().delete(request, *args, **kwargs)
        self.object.fichier.delete(False)
        if self.object.pdf:
            self.object.pdf.delete(False)
        messages.success(request, "Le document %s a bien été supprimé." % self.object)
        return response


class ConstatAddView(CreateUpdateView):
    model = Constat
    form_class = ConstatAddForm
    template_name = 'qualite/constat-add.html'
    success_url = reverse_lazy('qualite-home')

    @property
    def is_create(self):
        return not bool(self.kwargs)

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'user': self.request.user}

    def form_valid(self, form):
        form.instance.date = timezone.now()
        form.instance.createur = self.request.user
        response = super().form_valid(form)
        messages.success(self.request, "Merci, le constat a bien été enregistré.")
        return response


class ConstatPendingListView(ConstatsSuspensMixin, TemplateView):
    model = Constat
    template_name = 'qualite/constat-list-pending.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        sites = user.get_sites()
        filter_form = ConstatFilterForm(show_site=len(sites) > 1, data=self.request.GET)
        if filter_form.is_bound and filter_form.is_valid() and filter_form.cleaned_data.get('site'):
            sites = [filter_form.cleaned_data['site']]
        can_edit_constats = user.has_perm('qualite.change_constat')
        context.update({
            'filter_form': filter_form,
            'constats_suspens': Constat.objects.filter(
                site__in=sites, date_traite__isnull=True, traite_par__isnull=True).order_by('date'),
            'can_edit_constats': can_edit_constats,
        })
        if can_edit_constats:
            context['constats_traitement'] = Constat.objects.filter(
                site__in=sites, date_traite__isnull=True, traite_par__isnull=False).order_by('date')
        else:
            context['constats_suspens'] = context['constats_suspens'].filter(createur=user)
        return context


class ConstatListMixin:
    def get(self, request, *args, **kwargs):
        self.sites = request.user.get_sites()
        self.fform = ConstatFilterForm(show_site=len(self.sites) > 1, data=request.GET)
        if self.fform.is_bound:
            self.fform.is_valid()
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        if self.selection == 'mine':
            constats = Constat.objects.filter(createur=self.request.user, date_traite__isnull=False)
        else:
            constats = Constat.objects.filter(site__in=self.sites, date_traite__isnull=False)
        if self.fform.is_bound and self.fform.is_valid():
            constats = self.fform.apply(constats)
        return constats.select_related(
            'createur', 'resident', 'traite_par'
        ).prefetch_related('categories')


class ConstatListView(ConstatsSuspensMixin, ConstatListMixin, ListView):
    model = Constat
    template_name = 'qualite/constat-list.html'
    selection = 'all'
    paginate_by = 15

    def get_queryset(self):
        return super().get_queryset().order_by('-date')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.selection == 'mine':
            context['suspens'] = Constat.objects.filter(
                createur=self.request.user, date_traite__isnull=True
            ).order_by('-date')
            title = "Mes constats traités"
        else:
            title = "Constats traités ({})".format(" et ".join((s.upper() for s in self.sites)))

        context.update({
            'title': title,
            'filter_form': self.fform,
        })
        return context


class ConstatDetailView(ConstatsSuspensMixin, DetailView):
    model = Constat
    template_name = 'qualite/constat-detail.html'

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'qualite_manager': self.request.user.has_perm('qualite.add_document'),
        }


class ConstatEditView(PermissionRequiredMixin, UpdateView):
    model = Constat
    permission_required = 'qualite.change_constat'
    form_class = ConstatEditForm
    template_name = 'qualite/constat-edit.html'
    success_url = reverse_lazy('qualite-constats-pending')

    def dispatch(self, request, *args, **kwargs):
        self.qualite_manager = request.user.has_perm('qualite.add_document')
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'is_manager': self.qualite_manager}

    def form_valid(self, form):
        responsables_before = list(Constat.objects.get(pk=self.kwargs['pk']).responsables.all())
        if not form.changed_data == ['validate']:
            # Do not change traite_par if the only action is to validate.
            form.instance.traite_par = self.request.user
        response = super().form_valid(form)
        if 'responsables' in form.changed_data:
            # Send mail to new responsables
            resp_email = [
                resp.email for resp in form.cleaned_data['responsables'] if resp not in responsables_before and resp.email
            ]
            send_mail(
                '[DRI] Constat dans votre secteur',
                'Madame, Monsieur,\n'
                'Un nouveau constat concernant votre secteur est disponible dans le DRI.\n'
                'https://dri.foyerhandicap.ch{}\n\n'
                'Ceci est un message automatique, ne pas répondre.'.format(
                    reverse('qualite-constat', args=[form.instance.pk])
                ),
                'www-data@2xlibre.net',
                resp_email,
            )

        messages.success(self.request, 'Le constat a bien été enregistré.')
        return response


class ConstatChangeSuiviView(PermissionRequiredMixin, View):
    permission_required = 'qualite.change_constat'

    def post(self, *args, **kwargs):
        constat = get_object_or_404(Constat, pk=self.kwargs['pk'])
        constat.suivi_en_cours = not constat.suivi_en_cours
        constat.save()
        return JsonResponse({'result': 'OK'})


class ConstatExportView(ConstatListMixin, BaseXLSView):
    sheet_name = "Constats"
    col_widths = [18, 14, 14, 20, 9, 35, 35, 35, 20, 15, 18, 12, 20, 35, 35, 14, 18]
    selection = 'all'

    def get_headers(self):
        return [
            'Auteur', 'Date d’événement', 'Date de saisie', 'Lieu', 'Site',
            'Constat', 'Mesures', 'Suggestions', 'Catégories',
            'Gravité', 'Fréquence', 'Résultat', 'Résident', 'Complément', 'Décision',
            'Date de traitement', 'Traité par',
        ]

    def get_filename(self):
        return 'FH-constats.xlsx'

    def write_line(self, constat):
        self.ws.append([
            str(constat.createur),
            self.date_cell(constat.date_even, strip_time=True),
            self.date_cell(constat.date, strip_time=True),
            constat.lieu,
            constat.get_site_display(),
            constat.constat,
            constat.mesures_imm,
            constat.suggestions,
            ', '.join(c.nom for c in constat.categories.all()),
            constat.get_gravite_display(),
            constat.get_frequence_display(),
            constat.resultat,
            str(constat.resident) if constat.resident_id else None,
            constat.infos_compl,
            constat.decision,
            self.date_cell(constat.date_traite, strip_time=True),
            str(constat.traite_par),
        ])
