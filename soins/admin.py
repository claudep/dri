from django.contrib import admin

from .models import (
    ActesJournaliers, ActionSoins, BilanSoins, DiagnosticMedical,
    Medication, MedicationItem, ObservationSoins, SignesVitaux, SoinsDivers,
    SondageVesical, StatusUrinaire, VisiteMedicale,
)


@admin.register(DiagnosticMedical)
class DiagnosticMedicalAdmin(admin.ModelAdmin):
    search_fields = ['person__nom']
    list_display = ['person', 'date', 'origine', 'allergies']


@admin.register(Medication)
class MedicationAdmin(admin.ModelAdmin):
    search_fields = ['person__nom']
    list_display = ['date_prescription', 'person']


@admin.register(MedicationItem)
class MedicationItemAdmin(admin.ModelAdmin):
    search_fields = ['produit']
    list_display = ['produit', 'date_debut', 'date_fin', 'reserve']
    raw_id_fields = ['medication']


@admin.register(ObservationSoins)
class ObservationSoinsAdmin(admin.ModelAdmin):
    search_fields = ['person__nom', 'observation']
    list_display = ['person', 'date', 'type_soin']
    ordering = ['-date']


@admin.register(SignesVitaux)
class SignesVitauxAdmin(admin.ModelAdmin):
    list_display = [
        'person', 'date', 'poids', 'hgt', 'pouls', 't_a', 'temp', 'remarques'
    ]


@admin.register(SoinsDivers)
class SoinsDiversAdmin(admin.ModelAdmin):
    search_fields = ['person__nom']


@admin.register(VisiteMedicale)
class VisiteMedicaleAdmin(admin.ModelAdmin):
    search_fields = ['person__nom', 'questions', 'reponse']
    list_display = ['person', 'date_visite', 'type_visite', 'questions', 'reponse']


admin.site.register(ActesJournaliers)
admin.site.register(ActionSoins)
admin.site.register(BilanSoins)
admin.site.register(SondageVesical)
admin.site.register(StatusUrinaire)
