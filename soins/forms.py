from datetime import date

from django import forms
from django.core.exceptions import ValidationError

from common.fields import ListTextWidget, PickDateWidget
from person.forms import AttachmentFormMixin
from .models import (
    ActeJ, Medication, MedicationItem, ObservationSoins, SignesVitaux,
    SoinsDivers, VisiteMedicale
)


class ActeJForm(forms.ModelForm):
    class Meta:
        model = ActeJ
        exclude = ['actes', 'date']
        widgets = {'order': forms.HiddenInput}


class MedicationItemBaseForm(forms.ModelForm):
    class Meta:
        model = MedicationItem
        fields = []  # Added by subclasses
        widgets = {
            'date_debut': PickDateWidget, 'date_fin': PickDateWidget,
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        self.reserve = kwargs.pop('reserve')
        super().__init__(*args, **kwargs)

    def save(self, **kwargs):
        obj = super().save(**kwargs)
        obj.medication.date_prescription = date.today()
        obj.medication.impr_etiquette = False
        obj.medication.save()
        return obj


class MedicationItemFullForm(MedicationItemBaseForm):
    class Meta(MedicationItemBaseForm.Meta):
        fields = [
            'produit', 'type_med', 'sur_ordre', 'date_debut', 'date_fin',
            'matin', 'midi', 'soir', 'nuit', 'remarques'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.reserve:
            for fname in ('matin', 'midi', 'soir', 'nuit'):
                del self.fields[fname]
        if not self.user.has_perm('soins.validate_item'):
            self.fields['sur_ordre'].required = True


class MedicationItemEditForm(MedicationItemBaseForm):
    class Meta(MedicationItemBaseForm.Meta):
        fields = ['produit', 'type_med', 'date_fin', 'fin_sur_ordre']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.user.has_perm('soins.validate_item'):
            del self.fields['fin_sur_ordre']
        else:
            del self.fields['produit']
            self.fields['fin_sur_ordre'].required = True


class ObservationSoinsForm(AttachmentFormMixin, forms.ModelForm):
    class Meta:
        model = ObservationSoins
        fields = ['observation', 'type_soin']
        widgets = {
            'type_soin': ListTextWidget(['Soins jour', 'Soins nuit', 'Plaisir']),
        }


class SignesVitauxForm(forms.ModelForm):
    class Meta:
        model = SignesVitaux
        fields = ['poids', 'hgt', 'pouls', 't_a', 'temp', 'remarques']

    def clean(self):
        cleaned_data = super().clean()
        if not any([self.cleaned_data.get(k) for k in self._meta.fields]):
            raise ValidationError("Vous devez remplir au moins une valeur")

    def clean_pouls(self):
        pouls = self.cleaned_data['pouls']
        if pouls:
            try:
                pouls_num = int(pouls)
            except ValueError:
                raise ValidationError("Le pouls doit être un nombre")
            if pouls_num < 0 or pouls_num > 250:
                raise ValidationError("Le pouls doit être compris entre 0 et 250")
        return pouls

    def clean_t_a(self):
        t_a = self.cleaned_data['t_a']
        err = "La tension artérielle doit contenir 2 nombres séparés par une barre oblique"
        if t_a:
            vals = t_a.replace(' ', '').split('/')
            if len(vals) != 2:
                raise ValidationError(err)
            try:
                vals = [int(v) for v in vals]
            except ValueError:
                raise ValidationError(err)
        return t_a


class SoinsDiversForm(forms.ModelForm):
    class Meta:
        model = SoinsDivers
        fields = ['problemes', 'traitements', 'evaluations']


class VisiteMedicaleForm(forms.ModelForm):
    class Meta:
        model = VisiteMedicale
        fields = '__all__'  # Adjusted in __init__
        widgets = {
            'person': forms.HiddenInput,
            'date_visite': PickDateWidget,
        }

    def __init__(self, mode, *args, **kwargs):
        self.mode = mode
        super().__init__(*args, **kwargs)
        fields = [
            'person', 'date_visite', 'type_visite', 'questions'
        ] if mode == 'question' else ['date_visite', 'reponse']
        for f in list(self.fields.keys()):
            if f not in fields:
                self.fields.pop(f)

    @property
    def is_general(self):
        return not self.instance.person and not self.initial.get('person')
