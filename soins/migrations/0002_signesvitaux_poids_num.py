from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('soins', '0001_squashed_0035_person_nullable_add_site'),
    ]

    operations = [
        migrations.AddField(
            model_name='signesvitaux',
            name='poids_num',
            field=models.DecimalField(blank=True, decimal_places=1, max_digits=4, null=True),
        ),
    ]
