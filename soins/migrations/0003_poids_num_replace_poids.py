from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('soins', '0002_signesvitaux_poids_num'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='signesvitaux',
            name='poids',
        ),
        migrations.RenameField(
            model_name='signesvitaux',
            old_name='poids_num',
            new_name='poids',
        ),
        migrations.AlterField(
            model_name='signesvitaux',
            name='poids',
            field=models.DecimalField(blank=True, decimal_places=1, max_digits=4, null=True, verbose_name='Poids'),
        ),
    ]
