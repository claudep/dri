from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('soins', '0003_poids_num_replace_poids'),
    ]

    operations = [
        migrations.AddField(
            model_name='diagnosticmedical',
            name='date',
            field=models.DateTimeField(null=True, verbose_name='Date de mise à jour'),
        ),
        migrations.AddField(
            model_name='diagnosticmedical',
            name='historique',
            field=models.TextField(blank=True, verbose_name='Historique'),
        ),
        migrations.AddField(
            model_name='diagnosticmedical',
            name='pathologie',
            field=models.TextField(blank=True, verbose_name='Pathologie'),
        ),
    ]
