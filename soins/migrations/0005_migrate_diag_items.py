from datetime import datetime

from django.db import migrations
from django.utils.timezone import make_aware


def migrate_diag_items(apps, schema_editor):
    DiagnosticItem = apps.get_model('soins', 'DiagnosticItem')
    DiagnosticMedical = apps.get_model('soins', 'DiagnosticMedical')
    for diag in DiagnosticMedical.objects.all():
        try:
            latest_item = DiagnosticItem.objects.filter(person=diag.person).latest('date_diagnostic')
        except DiagnosticItem.DoesNotExist:
            print(f"No DiagnosticItem for {diag.person.nom} {diag.person.prenom}")
            continue
        diag.historique = latest_item.historique
        diag.pathologie = latest_item.pathologie
        dt = latest_item.date_diagnostic
        if dt:
            diag.date = make_aware(datetime(dt.year, dt.month, dt.day, 12, 0))
        diag.save()


class Migration(migrations.Migration):

    dependencies = [
        ('soins', '0004_diagitem_to_diagmedical'),
    ]

    operations = [
        migrations.RunPython(migrate_diag_items, migrations.RunPython.noop)
    ]
