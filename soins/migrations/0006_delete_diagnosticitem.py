from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('soins', '0005_migrate_diag_items'),
    ]

    operations = [
        migrations.DeleteModel(
            name='DiagnosticItem',
        ),
    ]
