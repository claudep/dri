from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('soins', '0006_delete_diagnosticitem'),
    ]

    operations = [
        migrations.AddField(
            model_name='medicationitem',
            name='remarques',
            field=models.CharField(blank=True, max_length=200, verbose_name='Remarques'),
        ),
    ]
