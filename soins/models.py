from datetime import date, timedelta

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.db.models import Q
from django.urls import reverse

from person.models import (
    ActionBase, Attachment, ObjectHistoryMixin, Person, PersonRelatedModel, User
)


class ActesJournaliers(PersonRelatedModel):
    person = models.OneToOneField(Person, on_delete=models.CASCADE)
    objectifs = models.TextField("Objectifs des soins", blank=True)

    _view_name_base = 'actesjourn'

    def __str__(self):
        return "Actes journaliers du %s pour %s" % (self.date, self.person)

    def can_edit(self, user):
        return user.has_perm('soins.change_actesjournaliers')

    def get_absolute_url(self):
        return reverse('actesjourn-detail', args=[self.person_id])

    def current_actes(self):
        return self.actej_set.exclude(archived_on__isnull=False).order_by('order')


class ActeJ(models.Model):
    actes = models.ForeignKey(ActesJournaliers, on_delete=models.CASCADE)
    order = models.SmallIntegerField(default=0)
    date = models.DateTimeField()
    # nullable en raison de l'existant
    auteur = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Créateur'
    )
    titre = models.CharField("Actes journaliers", max_length=200, blank=True)
    resident = models.BooleanField("R", default=False)
    personnel = models.BooleanField("P", default=False)
    description = models.TextField("Descriptions")
    archived_on = models.DateTimeField("Archivé le", null=True, blank=True, db_index=True)
    archived_for = models.ForeignKey(
        'self', null=True, blank=True, on_delete=models.CASCADE, related_name='archived'
    )

    def __str__(self):
        return "Item d'actes journaliers pour %s" % self.actes.person

    def can_edit(self, user):
        return user.has_perm('soins.change_actesjournaliers')

    def object_history_url(self):
        return reverse('actesjourn-history', args=[self.actes.person_id, self.pk])


class ObservationSoins(PersonRelatedModel):
    # Plusieurs variantes et autres valeurs, à discuter
    #TYPE_SOIN_CHOICES = (
    #    ('jour', "Soin jour"),
    #    ('nuit', "Soin nuit"),
    #    ('plaisir', "Plaisir"),
    #    ('anim', "Animation"),
    #)
    observation = models.TextField("Observation")
    type_soin = models.CharField("Type de soin", max_length=30)
    # Après migration, passer type_soin en jour/nuit et ajouter champ suivant pour le reste
    #autre = models.CharField("Autre", max_length=30, blank=True)
    fichiers = GenericRelation(Attachment, verbose_name='Fichiers')

    class Meta:
        verbose_name = "Observation de soins"
        verbose_name_plural = "Observations de soins"

    _view_name_base = 'obssoins'
    list_display = ['date:h', 'observation:w120:files', 'type_soin', 'auteur']
    searchable = {'txt': ['observation']}

    def __str__(self):
        return "Observation du %s pour %s" % (self.date, self.person)

    def can_edit(self, user):
        return user.is_superuser or (user == self.createur and self.date.date() == date.today())

    @property
    def is_nuit(self):
        return 'nuit' in self.type_soin

    def classes(self):
        return ['nuit'] if self.is_nuit else []


class ActionSoins(ActionBase):
    # legacy field, no longer edited:
    visa_fermee = models.CharField("Visa fermeture", max_length=25, blank=True)

    class Meta:
        verbose_name = "Action de soins"
        verbose_name_plural = "Actions de soins"

    _view_name_base = 'actionsoins'
    edit_perm = 'soins.change_actionsoins'

    def __str__(self):
        return "Action de soin pour %s (%s - %s)" % (self.person, self.date, self.get_etat_display())


class ActionEval(models.Model):
    action = models.ForeignKey(ActionSoins, on_delete=models.CASCADE)
    date = models.DateTimeField("Date")
    createur = models.ForeignKey(
        User, on_delete=models.PROTECT, verbose_name='Créateur', related_name='+'
    )
    texte = models.TextField("Évaluation")

    class Meta:
        verbose_name = "Évaluation d'action"
        verbose_name_plural = "Évaluations d'action"

    def __str__(self):
        return "Évaluation d'action pour %s (%s)" % (self.action.person, self.date)


class ActionAction(models.Model):
    action = models.ForeignKey(ActionSoins, on_delete=models.CASCADE)
    date = models.DateTimeField("Date")
    createur = models.ForeignKey(
        User, on_delete=models.PROTECT, verbose_name='Créateur', related_name='+'
    )
    texte = models.TextField("Action")

    class Meta:
        verbose_name = "Action d'action"
        verbose_name_plural = "Action d'action"

    def __str__(self):
        return "Action d'action pour %s (%s)" % (self.action.person, self.date)


class BilanSoins(PersonRelatedModel):
    date_bilan = models.DateField("Date du bilan")
    participants = models.CharField("Participants", max_length=70, blank=True)
    deroulement = models.TextField("Déroulement", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)
    rappel = models.TextField("Rappel des objectifs", blank=True)
    resultats = models.TextField("Résultats", blank=True)
    commentaires = models.TextField("Commentaires et propositions", blank=True)

    _view_name_base = 'bilansoins'
    edit_perm = 'soins.change_bilansoins'
    list_display = ['date_bilan', 'auteur']

    def __str__(self):
        return "Bilan de soin pour %s (%s)" % (self.person, self.date_bilan)


class DiagnosticMedical(ObjectHistoryMixin, models.Model):
    person = models.OneToOneField(Person, on_delete=models.CASCADE)
    date = models.DateTimeField("Date de mise à jour", null=True)
    allergies = models.TextField("Allergies", blank=True)
    origine = models.TextField("Origine", blank=True)
    historique = models.TextField("Historique", blank=True)
    pathologie = models.TextField("Pathologie", blank=True)

    _view_name_base = 'diagmedic-list'

    def __str__(self):
        return "Diagnostic médical pour %s" % (self.person)

    def get_absolute_url(self):
        return reverse('diagmedic-list', args=[self.person_id])

    def can_edit(self, user):
        return user.has_perm('soins.change_diagnosticmedical')


class Medication(PersonRelatedModel):
    date_prescription = models.DateField("Date prescription", null=True, blank=True)
    impr_etiquette = models.BooleanField(default=False)

    _view_name_base = 'medication'

    class Meta:
        unique_together = (('person', 'date_prescription'))

    def __str__(self):
        return "Médication pour %s (%s)" % (self.person, self.date_prescription)

    def get_absolute_url(self):
        return reverse('medication-list', args=[self.person_id])

    def produits(self):
        items = list(self.medicationitem_set.filter(reserve=False
            ).filter(Q(date_fin__isnull=True) | Q(date_fin__gte=date.today())
            ).select_related('createur_debut', 'createur_fin', 'valide_par'
            ).order_by('type_med', 'produit'))
        # Future items last
        current = [item for item in items if not item.is_future]
        future = [item for item in items if item.is_future]
        return current + sorted(future, key=lambda f: f.date_debut)

    def reserve(self):
        return self.medicationitem_set.filter(reserve=True
            ).filter(Q(date_fin__isnull=True) | Q(date_fin__gte=date.today())
            ).select_related('createur_debut', 'createur_fin', 'valide_par'
            ).order_by('type_med', 'produit')


class MedicationItem(models.Model):
    TYPE_MED_CHOICES = (
        ('comp', 'Comprimés'),
        ('gouttes', 'Gouttes'),
        ('suppo', 'Supp/IR'),
        ('x-autre', 'Autres'),
    )
    medication = models.ForeignKey(Medication, on_delete=models.CASCADE)
    produit = models.CharField("Nom de produit", max_length=150)
    type_med = models.CharField("Type de médicament", max_length=10, choices=TYPE_MED_CHOICES, default='comp')
    sur_ordre = models.CharField("Sur ordre de", max_length=25, blank=True)
    valide_par = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Validé par', related_name='+'
    )
    date_debut = models.DateField("Date début", null=True)
    visa_debut = models.CharField("Visa", max_length=10, blank=True)
    createur_debut = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Auteur début', related_name='+'
    )
    date_fin = models.DateField("Date fin", null=True, blank=True)
    visa_fin = models.CharField("Visa", max_length=10, blank=True)
    createur_fin = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Auteur fin', related_name='+'
    )
    fin_sur_ordre = models.CharField("Sur ordre de", max_length=25, blank=True)
    fin_valide_par = models.ForeignKey(
        User, on_delete=models.PROTECT, null=True, blank=True, verbose_name='Validé par', related_name='+'
    )
    reserve = models.BooleanField("Traitement de réserve", default=False)
    matin = models.CharField("Matin", max_length=10, blank=True)
    midi = models.CharField("Midi", max_length=10, blank=True)
    soir = models.CharField("Soir", max_length=10, blank=True)
    nuit = models.CharField("Nuit", max_length=10, blank=True)
    remarques = models.CharField("Remarques", max_length=200, blank=True)

    class Meta:
        verbose_name = "Ligne de médication"
        verbose_name_plural = "Lignes de médication"
        permissions = (
            ('validate_item', 'Authorized to validate medication item'),
        )

    def __str__(self):
        return "Ligne de médication pour %s (%s)" % (self.medication.person, self.produit)

    @classmethod
    def a_valider_list(cls, location):
        return cls.objects.exclude(date_fin__lt=date.today() - timedelta(days=60)).filter(
            Q(sur_ordre__length__gt=0, valide_par__isnull=True) |
            Q(fin_sur_ordre__length__gt=0, fin_valide_par__isnull=True)
        ).filter(medication__person__situation__startswith=location)

    @property
    def auteur_debut(self):
        return self.createur_debut if self.createur_debut_id else (self.visa_debut if self.visa_debut else '-')

    @property
    def auteur_fin(self):
        return self.createur_fin if self.createur_fin_id else (self.visa_fin if self.visa_fin else '')

    @property
    def is_future(self):
        return self.date_debut and self.date_debut > date.today()

    @property
    def is_passed(self):
        return self.date_fin and self.date_fin < date.today()

    @property
    def a_valider(self):
        return (
            (self.sur_ordre and not self.valide_par_id) or
            (self.fin_sur_ordre and not self.fin_valide_par_id)
        )

    @property
    def a_valider_debut(self):
        return self.sur_ordre and not self.valide_par_id


class SignesVitaux(PersonRelatedModel):
    poids = models.DecimalField("Poids", max_digits=4, decimal_places=1, blank=True, null=True)
    hgt = models.CharField("HGT", max_length=30, blank=True)
    # Should be integer, too much import errors for now
    pouls = models.CharField("Pouls", max_length=30, blank=True)
    t_a = models.CharField("T.A.", max_length=30, blank=True)
    # Should be decimal, too much import errors for now
    temp = models.CharField("Température", max_length=20, blank=True)
    remarques = models.TextField("Remarques", blank=True)

    class Meta:
        verbose_name = "Signes vitaux"
        verbose_name_plural = "Signes vitaux"

    _view_name_base = 'signesvitaux'
    edit_perm = 'soins.change_signesvitaux'
    list_display = ['date', 'poids', 'hgt', 'pouls', 't_a', 'temp', 'remarques', 'auteur']

    def __str__(self):
        return "Signes vitaux pour %s (%s)" % (self.person, self.date)

    # observation and type_soin properties are used when SignesVitaux objects
    # are mixed in ObservationSoins object list.
    @property
    def observation(self):
        return " - ".join([
            "{}: {}".format(label, getattr(self, fname))
            for fname, label in (
                ('poids', 'Poids'), ('hgt', 'HGT'), ('pouls', 'Pouls'),
                ('t_a', 'T.A.'), ('temp', 'Temp.'), ('remarques', 'Rem.'),
            ) if getattr(self, fname)
        ])

    @property
    def type_soin(self):
        return "Signes vitaux"


class SoinsDivers(PersonRelatedModel):
    ferme = models.BooleanField("Fermé", default=False)
    order = models.SmallIntegerField(default=0)
    problemes = models.TextField("Problèmes")
    traitements = models.TextField("Traitements", blank=True)
    evaluations = models.TextField("Évaluations", blank=True)

    class Meta:
        verbose_name = "Soins divers"
        verbose_name_plural = "Soins divers"

    _view_name_base = 'soinsdivers'
    edit_perm = 'soins.change_soinsdivers'
    list_display = ['date', 'problemes', 'traitements', 'evaluations']
    searchable = {'txt': ['problemes', 'traitement', 'evaluations']}

    def __str__(self):
        return "Soins divers pour %s (%s)" % (self.person, self.date)

    @property
    def closed(self):
        return self.ferme


class SondageVesical(PersonRelatedModel):
    # 'ferme' only kept for history
    ferme = models.BooleanField("Fermé", default=False)
    date_sondage = models.DateField("Date du sondage")
    type_sonde = models.CharField("Type de sonde", max_length=100)
    no_sonde = models.CharField("N° de sonde", max_length=6, blank=True)
    gonfle = models.CharField("Gonflé à", max_length=40, blank=True)
    prochain = models.DateField("Prochain sondage", null=True, blank=True)

    _view_name_base = 'sondageves'
    edit_perm = 'soins.change_sondagevesical'
    list_display = ['date_sondage', 'type_sonde', 'no_sonde', 'gonfle', 'prochain', 'auteur']

    def __str__(self):
        return "Sondage vésical pour %s (%s)" % (self.person, self.date)


class StatusUrinaire(PersonRelatedModel):
    date_feuille = models.DateField("DateFeuille", null=True, blank=True)
    date_saisie = models.DateField("Date de saisie", null=True, blank=True)
    densite = models.CharField("Densité", max_length=10, blank=True)
    ph = models.CharField("PH", max_length=10, blank=True)
    leucocytes = models.CharField("Leucocytes", max_length=10, blank=True)
    nitrites = models.CharField("Nitrites", max_length=10, blank=True)
    proteines = models.CharField("Protéines", max_length=10, blank=True)
    glucose = models.CharField("Glucose", max_length=10, blank=True)
    cetones = models.CharField("Cétones", max_length=10, blank=True)
    urobilinogene = models.CharField("Urobilinogène", max_length=10, blank=True)
    bilirubine = models.CharField("Bilirubine", max_length=10, blank=True)
    erythrocytes = models.CharField("Erythrocytes", max_length=10, blank=True)
    hemoglobine = models.CharField("Hémoglobine", max_length=10, blank=True)

    _view_name_base = 'statusurin'
    edit_perm = 'soins.change_statusurinaire'
    list_display = [
        'date', 'densite', 'ph', 'leucocytes', 'nitrites' , 'proteines',
        'glucose', 'cetones', 'urobilinogene', 'bilirubine', 'erythrocytes',
        'hemoglobine',
    ]

    def __str__(self):
        return "Status urinaire pour %s (%s)" % (self.person, self.date_saisie)


class VisiteMedicale(PersonRelatedModel):
    TYPEV_CHOICES = (
        ('med', 'Médecin'),
        ('psy', 'Psychiatre'),
        ('ergo', 'Ergothérapie'),
        ('physio', 'Physiothérapie'),
    )
    # Overwrite PersonRelatedModel.person to be nullable
    person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=True, null=True)
    site = models.CharField("Site", max_length=4, choices=(('fhmn', 'FHMN'), ('fhne', 'FHNE')), blank=True)
    date_visite = models.DateField("Date de visite", null=True)
    type_visite = models.CharField("Type de visite", max_length=10, choices=TYPEV_CHOICES, default='med')
    # Obsolete field, only 10 values from 2014.
    problemes = models.TextField("Problèmes", blank=True)
    questions = models.TextField("Questions")
    reponse = models.TextField("Réponses", blank=True)
    repondant = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, related_name='+', verbose_name='Par')
    reponse_date = models.DateTimeField("Date réponse", null=True, blank=True)

    _view_name_base = 'visitemed'
    list_display = ['date_visite', 'auteur', 'questions', 'type_visite', 'reponse', 'reponse_date', 'repondant']

    def __str__(self):
        return "Visite médicale pour %s (%s)" % (self.person, self.date_visite)

    def get_absolute_url(self):
        if self.person:
            return super().get_absolute_url()
        else:
            return reverse('visitegen-detail', kwargs={'location': self.site, 'obj_pk': self.pk})

    @classmethod
    def open_visites(self, location):
        return VisiteMedicale.objects.filter(
            Q(site=location) | Q(
                person__actif=True,
                person__situation__startswith=location,
            )
        ).filter(Q(reponse='') | Q(date_visite__gte=date.today())
        ).annotate(
            no_reponse=models.Case(
                models.When(reponse='', then=True), default=False, output_field=models.BooleanField()
            )
        ).order_by('-no_reponse', 'date_visite')

    def can_edit(self, user):
        return (
            user.is_superuser or
            (self.reponse == '' and user == self.createur)
        )

    def can_answer(self, user):
        return (
            user.has_perm('soins.change_visitemedicale') or
            self.type_visite == 'ergo' and user.has_perm('change_actionergo') or
            self.type_visite == 'physio' and user.has_perm('change_bilanphysio')
        )
