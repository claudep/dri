from datetime import date, timedelta

from django.core.exceptions import ObjectDoesNotExist
from django.utils.dateformat import format as dateformat

from reportlab.platypus import Paragraph, SimpleDocTemplate, Spacer, Table, TableStyle
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet

from person.pdf_output import PageNumCanvas, PersonPDFModel


class DiagnosticPDF(PersonPDFModel):
    title = 'Diagnostic médical'

    def __init__(self, **params):
        self.diagnostic = params.pop('diagnostic')
        super().__init__(**params)

    @staticmethod
    def prepare_text(value):
        return value.replace('\n', '<br/>').replace('\t', '&nbsp;' * 4)

    def produce_pdf(self, response):
        self.doc = SimpleDocTemplate(response, title=self.title, pagesize=A4,
            leftMargin=2*cm, rightMargin=1.5*cm, topMargin=1*cm, bottomMargin=1.7*cm)
        story = []
        self.person_header(story)
        diagnostic = self.person.diagnosticmedical
        if diagnostic.allergies:
            story.append(Paragraph('<b>Allergies :</b> {}'.format(diagnostic.allergies), style=self.style_n))
        story.append(Paragraph(
            '<b>Date diagnostic :</b> {}'.format(
                dateformat(diagnostic.date, 'l, d F Y') if diagnostic.date else '?'
            ),
            style=self.style_n
        ))
        if diagnostic.origine:
            story.append(Paragraph(
                '<b>Provenance :</b> {}'.format(diagnostic.origine), style=self.style_n
            ))
        story.extend([
            Paragraph(
                '<b>Diagnostic :</b><br/>{}'.format(self.prepare_text(diagnostic.pathologie)), style=self.style_n
            ),
            Paragraph(
                '<b>Compléments d’information - anamnèse</b><br/>{}'.format(self.prepare_text(diagnostic.historique)), style=self.style_n
            ),
        ])
        self.doc.build(
            story, onFirstPage=self.draw_first_header_footer,
            onLaterPages=self.draw_later_header_footer,
            canvasmaker=PageNumCanvas
        )


class MedicationLabel(PersonPDFModel):
    num_lines = {
        'big': 18,
        'small': 10,
    }
    col_widths = {
        'big': [6.8*cm, 1.4*cm, 1.4*cm, 1.4*cm, 1.4*cm, 4.5*cm],  # 17cm
        'small': [5*cm, 1.2*cm, 1.2*cm, 1.2*cm, 1.2*cm, 3.2*cm],  # 13cm
    }
    # Should match css colors for tr.gouttes/tr.x-autre
    gouttes_color = colors.blue
    suppo_color = colors.toColor('rgb(126,97,23)')
    autre_color = colors.toColor('rgb(255,99,0)')

    def __init__(self, **params):
        self.medic = params['object']
        self.title = str(self.medic)
        self.styles = getSampleStyleSheet()
        self.style_n = self.styles['Normal']
        self.styles_smaller = {
            'comp': ParagraphStyle('smaller', parent=self.style_n, fontSize=10, leading=9, spaceBefore=0, spaceAfter=0),
            'gouttes': ParagraphStyle('smaller', parent=self.style_n, fontSize=10, leading=9, spaceBefore=0, spaceAfter=0, textColor=self.gouttes_color),
            'suppo': ParagraphStyle('smaller', parent=self.style_n, fontSize=10, leading=9, spaceBefore=0, spaceAfter=0, textColor=self.suppo_color),
            'x-autre': ParagraphStyle('smaller', parent=self.style_n, fontSize=10, leading=9, spaceBefore=0, spaceAfter=0, textColor=self.autre_color),
        }
        super().__init__(person=self.medic.person)

    def produce_pdf(self, response):
        def produit_txt(line):
            txt = line.produit
            if line.date_fin:
                txt += ' <i><font size="9">(STOP le {})</font></i>'.format(dateformat(line.date_fin, 'd.m'))
            if line.date_debut and line.date_debut > date.today():
                txt = '<i><font size="9">DÈS {}:</font> {}</i>'.format(dateformat(line.date_debut, 'd.m'), txt)
            return txt

        self.doc = SimpleDocTemplate(response, title=self.title, pagesize=A4,
            leftMargin=2*cm, rightMargin=1.5*cm, topMargin=1*cm, bottomMargin=1.5*cm)
        story = []
        self.person_header(story)

        diagnostic = self.person.diagnosticmedical
        if diagnostic.allergies:
            story.append(Paragraph('<font color="red"><b>{}</b></font>'.format(diagnostic.allergies), style=self.style_n))

        medics = self.medic.produits()
        big_label = len(medics) > self.num_lines['small']
        medic_lines = [
            [Paragraph(produit_txt(line), self.styles_smaller[line.type_med]),
             line.matin, line.midi, line.soir, line.nuit,
             Paragraph(f'<font size="8">{line.remarques}</font>', self.styles_smaller[line.type_med])]
            for line in medics
        ]
        num_lines = max(self.num_lines['big'], len(medic_lines)) if big_label else self.num_lines['small']
        table_data = [
            ['Fiche semainier', 'Date prescription : %s' % dateformat(self.medic.date_prescription, 'd.m.Y') if self.medic.date_prescription else '-'],
            [Paragraph('<b>%s · N° chambre %s</b>' % (self.medic.person, self.medic.person.no_chambre), self.style_n), ''],
            ['Médicaments', 'Matin', 'Midi', 'Soir', 'Nuit', 'Remarques'],
        ]
        table_data.extend(medic_lines)
        table_data.extend([['', '', '', '', '']] * (num_lines - len(medic_lines)))
        table_style = [
            ('BOX', (0, 0), (-1, -1), 1.5, colors.black),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
            ('BACKGROUND', (0, 0), (-1, 0), colors.toColor('rgb(0,153,153)')),
            ('TEXTCOLOR', (0, 0), (-1, 0), colors.white),
            ('FONTSIZE', (0, 0), (-1, 0), 13 if big_label else 11),
            ('ALIGNMENT', (1, 0), (-1, 0), 'RIGHT'),
            ('LINEBEFORE', (1, 0), (-1, 0), 0, colors.toColor('rgba(0,153,153,1)')),
            ('SPAN', (1, 0), (-1, 0)),
            ('SPAN', (0, 1), (-1, 1)),
            ('BACKGROUND', (0, 2), (-1, 2), colors.toColor('rgb(102,255,204)')),
            ('FONTSIZE', (0, 2), (-1, 2), 8),
            ('ALIGNMENT', (1, 2), (-1, -1), 'CENTER'),
            ('FONTSIZE', (0, 3), (-1, -1), 9),
        ]
        for idx, medic in enumerate(medics):
            if medic.type_med == 'gouttes':
                table_style.append(('TEXTCOLOR', (0, 3 + idx), (-1, 3 + idx), self.gouttes_color))
            elif medic.type_med == 'suppo':
                table_style.append(('TEXTCOLOR', (0, 3 + idx), (-1, 3 + idx), self.suppo_color))
            elif medic.type_med == 'x-autre':
                table_style.append(('TEXTCOLOR', (0, 3 + idx), (-1, 3 + idx), self.autre_color))

        row_heights = {
            'big': [0.8*cm, 0.6*cm, 0.4*cm] + [8.7*cm / num_lines] * num_lines,
            'small': [0.8*cm, 0.6*cm, 0.5*cm] + [0.62*cm] * num_lines,
        }['big' if big_label else 'small']
        story.append(Table(
            table_data,
            colWidths=self.col_widths['big' if big_label else 'small'],
            rowHeights=row_heights,
            style=TableStyle(table_style)
        ))

        # Traitement de réserve
        table_data = [
            [Paragraph(' - '.join(txt for txt in [med.produit, med.remarques] if txt), self.styles_smaller['comp'])]
            for med in self.medic.reserve()
        ]
        if table_data:
            story.append(Spacer(1, 1.5*cm))
            story.append(Paragraph('<b>Traitement de réserve</b>', self.style_n))
            story.append(Table(table_data, colWidths=[14.4*cm], style=TableStyle([
                ('BOX', (0, 0), (-1, -1), 1.5, colors.black),
                ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ])))

        self.doc.build(story, onFirstPage=self.draw_first_header_footer)
