from datetime import date, timedelta

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from django.utils.dateformat import format as date_format

from person.models import Modification, Person, User
from person.tests import ModelBaseTextMixin, XLSX_CONTENT_TYPE
from .forms import SignesVitauxForm
from .models import (
    ActesJournaliers, ActeJ, ActionSoins, BilanSoins, DiagnosticMedical,
    Medication, MedicationItem, ObservationSoins, SignesVitaux, SoinsDivers,
    SondageVesical, StatusUrinaire, VisiteMedicale
)


class ActesJournaliersTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(username='jill', first_name='Jill', last_name='Martin', is_superuser=True)
        cls.pers = Person.objects.create(
            nom='Smith', prenom='John', situation='fhmn_int',
            entree_date=timezone.now()-timedelta(days=180)
        )

    def test_add_subitem(self):
        self.client.force_login(self.user)
        # Getting the detail view will autocreate the ActesJournaliers instance
        response = self.client.get(reverse('actesjourn-detail', args=[self.pers.pk]))
        self.assertEqual(response.status_code, 200)
        self.pers.actesjournaliers
        data = {
            'order': 0,
            'titre': 'Titre',
            'resident': 'on',
            'personnel': '',
            'description': 'Une\nDescription',
        }
        self.client.post(reverse('actesjourn-additem', args=[self.pers.pk]), data=data)
        self.assertEqual(self.pers.actesjournaliers.actej_set.count(), 1)
        actej = self.pers.actesjournaliers.actej_set.first()
        self.assertIsNotNone(actej.date)
        self.assertTrue(actej.resident)
        self.assertEqual(actej.auteur, self.user)
        # The next item should have an initial order greater than 0
        response = self.client.get(reverse('actesjourn-additem', args=[self.pers.pk]))
        self.assertGreater(response.context['form'].initial['order'], actej.order)

    def test_edit_subitem(self):
        ActesJournaliers.objects.create(person=self.pers)
        existing = ActeJ.objects.create(
            actes=self.pers.actesjournaliers, order=0, date=timezone.now() - timedelta(days=2),
            auteur=self.user, titre='Titre', description='Description'
        )
        self.client.force_login(self.user)
        # Test edit
        data = {'order': 0, 'titre': 'Nouveau', 'description': 'Description'}
        self.client.post(reverse('actesjourn-edititem', args=[self.pers.pk, existing.pk]), data=data)
        existing.refresh_from_db()
        self.assertEqual(existing.titre, "Nouveau")
        self.assertIsNone(existing.archived_on)
        archived = existing.archived.first()
        self.assertEqual(archived.titre, "Titre")
        self.assertIsNotNone(archived.archived_on)

    def test_delete_subitem(self):
        actes = ActesJournaliers.objects.create(person=self.pers)
        actej = ActeJ.objects.create(actes=actes, titre='Titre', date=timezone.now())
        self.client.force_login(self.user)
        response = self.client.post(reverse('actesjourn-delitem', args=[self.pers.pk, actej.pk]), data={})
        self.assertEqual(response.json(), {'results': 'OK'})
        self.assertEqual(actes.actej_set.count(), 1)
        self.assertEqual(actes.current_actes().count(), 0)

    def test_sort_subitems(self):
        actes = ActesJournaliers.objects.create(person=self.pers)
        acte1 = ActeJ.objects.create(actes=actes, order=0, titre='Titre1', date=timezone.now())
        acte2 = ActeJ.objects.create(actes=actes, order=1, titre='Titre2', date=timezone.now())
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('actesjourn-sortitems', args=[self.pers.pk]),
            data={'item_ids': '%s,%s' % (acte2.pk, acte1.pk)}
        )
        self.assertEqual(response.json(), {'results': 'OK'})
        response = self.client.get(
            reverse('actesjourn-detail', args=[self.pers.pk]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual([i.titre for i in response.context['subitems']], ['Titre2', 'Titre1'])


class ActionSoinsTests(ModelBaseTextMixin, TestCase):
    ModelClass = ActionSoins
    model_id = 'actionsoins'
    custom_data = {'problemes': "Un problème…"}

    def test_add_action_evaluation(self):
        act = ActionSoins.objects.create(
            person=self.person, problemes="Des problèmes…", objectifs="Des objectifs…"
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('actionsoins-detail', args=[self.person.pk, act.pk]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        addeval_url = reverse('actionsoins-addeval', args=[self.person.pk, act.pk])
        addaction_url = reverse('actionsoins-addaction', args=[self.person.pk, act.pk])
        self.assertContains(
            response,
            '<img class="icon-standalone" src="/static/img/add.svg" '
            'data-url="%s" onclick="addDynamicRow(this, \'evaluations\')" '
            'title="Ajouter une évaluation">' % addeval_url,
            html=True
        )
        self.assertContains(
            response,
            '<img class="icon-standalone" src="/static/img/add.svg" '
            'data-url="%s" onclick="addDynamicRow(this, \'actions\')" '
            'title="Ajouter une action">' % addaction_url,
            html=True
        )
        response = self.client.post(addeval_url, data={'texte': 'Une évaluation'})
        self.assertEqual(act.evaluations.first().texte, 'Une évaluation')
        response = self.client.post(addaction_url, data={'texte': 'Une action'})
        self.assertEqual(act.actions.first().texte, 'Une action')

    def test_edit_action(self):
        act = ActionSoins.objects.create(
            person=self.person, problemes="Des problèmes…", objectifs="Des objectifs…"
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('actionsoins-edit', args=[act.person.pk, act.pk]),
            data={'person': act.person.pk, 'problemes': 'Modifié', 'objectifs': 'Ici aussi'},
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertRedirects(
            response, reverse('actionsoins-detail', args=[act.person.pk, act.pk]),
            fetch_redirect_response=False
        )

    def test_close_action(self):
        act = ActionSoins.objects.create(
            person=self.person, createur=self.user,
            problemes="Des problèmes…", objectifs="Des objectifs…",
        )
        user = User.objects.create(username='tin', first_name='Tin', last_name='Tin')
        user.user_permissions.add(Permission.objects.get(codename='change_actionis'))
        user.user_permissions.add(Permission.objects.get(codename='change_actionsoins'))
        self.client.force_login(user)
        response = self.client.get(
            reverse('actionsoins-detail', args=[act.person.pk, act.pk]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertContains(response, "Fermer l’action")
        response = self.client.post(
            reverse('actionsoins-closeaction', args=[act.person.pk, act.pk]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertRedirects(response, act.get_absolute_url(), fetch_redirect_response=False)
        act.refresh_from_db()
        self.assertTrue(act.closed)


class BilanSoinsTests(ModelBaseTextMixin, TestCase):
    ModelClass = BilanSoins
    model_id = 'bilansoins'
    custom_data = {'date_bilan': '2019-03-04', 'objectifs': "Des objectifs…"}


class DiagnosticMedicalTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.person = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        cls.diag = DiagnosticMedical.objects.create(person=cls.person, pathologie="Du texte...")

    def test_pdf_page(self):
        user = User.objects.create(username='jill', first_name='Jill', last_name='Martin')
        self.client.force_login(user)
        response = self.client.get(reverse('diagmedic-print', args=[self.person.pk]))
        self.assertEqual(response.status_code, 200)


class ObservationSoinsTests(ModelBaseTextMixin, TestCase):
    ModelClass = ObservationSoins
    model_id = 'obssoins'
    custom_data = {'observation': "Regarde ça", 'type_soin': 'Soins jour'}

    def test_list_filter(self):
        date_now = timezone.now()
        obs1 = ObservationSoins.objects.create(person=self.person, date=date_now, observation='Des larmes')
        obs2 = ObservationSoins.objects.create(person=self.person, date=date_now, observation='Déjà lavé')
        SignesVitaux.objects.create(person=self.person, date=date_now, poids='63')
        self.client.force_login(self.user)
        list_url = reverse('%s-list' % self.model_id, args=[self.person.pk])
        response = self.client.get(list_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(len(response.context['object_list']), 3)
        response = self.client.get(
            list_url + '?text=LAVE', HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertEqual(response.context['object_list'][0], obs2)

    def test_list_future_modifs(self):
        """Above the observations soins, a list of future events is shown."""
        date_now = timezone.now()
        date_future = date_now + timedelta(days=2)
        modif1 = Modification.objects.create(
            person=self.person, date=date_future, user=self.user,
            precision='A future modif.', rubrique='soins.observationsoin',
        )
        obs1 = ObservationSoins.objects.create(person=self.person, date=date_now, observation='Des larmes')

        self.client.force_login(self.user)
        list_url = reverse('%s-list' % self.model_id, args=[self.person.pk])
        response = self.client.get(list_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(
            response,
            '<tr class="manual future"><td>%s</td><td>A future modif.</td></tr>' % (
                date_format(timezone.localtime(date_future), 'j F Y H:i'),),
            html=True
        )


class SignesVitauxTests(ModelBaseTextMixin, TestCase):
    ModelClass = SignesVitaux
    model_id = 'signesvitaux'
    custom_data = {'poids': "65"}

    def test_validation_pouls(self):
        for invalid in ('12/56', 'abc', '45.5', '500', '-12'):
            form = SignesVitauxForm({'pouls': invalid})
            self.assertFalse(form.is_valid())
        for valid in ('30', '220'):
            form = SignesVitauxForm({'pouls': valid})
            self.assertTrue(form.is_valid())

    def test_validation_ta(self):
        for invalid in ('1256', '12-34', '120/60 à 18h'):
            form = SignesVitauxForm({'t_a': invalid})
            self.assertFalse(form.is_valid())
        for valid in ('100/90', '150 / 100'):
            form = SignesVitauxForm({'t_a': valid})
            self.assertTrue(form.is_valid())


class SoinsDiversTests(ModelBaseTextMixin, TestCase):
    ModelClass = SoinsDivers
    model_id = 'soinsdivers'
    custom_data = {'problemes': "Un problème…"}

    def test_new_soins_divers_from_previous(self):
        previous = SoinsDivers.objects.create(
            createur=self.user, person=self.person, date=timezone.now(),
            problemes="Des problèmes…", traitements="Ceci est un traitement."
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('soinsdivers-new', args=[self.person.pk]),
            data=dict(problemes="Des problèmes…", traitements="Ceci est un traitement."),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        # Even if the new is identical to the previous, allow creation
        self.assertRedirects(response, reverse('soinsdivers-list', args=[self.person.pk]))

    def test_close_soins_divers(self):
        other_user = User.objects.create(username='capone', first_name='Al', last_name='Capone')
        date_soin = timezone.now() - timedelta(days=1)
        soin = SoinsDivers.objects.create(
            createur=self.user, person=self.person, problemes="Des problèmes…",
            date=date_soin
        )
        self.assertFalse(soin.ferme)
        self.user.user_permissions.add(Permission.objects.get(codename='access_fhmn'))
        self.client.force_login(other_user)
        # Only the creator can close the object
        get_url = reverse('soinsdivers-detail', args=[self.person.pk, soin.pk])
        response = self.client.get(get_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertNotContains(response, 'Fermer le soin</button>')

        self.client.force_login(self.user)
        response = self.client.get(get_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, 'Fermer le soin</button>')
        response = self.client.post(reverse('soinsdivers-close', args=[self.person.pk, soin.pk]))
        self.assertRedirects(response, soin.get_absolute_url())
        soin.refresh_from_db()
        self.assertTrue(soin.ferme)
        self.assertEqual(soin.date, date_soin)

    def test_sort_subitems(self):
        date_soin = timezone.now() - timedelta(days=1)
        soin1 = SoinsDivers.objects.create(
            createur=self.user, person=self.person, problemes="Des problèmes 1…",
            date=date_soin, order=1
        )
        soin2 = SoinsDivers.objects.create(
            createur=self.user, person=self.person, problemes="Des problèmes 2…",
            date=timezone.now(), order=0
        )
        self.user.user_permissions.add(Permission.objects.get(codename='access_fhmn'))
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('soinsdivers-list', args=[self.person.pk]), HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        # In Django 3.2, repr could be removed
        self.assertQuerysetEqual(response.context['object_list'], [repr(soin2), repr(soin1)])
        response = self.client.post(
            reverse('soinsdivers-sortitems', args=[self.person.pk]),
            data={'item_ids': '%s,%s' % (soin1.pk, soin2.pk)}
        )
        self.assertEqual(response.json(), {'results': 'OK'})
        response = self.client.get(
            reverse('soinsdivers-list', args=[self.person.pk]), HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertQuerysetEqual(response.context['object_list'], [repr(soin1), repr(soin2)])


class SondageVesicalTests(ModelBaseTextMixin, TestCase):
    ModelClass = SondageVesical
    model_id = 'sondageves'
    custom_data = {'date_sondage': "2019-05-28", 'type_sonde': "Type de sonde"}


class StatusUrinaireTests(ModelBaseTextMixin, TestCase):
    ModelClass = StatusUrinaire
    model_id = 'statusurin'
    custom_data = {'glucose': "normal"}


class MedicationTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.person = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        DiagnosticMedical.objects.create(person=cls.person)

    def test_model_creation(self):
        user = User.objects.create(username='jill', first_name='Jill', last_name='Martin')
        user.user_permissions.add(Permission.objects.get(codename='add_medicationitem'))
        self.client.force_login(user)
        # This will create a Medication instance if needed
        response = self.client.get(
            reverse('medication-list', args=[self.person.pk]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        medic = self.person.medication_set.first()
        medic.date_prescription = '2017-01-01'
        medic.impr_etiquette = True
        medic.save()
        url = reverse('medicationitem-newprod', args=[self.person.pk, medic.pk])
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(
            response,
            '<input type="text" name="produit" id="id_produit" required maxlength="150">',
            html=True
        )
        self.assertContains(
            response,
            '<input type="text" name="matin" id="id_matin" maxlength="10">',
            html=True
        )
        self.assertIn('sur_ordre', response.context['form'].fields)
        post_data = {
            'produit': 'typophénol',
            'type_med': 'comp',
            'sur_ordre': 'HNE',
            'date_debut': '12.3.2017', 'date_fin': '',
            'matin': '', 'midi': '1', 'soir': '', 'nuit': '',
        }
        response = self.client.post(url, data=post_data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.content, b'OK')
        medic.refresh_from_db()
        self.assertEqual(medic.date_prescription, date.today())
        self.assertFalse(medic.impr_etiquette)
        new_item = medic.medicationitem_set.first()
        self.assertFalse(new_item.reserve)
        self.assertEqual(new_item.createur_debut, user)
        self.assertEqual(new_item.midi, '1')
        self.assertTrue(new_item.a_valider)
        modif = self.person.modifications.latest('date')
        self.assertEqual(modif.precision, 'Ajout du médicament «typophénol»')
        # Validate medic item
        vuser = User.objects.create(username='jane', first_name='Jane', last_name='Martin')
        vuser.user_permissions.add(Permission.objects.get(codename='add_medicationitem'))
        vuser.user_permissions.add(Permission.objects.get(codename='validate_item'))
        self.client.force_login(vuser)
        response = self.client.post(
            reverse('medicationitem-validate', args=[self.person.pk, new_item.pk]), data={}
        )
        self.assertEqual(response.json()['results'], 'OK')
        new_item.refresh_from_db()
        self.assertFalse(new_item.a_valider)
        # Test edit is also forcing date_prescription
        medic.date_prescription = '2017-01-01'
        medic.save()
        edit_url = reverse('medicationitem-edit', args=[self.person.pk, new_item.pk])
        post_data['produit'] = 'typophénol B',
        self.client.force_login(vuser)
        response = self.client.post(edit_url, data=post_data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.content, b'OK')
        medic.refresh_from_db()
        self.assertEqual(medic.date_prescription, date.today())

        # Now a reserve item
        self.client.force_login(user)
        url = reverse('medicationitem-newres', args=[self.person.pk, medic.pk])
        post_data.update({'produit': 'Pantoprazol'})
        response = self.client.post(url, data=post_data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.content, b'OK')
        res_item = medic.medicationitem_set.filter(reserve=True).first()
        self.assertEqual(res_item.createur_debut, user)
        self.assertEqual(res_item.produit, 'Pantoprazol')
        # Edit it
        url = reverse('medicationitem-edit', args=[self.person.pk, res_item.pk])
        response = self.client.post(
            url,
            data={'date_fin': '2.12.2018', 'fin_sur_ordre': 'HNE', 'type_med': 'comp'},
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(response.content, b'OK')
        res_item.refresh_from_db()
        self.assertEqual(res_item.createur_fin, user)
        modif = self.person.modifications.latest('date')
        self.assertEqual(modif.precision, 'Arrêt du médicament «Pantoprazol» le 02.12.2018')
        # Removing end date, by validating user
        self.client.force_login(vuser)
        response = self.client.post(
            url,
            data={
                'produit': 'Pantoprazol',
                'type_med': 'comp',
                'date_debut': '12.3.2017', 'sur_ordre': 'HNE',
                'date_fin': '', 'fin_sur_ordre': '',
            },
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        res_item.refresh_from_db()
        self.assertIsNone(res_item.date_fin)
        modif = self.person.modifications.latest('date')
        self.assertEqual(modif.precision, 'Annulation de l’arrêt du médicament «Pantoprazol»')

    def test_avalider_list(self):
        user = User.objects.create(username='jill', first_name='Jill', last_name='Martin')
        medic = Medication.objects.create(person=self.person)
        dd = date.today() - timedelta(days=1)
        long_passed = date.today() - timedelta(days=70)
        # Those should not be in a_valider_list:
        MedicationItem.objects.bulk_create([
            # Those should not be in a_valider_list:
            MedicationItem(medication=medic, date_debut=dd, produit='Medic1'),
            MedicationItem(
                medication=medic, date_debut=dd, produit='Medic2', sur_ordre='HNE', date_fin=long_passed
            ),
            MedicationItem(
                medication=medic, date_debut=dd, produit='Medic3', sur_ordre='HNE', valide_par=user
            ),
            MedicationItem(
                medication=medic, date_debut=dd, produit='Medic4', fin_sur_ordre='HNE', fin_valide_par=user
            ),
            MedicationItem(medication=medic, date_debut=dd, produit='Medic5', reserve=True),
            # Those two should be
            MedicationItem(
                medication=medic, date_debut=dd, produit='MedicV1', sur_ordre='HNE'
            ),
            MedicationItem(
                medication=medic, date_debut=dd, produit='MedicV2', fin_sur_ordre='HNE'
            ),
        ])
        self.assertQuerysetEqual(
            MedicationItem.a_valider_list(location='fhmn'), ['MedicV1', 'MedicV2'],
            transform=lambda m:m.produit, ordered=False
        )

    def test_print_label(self):
        user = User.objects.create(username='jill', first_name='Jill', last_name='Martin')
        user.user_permissions.add(Permission.objects.get(codename='view_medication'))
        medic = Medication.objects.create(person=self.person)
        MedicationItem.objects.create(medication=medic, produit='Medic1')
        MedicationItem.objects.create(medication=medic, produit='Medic2')
        MedicationItem.objects.create(medication=medic, produit='Medic3', reserve=True)
        self.client.force_login(user)
        # Just test it doesn't crash
        response = self.client.get(
            reverse('medication-printlabel', args=[self.person.pk, medic.pk]) + '?discard=1'
        )
        self.assertEqual(response.status_code, 200)
        medic.refresh_from_db()
        self.assertTrue(medic.impr_etiquette)
        # 'Big' label:
        MedicationItem.objects.bulk_create([
            MedicationItem(medication=medic, produit='Medic%s' % idx)
            for idx in range(20)
        ])
        response = self.client.get(reverse('medication-printlabel', args=[self.person.pk, medic.pk]))
        self.assertEqual(response.status_code, 200)

    def test_medication_archives(self):
        user = User.objects.create(username='jill', first_name='Jill', last_name='Martin')
        user.user_permissions.add(Permission.objects.get(codename='view_medication'))
        self.client.force_login(user)
        medic = Medication.objects.create(person=self.person)
        MedicationItem.objects.create(medication=medic, produit='Dafalgan')
        MedicationItem.objects.create(medication=medic, produit='Medic2', date_fin='2040-12-31')
        MedicationItem.objects.create(medication=medic, produit='Medic3', date_fin='2017-06-07')
        MedicationItem.objects.create(medication=medic, produit='Dafalgan', date_fin='2018-06-07')
        response = self.client.get(
            reverse('medication-archives', args=[self.person.pk]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        # Only the items with a past date_fin are shown
        self.assertEqual(len(response.context['object_list']), 2)
        # With a search filter term
        response = self.client.get(
            reverse('medication-archives', args=[self.person.pk]) + '?text=dafal',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertEqual(response.context['object_list'][0].produit, 'Dafalgan')

    def test_export_by_med_type(self):
        user = User.objects.create(username='jill', first_name='Jill', last_name='Martin')
        user.user_permissions.add(Permission.objects.get(codename='view_medication'))
        self.client.force_login(user)
        response = self.client.get(reverse('medication-export-suppo', args=['fhmn']))
        self.assertEqual(response['Content-Type'], XLSX_CONTENT_TYPE)


class VisiteMedicaleTests(TestCase):
    def test_visite_form(self):
        user1 = User.objects.create(username='jill', first_name='Jill', last_name='Martin')
        user2 = User.objects.create(username='joe', first_name='Joe', last_name='Martin')
        person = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')

        self.client.force_login(user1)
        new_url = reverse('visitemed-new', args=[person.pk])
        response = self.client.get(new_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertNotContains(response, '<textarea name="reponse" cols="40" rows="10" id="id_reponse"></textarea>', html=True)
        self.assertNotContains(response, 'Visite générale')
        response = self.client.post(new_url, data={
            'person': str(person.pk),
            'date_visite': '2018-12-12',
            'type_visite': 'ergo',
            'questions': 'Voici ma question',
        })
        self.assertEqual(response.status_code, 302)
        visite = VisiteMedicale.objects.get(questions='Voici ma question')
        self.assertEqual(visite.createur, user1)
        self.assertIsNotNone(visite.date)
        quest_date = visite.date
        modif = person.modifications.latest('date')
        self.assertEqual(modif.precision, 'Ajout d’une question «ergo»')

        # Now another person is adding a response
        self.client.force_login(user2)
        edit_url = reverse('visitemed-edit', args=[person.pk, visite.pk])

        response = self.client.get(edit_url + '?mode=reponse', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 403)
        user2.user_permissions.add(Permission.objects.get(codename='change_visitemedicale'))

        response = self.client.get(edit_url + '?mode=reponse', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, '<textarea name="reponse" cols="40" rows="10" id="id_reponse"></textarea>', html=True)
        response = self.client.post(edit_url + '?mode=reponse', data={
            'date_visite': '2018-12-12',
            'reponse': 'Réponse du médecin',
        })
        visite.refresh_from_db()
        self.assertEqual(visite.createur, user1)
        self.assertEqual(visite.reponse, 'Réponse du médecin')
        self.assertEqual(visite.repondant, user2)
        self.assertIsNotNone(visite.reponse_date)
        self.assertEqual(visite.date, quest_date)
        modif = person.modifications.latest('date')
        self.assertEqual(modif.precision, 'Réponse à une question ergo')

    def test_visites_generales(self):
        user = User.objects.create(username='jill', first_name='Jill', last_name='Martin')
        user.user_permissions.add(Permission.objects.get(codename='access_fhne'))
        self.client.force_login(user)
        response = self.client.get(reverse('visitegen-new', args=['fhne', 'ergo']), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, '<option value="ergo" selected>Ergothérapie</option>', html=True)
        response = self.client.post(reverse('visitegen-new', args=['fhne', 'ergo']), data={
            'date_visite': '2020-05-30',
            'type_visite': 'ergo',
            'questions': 'Comment ça va?',
        })
        self.assertRedirects(response, reverse('qmed', args=['fhne', 'ergo']))
        visite = VisiteMedicale.objects.get(date_visite='2020-05-30')
        response = self.client.get(reverse('visitegen-detail', args=['fhne', visite.pk]), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(response, 'Comment ça va?')
        response = self.client.get(reverse('qmed', args=['fhne', 'ergo']), HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertIn(visite, list(response.context['object_list']))

    def test_open_visites(self):
        person = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        today = date.today()
        VisiteMedicale.objects.bulk_create([
            # Open visites
            VisiteMedicale(
                person=person, date_visite=today + timedelta(days=2), type_visite='med', questions='Visible1'
            ),
            VisiteMedicale(
                person=person, date_visite=today - timedelta(days=2), type_visite='med', questions='Visible2'
            ),
            VisiteMedicale(
                site='fhmn', date_visite=today + timedelta(days=2), type_visite='med', questions='Visible3'
            ),
            # Excluded visites
            VisiteMedicale(
                person=person, date_visite=today - timedelta(days=2), type_visite='med', questions='Unvisible2', reponse='blah'
            ),  # passed with a reponse (person-linked)
            VisiteMedicale(
                site='fhmn', date_visite=today - timedelta(days=2), type_visite='med', questions='Unvisible3', reponse='blah'
            ),  # passed with a reponse (site-linked)
        ])
        open_vis = VisiteMedicale.open_visites('fhmn')
        self.assertEqual(len(open_vis), 3)
        self.assertTrue(all(vis.questions.startswith('Visible') for vis in open_vis))

    def test_delete_visite(self):
        person = Person.objects.create(nom='Smith', prenom='John', situation='fhmn_int')
        visite = VisiteMedicale.objects.create(
            person=person, date_visite=date.today() + timedelta(days=2), type_visite='med', questions='Une question'
        )
        visite_gen = VisiteMedicale.objects.create(
            person=None, site='fhne', date_visite=date.today() + timedelta(days=2),
            type_visite='med', questions='Une question'
        )
        # A "normal" user cannot delete
        user = User.objects.create(username='jill', first_name='Jill', last_name='Martin')
        user.user_permissions.add(Permission.objects.get(codename='access_fhmn'))
        self.client.force_login(user)
        response = self.client.get(
            reverse('visitemed-detail', args=[person.pk, visite.pk]), HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertNotContains(response, "Supprimer")
        response = self.client.post(
            reverse('visitemed-delete', args=[person.pk, visite.pk]),
        )
        self.assertEqual(response.status_code, 403)
        # Now give permission to delete
        user.user_permissions.add(Permission.objects.get(codename='delete_visitemedicale'))
        response = self.client.get(
            reverse('visitemed-detail', args=[person.pk, visite.pk]), HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertContains(response, "Supprimer")
        response = self.client.post(
            reverse('visitemed-delete', args=[person.pk, visite.pk]),
        )
        self.assertEqual(VisiteMedicale.objects.filter(pk=visite.pk).count(), 0)
        response = self.client.post(
            reverse('visitemed-delete', args=[0, visite_gen.pk]),
        )
        self.assertRedirects(response, reverse('qmed', args=['fhne', 'med']))
        self.assertEqual(VisiteMedicale.objects.filter(pk=visite.pk).count(), 0)
