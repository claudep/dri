from django.urls import path

from person.views import ActionAddItemView, ActionCloseView
from . import views

urlpatterns = [
    path('actesjourn/', views.ActesJournaliersDetailView.as_view(), name='actesjourn-detail'),
    path('actesjourn/edit/', views.ActesJournaliersEditView.as_view(), name='actesjourn-edit'),
    path('actesjourn/additem/', views.ActeJEditView.as_view(is_create=True),
        name='actesjourn-additem'),
    path('actesjourn/edititem/<int:obj_pk>/', views.ActeJEditView.as_view(is_create=False),
        name='actesjourn-edititem'),
    path('actesjourn/delitem/<int:obj_pk>/', views.ActeJDeleteView.as_view(),
        name='actesjourn-delitem'),
    path('actesjourn/sortitems/', views.ActeJSortView.as_view(), name='actesjourn-sortitems'),
    path('actesjourn/history/<int:obj_pk>/', views.ActeJHistoryView.as_view(),
        name='actesjourn-history'),
    path('actesjourn/line/<int:obj_pk>/', views.ActeJLineOnlyView.as_view(), name='actesjourn-line'),

    path('actionsoins/', views.ActionSoinsListView.as_view(), name='actionsoins-list'),
    path('actionsoins/<int:obj_pk>/', views.ActionSoinsDetailView.as_view(), name='actionsoins-detail'),
    path('actionsoins/<int:obj_pk>/edit/', views.ActionSoinsEditView.as_view(), name='actionsoins-edit'),
    path('actionsoins/new/', views.ActionSoinsEditView.as_view(), name='actionsoins-new'),
    path('actionsoins/<int:obj_pk>/addeval/', ActionAddItemView.as_view(_model='soins.ActionEval'),
        name='actionsoins-addeval'),
    path('actionsoins/<int:obj_pk>/addaction/', ActionAddItemView.as_view(_model='soins.ActionAction'),
        name='actionsoins-addaction'),
    path('actionsoins/<int:obj_pk>/close/', ActionCloseView.as_view(_model='soins.ActionSoins'),
        name='actionsoins-closeaction'),

    path('bilan/', views.BilanSoinsListView.as_view(), name='bilansoins-list'),
    path('bilan/<int:obj_pk>/', views.BilanSoinsDetailView.as_view(), name='bilansoins-detail'),
    path('bilan/<int:obj_pk>/edit/', views.BilanSoinsEditView.as_view(), name='bilansoins-edit'),
    path('bilan/new/', views.BilanSoinsEditView.as_view(), name='bilansoins-new'),

    path('diagmedic/', views.DiagnosticMedicalView.as_view(), name='diagmedic-list'),
    path('diagmedic/edit/', views.DiagnosticMedicalEditView.as_view(), name='diagmedic-editbase'),
    path('diagmedic/print/', views.DiagnosticMedicalPDFView.as_view(), name='diagmedic-print'),

    path('medication/', views.MedicationListView.as_view(), name='medication-list'),
    path('medication/archives/', views.MedicationArchiveView.as_view(),
        name='medication-archives'),
    path('medication/<int:obj_pk>/printlabel/', views.MedicationPrintView.as_view(),
        name='medication-printlabel'),
    path('medication/<int:obj_pk>/newprod/', views.MedicationItemNewView.as_view(reserve=False),
        name='medicationitem-newprod'),
    path('medication/<int:obj_pk>/newres/', views.MedicationItemNewView.as_view(reserve=True),
        name='medicationitem-newres'),
    path('medication/<int:obj_pk>/edit/', views.MedicationItemEditView.as_view(),
        name='medicationitem-edit'),
    path('medication/<int:obj_pk>/validate/', views.MedicationItemValidateView.as_view(),
        name='medicationitem-validate'),

    path('obssoins/', views.ObservationSoinsListView.as_view(), name='obssoins-list'),
    path('obssoins/<int:obj_pk>/', views.ObservationSoinsDetailView.as_view(), name='obssoins-detail'),
    path('obssoins/<int:obj_pk>/edit/', views.ObservationSoinsEditView.as_view(), name='obssoins-edit'),
    path('obssoins/new/', views.ObservationSoinsEditView.as_view(), name='obssoins-new'),

    path('signesvitaux/', views.SignesVitauxListView.as_view(), name='signesvitaux-list'),
    path('signesvitaux/<int:obj_pk>/', views.SignesVitauxDetailView.as_view(), name='signesvitaux-detail'),
    path('signesvitaux/<int:obj_pk>/edit/', views.SignesVitauxEditView.as_view(), name='signesvitaux-edit'),
    path('signesvitaux/new/', views.SignesVitauxEditView.as_view(), name='signesvitaux-new'),

    path('soinsdivers/', views.SoinsDiversListView.as_view(), name='soinsdivers-list'),
    path('soinsdivers/<int:obj_pk>/', views.SoinsDiversDetailView.as_view(), name='soinsdivers-detail'),
    path('soinsdivers/<int:obj_pk>/edit/', views.SoinsDiversEditView.as_view(), name='soinsdivers-edit'),
    path('soinsdivers/<int:obj_pk>/close/', views.SoinsDiversCloseView.as_view(), name='soinsdivers-close'),
    path('soinsdivers/new/', views.SoinsDiversEditView.as_view(), name='soinsdivers-new'),
    path('soinsdivers/sortitems/', views.SoinsDiversSortView.as_view(), name='soinsdivers-sortitems'),

    path('sondage/', views.SondageVesicalListView.as_view(), name='sondageves-list'),
    path('sondage/<int:obj_pk>/', views.SondageVesicalDetailView.as_view(), name='sondageves-detail'),
    path('sondage/<int:obj_pk>/edit/', views.SondageVesicalEditView.as_view(), name='sondageves-edit'),
    path('sondage/new/', views.SondageVesicalEditView.as_view(), name='sondageves-new'),

    path('status/', views.StatusUrinaireListView.as_view(), name='statusurin-list'),
    path('status/<int:obj_pk>/', views.StatusUrinaireDetailView.as_view(), name='statusurin-detail'),
    path('status/<int:obj_pk>/edit/', views.StatusUrinaireEditView.as_view(), name='statusurin-edit'),
    path('status/new/', views.StatusUrinaireEditView.as_view(), name='statusurin-new'),

    path('visite/', views.VisiteMedicaleListView.as_view(), name='visitemed-list'),
    path('visite/<int:obj_pk>/', views.VisiteMedicaleDetailView.as_view(), name='visitemed-detail'),
    path('visite/<int:obj_pk>/edit/', views.VisiteMedicaleEditView.as_view(), name='visitemed-edit'),
    path('visite/<int:obj_pk>/delete/', views.VisiteMedicaleDeleteView.as_view(), name='visitemed-delete'),
    path('visite/new/', views.VisiteMedicaleEditView.as_view(), name='visitemed-new'),
]
