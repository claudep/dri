from datetime import date

from django.contrib.auth.mixins import PermissionRequiredMixin, UserPassesTestMixin
from django.db import transaction
from django.db.models import Max, Q
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.utils.dateformat import format
from django.views.generic import CreateView, DeleteView, ListView, TemplateView, View

from common.views import BasePDFView, BaseXLSView, CreateUpdateView, FormErrorsHeaderMixin
from person.forms import FilterForm
from person.models import Modification, Person
from person.paginator import MixedPaginator
from person.views import (
    BaseActionEditView, BaseActionDetailView, BaseDetailView, BaseListView,
    BaseEditView, GeneralTabMixin, NextEventsMixin, OnlyAjaxMixin
)
from .forms import (
    ActeJForm, MedicationItemEditForm, MedicationItemFullForm, ObservationSoinsForm,
    SignesVitauxForm, SoinsDiversForm, VisiteMedicaleForm
)
from .models import (
    ActesJournaliers, ActeJ, ActionSoins, BilanSoins, DiagnosticMedical,
    Medication, MedicationItem, ObservationSoins, SignesVitaux, SoinsDivers, SondageVesical,
    StatusUrinaire, VisiteMedicale,
)
from .pdf_output import DiagnosticPDF, MedicationLabel


class ActesJournaliersBaseView:
    model = ActesJournaliers

    def dispatch(self, *args, **kwargs):
        try:
            self.actes = ActesJournaliers.objects.get(person__pk=self.kwargs['pk'])
        except ActesJournaliers.DoesNotExist:
            pers = get_object_or_404(Person, pk=self.kwargs['pk'])
            self.actes = ActesJournaliers.objects.create(person=pers)
        return super().dispatch(*args, **kwargs)

    def get_object(self):
        return self.actes


class ActesJournaliersDetailView(ActesJournaliersBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'soins/actesjournaliers-detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'subitems': self.actes.current_actes(),
        })
        return context


class ActesJournaliersEditView(ActesJournaliersBaseView, BaseEditView):
    template_name = 'soins/actesjournaliers-detail.html'


class ActeJEditView(ActesJournaliersBaseView, FormErrorsHeaderMixin, CreateUpdateView):
    form_class = ActeJForm
    template_name = 'soins/actesjournaliers-edititem.html'

    def get_object(self):
        if self.is_create:
            return None
        return self.form_class._meta.model.objects.get(pk=self.kwargs['obj_pk'])

    def get_initial(self):
        if self.is_create:
            max_order = self.actes.actej_set.aggregate(Max('order')).get('order__max')
            return {
                'order': 0 if max_order is None else max_order + 1,
            }
        else:
            return super().get_initial()

    def form_valid(self, form):
        if form.instance.pk:
            # Create an archived copy of self
            archived = ActeJ.objects.get(pk=form.instance.pk)
            archived.pk = None
            archived.archived_on = timezone.now()
            archived.archived_for = form.instance
            archived.save()
        form.instance.actes = self.actes
        form.instance.date = timezone.now()
        form.instance.auteur = self.request.user
        with transaction.atomic():
            self.object = form.save()
            msg = '%s d’un acte dans les actes journaliers' % (
                'Ajout' if self.is_create else 'Modification',
            )
            Modification.save_modification(self.actes, self.request.user, msg=msg)
        return HttpResponse('OK')


class ActeJSortView(ActesJournaliersBaseView, View):
    """The view receives ordered ActeJ pks as comma-separated list."""
    def post(self, request, *args, **kwargs):
        item_ids = request.POST.get('item_ids').split(',')
        for idx, item_id in enumerate(item_ids):
            item = ActeJ.objects.get(pk=item_id)
            if item.order != idx:
                item.order = idx
                item.save()
        return JsonResponse({'results': 'OK'})


class ActeJHistoryView(TemplateView):
    template_name = 'soins/actej_history.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        actej = get_object_or_404(ActeJ, pk=self.kwargs['obj_pk'])
        context['history'] = list(actej.archived.all().order_by('archived_on')) + [actej]
        return context


class ActeJDeleteView(View):
    def post(self, request, *args, **kwargs):
        actej = get_object_or_404(ActeJ, pk=self.kwargs['obj_pk'])
        with transaction.atomic():
            actes = actej.actes
            actej.archived_on = timezone.now()
            actej.save()
            msg = "Suppression d’une ligne d’actes journaliers"
            Modification.save_modification(actes, self.request.user, msg=msg)
        return JsonResponse({'results': 'OK'})


class ActeJLineOnlyView(ActesJournaliersBaseView, TemplateView):
    template_name = 'soins/actej_line.html'

    def get_object(self):
        return self.actes.actej_set.get(pk=self.kwargs['obj_pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'item': self.get_object(),
            'classes': 'archived',
        })
        return context


class ActionSoinsBaseView:
    model = ActionSoins
    action_title = 'Actions soins'


class ActionSoinsListView(ActionSoinsBaseView, BaseListView):
    new_form_label = 'Créer nouvelle action de soins'

    def get_queryset(self):
        return super().get_queryset().order_by('-etat', '-date')


class ActionSoinsDetailView(ActionSoinsBaseView, BaseActionDetailView):
    pass


class ActionSoinsEditView(ActionSoinsBaseView, BaseActionEditView):
    template_name = 'base-action.html'


class BilanSoinsBaseView:
    model = BilanSoins


class BilanSoinsListView(BilanSoinsBaseView, BaseListView):
    new_form_label = 'Créer nouveau bilan soins'


class BilanSoinsDetailView(BilanSoinsBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class BilanSoinsEditView(BilanSoinsBaseView, BaseEditView):
    pass


class DiagnosticMedicalBaseView:
    model = DiagnosticMedical

    def get_object(self):
        return get_object_or_404(self.model, person__pk=self.kwargs['pk'])


class DiagnosticMedicalView(DiagnosticMedicalBaseView, BaseDetailView):
    def get_context_data(self, **kwargs):
        return dict(
            super().get_context_data(**kwargs),
            pdf_url=reverse('diagmedic-print', args=[self.object.person.pk])
        )


class DiagnosticMedicalEditView(DiagnosticMedicalBaseView, BaseEditView):
    is_create = False

    def get_success_url(self):
        return reverse('diagmedic-list', kwargs=self.kwargs)


class DiagnosticMedicalPDFView(DiagnosticMedicalBaseView, BasePDFView):
    pdf_class = DiagnosticPDF

    def pdf_context(self):
        return {
            'person': get_object_or_404(Person, pk=self.kwargs['pk']),
            'diagnostic': get_object_or_404(DiagnosticMedical, person__pk=self.kwargs['pk']),
        }


class MedicationListView(OnlyAjaxMixin, TemplateView):
    model = Medication
    template_name = 'soins/medication-list.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        person = get_object_or_404(Person, pk=self.kwargs['pk'])
        try:
            medic = person.medication_set.latest('date_prescription')
        except Medication.DoesNotExist:
            medic = Medication.objects.create(
                date_prescription=date.today(), person=person, createur=self.request.user
            )
        context.update({
            'allergies': person.diagnosticmedical.allergies,
            'medication': medic,
            'produits': medic.produits(),
            'reserve': medic.reserve(),
        })
        return context


class MedicationArchiveView(OnlyAjaxMixin, ListView):
    model = MedicationItem
    paginate_by = 40
    template_name = 'soins/medication-archives.html'
    tab_id = 'medication'
    search_fields = ['produit']

    def get_queryset(self):
        self.person = get_object_or_404(Person, pk=self.kwargs['pk'])
        qs = super().get_queryset().filter(
            medication__person=self.person, date_fin__lt=date.today()
        ).order_by('-date_fin')
        if 'text' in self.request.GET:
            form = FilterForm(self.search_fields, data=self.request.GET)
            if form.is_valid():
                qs = form.search(qs)
        return qs

    def get_context_data(self, *args, **kwargs):
        return dict(
            super().get_context_data(*args, **kwargs),
            person=self.person,
            filter_form=FilterForm(self.search_fields, data=self.request.GET),
            back_url=reverse('medication-list', args=[self.person.pk])
        )


class MedicationItemNewView(PermissionRequiredMixin, FormErrorsHeaderMixin, CreateView):
    permission_required = 'soins.add_medicationitem'
    form_class = MedicationItemFullForm
    template_name = 'soins/medicationitem-edit.html'
    reserve = False

    def get_form_kwargs(self):
        return dict(super().get_form_kwargs(), reserve=self.reserve, user=self.request.user)

    def form_valid(self, form):
        with transaction.atomic():
            form.instance.medication = get_object_or_404(Medication, pk=self.kwargs['obj_pk'])
            form.instance.reserve = self.reserve
            form.instance.createur_debut = self.request.user
            item = form.save()
            msg = 'Ajout du médicament «%s»' % item.produit
            if item.reserve:
                msg += ' (réserve)'
            Modification.save_modification(
                item.medication, self.request.user, created=True, msg=msg
            )
        # Nothing done with a redirect for now (refreshed client-side).
        return HttpResponse('OK')


class MedicationItemEditView(PermissionRequiredMixin, FormErrorsHeaderMixin, BaseEditView):
    permission_required = 'soins.add_medicationitem'
    model = MedicationItem
    template_name = 'soins/medicationitem-edit.html'

    def get_form_kwargs(self):
        return dict(super().get_form_kwargs(), reserve=self.object.reserve, user=self.request.user)

    def get_form_class(self):
        return MedicationItemFullForm if self.request.user.has_perm('soins.validate_item') else MedicationItemEditForm

    def form_valid(self, form):
        with transaction.atomic():
            if 'date_fin' in form.changed_data:
                if form.cleaned_data['date_fin']:
                    form.instance.createur_fin = self.request.user
                    msg = 'Arrêt du médicament «%s» le %s' % (
                        form.instance.produit, format(form.cleaned_data['date_fin'], 'd.m.Y')
                    )
                    if not self.request.user.has_perm('soins.validate_item'):
                        form.instance.fin_valide_par = None
                else:
                    form.instance.createur_fin = None
                    form.instance.fin_valide_par = None
                    form.instance.fin_sur_ordre = ''
                    msg = 'Annulation de l’arrêt du médicament «%s»' % form.instance.produit
            else:
                msg = 'Modification du médicament %s' % form.instance.produit
            self.object = form.save()
            Modification.save_modification(
                self.object.medication, self.request.user, msg=msg
            )
        return HttpResponse('OK')


class MedicationItemValidateView(PermissionRequiredMixin, View):
    permission_required = 'soins.validate_item'

    def post(self, request, *args, **kwargs):
        medic_item = get_object_or_404(MedicationItem, pk=kwargs['obj_pk'])
        if medic_item.sur_ordre and not medic_item.valide_par:
            medic_item.valide_par = request.user
        if medic_item.fin_sur_ordre and not medic_item.fin_valide_par:
            medic_item.fin_valide_par = request.user
        medic_item.save()
        return JsonResponse({'results': 'OK'})


class MedicationPrintView(PermissionRequiredMixin, BasePDFView):
    permission_required = 'soins.view_medication'
    pdf_class = MedicationLabel

    def get(self, *args, **kwargs):
        self.medic = get_object_or_404(Medication, pk=self.kwargs['obj_pk'])
        if self.request.GET.get('discard') == '1':
            self.medic.impr_etiquette = True
            self.medic.save()
        return super().get(*args, **kwargs)

    def pdf_context(self):
        return {'object': self.medic}


class MedicationExportByType(PermissionRequiredMixin, BaseXLSView):
    permission_required = 'soins.view_medication'
    typ = None
    col_widths = [32, 60, 12, 12, 12, 12]

    @property
    def sheet_name(self):
        return 'Résidents {} - {}'.format(
            self.kwargs['location'].upper(),
            dict(MedicationItem.TYPE_MED_CHOICES)[self.typ].replace('/', '_'),
        )

    def get_filename(self):
        return 'residents_{}_{}_{}.xlsx'.format(
            self.kwargs['location'].lower(), self.typ, date.strftime(date.today(), '%Y-%m-%d')
        )

    def get_queryset(self):
        return MedicationItem.objects.filter(
            medication__person__situation__startswith=self.kwargs['location'],
            reserve=False,
            type_med=self.typ
        ).filter(Q(date_fin__isnull=True) | Q(date_fin__gte=date.today())
        ).order_by('medication__person__nom')

    def get_headers(self):
        return ['Résident', 'Produit', 'Matin', 'Midi', 'Soir', 'Nuit']

    def write_line(self, line):
        self.ws.append([
            str(line.medication.person), line.produit, line.matin, line.midi, line.soir, line.nuit,
        ])


class ObservationSoinsBaseView:
    model = ObservationSoins


class ObsSoinsPaginator(MixedPaginator):
    addit_class = SignesVitaux

    def __init__(self, *args, has_search=False, **kwargs):
        if has_search:
            # We may search in the addit_class instead in the future.
            self.addit_class = None
        super().__init__(*args, **kwargs)


class ObservationSoinsListView(NextEventsMixin, ObservationSoinsBaseView, BaseListView):
    template_name = 'soins/observationssoins-list.html'
    new_form_label = 'Créer nouvelle observation de soins'
    search_fields = ['observation']
    event_category = 'Soins'

    def get_paginator(self, *args, **kwargs):
        return ObsSoinsPaginator(*args, has_search=bool(self.filter_data()), **kwargs)

    def get_context_data(self, **kwargs):
        self.person = get_object_or_404(Person, pk=self.kwargs['pk'])
        context = super().get_context_data(**kwargs)
        context['future_entries'] = Modification.objects.filter(
            person__pk=self.kwargs['pk']).filter(date__gt=timezone.now()
            ).order_by('-date')
        context['actions_soins'] = ActionSoins.objects.filter(person__pk=self.person.pk, etat='o')
        return context


class ObservationSoinsDetailView(ObservationSoinsBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class ObservationSoinsEditView(ObservationSoinsBaseView, BaseEditView):
    form_class = ObservationSoinsForm
    return_to_list = True


class SignesVitauxBaseView:
    model = SignesVitaux


class SignesVitauxListView(SignesVitauxBaseView, BaseListView):
    new_form_label = 'Créer nouvelle fiche signes vitaux'


class SignesVitauxDetailView(SignesVitauxBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'soins/signesvitaux-detail.html'


class SignesVitauxEditView(SignesVitauxBaseView, BaseEditView):
    template_name = 'soins/signesvitaux-detail.html'
    form_class = SignesVitauxForm
    return_to_list = True


class SoinsDiversBaseView:
    model = SoinsDivers


class SoinsDiversListView(SoinsDiversBaseView, BaseListView):
    new_form_label = 'Créer nouvelle fiche soins divers'
    template_name = 'soins/soinsdivers-list.html'

    def get_queryset(self):
        qs = super().get_queryset().order_by('order', '-date')
        self.is_archives = self.request.GET.get('view') == 'archives'
        if self.is_archives:
            qs = qs.filter(ferme=True)
        else:
            qs = qs.exclude(ferme=True)
        objs = list(qs)
        qs.model.prefetch_object_history(objs)
        return objs

    def get_fields_from_list_display(self, model):
        fields = super().get_fields_from_list_display(model)
        fields[0] = ('date:modif_by', 'Date/auteur')
        return fields

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_sortable'] = self.can_add()
        return context


class SoinsDiversDetailView(SoinsDiversBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'soins/soinsdivers-detail.html'


class SoinsDiversEditView(SoinsDiversBaseView, BaseEditView):
    template_name = 'soins/soinsdivers-detail.html'
    form_class = SoinsDiversForm
    return_to_list = True

    def get_initial(self):
        initial = super().get_initial()
        if self.is_create:
            previous = SoinsDivers.objects.filter(person__pk=self.kwargs['pk']).order_by('-date').first()
            if previous:
                initial.update({
                    'problemes': previous.problemes,
                    'traitements': previous.traitements,
                    'evaluations': previous.evaluations,
                })
        return initial


class SoinsDiversCloseView(View):
    def post(self, request, *args, **kwargs):
        soin = get_object_or_404(SoinsDivers, pk=kwargs['obj_pk'])
        if not soin.can_edit(request.user):
            raise PermissionDenied("Vous n’êtes pas autorisé à fermer ce soin")
        soin.ferme = True
        with transaction.atomic():
            soin.save()
            msg = "Fermeture du soin divers «%s»" % str(soin)
            Modification.save_modification(soin, request.user, msg=msg)
        return HttpResponseRedirect(soin.get_absolute_url())


class SoinsDiversSortView(SoinsDiversBaseView, View):
    """The view receives ordered ActeJ pks as comma-separated list."""
    def post(self, request, *args, **kwargs):
        item_ids = request.POST.get('item_ids').split(',')
        for idx, item_id in enumerate(item_ids):
            item = self.model.objects.get(pk=item_id)
            if item.order != idx:
                item.order = idx
                item.save()
        return JsonResponse({'results': 'OK'})


class SondageVesicalBaseView:
    model = SondageVesical


class SondageVesicalListView(SondageVesicalBaseView, BaseListView):
    new_form_label = 'Créer nouveau sondage vésical'


class SondageVesicalDetailView(SondageVesicalBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['field_names'].remove('ferme')
        return context


class SondageVesicalEditView(SondageVesicalBaseView, BaseEditView):
    exclude = BaseEditView.exclude + ['ferme']
    return_to_list = True


class StatusUrinaireBaseView:
    model = StatusUrinaire


class StatusUrinaireListView(StatusUrinaireBaseView, BaseListView):
    new_form_label = 'Créer nouveau status urinaire'


class StatusUrinaireDetailView(StatusUrinaireBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'soins/statusurinaire-detail.html'


class StatusUrinaireEditView(StatusUrinaireBaseView, BaseEditView):
    exclude = BaseEditView.exclude + ['date_feuille']
    template_name = 'soins/statusurinaire-detail.html'
    return_to_list = True


# Note: VisiteMedicaleOpenView (list of instances without responses) is defined in person views.

class VisiteMedicaleBaseView:
    model = VisiteMedicale

    @property
    def mode(self):
        mode = self.request.GET.get('mode', 'question')
        if mode == 'reponse':
            # Don't only trust querystring, check with code
            obj = getattr(self, 'object', self.get_object())
            if not obj.can_answer(self.request.user):
                mode == 'question'
        return mode


class VisiteMedicaleListView(VisiteMedicaleBaseView, BaseListView):
    new_form_label = 'Créer nouvelle visite médicale'

    def get_queryset(self):
        return super().get_queryset().order_by('-date_visite')


class VisiteMedicaleDetailView(VisiteMedicaleBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'
    template_name = 'soins/visitemedicale-detail.html'

    def get_nonajax(self, request, *args, **kwargs):
        if 'location' in kwargs:
            return GeneralTabMixin().get_nonajax(request, *args, **kwargs)
        return super().get_nonajax(request, *args, **kwargs)

    def modif_label(self):
        return 'Modifier' if self.mode == 'question' else 'Répondre'

    def back_to_list_url(self):
        if self.object.site:
            return reverse('qmed', kwargs={'location': self.object.site, 'vtyp': self.object.type_visite})
        return super().back_to_list_url()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['field_names'].remove('problemes')
        if not self.object.site:
            context['field_names'].remove('site')
        context['can_answer'] = self.object.can_answer(self.request.user)
        context['deletable'] = self.request.user.has_perm('soins.delete_visitemedicale')
        return context


class VisiteMedicaleEditView(UserPassesTestMixin, VisiteMedicaleBaseView, BaseEditView):
    form_class = VisiteMedicaleForm
    return_to_list = True

    def test_func(self):
        obj = self.get_object()
        if obj is None:
            return True
        if self.mode == 'reponse':
            return obj.can_answer(self.request.user)
        else:
            return obj.can_edit(self.request.user)

    def get_template_names(self):
        return ['soins/visitemedicale-reponse.html'] if self.mode == 'reponse' else ['soins/visitemedicale-detail.html']

    def get_initial(self):
        if self.is_create:
            return {'person': self.kwargs.get('pk'), 'type_visite': self.kwargs.get('vtyp')}
        return {}

    def get_form_kwargs(self):
        return dict(super().get_form_kwargs(), mode=self.mode)

    def form_valid(self, form):
        with transaction.atomic():
            if self.mode == 'question':
                form.instance.date = timezone.now()
                form.instance.createur = self.request.user
                if 'pk' not in self.kwargs:
                    form.instance.site = self.kwargs['location']
                msg = "Ajout d’une question «{}»".format(form.instance.type_visite)
            else:
                form.instance.repondant = self.request.user
                form.instance.reponse_date = timezone.now()
                msg = "Réponse à une question {}".format(form.instance.type_visite)
            self.object = form.save()
            Modification.save_modification(self.object, self.request.user, msg=msg)
        if 'pk' in self.kwargs:
            return HttpResponseRedirect(reverse('visitemed-detail', args=[self.object.person.pk, self.object.pk]))
        else:
            return HttpResponseRedirect(
                reverse('qmed', kwargs={'location': self.object.site, 'vtyp': self.object.type_visite})
            )


class VisiteMedicaleDeleteView(PermissionRequiredMixin, DeleteView):
    model = VisiteMedicale
    permission_required = 'soins.delete_visitemedicale'
    pk_url_kwarg = 'obj_pk'

    def get_success_url(self):
        if self.object.person:
            return reverse('visitemed-list', args=[self.object.person.pk])
        else:
            return reverse('qmed', args=[self.object.site, self.object.type_visite])
