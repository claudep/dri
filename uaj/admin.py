from django.contrib import admin

from .models import BilanUAJ, ObservationUAJ, VisiteUAJ

admin.site.register(BilanUAJ)
admin.site.register(ObservationUAJ)
admin.site.register(VisiteUAJ)
