from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uaj', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bilanuaj',
            old_name='horaire',
            new_name='horaires',
        ),
    ]
