from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uaj', '0002_rename_horaire_horaires'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bilanuaj',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='observationuaj',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='visiteuaj',
            name='date',
            field=models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Date'),
        ),
    ]
