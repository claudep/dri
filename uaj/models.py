from datetime import date

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from person.models import Attachment, Person, PersonRelatedModel


class BilanUAJ(PersonRelatedModel):
    date_bilan = models.DateField("Date du bilan")
    referent = models.CharField("Référent", max_length=30, blank=True)
    symptomes = models.TextField("Symptômes principaux", blank=True)
    horaires = models.TextField("Cadre et horaires", blank=True)
    rappel_objectifs = models.TextField("Rappel des objectifs", blank=True)
    resultats = models.TextField("Résultats", blank=True)
    commentaires = models.TextField("Commentaires et propositions", blank=True)
    objectifs = models.TextField("Objectifs", blank=True)

    _view_name_base = 'bilanuaj'

    def __str__(self):
        return "Bilan UAJ pour %s (%s)" % (self.person, self.date)


class ObservationUAJ(PersonRelatedModel):
    intervenant = models.CharField("Intervenant", max_length=50, blank=True)
    observation = models.TextField("Observation", blank=True)
    rubrique = models.CharField("Rubrique", max_length=70, blank=True)
    fichiers = GenericRelation(Attachment)

    _view_name_base = 'obsuaj'
    list_display = ['date', 'rubrique', 'observation:w70:files', 'auteur']

    def __str__(self):
        return "Observation UAJ pour %s (%s)" % (self.person, self.date)

    def can_edit(self, user):
        return user.is_superuser or (user == self.createur and self.date.date() == date.today())

    @property
    def auteur(self):
        aut = super().auteur
        return aut if aut != '-' else (self.intervenant or '-')


class VisiteUAJ(PersonRelatedModel):
    difficultes = models.TextField("Difficultés observées durant la visite", blank=True)
    horaire = models.CharField("Horaire", max_length=50, blank=True)
    indication = models.TextField("Indication UAJ", blank=True)
    anamnese_clin = models.TextField("Petite anamnèse clinique", blank=True)
    anamnese_perso = models.TextField("Petite anamnèse personnelle", blank=True)
    preavis = models.TextField("Préavis pour la commission d'admission", blank=True)
    problem_phys = models.TextField("Problématique physique", blank=True)
    problem_psy = models.TextField("Problématique psychique", blank=True)
    referent = models.CharField("Référent du jour", max_length=50, blank=True)
    ressources = models.TextField("Ressources observées durant la visite", blank=True)

    _view_name_base = 'visiteuaj'

    def __str__(self):
        return "Visite UAJ pour %s (%s)" % (self.person, self.date)
