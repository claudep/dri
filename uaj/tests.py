from django.test import TestCase
from django.urls import reverse

from person.models import Contact
from person.tests import ModelBaseTextMixin
from .models import BilanUAJ, ObservationUAJ, VisiteUAJ


class BilanUAJTests(ModelBaseTextMixin, TestCase):
    ModelClass = BilanUAJ
    model_id = 'bilanuaj'
    custom_data = {'date_bilan': '2019-03-04', 'symptomes': "Des symptômes…"}

    def test_initial_referent(self):
        self.client.force_login(self.user)
        url = reverse('%s-new' % self.model_id, args=[self.person.pk])
        self.person.repondant_ergo = Contact.objects.create(nom='Ergo', prenom='Jeanne')
        self.person.repondant_ergo_uaj = Contact.objects.create(nom='UAJ', prenom='Hervé')
        self.person.save()
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(
            response,
            '<input type="text" name="referent" value="Ergo Jeanne - UAJ Hervé" maxlength="30" id="id_referent">',
            html=True
        )


class ObservationUAJTests(ModelBaseTextMixin, TestCase):
    ModelClass = ObservationUAJ
    model_id = 'obsuaj'
    custom_data = {'rubrique': "Groupe cuisine"}


class VisiteUAJTests(ModelBaseTextMixin, TestCase):
    ModelClass = VisiteUAJ
    model_id = 'visiteuaj'
    custom_data = {'horaire': "8:00-10:00"}

