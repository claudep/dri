from django.urls import path

from . import views

urlpatterns = [
    path('bilan/', views.BilanUAJListView.as_view(), name='bilanuaj-list'),
    path('bilan/<int:obj_pk>/', views.BilanUAJDetailView.as_view(), name='bilanuaj-detail'),
    path('bilan/<int:obj_pk>/edit/', views.BilanUAJEditView.as_view(), name='bilanuaj-edit'),
    path('bilan/new/', views.BilanUAJEditView.as_view(), name='bilanuaj-new'),

    path('observation/', views.ObservationUAJListView.as_view(), name='obsuaj-list'),
    path('observation/<int:obj_pk>/', views.ObservationUAJDetailView.as_view(), name='obsuaj-detail'),
    path('observation/<int:obj_pk>/edit/', views.ObservationUAJEditView.as_view(), name='obsuaj-edit'),
    path('observation/new/', views.ObservationUAJEditView.as_view(), name='obsuaj-new'),

    path('visite/', views.VisiteUAJListView.as_view(), name='visiteuaj-list'),
    path('visite/<int:obj_pk>/', views.VisiteUAJDetailView.as_view(), name='visiteuaj-detail'),
    path('visite/<int:obj_pk>/edit/', views.VisiteUAJEditView.as_view(), name='visiteuaj-edit'),
    path('visite/new/', views.VisiteUAJEditView.as_view(), name='visiteuaj-new'),
]
