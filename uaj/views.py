from person.views import BaseDetailView, BaseListView, BaseEditView

from person.models import Person
from .models import BilanUAJ, ObservationUAJ, VisiteUAJ


class BilanUAJBaseView:
    model = BilanUAJ


class BilanUAJListView(BilanUAJBaseView, BaseListView):
    new_form_label = 'Créer nouveau bilan UAJ'


class BilanUAJDetailView(BilanUAJBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class BilanUAJEditView(BilanUAJBaseView, BaseEditView):
    def get_initial(self):
        initial = super().get_initial()
        if self.is_create:
            person = Person.objects.get(pk=initial['person'])
            initial['referent'] = ' - '.join(
                str(pers) for pers in [person.repondant_ergo, person.repondant_ergo_uaj] if pers
            )
        return initial


class ObservationUAJBaseView:
    model = ObservationUAJ


class ObservationUAJListView(ObservationUAJBaseView, BaseListView):
    new_form_label = 'Créer nouvelle observation UAJ'
    search_fields = ['rubrique', 'observation']


class ObservationUAJDetailView(ObservationUAJBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class ObservationUAJEditView(ObservationUAJBaseView, BaseEditView):
    # Replaced by createur
    exclude = BaseEditView.exclude + ['intervenant']


class VisiteUAJBaseView:
    model = VisiteUAJ


class VisiteUAJListView(VisiteUAJBaseView, BaseListView):
    new_form_label = 'Créer nouvelle visite UAJ'


class VisiteUAJDetailView(VisiteUAJBaseView, BaseDetailView):
    pk_url_kwarg = 'obj_pk'


class VisiteUAJEditView(VisiteUAJBaseView, BaseEditView):
    pass
