from datetime import timedelta

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.crypto import get_random_string


def random_string():
    return get_random_string(20)


def in_twelve_hours():
    return timezone.now() + timedelta(hours=12)


class WopiToken(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    file_id = models.CharField(max_length=100)
    valid_until = models.DateTimeField(default=in_twelve_hours)
    token = models.CharField(max_length=20, default=random_string)

    @classmethod
    def create_token(cls, user, file_field):
        return WopiToken.objects.create(user=user, file_id=cls._id_from_file_field(file_field))

    @staticmethod
    def _id_from_file_field(file_field):
        return f'{file_field.instance._meta.label}.{file_field.field.name}.{file_field.instance.pk}'

    def is_valid(self, file_field):
        return (
            timezone.now() <= self.valid_until
            and self.file_id == self._id_from_file_field(file_field)
        )
