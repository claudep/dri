from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('<slug:app_label>.<slug:model_name>.<int:pk>/<slug:field_name>/',
         views.OnlineEditorView.as_view(), name='wopi-show'),
    path('file/<slug:app_label>.<slug:model_name>.<int:pk>/<slug:field_name>/',
         views.WopiFileInfoView.as_view(), name='wopi-fileinfo'),
    path('file/<slug:app_label>.<slug:model_name>.<int:pk>/<slug:field_name>/contents',
         csrf_exempt(views.WopiFileGetPostView.as_view()), name='wopi-getfile'),
]
