import fcntl
from contextlib import contextmanager


@contextmanager
def locked_open(filename, mode='r'):
    """
    https://gist.github.com/lonetwin/7b4ccc93241958ff6967
    Locking this way will not prevent the file from access using
    any other api/method.
    """
    with open(filename, mode) as fd:
        fcntl.flock(fd, fcntl.LOCK_EX)
        yield fd
        fcntl.flock(fd, fcntl.LOCK_UN)
