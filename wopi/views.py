import mimetypes
import os
import urllib.parse
import urllib.request
import xml.etree.ElementTree as ET
from datetime import datetime

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse, HttpResponseForbidden, JsonResponse
from django.urls import reverse
from django.views.generic import TemplateView, View
from django.views.static import serve

from .models import WopiToken
from .utils import locked_open


class WopiAccessMixin:
    """A mixin used by application views to allow edition for a specific user and document."""
    def get_wopi_access(self, instance, field_name):
        token = WopiToken.create_token(user=self.request.user, file_field=getattr(instance, field_name))
        url = reverse('wopi-show', kwargs={
            'app_label': instance._meta.app_label,
            'model_name': instance._meta.model_name,
            'field_name': field_name,
            'pk': instance.pk,
        }) + f'?access_token={token.token}'
        return url


class OnlineEditorMixin:
    def get_instance_from_kwargs(self):
        model = ContentType.objects.get_by_natural_key(
            self.kwargs['app_label'], self.kwargs['model_name']
        ).model_class()
        return model.objects.get(pk=self.kwargs['pk'])

    def get_token_from_request(self):
        access_token = self.request.GET.get('access_token')
        return WopiToken.objects.get(token=access_token)

    def get_file_field_secure(self):
        """Return the file field after having checked the token validity."""
        self.token = self.get_token_from_request()
        instance = self.get_instance_from_kwargs()
        file_field = getattr(instance, self.kwargs['field_name'])
        if not self.token.is_valid(file_field):
            raise PermissionDenied("Désolé le jeton n'est pas ou plus valide.")
        return file_field


class OnlineEditorView(OnlineEditorMixin, TemplateView):
    """Read also https://sdk.collaboraonline.com/contents.html"""
    template_name = 'wopi/online-editor.html'

    def __init__(self, *args, **kwargs):
        mimetypes.read_mime_types('/etc/mime.types')

    def get(self, *args, **kwargs):
        self.file_field = self.get_file_field_secure()
        return super().get(*args, **kwargs)

    @staticmethod
    def get_editor_url(mime_type):
        with urllib.request.urlopen(f'{settings.ONLINE_EDITOR_URL}/hosting/discovery') as resp:
            content = resp.read().decode()

        root = ET.fromstring(content)
        action = root.find(f"net-zone/app[@name='{mime_type}']/action")
        if action is not None:
            url = action.attrib['urlsrc']
            return url

    @staticmethod
    def get_mime_type(file_name):
        return mimetypes.guess_type(file_name, strict=False)[0]

    def get_context_data(self, **kwargs):
        mime_type = self.get_mime_type(self.file_field.name)
        wopi_src = self.request.build_absolute_uri(reverse('wopi-fileinfo', kwargs=self.kwargs))
        wopi_url = self.get_editor_url(mime_type)
        if not wopi_url:
            return {'error': f"Impossible d’éditer les fichiers de type {mime_type}."}
        return {
            'wopi_url': f'{wopi_url}WOPISrc={urllib.parse.quote_plus(wopi_src)}',
            'access_token': self.request.GET.get('access_token'),
        }


class WopiFileInfoView(OnlineEditorMixin, View):
    def get(self, request, *args, **kwargs):
        """Implement CheckFileInfo interface"""
        file_field = self.get_file_field_secure()
        file_stat = os.stat(file_field.path)
        return JsonResponse({
            # Required
            'BaseFileName': os.path.basename(file_field.name),
            'OwnerId': 'partagé',
            'Size': file_stat.st_size,
            'UserId': self.token.user.username,
            # Optional
            'UserFriendlyName': self.token.user.get_full_name(),
            'UserCanWrite': True,
            'LastModifiedTime': datetime.fromtimestamp(file_stat.st_mtime).isoformat(),
        })


class WopiFileGetPostView(OnlineEditorMixin, View):
    def get(self, request, *args, **kwargs):
        """Implement GetFile interface"""
        file_field = self.get_file_field_secure()
        return serve(request, file_field.name, document_root=settings.MEDIA_ROOT)

    def post(self, request, *args, **kwargs):
        """Implement PutFile interface"""
        file_field = self.get_file_field_secure()
        with locked_open(file_field.path, mode='wb') as fh:
            fh.write(request.body)
        return HttpResponse()
